﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>İştemanikür</title>
	<meta name="description" content="Pratik, şık ve hijyenik manikürünüzü ofisinizde yapıyoruz!">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    <meta content='işte manikür' name='title'/>
	<meta name="keywords" content="ofiste manikür, ofiste bakım, ofiste el bakımı, ofiste güzellik, iş yeri, ofis, manikür, zaman kazanmak, zaman yönetimi, kariyer, mavala, chanel, oje, koruncuk vakfı, ofiste makyaj, mobil manikür, mobil bakım" />     
    <link rel="stylesheet" href="/resources/css/style.css">
<script src="/Resources/js/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/resources/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="/resources/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-41629856-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
try {
 document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
</script>
    <div>
    	<div class="redLine"></div>
    	<div class="landingHeader">
        <img src="resources/images/logo.jpg" width="201" />
        </div>
    	<div class="mainContainer">
       		<div class="landingContainer">
            	<div class="title1"><font color="#f11b19">»</font> Manikürü öğle tatillerine ya da haftasonuna mı sıkıştırmaya çalışıyorsunuz?</div>
                <div class="title1"><font color="#f11b19">»</font> Maniküre gitmek için trafikte vakit mi harcıyorsunuz?</div>
                <div class="title1"><font color="#f11b19">»</font> Manikür için zamanım yok mu diyorsunuz?</div>
                <h1 class="bigTitle"><font color="#ed1c24">İŞ’TE</font> MANİKÜRE HAZIR MISINIZ?</h1>
                
                <a class="readyButton" href="/anasayfa.aspx"></a>
                <div class="social">
                	<div class="facebookContainer">
                    <span class="socialTitleF">Facebook’ta Paylaş</span>
                 	<a class="facebookButton"></a>
                    </div>
                    <div class="twitterContainer">
                    <span class="socialTitleT">Tweetle</span>
                 	<a class="twitterButton"></a>
                    </div>
                </div>
                <div class="landingFooter">
<script> 
$(document).ready(function () {

$(".landingFooterWrapper").find('.landingFooterItem').fancybox({
'width': 700,
'height': 450,
'autoScale': false,
'transitionIn': 'none',
'overlayOpacity': 0.5,
'transitionOut': 'none',
'centerOnScroll': true,
'type': 'iframe'

});
});
</script>
                <div class="landingFooterWrapper">
                <div class="landingfooterLeftSSL"></div> 
                 <div class="footerCenter">
                <a class="landingFooterItem" data-fancybox-type="iframe" href="gizlilik.aspx">Gizlilik Sözleşmesi</a> | <a class="landingFooterItem" data-fancybox-type="iframe" href="kullanim.aspx">Kullanım Koşulları</a>
                </div>
                 <div class="footerright">
        <a href="http://zoniletisim.com/" target="_blank"> <img src="/resources/images/zonFooterLogo.png" width="60"></a>
        </div>
                </div>
                </div>
            </div> 
       
        
        </div>
    </div>
    </form>
</body>
</html>









