﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using ZonDBModel;
using System.Net.Mail;
using log4net;

public partial class henuzyok : System.Web.UI.Page
{
    public static ILog logger = LogManager.GetLogger("default");
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string saveForm(string ad,string soyad,string email, string sirket,string iletisimKisi, string tel)
    {
        string r = "0";
        try
        {
            if (email != "")
            {
                IletisimRepositories rep = new IletisimRepositories();
                var a = rep.saveHenuzyok(ad,soyad,email,sirket,iletisimKisi,tel);
                if (a.result == true)
                {
                    r = "Mesajınız Başarıyla Gönderildi.";
                    mailGonder(ad, soyad, email, sirket, iletisimKisi, tel);
                }
            }

            return r;
        }
        catch (Exception)
        {
            return r;
            throw;

        }
    }

    private static void mailGonder(string ad, string soyad, string email, string sirket, string iletisimKisi, string tel)
    {
        ZonDBEntities db = new ZonDBModel.ZonDBEntities();
        try
        {
            Zon_Ayarlar a = db.Zon_Ayarlar.FirstOrDefault();

           
            MailMessage mailToUser = new MailMessage();
            mailToUser.Body = "&nbsp;İştemanikür web sitesinden <strong>Bize Ulaşın Formu</strong> doldurulmuştur.<br><br><strong>Kullanıcı Bilgileri</strong> <br><br> Ad Soyad: " + ad + " " + soyad + "<br>Email: " + email + "<br>Şirket : " + sirket + "<br>İletişime Geçilecek Kişi: " + iletisimKisi + "<br>İletişime geçilecek telefon numarası :" + tel + "<br><br><br><br><br><br>";
            mailToUser.IsBodyHtml = true;
            mailToUser.From = new MailAddress(a.MailAdres);

            mailToUser.Subject = "İştemanikür Bize Ulaşın Formu";

            string receipent = a.BilgiMailAdres;
            string[] rList = receipent.Split(new char[] { ';' });
            foreach (string s in rList)
                mailToUser.To.Add(s);

            mailToUser.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");

            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(a.MailAdres, a.MailSifre);
            SmtpClient client = new SmtpClient(a.MailServer, 587);

            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            //client.Send(mailToMe);
            client.Send(mailToUser);
        }
        catch (Exception ex)
        {
            logger.Error(ex);

        }



    }
}