﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Collections.Specialized;
using log4net;
using System.Web.Services;
public partial class randevu : System.Web.UI.Page
{
    ZonDBEntities db = new ZonDBEntities();
    RandevuRepositories rep = new RandevuRepositories();
    ResultAction _result = new ResultAction();
    public List<Zon_Rezerve_Saat> rezervasyonSaat = new List<Zon_Rezerve_Saat>();
    public List<Zon_Randevu_Hizmet> hizmetTur = new List<Zon_Randevu_Hizmet>();
    public Zon_Randevu _randevu = new Zon_Randevu();
    public int rezervasyonID = 0;
    public string hourBoxClickID = "";
    Zon_Kullanici u = null;
    List<Zon_Sepet> sepet = new List<Zon_Sepet>();
    public static ILog logger = LogManager.GetLogger("default");
  // public List<Zon_Sepet> sepet = new List<Zon_Sepet>();
    int panelAcik = 0;
    public  void Page_Load(object sender, EventArgs e)
    {
        if (sepetKontrol())
            pnlHour.Visible = true;

        

        if (!Page.IsPostBack)
        {
            fillSirket();
        }

       
    }

    public bool hizmetKategori(int hizmetId)
    {
        bool ret = false;

        Zon_HizmetTur h = db.Zon_HizmetTur.Where(z => z.KategoriId == hizmetId).FirstOrDefault();
        if (h != null)
            ret = true;

        return ret;

    }

    public List<Zon_HizmetTur> hizmetKategoriList(int kategoriId,int randevuId )
    {
        var h=(from hizmet in db.Zon_HizmetTur
                   join randevuHizmet in db.Zon_Randevu_Hizmet on hizmet.Id equals randevuHizmet.HizmetId
                       where hizmet.KategoriId==kategoriId && randevuHizmet.RandevuId==randevuId select hizmet).OrderBy(z=>z.Sira).ToList();
        return h;
    }


    private void sepetHizmetKontrol(int randevuId)
    {
        List<Zon_Sepet> sepet = db.Zon_Sepet.Where(z=>z.UrunId==randevuId).ToList();
       

        foreach (var s in sepet)
        {
            Zon_Randevu_Hizmet rHizmet = db.Zon_Randevu_Hizmet.Where(z => z.RandevuId == randevuId && z.HizmetId==s.HizmetId).FirstOrDefault();

            if (rHizmet == null)
            {
                var deleteSepet = sepet.Where(z => z.Id == s.Id).FirstOrDefault();
                db.DeleteObject(deleteSepet);
                db.SaveChanges();
            }
        }

    }
    public  bool sepetKontrol()
    {
        bool ret = false;
        sepet = db.Zon_Sepet.Where(z => z.SessionId == Session.SessionID).ToList();
        if (sepet.Count > 0)
            ret = true;

        return ret;
    }
    protected void fillSirket()
    {
        ddlSirket.DataSource = rep.GetSirket();
        ddlSirket.DataTextField = "Value";
        ddlSirket.DataValueField = "Key";
        ddlSirket.DataBind();
        ddlSirket.Items.Insert(0, new ListItem("-- Şirket Seçiniz --", "-1"));
    }

    protected void ddlSirket_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int sirketId = int.Parse(ddlSirket.SelectedValue);
            ddlOfis.DataSource = rep.GetOfis(sirketId);
            ddlOfis.DataTextField = "Value";
            ddlOfis.DataValueField = "Key";
            ddlOfis.DataBind();
            ddlOfis.Items.Insert(0, new ListItem("-- Ofis Seçiniz --", "-1"));
            ddlTarih.Items.Clear();
        }
        catch (Exception ex) 
        {
            
            
        }
        
    }
    protected void ddlOfis_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int sirketId = int.Parse(ddlSirket.SelectedValue);
            int ofisId = int.Parse(ddlOfis.SelectedValue);
            ddlTarih.DataSource = rep.GetRandevuTarih(sirketId, ofisId);
            ddlTarih.DataTextField = "Value";
            ddlTarih.DataValueField = "Value";
            ddlTarih.DataBind();
            ddlTarih.Items.Insert(0, new ListItem("-- Tarih Seçiniz --", "-1"));
        }
        catch (Exception ex)
        {
            
          
        }
        
    }

    protected void ddlTarih_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           
            int sirketId = int.Parse(ddlSirket.SelectedValue);
            int ofisId = int.Parse(ddlOfis.SelectedValue);
            if (ddlTarih.SelectedValue != "-1")
            {
                DateTime tarih = DateTime.Parse(ddlTarih.SelectedValue);
                _randevu = rep.rezervasyonId(sirketId, ofisId, tarih);
                rezervasyonID = (int)_randevu.Id;
                rezervasyonSaat = rep.rezervasyonSaat(rezervasyonID);
                hizmetTur = rep.hizmetTur(rezervasyonID);


                if (sepetKontrol())
                    pnlHour.Visible = true;

                sepetHizmetKontrol(rezervasyonID);
            }
            else
                ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('Lütfen randevu tarihi seçiniz.', 'Uyarı', '');</script>");
        }
        catch (Exception ex)
        {

           
        }
       
        
    }
    public  bool sepetteVarmi(int saatId)
    {
        var sepet = db.Zon_Sepet.Where(z => z.UrunId == rezervasyonID && z.SaatId == saatId && z.SessionId ==Session.SessionID).FirstOrDefault();
        if (sepet != null)
            return true;
        else
            return false;
    }

    public int sepetVal(int saatId)
    {
        int sepet = (int)db.Zon_Sepet.Where(z => z.UrunId == rezervasyonID && z.SaatId == saatId && z.SessionId == Session.SessionID).FirstOrDefault().HizmetId;
        return sepet;
        
    }
    public bool RezerveVarmi(int saatId)
    {
        var rezerve = db.Zon_Rezerve.Where(z => z.UrunId == rezervasyonID && z.SaatId == saatId).ToList();
        if (rezerve.Count>0)
            return true;
        else
            return false;
    }
     protected void LinkButton1_Click(object sender, EventArgs e)
    {
        int sirketId = 0;
        int ofisId = 0;
        string flag = "0";
        Zon_Rezerve_Saat s = null;
        DateTime tarih = new DateTime();
        try
        {
            if (Session["user"] != null)
                u = (Zon_Kullanici)Session["user"];

            pnlHour.Visible = true;
            if (!string.IsNullOrEmpty(ddlSirket.SelectedValue))
            {
                sirketId = int.Parse(ddlSirket.SelectedValue);
                if(!string.IsNullOrEmpty(ddlOfis.SelectedValue))
                    ofisId = int.Parse(ddlOfis.SelectedValue);

                if (!string.IsNullOrEmpty(ddlTarih.SelectedValue))
                {
                    tarih = DateTime.Parse(ddlTarih.SelectedValue);
                    pnlHour.Visible = true;
                    _randevu = rep.rezervasyonId(sirketId, ofisId, tarih);
                    rezervasyonID = (int)_randevu.Id;
                    NameValueCollection coll = Request.Form;
                    String[] arr1 = coll.AllKeys;
                    List<randevuList> rList = new List<randevuList>();
                    randevuList rListItem = new randevuList();
                    int saatCount = db.Zon_Randevu_Saat.Where(z => z.RandevuId == rezervasyonID).ToList().Count +1;
                    for (int i = 1; i < saatCount; i++)
                    {
                        if (Request.Form.HasKeys() && Request.Form.AllKeys.Contains("hdnRandevu_" + i))
                        {
                            if (!string.IsNullOrEmpty(Request.Form["hdnRandevu_" + i]))
                            {
                                string values = Request.Form["hdnRandevu_" + i];
                                string[] val = values.Split('-');
                                int valSaatId = int.Parse(val[0]);
                                int valHizmetId = int.Parse(val[1]);
                                var satinAlinmisMi = db.Zon_Rezerve.Where(z => z.UrunId == rezervasyonID && z.SaatId == valSaatId).FirstOrDefault();
                                if (satinAlinmisMi == null)
                                {
                                    var sepet = db.Zon_Sepet.Where(z => z.UrunId == rezervasyonID && z.SaatId == valSaatId && z.SessionId == Session.SessionID).FirstOrDefault();
                                    if (sepet == null)
                                    {
                                        if (Session["user"] != null)
                                            _result = rep.ActionBasket("", u.ID, Session.SessionID, rezervasyonID, val[0], val[1], "add");
                                        else
                                            _result = rep.ActionBasket("", 0, Session.SessionID, rezervasyonID, val[0], val[1], "add");
                                    }
                                    else
                                    {
                                        if (sepet.HizmetId != valHizmetId)
                                        {
                                            if (Session["user"] != null)
                                                _result = rep.ActionBasket(sepet.Id.ToString(), u.ID, Session.SessionID, rezervasyonID, val[0], val[1], "update");
                                            else
                                                _result = rep.ActionBasket(sepet.Id.ToString(), 0, Session.SessionID, rezervasyonID, val[0], val[1], "update");
                                        }
                                    }
                                    flag = "1";
                                    //break;
                                }
                                else
                                {
                                    flag = "0";
                                    s = db.Zon_Rezerve_Saat.Where(z => z.Id == valSaatId).FirstOrDefault();
                                }
                            }
                        }
                    }
                }
            }
            if (flag == "1")
            {
                if (Session["user"] == null)
                    Response.Redirect("/uyelik.aspx?returnUrl=odeme");
                else
                    Response.Redirect("/odeme.aspx", false);
            }
            else
            {
                pnlHour.Visible = true;
                //ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('" +string.Format("{0:t}",s.BaslangicSaat)+"-"+string.Format("{0:t}",s.BitisSaat)+ " saatleri arası randevu satın alınmış." + "', 'Randevu', '');</script>");
                Response.Redirect("/odeme.aspx");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    [WebMethod]
    public static string randevuK(string sirket, string ofis, string tarih, string saat)
    {
        string r = "0";
        try
        {
            if (saat != "")
            {
            RandevuRepositories rep = new RandevuRepositories();
            ZonDBEntities db = new ZonDBEntities();
            int id = rep.rezervasyonId(int.Parse(sirket), int.Parse(ofis),DateTime.Parse(tarih)).Id;
           
                int saatid = int.Parse(saat);
                var satinAlinmisMi = db.Zon_Rezerve.Where(z => z.UrunId == id && z.SaatId == saatid).FirstOrDefault();
                if (satinAlinmisMi == null)
                    r = "0";
                else
                    r = "1";
            }

            return r;
        }
        catch (Exception)
        {
            return r;
        }
    }

    [WebMethod]
    public static string randevuDelete(string sirket, string ofis, string tarih, string saat)
    {
        string r = "0";
        try
        {
            if (saat != "")
            {
                RandevuRepositories rep = new RandevuRepositories();
                ZonDBEntities db = new ZonDBEntities();
                string ses = HttpContext.Current.Session.SessionID;
                int id = rep.rezervasyonId(int.Parse(sirket), int.Parse(ofis), DateTime.Parse(tarih)).Id;

                int saatid = int.Parse(saat);
                var sepet = db.Zon_Sepet.Where(z => z.UrunId ==id && z.SaatId==saatid && z.SessionId==ses).FirstOrDefault();
                if (sepet != null)
                {
                    db.DeleteObject(sepet);
                    db.SaveChanges();
                }
               
            }

            return r;
        }
        catch (Exception)
        {
            return r;
        }
    }
}

public class randevuList
{
    public string saatId { get; set; }
    public string hizmetId { get; set; }
}
