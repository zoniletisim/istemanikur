﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="randevu.aspx.cs" Inherits="randevu" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <link href="/Resources/css/tips.css" rel="stylesheet" type="text/css" />
    <script src="/Resources/js/tips.js" type="text/javascript"></script>

	<div class="randevuContainer">
        <div class="randevuTop">
        	<a class="randevuTopRandevu" href="randevu.aspx">RANDEVU</a>
            <div class="randevuTopOdemePassive">ÖDEME</div>
            <div class="randevuTopOnayPassive">RANDEVU ONAY</div>
        </div>
 		
		<div class="randevuHeaderBox">        
        <div class="randevuHeaderTexts">
            <p class="title1">Manikür sistemimiz modern, kaliteli ve hızlı.
Tek yapmanız gereken size uygun zamanı belirlemek ve
online randevunuzu almak.</p>

        </div>
        <div class="icerikImage">
        <img src="Resources/images/randevuBanner.jpg" width="960" height="190"/>
        </div> 
    </div>
    <div class="shadow"></div>
        <div class="locationSelectWrapper">
        	<div class="selectBoxes"><span></span>
                <asp:DropDownList ID="ddlSirket" runat="server" AutoPostBack="true" ClientIDMode="Static"
                    onselectedindexchanged="ddlSirket_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="selectBoxes"><span></span>
             <asp:DropDownList ID="ddlOfis" runat="server" AutoPostBack="true" ClientIDMode="Static"
                    onselectedindexchanged="ddlOfis_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="selectBoxes"><span></span>
            	   <asp:DropDownList ID="ddlTarih" runat="server"  AutoPostBack="true" ClientIDMode="Static"
                    onselectedindexchanged="ddlTarih_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <%if(_randevu.Aciklama!=null){ %>
            <div class="randevuAciklama"><span>NOT:</span> <%=_randevu.Aciklama%> </div>
            <%} %>
        </div>
        
        <div class="hours">
       <script> 
        $(document).ready(function () {
            $(".hours").find('.randevuAlButton').fancybox({
            'width': 346,
  
            'autoScale': false,
            'transitionIn': 'none',
			'overlayOpacity': 0.3,
            'transitionOut': 'none'
        });
        $('label').each(function () {
            $(this).qtip({
                content: {text: $(this).next('.tooltiptext')},
                position: { my: 'bottom center', at: 'top center' },
                style: { classes: 'qtip-dark qtip-shadow' }
               });
        });
    });
	</script>
    <div style="display: none">
    <style>
        .tooltiptext{
    display: none;
    width:230px;
    text-align:center;
    font-size:13px;
}
	.ui-accordion .ui-accordion-header {
position:inherit !important;
background: #797979 !important;
border: 1px solid #5C5C5C !important;
text-align: center !important;
margin-bottom:6px;
font-family:calibri;
font-size:13px;
font-weight:bold;
padding-top: 7px;
height:19px;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
background: #BB0000 !important;
}
.ui-widget-content {
margin-top: -6px !important;;
padding: 1em 0.2em !important;
height: 100%;
margin-bottom: 6px !important;;
}
#accordion .ui-icon { display: none; }
	</style>
    <script>
$(function() {
    $( "#accordion" ).accordion({ active: false, autoHeight: false,collapsible: true });
  });
  </script>
    <div id="popupRandevu">
    	<span class="popupRandevuTitle">HANGİ SERVİSİMİZ İÇİN RANDEVU VERELİM?</span>
        <div id="accordion">
      <%foreach (var h in hizmetTur.Where(z => z.Zon_HizmetTur.KategoriId.Value == -1).OrderBy(z => z.Zon_HizmetTur.Sira))
        {
            if (hizmetKategori(h.HizmetId.Value))
            {%> 
        <h3 class="hizmetTitle"><%=h.Zon_HizmetTur.HizmetAd%><i class="arrowDown"></i></h3>
        <div class="hizmetTur2">
        <%var hr = hizmetKategoriList( h.HizmetId.Value,h.RandevuId.Value);
          foreach (var sub in hr)
          {
           %>
        	<label class="hizmetRadioLabel hizmetTur"  style="border: none !important;margin-bottom:3px !important;" id="hizmet_<%=sub.Id%>_<%=sub.HizmetAd %>"><input name="hizmetTuru" type="radio" value="" /><%=sub.HizmetAd %> </label>
            <div class="tooltiptext"><%=sub.Aciklama %></div>
           <%} %>
        </div>
        <%}
        }%> 
    	</div>
       <%foreach (var h in hizmetTur.Where(z => z.Zon_HizmetTur.KategoriId.Value == -1).OrderBy(z => z.Zon_HizmetTur.Sira))
         {
             if (!hizmetKategori(h.HizmetId.Value)){ %>
               <span class="hizmetTur" id="hizmet_<%=h.HizmetId%>_<%=h.Zon_HizmetTur.HizmetAd %>"><%=h.Zon_HizmetTur.HizmetAd %> </span>
        <%}} %>
    </div>
	</div>
            <asp:Panel ID="pnlHour"  runat="server">
            <% string tempClass = "";
               bool sepetKontrol = false;
               string display = "none";
               bool RezervasyonKontrol = false;
                    int i = 1;
                    foreach (var r in rezervasyonSaat.Where(z=>z.Aktif=="1"))
                    {
                        int hizmetId = 0;
                    sepetKontrol=sepetteVarmi(r.Id);
                    RezervasyonKontrol = RezerveVarmi(r.Id);
                    if(sepetKontrol)
                        hizmetId = sepetVal(r.Id);
                         
                    if (RezervasyonKontrol)
                        tempClass = "hourBoxPassive";
                    else if (sepetKontrol && !RezervasyonKontrol)
                    {
                        display = "block";
                        tempClass = "hourBoxMakyaj";
                    }
                    else if (!sepetKontrol && !RezervasyonKontrol)
                    {
                        display = "none";
                        tempClass = "hourBox";
                    }
                        %>
            <div id="randevuSaat">
        	<div class="<%=tempClass%>" title="" id="hourBox_<%=r.Id %>">
            	<span class="hourBoxHour1"><%=string.Format("{0:t}", r.BaslangicSaat)%></span>
                <span class="hourBoxHour2"><%=string.Format("{0:t}", r.BitisSaat)%></span>
                <%if (!RezervasyonKontrol)
                  { %>
                <a class="randevuAlButton fancybox" href="#popupRandevu" id="randevuPopup_<%=r.Id %>">Randevu Al</a>
                 <div class="delete" data="<%=r.Id%>" id="deleteButton_<%=r.Id%>""  style="display:<%=display%>;position:absolute;cursor:pointer;">X</div>
                  <% if (hizmetId != 0)
                     { %>   
                <input id="hdnRandevu_<%=r.Id%>" name="hdnRandevu_<%=i%>" type="hidden" value="<%=r.Id%>-<%=hizmetId %>" />
               <%}
                     else
                     { %>
                  <input id="hdnRandevu_<%=r.Id%>" name="hdnRandevu_<%=i%>" type="hidden" value="" />
               <%}
                  }
                  else
                  {%>
                 <a class="randevuAlButtonDolu">Dolu</a>
                  <%} %>
            </div>
            <asp:HiddenField ID="hiddenRBox" ClientIDMode="Static" runat="server" />
           </div>
            <%i++;
             } %>
         
           
         </asp:Panel>
        </div>
        <%--<div class="randevuContinue"></div> --%>
         <asp:LinkButton ID="LinkButton1" class="randevuContinue" runat="server" 
            onclick="LinkButton1_Click" />
    </div>

  <%--  <asp:Label ID="Label1" runat="server" CssClass="loading" Visible="false" Text="yükleniyor"></asp:Label>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#popupRandevu").find(".hizmetTur").click(function () {
            var s = $(this).attr("id").split('_');
            var hizmetId = s[1];
            var saatId = $("#hiddenRBox").val();
            $("#hourBox_" + saatId).addClass("hourBoxMakyaj");
            $("#hourBox_" + saatId).attr("title", s[2]);
            $("#hdnRandevu_" + saatId).val(saatId + '-' + hizmetId);
            $("#deleteButton_" + saatId).attr("data", saatId);
            $("#deleteButton_" + saatId).css("display", "block");

            //jQuery.fancybox.close();
        });
        
        
        $(".hizmetTur").click(function () {
           jQuery.fancybox.close();
        });
        
        $(".randevuAlButton").click(function () {
            var bId = $(this).attr("id").split('_');
            var bId = bId[1];
            $("#hiddenRBox").val(bId);
        });
        $(".delete").click(function () {
           
            var sirket = $('#ddlSirket').val();
            var ofis = $('#ddlOfis').val();
            var tarih = $('#ddlTarih').val();

            var bId = $(this).attr("data");
            $("#hdnRandevu_" + bId).val("");
            $("#deleteButton_" + bId).css("display", "none");
            $("#hourBox_" + bId).removeClass("hourBoxMakyaj");
            $("#hourBox_" + bId).addClass("hourBox");
            $("#hourBox_" + bId).attr("title", "");
            $.ajax({

                type: "post",
                url: "randevu.aspx/randevuDelete",
                data: "{sirket: '" + sirket + "',ofis: '" + ofis + "',tarih: '" + tarih + "',saat: '" + bId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == "1") {

                        jAlert("Randevu Satın Alınmış", 'Randevu', '');
                        ret = "1";

                    }
                    else if (response.d == "0") {
                        ret = "0";

                    }
                    console.log(ret);
                },
                error: function () {

                }

            });
        });

    });

    $('#adsdasd').click(function () {
        var ret;
        var sirket = $('#ddlSirket').val();
        var ofis = $('#ddlOfis').val();
        var tarih = $('#ddlTarih').val();
        $('#randevuSaat  input[type=hidden]').each(function () {

            var deger = $(this).val();
            // alert(deger);
            if (deger != "") {
                var degerS = deger.split('-');
                var saatId = deger[0];
                var ret = "";
                $.ajax({

                    type: "post",
                    url: "randevu.aspx/randevuK",
                    data: "{sirket: '" + sirket + "',ofis: '" + ofis + "',tarih: '" + tarih + "',saat: '" + saatId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == "1") {

                            jAlert("Randevu Satın Alınmış", 'Randevu', '');
                            ret = "1";
                           
                        }
                        else if (response.d == "0") {
                            ret = "0";

                        }
                        console.log(ret);
                    },
                    error: function () {

                    }

                });

            }

        });

        if (ret == "1")
            alert(ret);
        else
            alert(ret)

    })
</script>
</asp:Content>















