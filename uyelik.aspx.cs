﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uyelik : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    RandevuRepositories repRezervasyon = new RandevuRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.Url.ToString().Contains("http://"))
        //    Response.Redirect(Request.Url.ToString().Replace("http://", "https://"), false);
        if (!Page.IsPostBack)
        {
            fillSirket();

        }
    }
    protected void fillSirket()
    {
        var a=repRezervasyon.GetSirketUyelik();

        ddlSirket.DataSource = repRezervasyon.GetSirketUyelik();
        ddlSirket.DataTextField = "Value";
        ddlSirket.DataValueField = "Key";
        ddlSirket.DataBind();
        ddlSirket.Items.Insert(0, new ListItem("-- Şirket Seçiniz --", "-1"));
        ddlSirket.Items.Insert(a.Count+1, new ListItem("Diğer", "999"));
    }

    protected void ddlSirket_SelectedIndexChanged(object sender, EventArgs e)
    {
         Dictionary<string, string> ofis=new Dictionary<string,string>();
        int sirketId = int.Parse(ddlSirket.SelectedValue);
        if (sirketId != 999)
        {
            ofis = repRezervasyon.GetOfis(sirketId);
            if (ofis.Count > 0)
            {
                ddlOfis.DataSource = ofis;
                ddlOfis.DataTextField = "Value";
                ddlOfis.DataValueField = "Key";
                ddlOfis.DataBind();
                ddlOfis.Items.Insert(0, new ListItem("-- Ofis Seçiniz --", "-1"));
                ddlOfis.Visible = true;
                txtSirket.Visible = false;
                RequiredFieldValidator9.Visible = false;
            }
            else
            {
                ddlOfis.Visible = false;
                txtSirket.Visible = false;
                RequiredFieldValidator9.Visible = false;
            }
        }
        else
        {
            ddlOfis.Visible = false;
            txtSirket.Visible = true;
            RequiredFieldValidator9.Visible = true;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (txtUser.Text != "" && txtPassword.Text != "")
        {
            var user = rep.CheckUser(txtUser.Text, txtPassword.Text);
            if (user.result == true)
            {
                Session.Add("user", user.data);

                rep.userLastLoginUpdate(txtUser.Text, txtPassword.Text);
                if (Request.QueryString["returnUrl"]!=null)
                    Response.Redirect("/odeme.aspx", false);
                else
                    Response.Redirect("/anasayfa.aspx", false);
            }
            else
            {
                //Panel1.Visible = true;

                lblMessage.Text = user.message;
                ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('" + user.message + "', 'Üyelik Kayıt', '');</script>");
                txtUser.Focus();
            }
        }
    }

    protected void cvEmail_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            var u = rep.emailKontrol(txtEmail.Text);
            if (u.result)
                args.IsValid = true;
            else
                args.IsValid = false;
        }
        catch (Exception ex)
        {

        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        if (!chkSozlesme.Checked)
            ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('Lütfen üyelik sözleşmesini onaylayın!', 'Üyelik Kayıt', '');</script>");
        else
        {
            var a = rep.saveUser(Request.Form);
            if (a.result == true)
            {
                //Session.Add("user", a.data);
               // Response.Redirect("/anasayfa.aspx");

                var ac = rep.KullaniciAktivasyon(a.id);
                ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('Kaydınız başarıyla yapılmış.<br />Üyeliğinizi aktif duruma getirmek için e-mail adresinize <br/> gönderilen aktivasyon linkine tıklayınız.', 'Üyelik Kayıt'," + "function (result) {if (result) {window.location.href = 'anasayfa.aspx'}});</script>");
                
            }
        }
    }
}