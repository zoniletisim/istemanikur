﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="odemeResult.aspx.cs" Inherits="odemeResult" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities(); %>
<div class="mainContainer">
	<div class="odemeContainer">
        <div class="randevuTop">
        	<a class="randevuTopRandevuPassive" href="randevu.aspx">RANDEVU</a>
            <a class="randevuTopOdemePassive" href="odeme.aspx">ÖDEME</a>
            <div class="randevuTopOnay">RANDEVU ONAY</div>
        </div>
         <%if(Request.QueryString["sNo"] != null){ %>
        <div class="odemeResultBanner">
        	<span class="odemeResultBannerTitle">ÖDEMENİZ<br /><font color="#e30000"><b>BAŞARIYLA ALINMIŞTIR.</b></font></span>	
            <span class="odemeResultBannerText1">Etkinlik yeri ve<br /> rezervasyon numarasını lütfen not ediniz.<br /> Rezervasyon No:<b style="font-size:14px;color:#e30000"> <%=siparis.siparisNo%></b></span>
            <span class="odemeResultBannerText2">Randevudan<br /> 5 dk önce geliniz.</span>
<span class="odemeResultBannerText3">Servisinizden memnun kalırsanız,
servis alanındaki tip box'a 
bahşiş bırakabilirsiniz.</span>
        </div>
        <div class="shadow"></div>
        
       

        <div class="odemeBasket">
        	<div class="items">
            <div class="title">
            <table width="960">
            <tr>
               
                <td style="width:120px">Hizmet</td>
                <td style="width:120px">Randevu Günü</td>
                <td style="width:120px">Randevu Saati</td>
                <td style="width:100px">Firma</td>
                <td style="width:100px">Ofis</td>
                <td style="width:70px">Fiyat</td>
            </tr>
            </table>
            </div>
            <%
                decimal toplamTutar = 0;
                foreach (var s in siparis.siparisDetay)
              {
                  ZonDBModel.Zon_Randevu i = (from item in db.Zon_Randevu
                                              where item.Id == s.UrunID
                                              select item).FirstOrDefault();

                  ZonDBModel.Zon_HizmetTur h = (from item in db.Zon_HizmetTur
                                                where item.Id == s.HizmetId
                                                select item).FirstOrDefault();
                  ZonDBModel.Zon_Rezerve_Saat rs = (from item in db.Zon_Rezerve_Saat
                                                    where item.Id == s.SaatId
                                                    select item).FirstOrDefault();

                    if(s.IndirimliFiyat!=null)
                        toplamTutar += s.IndirimliFiyat.Value;
                    else
                        toplamTutar += s.ToplamFiyat.Value;
                    %>
                <div class="item">
                    
                  
                    <div class="basketurun"><%=h.HizmetAd %></div>
                    <div class="basketGun"><%=string.Format("{0:d MMMM yyyy dddd}", i.Tarih)%></div>
                    <%if(i.SaatSablonId!=null){ %>
                    <div class="basketSaat"><%=string.Format("{0:t}", rs.BaslangicSaat.Value.AddMinutes((int)i.SaatSablonId))%> - <%=string.Format("{0:t}", rs.BitisSaat.Value.AddMinutes((int)i.SaatSablonId))%></div>
                    <%}else{ %>
                    <div class="basketSaat"><%=string.Format("{0:t}", rs.BaslangicSaat.Value)%> - <%=string.Format("{0:t}", rs.BitisSaat.Value)%></div>
                    <%} %>
                    <div class="basketFirma"><%=i.Zon_Ofis.Zon_Sirket.SiretAd%></div>
                    <div class="basketOfis"><%=i.Zon_Ofis.OfisAd%></div>
                    <div class="basketPrice">
                    <%if(s.IndirimliFiyat!=null){ %>
                     <span style="color:#A8A6A6;text-decoration:line-through"><%=s.ToplamFiyat%> TL</span>
                    <%=s.IndirimliFiyat%> TL
                    <%}else{ %>
                    <%=s.ToplamFiyat%> TL
                    <%} %>
                    </div>
                 
                    <%--<div class="delete">
                       <a href="?del=1&pid=<%=s.Id%>" class="deleteMainBasket SepetSil basketIconClose" title="Sepetten Sil"></a>
                    </div> --%>

                </div>
                <% 
              } %>
              
                <div class="sepetToplamTutar">
                 <ul style="width: 180px;">
                 <%
                     if (siparis.hediyeCekTutar != 0)
                   { %>
                <li style="float:right;margin-top:5px;padding-top:5px;font-size: 14px;"><div class="text"><b>HEDİYE ÇEKİ: </b></div>
                        <div class="value" id=""> <%=string.Format("{0:N2}", siparis.hediyeCekTutar)%> TL </div>
                        </li>
                        <%}if(Session["hizmetIndirimTutar"] != null){ %>
                        <li style="float:right;margin-top:5px;padding-top:5px;font-size: 14px;"><div class="text"><b>2. HİZMET İNDİRİMİ: </b></div>
                        <div class="value" id="Div1"> <%=Session["hizmetIndirimTutar"]%> TL </div>
                        </li>
                        <%} %>
                     <li style="float:right;margin-top:5px;padding-top:5px;font-size: 14px;"><div class="text"><b>TOPLAM: </b></div>
                        <div class="value" id="totalPrice"> <%=string.Format("{0:N2}", siparis.seciliSiparis.ToplamTutar) %> TL </div>
                    </li>
                </ul>
            </div>
            </div>		
        </div>
         
        <%}
          else if (Request.QueryString["error"] != null)
          { %>
            <div class="odemeResultBannerBasarisiz">
        	<span class="odemeResultBannerTitle" style="width:600px;text-align:left;">ÜZGÜNÜZ <br /><font color="#e30000"><b>ÖDEME BAŞARISIZ.</b></font></span>	
            <br />
           <div class="odemeResultBannerText2" style="width:600px;margin-top:20px;margin-left:90px;">Hata: <%=Request.QueryString["error"].ToString()%></div>
           
        </div>
        <div class="shadow"></div>
        <%} %>
      </div> 
</div>
</asp:Content>














