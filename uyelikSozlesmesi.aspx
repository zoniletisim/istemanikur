﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uyelikSozlesmesi.aspx.cs" Inherits="uyelikSozlesmesi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="/resources/css/style.css">
</head>

<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        try {
            document.execCommand('BackgroundImageCache', false, true);
        } catch (e) { }
</script>
    <div class="popupTextsContaıner" style="font-size:12px;line-height:18px">
    <div class="popupTextsTitle" style="font-size:16px;">Kullanım Koşulları ve Üyelik Sözleşmesi</div>
    
Madde 1: Taraflar<br />
İştemanikür: istemanikur.com internet sitesinin faaliyetlerini yürüten  İştemanikür Müge Meydan<br />
Üye : istemanikur.com internet sitesine üye olan internet kullanıcısı<br /><br />
 
Madde 2: Sözleşmenin Konusu<br />
İşbu Sözleşme'nin konusu İştemanikür Müge Meydan'ın sahip olduğu internet sitesi istemanikur.com'dan üyenin faydalanma şartlarının belirlenmesidir.<br /><br />
 
Madde 3: Tarafların Hak ve Yükümlülükleri<br />
3.1. Üye, istemanikur.com internet sitesine üye olurken verdiği kişisel ve diğer sair bilgilerin kanunlar önünde doğru olduğunu, İştemanikür'ün bu bilgilerin gerçeğe aykırılığı nedeniyle uğrayacağı tüm zararları aynen ve derhal tazmin edeceğini beyan ve taahhüt eder.<br /><br />
3.2. Üye, kayıt işlemi sırasında belirlemiş olduğu şifresi başka kişi ya da kuruluşlara veremez, üyenin sözkonusu şifreyi kullanma hakkı bizzat kendisine aittir. Bu sebeple doğabilecek tüm sorumluluk ile üçüncü kişiler veya yetkili merciler tarafından İştemanikür'e karşı ileri sürülebilecek tüm iddia ve taleplere karşı, İştemanikür'ün sözkonusu izinsiz kullanımdan kaynaklanan her türlü tazminat ve sair talep hakkı saklıdır.<br /><br />
3.3. Üye istemanikur.com internet sitesini kullanırken yasal mevzuat hükümlerine riayet etmeyi ve bunları ihlal etmemeyi baştan kabul ve taahhüt eder. Aksi takdirde, doğacak tüm hukuki ve cezai yükümlülükler tamamen ve münhasıran üyeyi bağlayacaktır.<br /><br />
3.4. Üye, istemanikur.com internet sitesini hiçbir şekilde kamu düzenini bozucu, genel ahlaka aykırı, başkalarını rahatsız ve taciz edici şekilde, yasalara aykırı bir amaç için, başkalarının fikri ve telif haklarına tecavüz edecek şekilde kullanamaz. Ayrıca, üye başkalarının hizmetleri kullanmasını önleyici veya zorlaştırıcı faaliyet (spam, virus, truva atı, vb.) ve işlemlerde bulunamaz.<br /><br />
3.5. İştemanikür, mevcut imkanlar dahilinde tedbirlerin alınmış olmasına rağmen, üye verilerinin yetkisiz kişilerce okunmasından ve üye yazılım ve verilerine gelebilecek zararlardan dolayı sorumlu olmayacaktır. Üye, istemanikur.com internet sitesinin kullanılmasından dolayı uğrayabileceği herhangi bir zarar yüzünden İştemanikür'den tazminat talep etmemeyi peşinen kabul etmiştir.<br /><br />
3.6. Üye, diğer internet kullanıcılarının yazılımlarına ve verilerine izinsiz olarak ulaşmamayı veya bunları kullanmamayı kabul etmiştir. Aksi takdirde, bundan doğacak hukuki ve cezai sorumluluklar tamamen üyeye aittir.<br /><br />
3.7. İşbu üyelik sözleşmesi içerisinde sayılan maddelerden bir ya da birkaçını ihlal eden üye işbu ihlal nedeniyle cezai ve hukuki olarak şahsen sorumlu olup, İştemanikür'i bu ihlallerin hukuki ve cezai sonuçlarından ari tutacaktır. Ayrıca; işbu ihlal nedeniyle, olayın hukuk alanına intikal ettirilmesi halinde, İştemanikür'ün üyeye karşı üyelik sözleşmesine uyulmamasından dolayı tazminat talebinde bulunma hakkı saklıdır.<br /><br />
3.8. İştemanikür'ün her zaman tek taraflı olarak gerektiğinde üyenin üyeliğini silme, müşteriye ait dosya, belge ve bilgileri silme hakkı vardır. Üye işbu tasarrufu önceden kabul eder. Bu durumda, İştemanikür'ün hiçbir sorumluluğu yoktur.<br /><br />
3.9. istemanikur.com internet sitesi yazılım ve tasarımı İştemanikür mülkiyetinde olup, bunlara ilişkin telif hakkı ve/veya diğer fikri mülkiyet hakları ilgili kanunlarca korunmakta olup, bunlar üye tarafından izinsiz kullanılamaz, iktisap edilemez ve değiştirilemez. Bu web sitesinde adı geçen başkaca şirketler ve ürünleri sahiplerinin ticari markalarıdır ve ayrıca fikri mülkiyet hakları kapsamında korunmaktadır.<br /><br />
3.10. İştemanikür tarafından istemanikur.com internet sitesinin iyileştirilmesi, geliştirilmesine yönelik olarak ve/veya yasal mevzuat çerçevesinde siteye erişmek için kullanılan Internet Protokol (IP) adresi, siteye erişilen tarih ve saat, sitede bulunulan sırada erişilen sayfalar ve siteye doğrudan bağlanılmasını sağlayan web sitesinin adresi gibi birtakım bilgiler toplanabilir.<br /><br />
3.11. İştemanikür kullanıcılarına daha iyi hizmet sunmak, ürünlerini ve hizmetlerini iyileştirmek, sitenin kullanımını kolaylaştırmak için kullanımını kullanıcılarının özel tercihlerine ve ilgi alanlarına yönelik çalışmalarda üyelerin kişisel bilgilerini kullanabilir. İştemanikür, üyenin istemanikur.com internet sitesi üzerinde yaptığı hareketlerin kaydını bulundurma hakkını saklı tutar.<br /><br />
3.12. İştemanikür'e üye olan kişi, yürürlükte bulunan ve/veya yürürlüğe alınacak uygulamalar kapsamında İştemanikür tarafından kendisine ürün ve hizmet tanıtımları, reklamlar, kampanyalar, avantajlar, anketler ve diğer müşteri memnuniyeti uygulamaları sunulmasına izin verdiğini beyan ve kabul eder. Üye aksini bildirmediği sürece İştemanikür'ün kendisi ile internet, telefon, SMS, vb iletişim kanalları kullanarak irtibata geçmesine izin verdiğini beyan ve kabul eder.<br /><br />
3.13. İştemanikür, üyenin kişisel bilgilerini yasal bir zorunluluk olarak istendiğinde veya (a) yasal gereklere uygun hareket etmek veya İştemanikür'e tebliğ edilen yasal işlemlere uymak; (b) İştemanikür ve İştemanikür web sitesi ailesinin haklarını ve mülkiyetini korumak ve savunmak için gerekli olduğuna iyi niyetle kanaat getirdiği hallerde açıklayabilir.<br /><br />
3.14. İştemanikür web sitesinin virus ve benzeri amaçlı yazılımlardan arındırılmış olması için mevcut imkanlar dahilinde tedbir alınmıştır. Bunun yanında nihai güvenliğin sağlanması için kullanıcının, kendi virus koruma sistemini tedarik etmesi ve gerekli korunmayı sağlaması gerekmektedir. Bu bağlamda üye istemanikur.com sitesi'ne girmesiyle, kendi yazılım ve işletim sistemlerinde oluşabilecek tüm hata ve bunların doğrudan yada dolaylı sonuçlarından kendisinin sorumlu olduğunu kabul etmiş sayılır.<br /><br />
3.15. İştemanikür, sitenin içeriğini dilediği zaman değiştirme, kullanıcılara sağlanan herhangi bir hizmeti değiştirme yada sona erdirme veya istemanikur.com web sitesi'nde kayıtlı kullanıcı bilgi ve verilerini silme hakkını saklı tutar.<br /><br />
3.16. İştemanikür, üyelik sözleşmesinin koşullarını hiçbir şekil ve surette ön ihbara ve/veya ihtara gerek kalmaksızın her zaman değiştirebilir, güncelleyebilir veya iptal edebilir. Değiştirilen, güncellenen yada yürürlükten kaldırılan her hüküm , yayın tarihinde tüm üyeler bakımından hüküm ifade edecektir.<br /><br />
3.17. Taraflar, İştemanikür'e ait tüm bilgisayar kayıtlarının tek ve gerçek münhasır delil olarak, HUMK madde 287'ye uygun şekilde esas alınacağını ve söz konusu kayıtların bir delil sözleşmesi teşkil ettiği hususunu kabul ve beyan eder.<br /><br />

Madde 4: Sözleşmenin Feshi<br />
İşbu sözleşme üyenin üyeliğini iptal etmesi veya İştemanikür tarafından üyeliğinin iptal edilmesine kadar yürürlükte kalacaktır. İştemanikür üyenin üyelik sözleşmesinin herhangi bir hükmünü ihlal etmesi durumunda üyenin üyeliğini iptal ederek sözleşmeyi tek taraflı olarak feshedebilecektir.<br /><br />
 
Madde 5: İhtilaflerin Halli<br />
İşbu sözleşmeye ilişkin ihtilaflerde İstanbul Mahkemeleri ve İcra Daireleri yetkilidir.<br /><br />
 
Madde 6: Yürürlük<br />
Üyenin, üyelik kaydı yapması üyenin üyelik sözleşmesinde yer alan tüm maddeleri okuduğu ve üyelik sözleşmesinde yer alan maddeleri kabul ettiği anlamına gelir. İşbu Sözleşme üyenin üye olması anında akdedilmiş ve karşılıklı olarak yürürlülüğe girmiştir.<br />
    </div>
    </form>
</body>
</html>

