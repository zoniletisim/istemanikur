﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="henuzyok.aspx.cs" Inherits="henuzyok" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/resources/css/style.css">
</head>

<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
try {
 document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
</script>
    <div>
    	<div class="henuzyokForm">
        	<div class="henuzyokTitle">
                <p><font color="#f11b19">»</font> Sitemize üye olun.</p>
                
                <p><font color="#f11b19">»</font> İlgili biriminizle bizim için görüşme tarihi ayarlayın.</p> 
                
                <p><font color="#f11b19">»</font> Ofisinizde olduğumuz günlerde ilk manikürünüz bizden.</p>
            </div>
        	<div class="henuzyokFormIn">
	            <input name="" type="text" id="txtAd" class="henuzyokFormInput" placeholder="Ad"/>
                <input name="" type="text" id="txtSoyad" class="henuzyokFormInput" placeholder="Soyad"/>	
                <input name="" type="text" id="txtEmail" class="henuzyokFormInput" placeholder="E-posta"/>	
                <input name="" type="text" id="txtSirket" class="henuzyokFormInput" placeholder="Şirket"/>	
                <input name="" type="text" id="txtIletisimKisi" class="henuzyokFormInput" placeholder="İletişime geçilecek kişi"/>	
                <input name="" type="text" id="txtTel" class="henuzyokFormInput" placeholder="İletişime geçilecek telefon numarası"/>	
                <input name="" type="button" class="henuzyokFormSubmit" value=""/>	
                 <div class="Confirm">
         <span id="lblError" style="color:Red;margin-left:20px;font-size:13px;margin-top:0px;float:left;"></span>
         <span id="lblSuccess" style="color:Green;margin-left:20px;font-size:16px;margin-top:0px;float:left;"></span>
         <div id="loading" style="display:none"><img src="/resources/images/loading.gif" /> İşleminiz Yapılıyor Lütfen Bekleyiniz...</div>
        
        </div>
            </div>

        </div>
    	
    </div>
    <script src="/Resources/js/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="/Resources/js/jquery.placeholder.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('input, textarea').placeholder();
        });
  </script>
    </form>

<script type="text/javascript">
    $('.henuzyokFormSubmit').click(function () {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if ($('#txtEmail').val() != "" && $('#txtAd').val() != "" && $('#txtTel').val() != "" && $('#txtKonu').val() != "0" && $('#txtSoyad').val() != "" && $('#txtIletisimKisi').val() != "" && $('#txtSirket').val() != "") {
            if (emailReg.test($('#txtEmail').val())) {
                $('#loading').css("display", "block");
                $('#henuzyokFormSubmit').css("display", "none");
                $.ajax({
                    type: "post",
                    url: "henuzyok.aspx/saveForm",
                    data: "{ad:'" + $('#txtAd').val() + "',soyad:'" + $('#txtSoyad').val() + "',email:'" + $('#txtEmail').val() + "',sirket:'" + $('#txtSirket').val() + "',iletisimKisi:'" + $('#txtIletisimKisi').val() + "',tel:'" + $('#txtTel').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $('#txtEmail').val('');
                        $('#txtAd').val('');
                        $('#txtSoyad').val('');
                        $('#txtTel').val('');
                        $('#txtSirket').val('');
                        $('#txtIletisimKisi').val('');
                        $('#lblError').text('');
                        $('#lblSuccess').text('Kaydınız Başarıyla Yapıldı. ');
                        $('#loading').css("display", "none");
                        $('#henuzyokFormSubmit').css("display", "block");
                    },
                    error: function () {

                    }
                });
            }
            else {
                $('#lblError').html("Lütfen geçerli email adresi girin.");
            }
        }
        else {
            $('#lblError').html("Lütfen Tüm Alanları Doldurun.");
        }
    });
   
</script>
</body>
</html>









