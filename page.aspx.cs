﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class page : System.Web.UI.Page
{
    SayfaRepositories rep = new SayfaRepositories();
    public Zon_SayfaIcerik sayfa=null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
           sayfa= rep.GetSayfa(Convert.ToInt32(Request.QueryString["id"].ToString()),(int)Constants.safyaIcerikTip.sayfa,1); 
        }
    }
}