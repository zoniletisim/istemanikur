﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="uyelik.aspx.cs" Inherits="uyelik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div class="mainContainer">
<script>
    $(document).ready(function () {
    $(".uyelikSignupContainer").find('.uyelikSozlesmesi').fancybox({
        'width': 800,
        'height': 600,
        'autoScale': false,
        'transitionIn': 'none',
        'overlayOpacity': 0.5,
        'transitionOut': 'none',
        'centerOnScroll': true,
        'type': 'iframe'
    });
});
</script>
	<div class="uyelikContainer">
        <div class="uyelikBanner">
        <div class="bannerTitle1">
    	  <p>&nbsp;</p>
    	  <p>Pratik, şık ve hijyenik manikürünüzü</p> 
    	  <p><font color="#e30000"><b>ofisinizde </b></font>yapıyoruz!</p>
    	</div>
        
        </div>
        <div class="uyelikShadow"></div>
        <div class="uyelikLoginContainer">
        	<div class="uyelikLoginTitle">ÜYE GİRİŞİ</div>	
            <div class="uyelikLoginText">Daha önce belirlemiş olduğunuz e-posta adresiniz ve şifreniz ile giriş yapın. Üye değilseniz, sağ taraftaki formu doldurarak, kolayca üye olun!</div>
            <asp:TextBox ID="txtUser" CssClass="loginFormInput" placeholder="E-posta adresinizi giriniz." runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="loginValidate" ValidationGroup="login" Text="<img src='../Resources/images/Exclamation.gif' title='Email Adresinizi Giriniz.' />" runat="server" ControlToValidate="txtUser" ErrorMessage="Email Adresinizi Giriniz."></asp:RequiredFieldValidator>
        
            <asp:TextBox ID="txtPassword" ValidationGroup="login" TextMode="Password" CssClass="loginFormInput" placeholder="Şifrenizi giriniz." runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="loginValidate" ValidationGroup="login" Text="<img src='../Resources/images/Exclamation.gif' title='Şifresnizi Giriniz.' />" runat="server" ControlToValidate="txtPassword" ErrorMessage="Şifre Giriniz."></asp:RequiredFieldValidator>
           
           
              <asp:CheckBox ID="chkBeniHatirla"  ClientIDMode="Static" class="loginFormCheckbox" runat="server" Text="Beni hatırla" />
            <a class="forgotPassword" style="color: red;text-decoration: none;" href="/sifreHatirlat.aspx">Şifremi unuttum!</a>
            <asp:Button ID="Button1" ValidationGroup="login"  runat="server" Text="" CssClass="uyelikLoginButton" onclick="Button1_Click" />

             <asp:Panel ID="Panel1" Visible="false" runat="server">
            <div class="validateUyeGirisi">
                <asp:Label ID="lblMessage" CssClass="validationsummary" runat="server" Text=""></asp:Label>
            </div>  
        </asp:Panel>
        </div>
        <div class="uyelikSignupContainer">
       		<div class="uyelikSignupTitle">YENİ ÜYE</div>	
            <div class="uyelikSignupText">Henüz üye olmadıysanız aşağıdaki formu doldurarak kolayca üye olun!</div>
            <asp:TextBox class="SignupFormInput" ID="txtAd"  placeholder="* Ad" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="loginValidate" Text="<img src='../Resources/images/Exclamation.gif' title='Adınızı Giriniz.' />"  runat="server" ControlToValidate="txtAd" ErrorMessage="Adınızı Giriniz."></asp:RequiredFieldValidator>

            <asp:TextBox class="SignupFormInput" ID="txtSoyad" placeholder="* Soyad"  runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="loginValidate" Text="<img src='../Resources/images/Exclamation.gif' title='Soyadınızı Giriniz.' />" runat="server" ControlToValidate="txtSoyad" ErrorMessage="Soyadınızı Giriniz."></asp:RequiredFieldValidator>
               
             <asp:TextBox class="SignupFormInput" ID="txtEmail" placeholder="*Şirket e- postanız (Kullanıcı adınız olacaktır.)"  runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="loginValidate" Text="<img src='../Resources/images/Exclamation.gif' title='Email adresinizi Giriniz.' />" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email Giriniz."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmailValid" CssClass="loginValidate" runat="server"  ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Text="<img src='../Resources/images/Exclamation.gif' title='Geçerli email adresi giriniz.' />" ControlToValidate="txtEmail" ErrorMessage="Geçerli email adresi giriniz."></asp:RegularExpressionValidator>
              <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic"
                      onservervalidate="cvEmail_ServerValidate"  Text="<img src='../Resources/images/Exclamation.gif' title='Bu mail adresi kullanılıyor.' />" ErrorMessage="Bu mail adresi kullanılıyor."
                         ></asp:CustomValidator>

            <asp:TextBox class="SignupFormInput" ID="txtSifre" TextMode="Password" placeholder="*Şifre"  runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="loginValidate" Text="<img src='../Resources/images/Exclamation.gif' title='Şifresinizi Giriniz.' />" runat="server" ControlToValidate="txtSifre" ErrorMessage="Şifrenizi Giriniz."></asp:RequiredFieldValidator>
             
            <asp:TextBox class="SignupFormInput" ID="txtTel"  placeholder="* Telefon" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" CssClass="loginValidate" Text="<img src='../Resources/images/Exclamation.gif' title='Telefon Giriniz.' />"  runat="server" ControlToValidate="txtAd" ErrorMessage="Adınızı Giriniz."></asp:RequiredFieldValidator>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
              <asp:DropDownList ID="ddlSirket" runat="server" class="SignupFormSelect" AutoPostBack="true" onselectedindexchanged="ddlSirket_SelectedIndexChanged"></asp:DropDownList>
           <asp:RequiredFieldValidator ID="RequiredFieldValidator8" CssClass="loginValidate" Text="<img src='../Resources/images/Exclamation.gif' title='Email adresinizi Giriniz.' />" runat="server" ControlToValidate="ddlSirket" InitialValue="-1" ErrorMessage="Şirket Seçiniz"></asp:RequiredFieldValidator>
            <%--<span class="sirketNedenYok">Şirketim bu listede yok! <a href="#" style="color: red;text-decoration: none;">Neden?</a></span>--%>
            <asp:DropDownList ID="ddlOfis"  class="SignupFormSelect" runat="server"></asp:DropDownList>
            <asp:TextBox class="SignupFormInput" ID="txtSirket"  placeholder="* Şirket Giriniz." Visible="false" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" CssClass="loginValidate" Text="<img src='../Resources/images/Exclamation.gif' title='Email adresinizi Giriniz.' />" runat="server" ControlToValidate="txtSirket" Visible="false" ErrorMessage="Şirket Adı Giriniz."></asp:RequiredFieldValidator>
            </ContentTemplate>
            </asp:UpdatePanel>

           
            <span class="kullanimSartlari">Lütfen üye olmadan önce  <a class="uyelikSozlesmesi" data-fancybox-type="iframe" href="/uyelikSozlesmesi.aspx"> kullanım ve üyelik </a>sözlemesini okuyup onaylayın.</span>
            <div class="checkDiv">  
            <asp:CheckBox ID="chkSozlesme" ClientIDMode="Static"  Checked="true" class="SignupFormCheckbox" runat="server" Text="Üyelik sözleşmesini onaylıyorum." />
            </div>
            <div class="checkDiv">
                <asp:CheckBox ID="chkBilgi" runat="server" Checked="true" Text="İştemanikür'den bilgi, duyuru ve kampanya e-maili almak istiyorum." />
            </div>
              <asp:Button ID="Button2" runat="server"  Text="" CssClass="uyelikSignupButton"  onclick="Button2_Click" /></div>
                <div style="clear:both"></div>
        </div>
      
    </div>
      <div style="clear:both"></div>
       <div style="display: none">
    <div id="popupRandevu">
    	<span class="popupRandevuTitle">Kullanım ve Üyelik Sözleşmesi </span>
     
    </div>
	</div>
    <script src="Resources/js/jquery.placeholder.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('input, textarea').placeholder();

    });
 
  </script>
</asp:Content>











