﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Web.Services;
public partial class odeme : System.Web.UI.Page
{
    public SepetModel sepetUrun = new SepetModel();
    public decimal toplamTutar = 0;
    RandevuRepositories rep = new RandevuRepositories();
    SiparisRepositories SiparisRep = new SiparisRepositories();
    ZonDBEntities db = new ZonDBEntities();
    Zon_Kullanici u = null;
    public int ofisId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Url.ToString().Contains("http://"))
                Response.Redirect(Request.Url.ToString().Replace("http://", "https://"), false);
            if (Session["user"] == null)
                Response.Redirect("/uyelik.aspx");
            else
                u = (Zon_Kullanici)Session["user"];

            sepetUrun = rep.getSepet(Session.SessionID);
            sepetToplam.Value = sepetUrun.genelToplam.ToString();
            txtCvc.Attributes.Add("autocomplete", "Off");
            //txtKartIsim.Attributes.Add("autocomplete", "Off");
            txtKartNo1.Attributes.Add("autocomplete", "Off");
            txtKartNo2.Attributes.Add("autocomplete", "Off");
            txtKartNo3.Attributes.Add("autocomplete", "Off");
            txtKartNo4.Attributes.Add("autocomplete", "Off");
            if (!Page.IsPostBack)
            {

                if (!string.IsNullOrEmpty(u.TcNo))
                    txtTcKimlik.Text = u.TcNo;
                ddAy.DataSource = Constants.GetKrediKartSkAy();
                ddAy.DataTextField = "Value";
                ddAy.DataValueField = "Key";
                ddAy.DataBind();

                ddYil.DataSource = Constants.GetKrediKartSkYil();
                ddYil.DataTextField = "Value";
                ddYil.DataValueField = "Key";
                ddYil.DataBind();

                if (Request.QueryString["del"] != null)
                {
                    string id = Request.QueryString["pid"];

                    var s = rep.SepetSil(id);
                    Response.Redirect("/odeme.aspx");
                }
                if (Request.QueryString["Not"] != null)
                {

                    string note = Request.QueryString["Not"];
                    Session.Add("BasketNote", note);
                    Response.Redirect("odeme.aspx");
                    //var s = rep.SepetSil(id);
                    //sepetUrun = rep.getSepet(Session.SessionID);
                }
            }

            else
            {
                if (Session["user"] != null)
                {
                    Zon_Randevu r = null;
                    string adresId = "";
                    u = (Zon_Kullanici)Session["user"];
                    OdemeRepositories odeme = new OdemeRepositories();
                    ResultAction islemDurm = new ResultAction();
                    var sepet = db.Zon_Sepet.Where(z => z.SessionId == Session.SessionID).FirstOrDefault();
                    if (sepet != null)
                        r = db.Zon_Randevu.Where(z => z.Id == sepet.UrunId).FirstOrDefault();

                    else
                    {
                        Response.Redirect("/Odemeresult.aspx?error=Sepetiniz Boş.");
                    }



                    if (r != null)
                    {
                        islemDurm = odeme.odeme(Request.Form, adresId = SiparisRep.adres((int)r.OfisId, u), adresId = SiparisRep.adres((int)r.OfisId, u));

                        if (islemDurm.result)
                            Response.Redirect("/Odemeresult.aspx?sNo=" + islemDurm.data);
                        else
                        {
                            Session.Add("Error", islemDurm.data);
                            Response.Redirect("/Odemeresult.aspx?error=" + islemDurm.data.ToString());
                        }
                    }
                    else
                    {
                        Response.Redirect("/Odemeresult.aspx?error=Adres bilgilerine ulaşılamıyor.");
                    }
                }
                else
                {
                    Response.Redirect("/uyelik.aspx");
                }


            }
        }
        catch (Exception ex)
        {
        }
        
    }

    [WebMethod]
    public static hediyeCek hediyeCeki(string kod)
    {

        ZonDBEntities db = new ZonDBEntities();
        HediyeCekRepositories rep = new HediyeCekRepositories();
        //JavaScriptSerializer jss = new JavaScriptSerializer();
        hediyeCek h = new hediyeCek();

        Zon_Kullanici u = (Zon_Kullanici)HttpContext.Current.Session["user"];
        var result = rep.checkHediyeCeki(kod, u.ID);
        if (!result.result)
        {
            h.durum = "0";
            h.cekTutar = result.message;
        }
        else
        {
            h.durum = "1";
            string[] s = result.message.Split(':');
            decimal tutar = Convert.ToDecimal(s[0]);
            decimal Toplam = Convert.ToDecimal(s[1]);
            if(Toplam<0)
                h.toplamTutar = "0,00";
            else
                h.toplamTutar = Toplam.ToString("N");
            h.cekTutar = tutar.ToString("N");
           
        }


        return h;
    }

    [WebMethod]
    public static string cekSil()
    {
       HttpContext.Current.Session["hediyeCekKod"] = null;
       return "1";
    }

    public class hediyeCek
    {
        public string durum;
        public string cekTutar;
        public string toplamTutar;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
      
    }
}