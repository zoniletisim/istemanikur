﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class mesafeliSozlesme : System.Web.UI.Page
{
    public SepetModel o = new SepetModel();
    SiparisRepositories odemeRep = new SiparisRepositories();
    RandevuRepositories rep = new RandevuRepositories();
    public Zon_Kullanici u = null;
    public int banka = 0;
    public int taksit = 0;
    ZonDBEntities db = new ZonDBEntities();
    public Zon_Ofis ofis = new Zon_Ofis();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            u = (Zon_Kullanici)Session["user"];
            //banka =Convert.ToInt32(Request.QueryString["posBank"]);
            //taksit =Convert.ToInt32(Request.QueryString["Taksit"]);
            ofis = db.Zon_Ofis.Where(z => z.Id == u.OfisId).FirstOrDefault();
            if (Request.QueryString["ofisId"] != null)
            { 
                int ofisid=int.Parse(Request.QueryString["ofisId"]);
                ofis = db.Zon_Ofis.Where(z => z.Id == ofisid).FirstOrDefault();
            }

            o = rep.getSepet(Session.SessionID);
        }
        else
            Response.Redirect("/Uyelik.aspx");
    }
}