﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="kullanim.aspx.cs" Inherits="kullanim" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/resources/css/style.css">
</head>

<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
try {
 document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
</script>
    <div class="popupTextsContaıner" style="font-size:12px;line-height:18px;">
    <div class="popupTextsTitle">Kullanım Koşulları</div>
    	<p>istemanikur.com bu internet sitesinin genel görünüm ve dizaynı ile internet sitesindeki tüm bilgi, resim, markalar, istemanikur.com alan adı, logo, ikon, demonstratif, yazılı, elektronik, grafik veya makinede okunabilir şekilde sunulan teknik veriler, bilgisayar yazılımları, uygulanan satış sistemi, iş metodu ve iş modeli de dahil tüm materyallerin (“Materyaller”) ve bunlara ilişkin fikri ve sınai mülkiyet haklarının sahibi veya lisans sahibidir ve yasal koruma altındadır.</p>
    	<p>&nbsp;</p>
    	<p>Internet sitesinde bulunan hiçbir Materyal; önceden izin alınmadan ve kaynak gösterilmeden, kod ve yazılım da dahil olmak üzere, değiştirilemez, kopyalanamaz, çoğaltılamaz, başka bir lisana çevrilemez, yeniden yayımlanamaz, başka bir bilgisayara yüklenemez, postalanamaz, iletilemez, sunulamaz ya da dağıtılamaz.</p>
    	<p>&nbsp;</p>
    	<p>Internet sitesinin bütünü veya bir kısmı başka bir internet sitesinde izinsiz olarak kullanılamaz. Aksine hareketler hukuki ve cezai sorumluluğu gerektirir. istemanikur.com’un burada açıkça belirtilmeyen diğer tüm hakları saklıdır. </p>
    </div>
    </form>
</body>
</html>









