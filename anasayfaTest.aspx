﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPageDefault.master" AutoEventWireup="true" CodeFile="anasayfaTest.aspx.cs" Inherits="anasayfaTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link rel="stylesheet" type="text/css" href="Resources/slider/css/style2.css" />
<script type="text/javascript" src="Resources/slider/js/modernizr.custom.28468.js"></script>
<script type="text/javascript" src="Resources/slider/js/jquery.cslider.js"></script>
		<script type="text/javascript">
			$(function() {
			
				$('#da-slider').cslider({
					bgincrement	: 0
				});
			
			});
		</script>	
<div id="da-slider" class="da-slider">
				<div class="da-slide">
					<h2>BİZ FARKLIYIZ!<br />
    	  <br />
    	  Pratik, şık ve hijyenik manikürünüzü<br />
    	  <font color="#e30000"><b>ofisinizde</b></font>yapıyoruz!</h2>
					<p>Tek yapmanız gereken size uygun zamanı belirlemek ve online randevunuzu almak. iştemanikür işinize gelir!</p>
					<a class="da-link" href="/randevu.aspx">Randevu Al</a>
					<div class="da-img"><img src="Resources/slider/images/5.png" alt="image01" style="margin-left: -150px;" height="225"/></div> 
				</div>
				
				<div class="da-slide">
					<h2><br />
    	  <br />
    	  Uzat elini<br />
Schwarzkopf Professional<br />
    	  <font color="#e30000"><b>biz geliyoruz!</b></font></h2>
					<p></p>
					<a class="da-link" href="/randevu.aspx">Randevu Al</a>
					<div class="da-img"><img src="Resources/slider/images/1.png" alt="image01" style="margin-left: -150px;" height="225"/></div> 
				</div>
				<div class="da-slide">
					<h2><br />
    	  <br />
    	  Elinizi<br />
    	  <font color="#e30000"><b>Koruncuk Vakfı</b></font> için uzatın!!</h2>
					<p style="margin-top:-10px;">Bize uzatılan her eli<br />
Türkiye Korunmaya Muhtaç<br />
Çocuklar Vakfı ile buluşturuyor,<br />
adınıza 1TL'lik bağış yapıyoruz.</p>
					<a class="da-link" href="/randevu.aspx">Randevu Al</a>
					<div class="da-img"><img src="Resources/slider/images/2.png" alt="image01" style="margin-left: -150px;" height="225"/></div>
				</div>
                
				<div class="da-slide">
					<h2><br />
    	  <br />
    	  <b>işte</b>manikür<br />
    	  <font color="#e30000"><b>çok yakında</b></font><br />Eczacıbaşı’nda!</h2>
					<p></p>
					<a class="da-link" href="/randevu.aspx">Randevu Al</a>
					<div class="da-img"><img src="Resources/slider/images/4.png" alt="image01" style="margin-left: -150px;" height="225"/></div>
				</div>                

				
				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
			</div>

    <div class="shadow"></div>
         <div class="altBizeUlasin">
        <script type="text/javascript"> 
        $(document).ready(function () {
        $(".altBizeUlasinWrapper").find('.altBizeUlasinButton').fancybox({
        'width': 530,
        'height': 550,
        'autoScale': false,
        'transitionIn': 'none',
        'overlayOpacity': 0.5,
        'transitionOut': 'none',
        'centerOnScroll': true,
        'type': 'iframe'

        });


        $(".footerWrapper").find('.footerItem').fancybox({
        'width': 700,
        'height': 450,
        'autoScale': false,
        'transitionIn': 'none',
        'overlayOpacity': 0.5,
        'transitionOut': 'none',
        'centerOnScroll': true,
        'type': 'iframe'

        });


        </script>
		        <div class="altRandevuWrapper">
			        <img class="altRandevuImage" src="Resources/images/altFooterRandevuImage.png">
        	        <div class="altRandevuTitle">
                    RANDEVU AL,  <font color="#e30000"><b>HEMEN GELELİM!</b></font>
            
                    </div>
                    <div class="altRandevuText">
                    Ofisinize geliyoruz, profesyonel şıklıkta manikürünüzü
        <br />
        <font color="#e30000">15 dakikada yapıyoruz!</font>
                    </div>
           
                    <a class="altRandevuButton" href="/randevu.aspx" data-fancybox-type="iframe">
            
                    </a>
            
                </div>
		        <span class="altFooterSep"></span>
    	        <div class="altBizeUlasinWrapper">
			        <img class="altBizeUlasinImage" src="Resources/images/altFooterContactImage.png">
        	        <div class="altBizeUlasinTitle">
                    OFİSİNİZDE   <font color="#e30000"><b>HENÜZ</b></font> YOK MU?
            
                    </div>
                    <div class="altBizeUlasinText">
                    Siz gelmeyin, biz size geliriz. İlgileniyorsanız bizi şirketinize önerin.<br>
        <font color="#e30000"><b>3 ay boyunca manikürünüz bizden.</b> </font>
                    </div>
           
                    <a class="altBizeUlasinButton" href="/henuzyok.aspx" data-fancybox-type="iframe">
            
                    </a>
            
                </div>
		
		
    
            </div>

    <div class="stepsContainer">
    	<div class="stepsTitle">İŞ'TE MANİKÜR İÇİN <font style="font-size:30px">3</font> ADIM</div>
            <script>
	
	$(function () {
	
	$('.stepsLeft').find('#tab1').click(function(){
	 
     $('#stepContent1').show();
	 $('#stepContent2').hide();
	 $('#stepContent3').hide();
	 $("#tab1").addClass('randevuButtonActive');
	 $("#tab1").removeClass('randevuButton');
	 $("#tab2").addClass('odemeButton');
	 $("#tab2").removeClass('odemeButtonActive');
	 $("#tab3").addClass('manikurButton');
	 $("#tab3").removeClass('manikurButtonActive');
   });
   
   $('.stepsLeft').find('#tab2').click(function(){
	 
     $('#stepContent1').hide();
	 $('#stepContent2').show();
	 $('#stepContent3').hide();
	 $("#tab1").removeClass('randevuButtonActive');
	 $("#tab1").addClass('randevuButton');
	 $("#tab2").removeClass('odemeButton');
	 $("#tab2").addClass('odemeButtonActive');
	 $("#tab3").addClass('manikurButton');
	 $("#tab3").removeClass('manikurButtonActive');
   });
   
   $('.stepsLeft').find('#tab3').click(function(){
	 
     $('#stepContent1').hide();
	 $('#stepContent2').hide();
	 $('#stepContent3').show();
	 $("#tab1").removeClass('randevuButtonActive');
	 $("#tab1").addClass('randevuButton');
	 $("#tab2").addClass('odemeButton');
	 $("#tab2").removeClass('odemeButtonActive');
	 $("#tab3").removeClass('manikurButton');
	 $("#tab3").addClass('manikurButtonActive');
   });
   
    }); 
	</script>
        <div class="stepsLeft">
        	<a class="randevuButtonActive" id="tab1"></a>
            <a class="odemeButton" id="tab2"></a>
            <a class="manikurButton" id="tab3"></a>
        </div>
        
        <div class="stepsRight">
        	<div class="stepsContent" id="stepContent1">
            <span>Kayıt sayfamızdan istemanikur.com ofisinizde servis veriyor mu kontrol edin.
Ofisinizde olduğumuz gün ve saatleri inceleyin. Size en uygun gün ve saati seçin. Siz bize gelmeyin, biz size gelelim!</span>
			<img class="stepsContainerImage" src="Resources/images/adim1.jpg" width="200" />
            </div>
            <div class="stepsContent" id="stepContent2" style="display:none;">
            <span>Seçtiğiniz servise göre ödemenizi dilediğiniz  kredi kartınızla güvenli  ve kolay bir şekilde gerçekleştirebilirsiniz. Aldığınız servisin 1 TL’si ile  Koruncuk Vakfı’na bağışta bulunuyorsunuz! Unutmayın iştemanikür ile  kazandığınız zamana değer biçilemez.</span>
            <img class="stepsContainerImage" src="Resources/images/adim2.jpg" width="200" />
            </div>
            <div class="stepsContent" id="stepContent3" style="display:none;">
            <span>Servisinizi vermeye hazırız. Zengin oje skalamızdan istediğiniz rengi belirleyin. Servis sırasında iPad’lerimiziden  derginizi okuyun, maillerinizi kontrol edin, çayınızı için. İşinizin başına döndüğünüzde listenizdeki bir işi halletmiş olmanın rahatlığı sizi bekliyor. Bu manikür işinize gelir!</span>
            <img class="stepsContainerImage" src="Resources/images/adim3.jpg" width="200" />
            </div>
        </div>
    
    </div>
  <%--  <div class="shadow"></div>
    <div class="altBizeUlasin">
    	<div class="altBizeUlasinWrapper">
        	<div class="altBizeUlasinTitle">
            OFİSİNİZDE <font color="#e30000"><b>HENÜZ</b></font> YOK MU?
            
            </div>
            <div class="altBizeUlasinText">
            Profesyonel şıklıkta manikürlü elleriniz olması için ofisinize gelelim.<br />
İlgileniyorsanız bizi şirketinize önerin,<br />
<font color="#e30000"><b>size 6 ay boyunca manikür servisinizi hediye edelim!</b></font>
            </div>
            <a class="altBizeUlasinButton">
            
            </a>
            
        </div>
    
    </div>--%>



</asp:Content>









