// JavaScript Document

function trace(msg) {
	if(console)
		console.log(msg);
	else alert(msg);
}

function removePx(str) {
	str = str + "";
	str = str.toLowerCase();
	while(str.indexOf("px") > 0)
		str = str.replace("px", "");
	str = parseInt(str);
	return str;
}
	
$(document).ready(function() {	
	adjustBottomCloud();
	bindPlaceHolders();
});

function bindPlaceHolders(input) {

	if(input) {
		bindSinglePlaceHolder(input);	
	} else {
		$("input[type=text], input[type=password]").each(function(index, element) {
			bindSinglePlaceHolder(element);
		});
	}
	
	function bindSinglePlaceHolder(element) {
		var placeHolder = $(element).attr('plc');
		var originalColor = $(element).css('color');
		var plcColor = $(element).attr('plcColor');
		
		var inputType = $(element).attr('type');
		var isPasswordInput = (inputType.toLowerCase() == 'password');
		trace(isPasswordInput);
		
		if(typeof plcColor == 'undefined')
			plcColor = originalColor;

		if(typeof placeHolder != 'undefined') {
			
			if($(element).val() == '') {
				$(element).val(placeHolder);
				$(element).css('color', plcColor);
				$(element).prop('type', 'text');
			}
			
			$(element).focus(function(){
				if($(element).val() == placeHolder) {
					$(element).val('');
					$(element).css('color', originalColor);
					if(isPasswordInput)
						$(element).prop('type', 'password');
				}
			});
			
			$(element).blur(function(){
				if($(element).val() == '') {
					$(element).val(placeHolder);
					$(element).css('color', plcColor);
					$(element).prop('type', 'text');
				}
			});
		}
	}
}

function adjustBottomCloud() {
	if($('.bottomCloud').size() <= 0)
		return;
	if($('.middleContainer').size() <= 0)
		return;
	
	var position = removePx($('.middleContainer').offset().top) + removePx($('.middleContainer').height());
	position -= 163;
	
	$('.bottomCloud').css('top', position);
}

function verticalAlign(element, type) {
	var parentHeight = removePx($(element).parent().height());
	var elementHeight = removePx($(element).height());
//var elementHeight = removePx($(element)[0].scrollHeight);
/*
	if($(element).hasClass('aCheckBoxLabel')) {
		trace(element);
		trace("parentHeight: " + parentHeight + ", elementHeight: " + elementHeight);
	}
*/	
	if(type == 'top')
		var marginTop = 0;
	else if(type == 'bottom') 
		var marginTop = parentHeight - elementHeight;
	else var marginTop = (parentHeight - elementHeight) / 2;

	$(element).css('margin-top', marginTop);
}

function disableEventPropagation() {
	
	if (event.stopPropagation)
		event.stopPropagation();
	else if (window.event)
		window.event.cancelBubble = true;

}

function getHash(indexIn) {
	var hash = window.location.hash;
	if(hash.indexOf('#') == 0)
		hash = hash.substring(1);
		
	var arr = hash.split('?');
	for(var i = 0; i < arr.length; i++) {
		var arr2 = arr[i].split('=');
		var index = arr2[0];
		var value = arr2[1];
		if(index == indexIn)
			return replaceHashCharacters(value, true).toString();
	}
	return false;
}

function setHash(indexIn, valueIn) {
	indexIn = indexIn.toString();
	valueIn = valueIn.toString();
	indexIn = replaceHashCharacters(indexIn, false);
	
	var hash = window.location.hash;
	hash = hash.toString();

	if(hash.indexOf('#') == 0)
		hash = hash.substring(1);
	
	var arr = hash.split('?');

	var indexFound = false;
	for(var i = 0; i < arr.length; i++) {
		arr[i] = arr[i].split('=');
		if(arr[i][0] == indexIn) {
			indexFound = true;
			arr[i][1] = valueIn;
		}
	}
	if(!indexFound)
		arr.push(new Array(indexIn, valueIn));
	var arrFinal = new Array();
	for(var i = 0; i < arr.length; i++) {
		if(arr[i][0] != null && arr[i][1] != null && arr[i][0] != '' && arr[i][1] != '')
			arrFinal.push(replaceHashCharacters(arr[i][0], false) + '=' + replaceHashCharacters(arr[i][1], false));
	}
	var newHash = arrFinal.join('?');
	window.location.hash = newHash;
}

function replaceHashCharacters(str, revers) {
	if(!str)
		return '';
	if(revers) {
		str = str.split('[_Q_]');
		str = str.join('?');
		str = str.split('[_H_]');
		str = str.join('#');
		str = str.split('[_E_]');
		str = str.join('=');
		return str;
	}
	str = str.split('?');
	str = str.join('[_Q_]');
	str = str.split('#');
	str = str.join('[_H_]');
	str = str.split('=');
	str = str.join('[_E_]');
	return str;
}