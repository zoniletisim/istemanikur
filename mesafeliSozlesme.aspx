﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mesafeliSozlesme.aspx.cs" Inherits="mesafeliSozlesme" %>
<%@ Import Namespace="ZonDBModel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Sozlesme</title>
    <link href="/Resources/css/sozlesme.css" rel="stylesheet" type="text/css" />
</head>

<body style="width:850px;line-height:18px;">
    <table class="sozlesme2">
	<tbody>
		<tr>
			<td>
				<h2>
                <br />
					SATIŞ SÖZLEŞMESİ</h2>
			<br />
                <p>
                 İşbu sözleşme 13.06.2003 tarih ve 25137 sayılı Resmi Gazetede yayınlanan Mesafeli Sözleşmeler Uygulama Usul ve Esasları Hakkında Yönetmelik gereği internet üzerinden gerçekleştiren satışlar için sözleşme yapılması zorunluluğuna istinaden düzenlenmiş olup, maddeler halinde aşağıdaki gibidir. 
                </p>
                <br />
                <h4>
                MADDE 1 - KONU
                </h4>
                <p>
                İşbu sözleşmenin konusu, SATICI'nın, ALICI'ya satışını yaptığı, bakım hizmetinin satışı ve verilmesi ile ilgili olarak 4077 sayılı Tüketicilerin Korunması Hakkındaki Kanun-Mesafeli Sözleşmeleri Uygulama Esas ve Usulleri Hakkında Yönetmelik hükümleri gereğince tarafların hak ve yükümlülüklerini kapsamaktadır.

                </p>
				<br />
				<h4>
					MADDE 2.1 - SATICI BİLGİLERİ</h4>
				<table>
					<tbody>
						<tr>
							<td width="15%">
								Ünvanı</td>
							<td>
								: Müge Meydan İştemanikür (Bundan sonra İŞTEMANİKÜR olarak anılacaktır)</td>
						</tr>
						<tr>
							<td>
								Adresi</td>
							<td>
								: Bayar Cad. Oyak Sitesi, A Grubu Sevgi Apt. K:5 D:10 Kozyatağı / İSTANBUL</td>
						</tr>
						<tr>
							<td>
								Telefon</td>
							<td>
								: 0 532 774 77 04   </td>
						</tr>
						
						<tr>
							<td>
								Web Adresi</td>
							<td>
								: www.istemanikur.com</td>
						</tr>
						<tr>
							<td>
								E-mail</td>
							<td>
								: info@istemanikur.com</td>
						</tr>
					</tbody>
				</table>
                <br />
                <h4>
					MADDE 2.2 - ALICI BİLGİLERİ</h4>
                    <p>
                    Alıcı olarak www.istemanikur.com online hizmet randevu satış sitesine üye olan kişi. Üye olurken kullanılan adres ve iletişim bilgileri esas alınır. 
                    </p>

				<table>
					<tbody>
						<tr>
							<td width="20%">
								Adı/Soyadı/Ünvanı</td>
							<td>
								: <%=u.Ad %> <%=u.Soyad %> </td>
						</tr>
						<tr>
							<td>
								Adresi</td>
							<td>
								: <%=ofis.Adres %>
                               
                                 </td>
						</tr>
                        <tr>
							<td width="20%">
								Telefon</td>
							<td>
								: <%=u.Telefon %> </td>
						</tr>
                        <tr>
							<td width="20%">
								E-mail</td>
							<td>
								: <%=u.Email%> </td>
						</tr>
					</tbody>
				</table>
				<br />
				<h3>
					MADDE 3 - SÖZLEŞME KONUSU ÜRÜN,  HİZMET BİLGİLERİ</h3>
				<p>
					İşbu sözleşme konusu hizmet, güzellik bakım hizmeti olup,  hizmetin; satış bedeli, ödeme şekli, veriliş yeri, tarihi, randevu saati siparişin sonlandığı andaki bilgilerden oluşmaktadır ve alıcının elektronik posta adresine de gönderilmektedir.</p>
				

<table class="sepet-tbl" border="0">
        <tr style="padding: 0; margin: 0">
            <td class="return-description">Servis</td>
            <td class="return-price">Randevu Günü</td>
            <td class="return-total">Randevu Saati</td>
            <td class="return-total">Firma</td>
            <td class="return-total">Ofis</td>
            <td class="return-total">Fiyat</td>
        </tr>
          <%
              decimal indirimliFiyat = 0;
              foreach (var s in o.sepetListe)
            { %>
                                            
    <tr>
        <td><%=s.HizmetAd %></td>
        <td><%=string.Format("{0:d MMMM yyyy dddd}", s.Tarih)%></td>
        <td><%=string.Format("{0:t}", s.BaslangicSaat)%> - <%=string.Format("{0:t}", s.BitisSaat)%></td>
        <td><%=s.SiretAd %></td>
        <td><%=s.OfisAd %></td>
        <td>
        <%if (s.MiktarYuzde != null)
                      {
                          indirimliFiyat = ((decimal)s.Fiyat *(100-Convert.ToDecimal(s.MiktarYuzde))) / 100;
                           %>
                            <span style="color:#A8A6A6;text-decoration:line-through"> <%=s.Fiyat%> TL</span>
                            <%=Math.Ceiling(indirimliFiyat).ToString("#0,0.00")%> TL
                           <%}else{ %>
                    <%=s.Fiyat%> TL
                    <%} %>
        </td>   
    </tr>   
  <%}%>
                
                
                       
            
    <tr>
            <td class="right-ta summary" style="height: 10px;" colspan="5">
                <strong>
                   Hizmet Toplamı
                </strong>
            </td>
            <th nowrap="nowrap" >
            <%=string.Format("{0:N}",o.urunToplam) %> TL
            </th>
    </tr>
    
        
   
         
    <tr>
            <td class="right-ta summary" colspan="5">
                <strong>
                    Genel Toplam
                </strong>
            </td>
            <th class="summary-total" nowrap="nowrap">
            <%=string.Format("{0:N}", o.genelToplam)%> TL
            </th>
    </tr>
   
</table>

				<br />
				<table width="60%">
					<tbody>
						<tr>
							<td>
								<strong>Adres Bilgileri</strong></td>
							<td>
								&nbsp;</td>
						</tr>
						<tr>
                            <td>Hizmet Adresi</td>
							<td>Fatura Adresi</td>
						</tr>
						<tr>
							<td><%=ofis.Adres%></td>
							<td><%=ofis.Adres%></td>
						</tr>
					</tbody>
				</table>
				<br /><br />

		<div style="width:750px;float:left;">
        <h4>MADDE 4 - GENEL HÜKÜMLER</h4>
       <p> 4.1 - ALICI, Madde 3'te belirtilen sözleşme konusu randevunun temel nitelikleri, satış fiyatı ve ödeme şekli ile teslimata ilişkin tüm ön bilgileri okuyup bilgi sahibi olduğunu ve elektronik ortamda gerekli teyidi verdiğini beyan eder.</p>

        <p>4.2 - Sözleşme konusu hizmet, ALICI'nın şahsen kendine verilir. Bu nedenle alıcı hizmeti alırken verilen randevu kodunu, kimlik kartını ve bu alışverişte kullanmış olduğu kredi kartını İŞTEMANİKÜR görevli personeline ibraz etmek zorundadır.</p>
<br />
<h4> MADDE 5 - CAYMA HAKKI</h4>

<p>ALICI, sözleşme konusu hizmetin, sipariş aşamasında ve ALICI'ya gönderilmiş olan elektronik postada belirtilen randevu saatinden 6 (altı saat) öncesine kadar cayma hakkına sahiptir. Cayma hakkının kullanılması için bu süre içinde İŞTEMANİKÜR 'e şahsen,  e-mail veya telefon ile bildirimde bulunulması şarttır. Bu hakkın kullanılması halinde, hizmet bedeli ALICI’nın İŞTEMANİKÜR hesabına kredi olarak eklenir veya talebe göre takip eden 2 gün içinde kullanıcının kredi kartına iadesi yapılır.  Randevu saatine 1 (bir) saat kalaya kadar olan iptallerde  hizmet bedeli ALICI’nın İŞTEMANİKÜR hesabına kredi olarak eklenir, hizmet bedeli iadesi yapılmaz. </p>
<p>Hizmet saatine 30 dakika (yarım saat) kalıncaya kadar randevu ertelenebilir, bu durumda ALICI'ya İŞTEMANİKÜR  tarafından hizmetin verileceği diğer tarihler için randevu hakkı verilir, ALICI bu randevuyu 2(iki) ay süre ile aynı hizmet için kullanabilir. Servis saatine 1 (bir) saatten daha az bir süre kalmışsa hizmet cayma hakkı kullanılamaz, yarım saat kalana kadar randevu değişikliği yapılabilir.</p>
<p>Servisin verileceği saat ve yerde bulunulmadığı takdirde randevu geçersiz sayılır. Hizmet bedeli iadesi yapılamaz.</p>
<br />
<h4> MADDE 6 - ÖZEL HÜKÜMLER</h4>

<p>6.1 - Internetten hizmet alırken randevu saat  şeması ekranda gösterilmektedir. Bu şemada randevu için uygun saatler belirtilmektedir. ALICI randevu seçerken alacağı hizmetin cinsini doğru olarak seçmekle yükümlüdür. Aksi durumda hizmetin verilmesi sırasında başka hizmet verilmesini isterse satın aldığı hizmet geçersiz sayılır ve hizmet bedeli iadesi yapılmaz.</p>
<p>6.2 - Alınan Randevu, satın alım işleminde belirtilen Tarih/Saat ve yer için geçerlidir. Belirtilen gün ve saatte kullanılmayan randevu geçersiz sayılır.</p>
<p>İşbu sözleşmenin uygulanmasında, Sanayi ve Ticaret Bakanlığınca ilan edilen değere kadar Tüketici Hakem Heyetleri ile İŞTEMANİKÜR 'in yerleşim yerindeki Tüketici Mahkemeleri yetkilidir. </p>
<br />
<p>Siparişin sonuçlanması durumunda ALICI işbu sözleşmenin tüm koşullarını kabul etmiş sayılacaktır.
</p>
	</div>
 <br />
	        <b>Satıcı</b>: Müge Meydan İştemanikür<br />
            <b>Alıcı:</b> <%=u.Ad %> <%=u.Soyad %><br />
            <b>Tarih:</b> <%=DateTime.Now.ToShortDateString()%>
			</td>
		</tr>
	</tbody>
</table>

	
	
</body>

</html>



