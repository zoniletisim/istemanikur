﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="servislerimiz.aspx.cs" Inherits="servislerimiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="mainContainer">
	<div class="servislerimizBanner">
    	<div class="servislerimizTitle">Servislerimiz</div>
		<!--h1 class="servisbigTitle"><font color="#ed1c24">İŞ’TE</font> MANİKÜRE HAZIR MISINIZ?</h1>
        <a class="readyButton" href="anasayfa.aspx"></a-->
    </div>
    <div class="shadow"></div>
    <div class="servisler">
    	<div class="seperatorServis"></div>
        <div class="servisContainer">
        	<div class="servisImage"><img src="resources/images/manikur.png" width="70" /></div>
            <div class="servisTitle">SMART Manikür</div>
            <div class="servisText">Herkesin  hijyen poşeti kendi önünde açılıyor. Pratik manikürümüzü yapıyoruz. Renk seçeneğimiz 30’dan fazla. Kullandığımız Markalar: Mavala, Essie, Color Club, Incoco. İsterseniz kendi ojenizi de getirebilirsiniz! <br /><a href="/mavalaManikur.aspx" class="malava">Mavala'nın hijyenik ve modern manikür sistemi ile tanıştınız mı?.</a></div>
            <!--div class="servisPrice">34<span style="font-size:15px;">TL</span></div-->
			<!--a href="/randevu.aspx" class="servisSatinAl"> RANDEVU AL</a-->
        </div>
        <div class="seperatorServis"></div>
        <div class="servisContainer">
        	<div class="servisImage"><img src="Resources/images/chanel.png" width="70" /></div>
            <div class="servisTitle">SMART LUX Manikür</div>
            <div class="servisText">Tırnak modasını Chanel' den takip ediyorum diyorsanız lükse bizimle akıllıca ulaşın! <br />Pratik manikürünüzü Mavala ile yapıp, Chanel oje ile ışıltı katalım. <br />Mevcut Renklerimiz: May, Peche Nacre, Peridot, Provocation, Rouge Rubis, Particulière, Pirate, Accessoire, Élixir, Distraction</div>
            <!--div class="servisPrice">39<span style="font-size:15px;">TL</span></div-->
			<!--a href="/randevu.aspx" class="servisSatinAl"> RANDEVU AL</a-->
        </div>
        <div class="seperatorServis"></div>
        <div class="servisContainer">
        	<div class="servisImage"><img src="Resources/images/istemanikur_E.png" width="70" /></div>
            <div class="servisTitle">MASCULINE Manikür</div>
            <div class="servisText">Beyler bu manikür sizin için. Tüm iş insanlarının tırnak bakımını yapıyoruz. Mavala ile manikürünüzü yapıp, temiz ve bakımlı tırnaklarla işinize dönebilirsiniz.
            <p style="font-size: 13px;margin-left: 2px;margin-top: 5px;"></p></div>
            <!--div class="servisPrice">34<span style="font-size:15px;">TL</span></div-->
			<!--<a href="/randevu.aspx" class="servisSatinAl"> RANDEVU AL</a-->
        </div>
       <!--div class="seperatorServis"></div>
    	<div class="servisContainer">
        	<div class="servisImage"><img src="resources/images/makyaj.png" width="70" /></div>
            <div class="servisTitle">Makyaj</div>
            <div class="servisText">
&quot;Evden çok erken çıkıyorum, makyaj yapmaya vaktim yok.&quot; veya &quot;Acil işim   çıktı makyaj yaptırmam gerekli!&quot; diyorsanız biz buradayız. İsterseniz kendi malzemelerinizi de getirebilirsiniz! İşte makyaj da işinize gelir!</div>
            <!--div class="servisPrice">39<span style="font-size:15px;">TL</span></div-->
            <!--a href="/randevu.aspx" class="servisSatinAl"> RANDEVU AL</a-->
        </div-->
        <div class="seperatorServis"></div>
        <div class="servisContainer">
        	<div class="servisImage"><img src="Resources/images/ojekasdudakust.png" width="70" /></div>
            <div class="servisTitle">Kaş, dudaküstü, oje servisi</div>
            <div class="servisText">"Günüm çok yoğun. Hızlıca bakıma ihtiyacım var." diyorsanız:  Ojenizi sürelim, kaş ve dudak üstünüzü cımbız ve iple temizleyelim. İşte hazırsınız!
            <p style="font-size: 13px;margin-left: 2px;margin-top: 5px;">*Chanel hariç</p></div>
            <!--div class="servisPrice">29<span style="font-size:15px;">TL</span></div-->
			<!--a href="/randevu.aspx" class="servisSatinAl"> RANDEVU AL</a-->
        </div>
       <!--div class="seperatorServis"></div>
    	<div class="servisContainer">
        	<div class="servisImage"><img src="resources/images/makyaj.png" width="70" /></div>
            <div class="servisTitle">Makyaj</div>
            <div class="servisText">
&quot;Evden çok erken çıkıyorum, makyaj yapmaya vaktim yok.&quot; veya &quot;Acil işim   çıktı makyaj yaptırmam gerekli!&quot; diyorsanız biz buradayız. İsterseniz kendi malzemelerinizi de getirebilirsiniz! İşte makyaj da işinize gelir!</div>
            <!--div class="servisPrice">39<span style="font-size:15px;">TL</span></div-->
            <!--a href="/randevu.aspx" class="servisSatinAl"> RANDEVU AL</a-->
        </div-->
        <div class="seperatorServis"></div>
    	<div class="servisContainer">
        	<div class="servisImage"><img src="resources/images/kuruoje.jpg" width="70" style="margin-top: 17px;" /></div>
            <div class="servisTitle">Kuru Oje bandı uygulaması*</div>
            <div class="servisText">
Tırnaklara uygulanan desenleri seviyorsanız INCOCO® kuru oje bantlarıyla kariyerinize  tarzınızı yansıtan bir dokunuş yapalım. Bekleme süresi olmadan işinizin başına dönün. İstediğiniz zaman asetonla kolayca çıkarabilirsiniz.
<p style="font-size: 13px;margin-left: 2px;margin-top: 5px;">*Manikür dahil değildir.</p></div>
            <!--div class="servisPrice">39<span style="font-size:15px;">TL</span></div-->
			<!--a href="/randevu.aspx" class="servisSatinAl"> RANDEVU AL</a-->
        </div>
    </div>
</div>
</asp:Content>
























