﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Xml;

public partial class uye_AdresListe : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    public List<Zon_Musteri_Adresleri> adres = new List<Zon_Musteri_Adresleri>();
    ResultAction deleteAdres = new ResultAction();
    protected void Page_Load(object sender, EventArgs e)
    {
        Zon_Kullanici u = null;
        if (Session["user"] != null)
        {
            if (!Page.IsPostBack)
            {
                u = (Zon_Kullanici)Session["user"];
                
                adres = rep.GetAdressList(u.ID);


            }

            if (Request.QueryString["action"] != null)
            {
                if (Request.QueryString["id"] != null)
                    deleteAdres = rep.deleteUserAdres(Convert.ToInt32(u.ID), Convert.ToInt32(Request.QueryString["id"]));

                adres = rep.GetAdressList(u.ID);
            }
        }
        else
            Response.Redirect("../default.aspx");

    }
}