﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using ZonDBModel;

public partial class uye_AdresBilgileri : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    public Zon_Musteri_Adresleri adres = new Zon_Musteri_Adresleri();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.RadioAdresTip.Items[1].Attributes.Add("class", "fatura");
        if (!Page.IsPostBack)
        {
            if (Session["user"] != null)
            {
                ddSehir.DataSource = Constants.GetSehir();
                ddSehir.DataTextField = "Value";
                ddSehir.DataValueField = "Key";
                ddSehir.DataBind();
                if (Request.QueryString["action"] != null)
                {
                    if (Request.QueryString["action"] == "update")
                    {
                        Zon_Kullanici u = null;
                        hiddenAction.Value = "update";
                        btnGuncelle.Text = "Güncelle";
                        if (Session["user"] != null)
                            u = (Zon_Kullanici)Session["user"];

                        adres = rep.GetAdress(u.ID, Request.QueryString["id"]);
                        txtAdresTanim.Text = adres.Tanim;
                        txtAdres.Text = adres.Adres;
                        txtPostaKodu.Text = adres.PostaKodu;
                        txtIlce.Text= adres.Ilce;
                        ddSehir.SelectedValue = adres.Sehir;
                        if (adres.AdresTipi == 1)
                            RadioAdresTip.SelectedValue = "1";
                        else
                            RadioAdresTip.SelectedValue = "2";
                        txtTcKimlik.Text = adres.TCNo;
                        txtVergiDaire.Text = adres.VergiDairesi;
                        txtVergiNo.Text = adres.VergiNo;
                        ddFaturaTip.SelectedValue = adres.FaturaTip;
                        txtFirmaUnvan.Text = adres.FirmaUnvan;
                        txtCepTel.Text = adres.TelCep;
                        txtSabitTel.Text = adres.TelSabit;


                    }
                    else if (Request.QueryString["action"] == "new")
                    {
                        if (Request.QueryString["t"] != null)
                        {
                            if (Request.QueryString["t"] == "ft")
                                RadioAdresTip.SelectedValue = "2";
                        }
                        hiddenAction.Value = "insert";
                        btnGuncelle.Text = "Ekle";
                    }
                }
            }
            else
                Response.Redirect("../default.aspx");
        }
    }

    protected void btnGuncelle_Click(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            Zon_Kullanici u = (Zon_Kullanici)Session["user"];
            var userAdres = rep.UserAdres(u.Email, RadioAdresTip.SelectedValue, txtAdresTanim.Text, txtAdres.Text, txtPostaKodu.Text, ddSehir.SelectedValue, txtIlce.Text, Request.QueryString["id"], hiddenAction.Value, ddFaturaTip.SelectedValue, txtTcKimlik.Text, txtFirmaUnvan.Text, txtVergiDaire.Text, txtVergiNo.Text,txtCepTel.Text,txtSabitTel.Text);
            if (userAdres.result)
            {
                if (Request.QueryString["r"] != null)
                {
                    if (Request.QueryString["r"] == "finish")
                        Response.Redirect("../adresSec.aspx");
                }
                else
                    Response.Redirect("adresListe.aspx", false);
            }
        }
        else
            Response.Redirect("../default.aspx");

    }
   
}