﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitemasterpage.master" AutoEventWireup="true" CodeFile="hediyeCekleri.aspx.cs" Inherits="uye_hediyeCekleri" %>
<%@ Register src="menu.ascx" tagname="menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="content">
  <uc1:menu ID="menu1" runat="server" />
  <div class="usermain-middle">
  <%--  <div class="title">Siparişlerim</div>--%>
        <div class="usermain-kullaniciBilgi" style="margin-top:25px;">
        <div class="adres-content-col" style="margin:0px 0 0 20px">
             <div class="adres-content-col" style="margin:5px 0 0 20px">
           <h3 style="margin-left:0px;font-weight:bold;">Aktif Hediye Çekleriniz</h3>
             <br />

            <div class="siparis-table">
           <table width="700" class="c">
           <tr style="background:#FFF2F2;height:30px;font-size:13px;font-weight:bold;color:#535353">
                <td style="border-right:1px solid #fff;">Hediye Çeki Kodu</td>
                <td style="border-right:1px solid #fff;">Tutarı</td>
                <td style="border-right:1px solid #fff;">Son Kullanma Tarihi</td>
                <td style="border-right:1px solid #fff;">Min Sipariş Tutarı</td>
              
           </tr>
           <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
           <tr style="background:#efefef;height:2px;">
                <td colspan="6"></td>
           </tr>
            <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
          <%var teslimat=cekList.Where(x=>x.Kullanim=="0").ToList();
            if (teslimat.Count > 0)
            {
                foreach (var a in teslimat)
                {%>
           <tr style="background:#F4F4F4;color:#fff;height:30px;font-size:12px;border-bottom:1px solid #fff;color:#333">
                <td style="border-right:1px solid #fff"><%=a.Kod%></td>
                <td style="border-right:1px solid #fff"><%=a.Tutar.Value.ToString("N")%> TL</td>
                <td style="border-right:1px solid #fff"><%=a.BitisTarih.Value.ToShortDateString()%></td>
                <td><%=a.MinTutar.Value.ToString("N")%> TL ve Üzeri Alışverişlerde Geçerlidir.</td>
           </tr>
           <%}
            } %>
            <%else
            { %>
            <tr>
                <td colspan="4"><span style="font-size:12px">Kayıtlı Aktif Hediye Çekiniz Yok.</span></td>
            </tr>
            <%} %>
       </table> 
       </div> 

       <br /><br /><br />
        <h3 style="margin-left:0px"><b>Kullanılmış Hediye Çekleriniz</b></h3>
             <br />
       <div class="siparis-table">
           <table width="700">
           <tr style="background:#FFF2F2;height:30px;font-size:13px;font-weight:bold;color:#535353">
                <td style="border-right:1px solid #fff;">Hediye Çeki Kodu</td>
                <td style="border-right:1px solid #fff;">Tutarı</td>
                <td style="border-right:1px solid #fff;">Son Kullanma Tarihi</td>
                <td style="border-right:1px solid #fff;">Min Sipariş Tutarı</td>
              
           </tr>
           <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
           <tr style="background:#efefef;height:2px;">
                <td colspan="6"></td>
           </tr>
            <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
          <%var teslimat2=cekList.Where(x=>x.Kullanim=="1").ToList();
            if (teslimat2.Count > 0)
            {
                foreach (var a in teslimat2)
                {%>
           <tr style="background:#F4F4F4;color:#fff;height:30px;font-size:12px;border-bottom:1px solid #fff;color:#333">
                <td style="border-right:1px solid #fff"><%=a.Kod%></td>
                <td style="border-right:1px solid #fff"><%=a.Tutar.Value.ToString("N")%> TL</td>
                <td style="border-right:1px solid #fff"><%=a.BitisTarih.Value.ToShortDateString()%></td>
                <td><%=a.MinTutar.Value.ToString("N")%> TL ve Üzeri Alışverişlerde Geçerlidir.</td>
           </tr>
           <%}
            } %>
            <%else
            { %>
            <tr>
                <td colspan="4"><span style="font-size:12px">Kayıtlı Kullanılmış Hediye Çekiniz Yok.</span> </td>
            </tr>
            <%} %>
       </table> 
       </div>
           </div>

        </div>
    </div>
  </div>
  </div>
</asp:Content>



