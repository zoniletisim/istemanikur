﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class uye_SifreDegistir : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {
            
       
    }
    protected void btnGuncelle_Click(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            Zon_Kullanici user = (Zon_Kullanici)Session["user"];
            if (Password1.Text == Password2.Text)
            {
                var u = rep.changePassWord(user.Email, txtEskiSifre.Text, Password1.Text);
                if(u.result)
                    ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('Bilgileriniz Güncellendi.', 'Üyelik Bilgileri', '');</script>");
                    
                else
                    ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('İşlem Yapılırken Hata Oluştu.', 'Üyelik Bilgileri', '');</script>");
            }
        }
        else
            Response.Redirect("../default.aspx");
    }
}