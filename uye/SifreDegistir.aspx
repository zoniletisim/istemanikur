﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="SifreDegistir.aspx.cs" Inherits="uye_SifreDegistir" %>
<%@ Register src="menu.ascx" tagname="menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="content">
  <uc1:menu ID="menu1" runat="server" />
    <div class="usermain-middle">
   <%--  <div class="title">Şifre Değiştir</div>--%>
        <div class="usermain-kullaniciBilgi" style="margin-top:25px;">
         
         <%--<div class="ui-widget" >
			<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0.7em;"> 
				<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.7em;"></span>
				<strong>  Şifreniz değiştirilmiştir.</strong> </p>
			</div>
		</div>
        
       <div class="ui-widget" >
			<div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0.7em;"> 
				<p><span class="ui-icon ui-icon-error" style="float: left; margin-right: 0.7em;"></span>
				<strong> Lütfen bilgilerinizi kontrol ederek tekrar edeyiniz.</strong> </p>
			</div>
		</div>--%>

    <div class="ui-form">
       <div class="adres-content-col" style="margin:5px 0 0 20px">
            <table class="form-table">
           <tr>
                <td>Eski Şifreniz:</td>
                <td> 
                    <asp:TextBox ID="txtEskiSifre" TextMode="Password" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="Eski Şifrenizi Giriniz." SetFocusOnError="true" ViewStateMode="Disabled"  EnableTheming="true" Display="Dynamic" ControlToValidate="txtEskiSifre"></asp:RequiredFieldValidator>
                </td>
           </tr>
           <tr>
                <td>Yeni Şifre</td>
                <td>
                <asp:TextBox ID="Password1" TextMode="Password" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ErrorMessage="Yeni Şifrenizi Giriniz." ControlToValidate="Password1" Display="Dynamic"></asp:RequiredFieldValidator>
              </td>
           </tr>
           <tr>
                <td>Yeni Şifre(Tekrar)</td>
                <td>  
                <asp:TextBox ID="Password2" TextMode="Password" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ErrorMessage="Yeni Şifrenizi Tekrar Giriniz." ControlToValidate="Password2" Display="Dynamic"></asp:RequiredFieldValidator>
              </td>
           </tr>
            <tr class="btns">
                <td colspan="2">
                    <asp:Button ID="btnGuncelle" runat="server" Text="Güncelle" 
                        onclick="btnGuncelle_Click" CssClass="globalButton butonGuncelle" />
                </td>
            </tr>
           </table>
     </div>
       </div>
     </div>
    </div>
    </div>
</asp:Content>



