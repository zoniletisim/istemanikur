﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Collections;
using System.Reflection;
public partial class uye_Siparislerim : System.Web.UI.Page
{

    public SiparisModel siparis = new SiparisModel();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["user"] != null)
            {
                SiparisRepositories rep = new SiparisRepositories();
                Zon_Kullanici u = (Zon_Kullanici)Session["user"];

                siparis = rep.GetUserSipariss(Convert.ToInt32(u.ID));
            }
            else
                Response.Redirect("../default.aspx");
        }

    }
}