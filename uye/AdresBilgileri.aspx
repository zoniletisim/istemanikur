﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="AdresBilgileri.aspx.cs" Inherits="uye_AdresBilgileri" %>
<%@ Register src="menu.ascx" tagname="menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="content">
  <uc1:menu ID="menu" runat="server" />
  <asp:HiddenField ID="hiddenAction" runat="server" />
  <style type="text/css">
    .fatura{float:left;padding-left:20px;}
    </style>
  <div class="usermain-middle">
    <%-- <div class="title">Üyelik Bilgileri</div>--%>
        <div class="usermain-kullaniciBilgi" style="margin-top:0px;">
           <div class="ui-form" style="margin:5px 0 0 20px">
           <div class="adresTip">
               <asp:RadioButtonList ID="RadioAdresTip" RepeatDirection="Horizontal" ClientIDMode="Static" RepeatLayout="Flow" runat="server">
               <asp:ListItem Value="1" Selected>Teslimat Adresi</asp:ListItem>
               <asp:ListItem Value="2">Fatura Adresi</asp:ListItem>
               </asp:RadioButtonList>
            </div>
           <div class="adresList">
            <ul id="adresU">
                <li>
                    <div class="text">Adres Tanımı:</div>
                    <div class="val">
                    <asp:TextBox ID="txtAdresTanim"  runat="server" Width="300" ClientIDMode="Static"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red"
                        ErrorMessage="Adres Tanımı Giriniz." ControlToValidate="txtAdresTanim"></asp:RequiredFieldValidator>
                    </div> 
                </li>
                 <li>
                    <div class="text">Adres:</div>
                    <div class="val" style="height:127px">
                    <asp:TextBox ID="txtAdres" TextMode="MultiLine" Width="300" Height="100" runat="server"  ClientIDMode="Static"></asp:TextBox>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ForeColor="Red"
                        ErrorMessage="Adres  Giriniz." ControlToValidate="txtAdres"></asp:RequiredFieldValidator>
                    </div> 
                </li>
                 <li style="height:48px">
                    <div class="text">Posta Kodu:</div>
                    <div class="val">
                   <asp:TextBox ID="txtPostaKodu" TextMode="SingleLine" runat="server" style="width:100px"  ClientIDMode="Static"></asp:TextBox><br />
                  
                    </div> 
                </li>
                 <li style="height:48px">
                    <div class="text">Şehir:</div>
                    <div  class="val">
                    <div class="combowrap" style="float:left;">
                   <asp:DropDownList ID="ddSehir" CssClass="odemeSelect1" runat="server" ClientIDMode="Static">
                       </asp:DropDownList>
                       </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="Red"
                        ErrorMessage="Şehir Seçiniz." ControlToValidate="ddSehir"></asp:RequiredFieldValidator>
                    </div> 
                </li>
                 <li style="height:48px">
                    <div class="text">İlçe:</div>
                    <div class="val">
                   <asp:TextBox ID="txtIlce" TextMode="SingleLine" runat="server"  ClientIDMode="Static"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="Red"
                        ErrorMessage="İlçe Seçiniz." ControlToValidate="txtIlce"></asp:RequiredFieldValidator>
                    </div> 
                </li>
                    <li style="height:48px">
                    <div class="text">Cep Tel:</div>
                    <div class="val">
                   <asp:TextBox ID="txtCepTel" TextMode="SingleLine" runat="server"  ClientIDMode="Static"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ForeColor="Red"
                        ErrorMessage="Cep Telefonu Giriniz." ControlToValidate="txtCepTel"></asp:RequiredFieldValidator>
                    </div> 
                </li>
                    <li style="height:48px">
                    <div class="text">Sabit Tel:</div>
                    <div class="val">
                   <asp:TextBox ID="txtSabitTel" TextMode="SingleLine" runat="server"  ClientIDMode="Static"></asp:TextBox><br />
                    </div> 
                </li>
                 <li id="ddFaturaTipUl" style="height:48px">
                    <div class="text">Fatura Tipi:</div>
                    <div class="val">
                    <div class="combowrap" style="width:150px">
                   <asp:DropDownList ID="ddFaturaTip" runat="server"  Width="150" ClientIDMode="Static" CssClass="odemeSelect1">
                   <asp:ListItem Value="0">Bireysel Fatura</asp:ListItem>
                   <asp:ListItem Value="1">Kurumsal Fatura</asp:ListItem>
                       </asp:DropDownList>
                       </div>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="Red"
                        ErrorMessage="Fatura Tipi Seçiniz" ControlToValidate="ddFaturaTip"></asp:RequiredFieldValidator>--%>
                    </div> 
                </li>
                <li id="txtTcKimliku" class="hide" style="height:48px">
                    <div class="text">Tc Kimlik No:</div>
                    <div class="val">
                   <asp:TextBox ID="txtTcKimlik" TextMode="SingleLine" runat="server" MaxLength="11"  Width="149" ClientIDMode="Static"></asp:TextBox><br />
                   
                    </div> 
                </li>
                  <li id="txtFirmaUnvanu" class="hide" style="height:48px">
                    <div class="text">Firma Ünvanı:</div>
                    <div class="val">
                   <asp:TextBox ID="txtFirmaUnvan" TextMode="SingleLine" runat="server"  Width="149" ClientIDMode="Static"></asp:TextBox><br />
                   
                    </div> 
                </li>
                <li id="txtVergiDaireu" class="hide" style="height:48px">
                    <div class="text">Vergi Dairesi:</div>
                    <div class="val">
                   <asp:TextBox ID="txtVergiDaire" TextMode="SingleLine" runat="server"  Width="149" ClientIDMode="Static"></asp:TextBox><br />
                   
                    </div> 
                </li>
                <li id="txtVergiNou" class="hide" style="height:48px">
                    <div class="text">Vergi No:</div>
                    <div class="val">
                   <asp:TextBox ID="txtVergiNo" TextMode="SingleLine" runat="server"   Width="149" ClientIDMode="Static"></asp:TextBox><br />
                  
                    </div> 
                </li>
                 <li>
                    <div class="text"></div>
                    <div class="val">
                  <asp:Button ID="btnGuncelle" runat="server" Text="Güncelle" 
                        onclick="btnGuncelle_Click" CssClass="globalButton butonDevam" style="float:left;" />
                    </div> 
                </li>
            </ul>
           </div>
        </div>
          
    </div>
</div>
</div>

    <script src="../Resources/js/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#ddFaturaTipUl").css("display", "none");
        $('#<%=RadioAdresTip.ClientID%> input[type="radio"]').each(function () {
            $(this).click(function () {
                if ((this).value == 2) {
                    $("#txtTcKimliku").css("display", "block");
                    $("#txtFirmaUnvanu").css("display", "none");
                    $("#txtVergiDaireu").css("display", "none");
                    $("#txtVergiNou").css("display", "none");
                    $("#ddFaturaTipUl").css("display", "block");
                }
                else {
                    $("#txtTcKimliku").css("display", "none");
                    $("#txtFirmaUnvanu").css("display", "none");
                    $("#txtVergiDaireu").css("display", "none");
                    $("#txtVergiNou").css("display", "none");
                    $("#ddFaturaTipUl").css("display", "none");
                }
            });
            if ($(this).attr('checked') == 'checked')
                $(this).click();
        });
    });

$(document).ready(function () {
    $("#ddFaturaTip").change(function () {   
        if ($("#ddFaturaTip").val()=="0") {
            $("#txtTcKimliku").css("display", "block");
            $("#txtFirmaUnvanu").css("display", "none");
            $("#txtVergiDaireu").css("display", "none");
            $("#txtVergiNou").css("display", "none");

        }
        else {
            $("#txtTcKimliku").css("display", "none");
            $("#txtFirmaUnvanu").css("display", "block");
            $("#txtVergiDaireu").css("display", "block");
            $("#txtVergiNou").css("display", "block");

           }
    });

    if ($("#ddFaturaTip option:selected"))
        $('#ddFaturaTip option').change();
});

jQuery(function ($) {
    $("#txtCepTel").mask("9(999)999 99 99");
    $("#txtSabitTel").mask("9(999)999 99 99");
});
</script>

</asp:Content>



