﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="AdresListe.aspx.cs" Inherits="uye_AdresListe" %>
<%@ Register src="menu.ascx" tagname="menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="content">
<uc1:menu ID="menu" runat="server" />
<div class="usermain-middle">
<%--<div class="title">Adres Listesi</div>--%>
    <div class="usermain-kullaniciBilgi" style="margin-top:0px;">
        <div class="adres-content-col" style="margin:5px 0 0 20px">
        <a href="adresBilgileri.aspx?action=new" style="float:left;margin-bottom:20px;font-size:14px;" class="globalButton butonYeniAdresEkle">Yeni Adres Ekle</a>
            <div class="adresListesi">
                    <div class="adresTitle">Servis Adresleriniz</div>
                     <%var teslimat=adres.Where(x=>x.AdresTipi==1).ToList(); 
                      if(teslimat.Count>0)
                      {
                          foreach (var a in adres)
                          {
                              if (a.AdresTipi == 1)
                              {%>
                    <div class="adresContent">
                    <table>
                        <tr>
                        <td><div class="adresTanim"><%=a.Tanim%> </div>
                       <div class="adres"><%=a.Adres%> / <%=a.Ilce%> /<%=a.Sehir%> </div></td>
                        <td>
                        <a href="AdresBilgileri.aspx?id=<%=a.ID%>&action=update" class="guncelle " title="Güncelle"></a>
                       </td><td>
                        <a href="AdresListe.aspx?id=<%=a.ID%>&action=delete" class="sil deleteAdres" title="Sil"></a>
                        </td>
                        </tr>
                    </table>
                    </div>
                    <%}
                          }
                      }%>
            
                 <%else
                        { %> 
                        <div class="adresContent">Kayıtlı Teslimat Adresiniz Yok.</div>
                      
                         <% } %>
         
                     <div class="adresListesi" style="margin-top:35px;">     
                    <div class="adresTitle">Fatura Adresleriniz</div>
                    <%var fatura=adres.Where(x=>x.AdresTipi==2).ToList(); 
                      if(fatura.Count>0)
                      {
                          foreach (var a in adres)
                          {
                              if (a.AdresTipi == 2)
                              {%>
                    <div class="adresContent">
                    <table>
                        <tr>
                        <td><div class="adresTanim"><%=a.Tanim%> </div>
                       <div class="adres"><%=a.Adres%> / <%=a.Ilce%> /<%=a.Sehir%> </div></td>
                        <td>
                        <a href="AdresBilgileri.aspx?id=<%=a.ID%>&action=update" class="guncelle" title="Güncelle"></a>
                       </td><td>
                        <a href="AdresListe.aspx?id=<%=a.ID%>&action=delete" class="sil deleteAdres" title="Sil"></a>
                        </td>
                        </tr>
                    </table>
                    </div>
                             <% }
                          }
                      }%>
                      
                       <%else
                        { %> 
                        <div class="adresContent">Kayıtlı Fatura Adresiniz Yok.</div>
                      
                         <% } %>
                    </div>
        </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $('.deleteAdres').click(function () {
        linkHref = $(this).attr('href');
        jConfirm('Adres silinecek, işlemi onaylıyor musunuz?', null, function (result) {
            if (result) {
                window.location = linkHref;
            }
        });
        return false;
    });
 </script>
</asp:Content>




