﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Xml;
public partial class uye_UyelikBilgileri : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    RandevuRepositories repRandevu = new RandevuRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.radioCinsiyet.Items[1].Attributes.Add("class", "Rerkek");



        if (!Page.IsPostBack)
        {
            getUser();
            //fillSirket();
            //fillOfis();
        }
    }

    protected void btnGuncelle_Click(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            Zon_Kullanici u = (Zon_Kullanici)Session["user"];

            var user = rep.UserUpdate(txtad.Text, txtsoyad.Text, txtEposta.Text, txtTel.Text, txtdogumTarihi.Text, radioCinsiyet.SelectedValue, "", "", u.Email);
            if (user.result)
            {
                Session.Add("user", user.data);
                getUser();
                ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('Bilgileriniz Güncellendi.', 'Üyelik Bilgileri', '');</script>");
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('İşlem Yapılırken Hata Oluştu.', 'Üyelik Bilgileri', '');</script>");
        }
        else
            Response.Redirect("../default.aspx");

    }

    public void getUser()
    {
        try
        {
            if (Session["user"] != null)
            {
                Zon_Kullanici user = (Zon_Kullanici)Session["user"];
                var u = rep.GetUser(user.Email);
                txtad.Text = u.Ad;
                txtsoyad.Text = u.Soyad;
                txtEposta.Text = u.Email;
                txtTel.Text = u.Telefon;
                //ddlSirket.SelectedValue = u.SirketId.ToString();
                //ddlOfis.SelectedValue = u.OfisId.ToString();
                if (u.DogumTarihi!=null)
                    txtdogumTarihi.Text = u.DogumTarihi.Value.ToShortDateString();
                if (u.Cinsiyet == "Erkek")
                    radioCinsiyet.SelectedValue = "Erkek";
                else
                    radioCinsiyet.SelectedValue = "Kadın";

               
                //ddSehir.DataSource = Constants.GetSehir();
                //ddSehir.DataTextField = "Value";
                //ddSehir.DataValueField = "Key";
                //ddSehir.DataBind();


                //ddlIlce.DataSource = Constants.GetIlce(u.Il.ToString());
                //ddlIlce.DataTextField = "Value";
                //ddlIlce.DataValueField = "Key";
                //ddlIlce.DataBind();
                
               //ddSehir.SelectedValue = u.Il.ToString();
               //ddSehir.SelectedValue = u.Ilce.ToString();

               
            }
            else
                Response.Redirect("../default.aspx");
        }
        catch (Exception ex)
        {

            throw ex;
        }  
    
    }
    //protected void fillSirket()
    //{
    //    ddlSirket.DataSource = repRandevu.GetSirket();
    //    ddlSirket.DataTextField = "Value";
    //    ddlSirket.DataValueField = "Key";
    //    ddlSirket.DataBind();
    //    ddlSirket.Items.Insert(0, new ListItem("-- Şirket Seçiniz --", "-1"));
    //}
    //protected void fillOfis(int sirketId)
    //{
    //    ddlOfis.DataSource = repRandevu.GetOfis(sirketId);
    //    ddlOfis.DataTextField = "Value";
    //    ddlOfis.DataValueField = "Key";
    //    ddlOfis.DataBind();
    //    ddlOfis.Items.Insert(0, new ListItem("-- Ofis Seçiniz --", "-1"));
    //}
    //protected void fillOfis()
    //{
    //    ddlOfis.DataSource = repRandevu.GetOfis();
    //    ddlOfis.DataTextField = "Value";
    //    ddlOfis.DataValueField = "Key";
    //    ddlOfis.DataBind();
    //    ddlOfis.Items.Insert(0, new ListItem("-- Ofis Seçiniz --", "-1"));
    //}
    //protected void ddlSirket_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    int sirketId = int.Parse(ddlSirket.SelectedValue);
    //    ddlOfis.DataSource = repRandevu.GetOfis(sirketId);
    //    ddlOfis.DataTextField = "Value";
    //    ddlOfis.DataValueField = "Key";
    //    ddlOfis.DataBind();
    //    ddlOfis.Items.Insert(0, new ListItem("-- Ofis Seçiniz --", "-1"));
    //}

}