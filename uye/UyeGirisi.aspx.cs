﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class uye_UyeGirisi : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (txtUser.Text != "" && txtPassword.Text != "")
        {
            var user = rep.CheckUser(txtUser.Text, txtPassword.Text);
            if (user.result == true)
            {
                Session.Add("user", user.data);
                rep.userLastLoginUpdate(txtUser.Text, txtPassword.Text);
                if(Session["adresDevam"]=="0")
                    Response.Redirect("../adresSec.aspx", false);
                else
                    Response.Redirect("../default.aspx", false);
            }
            else
            {
                Panel1.Visible = true;

                lblMessage.Text = user.message;
                txtUser.Focus();
            }
        }
    }
}