﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="Siparislerim.aspx.cs" Inherits="uye_Siparislerim" %>
<%@ Register src="menu.ascx" tagname="menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="content">
  <uc1:menu ID="menu1" runat="server" />
  <div class="usermain-middle">
  <%--  <div class="title">Siparişlerim</div>--%>
        <div class="usermain-kullaniciBilgi" style="margin-top:25px;">
         <div class="adres-content-col" style="margin:0px 0 0 20px">
            <table class="orders generalTable">
                <tr>
                    <th>Randevu No</th>
                    <th>Tarih</th>
                    <th>Tutar</th>
                    <th>Durum</th>
                </tr>
                <%if(siparis!=null)
                  {
                      foreach (var s in siparis.siparisLists.OrderByDescending(z=>z.EklenmeTarihi))
                      { 
                  %>
                <tr>
                    <td><a href="siparisDetay.aspx?sNo=<%=s.ID%>"> <%=s.SiparisNO%></a></td>
                    <td><%=string.Format("{0:d}", s.EklenmeTarihi)%></td>
                    <td><%=string.Format("{0:N}", s.ToplamTutar)%> TL</td>
                    <td><%=s.Aciklama%></td>
                </tr>
                 <%}
                  } %>
            </table>
            </div>
    </div>
  </div>
  </div>
</asp:Content>





