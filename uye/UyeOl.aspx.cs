﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class uye_UyeOl : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            var a = rep.saveUser(Request.Form);
            if (a.result == true)
            {
                Session.Add("user", a.data);
                Response.Redirect("uyeOK.aspx");
            }
        }
    }
    protected void cvEmail_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            var u = rep.emailKontrol(txtEmail.Text);
            if(u.result)
                args.IsValid = true;
            else
                args.IsValid = false;
        }
        catch (Exception ex)
        {
           
        }
    }
}