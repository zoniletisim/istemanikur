﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="UyelikBilgileri.aspx.cs" Inherits="uye_UyelikBilgileri" %>
<%@ Register src="menu.ascx" tagname="menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="content">
<style type="text/css">
    .ui-widget-content {height:170px !important;}
    .Rerkek{float:left;padding-left:20px;}
    </style>
    <uc1:menu ID="menu1" runat="server" />
    <div class="usermain-middle">
        <div class="usermain-kullaniciBilgi" style="margin-top:25px;">
         <div class="ui-form">
           <div class="adres-content-col" style="margin:5px 0 0 20px">
                <table class="form-table">
                <tr>
                    <td>Ad:</td>
                    <td><asp:TextBox ID="txtad" runat="server" ClientIDMode="Static"></asp:TextBox><br />
                       <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="Adınızı Giriniz." ControlToValidate="txtad"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Soyad:</td>
                    <td><asp:TextBox ID="txtsoyad" runat="server" ClientIDMode="Static"></asp:TextBox><br />
                     <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator2" runat="server" 
                        ErrorMessage="Soyadınızı Giriniz." ControlToValidate="txtsoyad"></asp:RequiredFieldValidator>
                   </td>
                </tr>
                <tr>
                    <td>E-posta:</td>
                   <td><asp:TextBox ID="txtEposta" runat="server" ClientIDMode="Static"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" runat="server" 
                        ErrorMessage="E-posta Giriniz." ControlToValidate="txtEposta"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                 <tr>
                    <td>Telefon:</td>
                   <td><asp:TextBox ID="txtTel" runat="server" ClientIDMode="Static"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator8" runat="server" 
                        ErrorMessage="Telefon Giriniz." ControlToValidate="txtTel"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Doğum Tarihi:</td>
                   <td><asp:TextBox ID="txtdogumTarihi" runat="server"  ClientIDMode="Static"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" runat="server" 
                        ErrorMessage="Doğum Tarihinizi Girin" ControlToValidate="txtdogumTarihi"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr style="height:45px;">
                    <td>Cinsiyet:</td>
                   <td> 
                       <asp:RadioButtonList ID="radioCinsiyet" RepeatDirection="Horizontal" style="padding:5px"  runat="server">
                       <asp:ListItem Value="Kadın">Kadın</asp:ListItem>
                       <asp:ListItem Value="Erkek">Erkek</asp:ListItem>
                       </asp:RadioButtonList>
                       <br />
                    </td>
                </tr>
               <%-- <tr>
                    <td>Şehir:</td>
                   <td>
                   <div style="padding-top:4px;width:200px">
                     <div class="combowrap">
                       <asp:DropDownList ID="ddSehir"  CssClass="odemeSelect1" runat="server">
                       </asp:DropDownList>
                       </div>
                       </div>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddSehir"
                                 ErrorMessage="Şehir Seçiniz." ForeColor="red" InitialValue="0"></asp:RequiredFieldValidator>
                     
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ErrorMessage="Şehir Seçiniz." ControlToValidate="ddSehir"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                 <tr>
                    <td>İlçe:</td>
                   <td>
                   <div style="padding-top:4px;width:200px">
                     <div class="combowrap">
                       <asp:DropDownList ID="ddlIlce"  CssClass="odemeSelect1" runat="server">
                       </asp:DropDownList>
                       </div>
                       </div>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlIlce"
                                 ErrorMessage="İlçe Seçiniz." ForeColor="red" InitialValue="0"></asp:RequiredFieldValidator>
                     
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                        ErrorMessage="İlçe Seçiniz." ControlToValidate="ddlIlce"></asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
                 <%-- <tr>
                    <td>Şirket:</td>
                   <td>
                   <div style="padding-top:4px;width:200px">
                     <div class="combowrap">
                       <asp:DropDownList ID="ddlSirket"  CssClass="odemeSelect1" runat="server" AutoPostBack="true">
                       </asp:DropDownList>
                       </div>
                       </div>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlSirket"
                                 ErrorMessage="Şirket Seçiniz." ForeColor="red" InitialValue="0"></asp:RequiredFieldValidator>
                     
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                        ErrorMessage="Şirket Seçiniz." ControlToValidate="ddlSirket"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                  <tr>
                    <td>Ofis:</td>
                   <td>
                   <div style="padding-top:4px;width:200px">
                     <div class="combowrap">
                       <asp:DropDownList ID="ddlOfis"  CssClass="odemeSelect1" runat="server">
                       </asp:DropDownList>
                       </div>
                       </div>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlOfis"
                                 ErrorMessage="Ofis Seçiniz." ForeColor="red" InitialValue="0"></asp:RequiredFieldValidator>
                     
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                        ErrorMessage="Ofis Seçiniz." ControlToValidate="ddlOfis"></asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
               
                <tr class="btns">
 <td colspan="2" class="guncelleSubmit">
                        <asp:Button ID="btnGuncelle" runat="server" Text="" 
                            onclick="btnGuncelle_Click" CssClass="globalButton butonGuncelle" />
                    </td>
                </tr>
             </table>
          
            </div>
        </div>
    </div>
    </div>
    </div>
<script src="../Resources/js/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
<script src="../Resources/js/jquery.ui.datepicker-tr.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $("#txtdogumTarihi").datepicker({
            yearRange: "-70:+0",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd.mm.yy'
        });
    });
        

    jQuery(function ($) {
        $("#txtTel").mask("(999)999 99 99");
        $("#txtdogumTarihi").mask("99.99.9999")
    });
  
</script>

</asp:Content>



