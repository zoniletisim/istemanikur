﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="siparisDetay.aspx.cs" Inherits="uye_siparisDetay" %>
<%@ Register src="menu.ascx" tagname="menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="content">
<%ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities(); %>
<%decimal urunTotal=0;%>
<style>
.siparisOzetTable td{border-bottom:1px solid #efefef;line-height:30px;}
</style>
 <uc1:menu ID="menu1" runat="server" />
  <div class="usermain-middle">
   <%-- <div class="title">Siparişlerim</div>--%>
        <div class="usermain-kullaniciBilgi" style="margin-top:25px;">
         <div class="adres-content-col" style="margin:0px 0 0 20px">
        
<div class="header">
    <div class="status">
        <strong>Randevu Durumu: </strong>
       <span class="inCargo"><%=siparis.seciliSiparis.Zon_Siparis_Durumlari.Aciklama %> </span>
        </div><a class="back" href="Siparislerim.aspx">< Rezervasyon listesine geri dön</a>
</div>
<div class="orderDetailRw1">
    <div class="orderDetailCl2">
          <img src="../Resources/images/siparisOkImage.jpg" />
    </div>
     <div class="orderDetailCl3">
       
        <img src="../Resources/images/ok.png" width="50" /><br />
        <span style="color:#4db849;font-size:20px;" >Teşekkür Ederiz.</span><br />
       <%-- <span style="font-size:13px;text-align:left;" ><b> Sipariş Başarıyla Tamamlandı.</b></span><br />--%>
        <span style="font-size:13px;text-align:left;float:left;padding-top:10px;padding-left:4px" >Rezervasyonunuz ile ilgili tüm sorularınızı <a style="font-weight:bold; text-decoration:underline" href="../iletisim.aspx">İletişim</a> sayfamızdan bize ulaştırabilirsiniz. </span>
     </div>
    <div class="orderDetailCl1">
      <ul>
        <li>
            <b>Rezervasyon No</b>
            <span class="refNo"><%=siparis.seciliSiparis.SiparisNO %></span>
        </li>
        <li>
            <b>Ödeme Şekli</b>
            <span><%=siparis.seciliSiparis.OdemeTuru %> 
            <%if (siparis.seciliSiparis.Taksit == 0)
              {%>
               - Peşin 
              <%} %>
              <%else
              {%>
              - <%=siparis.seciliSiparis.Taksit%> Taksit
              <%} %>
              </span>
        </li>
       <%-- <li>
            <b>Kart Bilgileri</b>
            <span>
            <%
                string temp="";
                string banka = siparis.seciliSiparis.Banka;
              if (banka == "1")
                  temp = "İş Bankası";
              else if (banka == "2")
                  temp += "Yapı Kredi";
              else if (banka == "3")
                  temp += "VakıfBank";
              else if (banka == "4")
                  temp += "Finans Bank";
              else if (banka == "5")
                  temp += "Teb";
              else if (banka == "6")
                  temp += "Hsbc Advantage";
              else if (banka == "7")
                  temp += "Akbank Axess";
              else if (banka == "999")
                  temp += "Diğer";
               %>
            
            </span>
        </li>--%>
        <li>
            <b>Hizmet Tutarı</b>
            <span><%=siparis.seciliSiparis.ToplamTutar.Value.ToString("N") %> TL</span>
        </li>
      </ul>
    </div>
</div>
<div class="orderDetailRw2">
    <div class="titleR2">Adres Bilgileri</div>

    <div class="tAdres">
        <span style="border-bottom:1px solid #efefef;float:left;width:200px;margin-bottom:4px;padding-bottom:4px;"><b>Servis Adresi</b></span><br />
        <span><%=siparis.seciliSiparis.TeslimatAdres%></span><br />
        <span><%=siparis.seciliSiparis.Zon_Kullanici.Telefon %> </span>

    </div>
    <div class="fAdres">
        <span style="border-bottom:1px solid #efefef;float:left;width:200px;margin-bottom:4px;padding-bottom:4px;"><b>Fatura Adresi</b></span><br />
        <span><%=siparis.seciliSiparis.FaturaAdres%></span><br />
        <span><%=siparis.seciliSiparis.Zon_Kullanici.Telefon%></span>

    </div>
</div>


    <%if (siparis.siparisDetay.Count > 0)
      { %>
    
        <div class="col1">
            <div class="items">
            <div class="titleTable">
            <table width="700">
            <tr>
                <th style="width:50px"></th>
                <th style="width:135px">Servis</th>
                <th style="width:100px">Tarih</th>
                <th style="width:100px">Saat</th>
                <th style="width:100px">Firma</th>
                <th style="width:100px">Ofis</th>
                <th style="width:120px">Fiyat</th>
                
            </tr>
            </table>
            </div>
             
           <%
          foreach (var s in siparis.siparisDetay)
          {
              //urunTotal += s.BirimFiyat.Value;
                ZonDBModel.Zon_Randevu i = (from item in db.Zon_Randevu
                                                    where item.Id == s.UrunID
                                                    select item).FirstOrDefault();
                  
                ZonDBModel.Zon_HizmetTur h = (from item in db.Zon_HizmetTur
                                   where item.Id == s.HizmetId
                                   select item).FirstOrDefault();
                ZonDBModel.Zon_Rezerve_Saat rs = (from item in db.Zon_Rezerve_Saat
                                   where item.Id == s.SaatId
                                   select item).FirstOrDefault();
              
                  urunTotal += h.Fiyat.Value;
                    %>
                <div class="item">
                     <div class="model" style="margin-left:40px;width:130px"><%=h.HizmetAd%></div>
                     <div class="model" style="width:105px"><%=string.Format("{0:d MMMM yyyy dddd}", i.Tarih)%></div>
                     <%if(i.SaatSablonId!=null){ %>
                     <div class="model" style="margin-left:20px;width:100px"><%=string.Format("{0:t}", rs.BaslangicSaat.Value.AddMinutes((int)i.SaatSablonId))%> - <%=string.Format("{0:t}", rs.BitisSaat.Value.AddMinutes((int)i.SaatSablonId))%></div>
                     <%}else{ %>
                     <div class="model" style="margin-left:20px;width:100px"><%=string.Format("{0:t}", rs.BaslangicSaat.Value)%> - <%=string.Format("{0:t}", rs.BitisSaat.Value)%></div>
                     <%} %>
                     <div class="model" style="width:110px"><%=i.Zon_Ofis.Zon_Sirket.SiretAd%></div>
                     <div class="model" style="width:80px"><%=i.Zon_Ofis.OfisAd%></div>
                     <div class="model" style="width:50px;text-align:right;">
                     <%if(s.IndirimliFiyat!=null){ %>
                     <span style="color:#A8A6A6;text-decoration:line-through"><%=s.ToplamFiyat%> TL</span>
                    <%=s.IndirimliFiyat%> TL
                    <%}else{ %>
                    <%=s.ToplamFiyat%> TL
                    <%} %>
                     
                     
                     </div>
                   </div>
            <%} %>
        </div>
        <%} %>
        <table class="siparisOzetTable" style="float:right;margin-right:0px;width:250px;line-height:19px;">
        <%if (siparis.hediyeCekTutar != 0)
          { %>
         <tr>
        <td><b style="color:#C63D3D">Hediye Çeki:</b></td>
        <td><b> <%=siparis.hediyeCekTutar.ToString("N")%> TL</b></td>
        </tr>
        <%} %>
        <tr>
        <td><b style="color:#C63D3D">Genel Toplam:</b></td>
        <td><b> <%=siparis.seciliSiparis.ToplamTutar.Value.ToString("N") %> TL</b></td>
        </tr>
        </table>
    </div>
</div>
    </div></div>
</asp:Content>



