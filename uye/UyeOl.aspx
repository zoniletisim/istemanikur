﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="UyeOl.aspx.cs" Inherits="uye_UyeOl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="uyeOlContainer">
        <div class="uyeOlForm">
            <div class="uyeOlFormHeader">
                <div class="uyeOlFormHeaderTitle">
                    Üye Ol</div>
            </div>
            <div class="formContent">
            <table>
            <tr style="height:45px;">
                <td style="width:80px">Ad:</td>
                <td> <div class="uyeOlFormName"><asp:TextBox ID="txtAd"  runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" Text="<img src='../Resouces/img/Exclamation.gif' title='Adınızı Giriniz.' />"  runat="server" ControlToValidate="txtAd" ErrorMessage="Adınızı Giriniz."></asp:RequiredFieldValidator></div>
                    
                </td>
            </tr>
            <tr style="height:45px;">
                <td>Soyad:</td>
                <td> <div class="uyeOlFormName"><asp:TextBox ID="txtSoyad"  runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Text="<img src='../Resouces/img/Exclamation.gif' title='Soyadınızı Giriniz.' />" runat="server" ControlToValidate="txtSoyad" ErrorMessage="Soyadınızı Giriniz."></asp:RequiredFieldValidator>
               </div> </td>
            </tr>
              <tr style="height:45px;">
                <td>Email:</td>
                <td> <div class="uyeOlFormName"><asp:TextBox ID="txtEmail"  runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Text="<img src='../Resouces/img/Exclamation.gif' title='Email adresinizi Giriniz.' />" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email Giriniz."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server"  ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Text="<img src='../Resouces/img/Exclamation.gif' title='Geçerli email adresi giriniz.' />" ControlToValidate="txtEmail" ErrorMessage="Geçerli email adresi giriniz."></asp:RegularExpressionValidator>
              <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic"
                      onservervalidate="cvEmail_ServerValidate"  Text="<img src='../Resouces/img/Exclamation.gif' title='Bu mail adresi kullanılıyor.' />" ErrorMessage="Bu mail adresi kullanılıyor."
                         ></asp:CustomValidator>
                 </div></td>
            </tr>
            <tr style="height:45px;">
                <td>Şifre:</td>
                <td> <div class="uyeOlFormName"><asp:TextBox ID="txtPassword" TextMode="Password"  runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Text="<img src='../Resouces/img/Exclamation.gif' title='Şifresinizi Giriniz.' />" runat="server" ControlToValidate="txtPassword" ErrorMessage="Şifrenizi Giriniz."></asp:RequiredFieldValidator>
                </div></td>
            </tr>
            <tr style="height:45px;">
                <td>Şifre(Tekrar):</td>
                <td> <div class="uyeOlFormName"><asp:TextBox ID="txtPassword2" TextMode="Password" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Text="<img src='../Resouces/img/Exclamation.gif' title='Şifresinizi tekrar Giriniz.' />" runat="server" ControlToValidate="txtPassword2" ErrorMessage="Şifreni Tekrar Giriniz."></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvSifretekrar" runat="server"
                ControlToCompare="txtPassword" ControlToValidate="txtPassword2"
                ErrorMessage="Şifreleriniz Uyuşmuyor" Text="<img src='../Resouces/img/Exclamation.gif' title='Girilen şifreler uyuşmuyor.' />"></asp:CompareValidator>
                </div></td>
            </tr>
            </table>
            <div class="uyeOlFormSubmit">
                    <asp:Button ID="Button1" runat="server"  Text="Üye Ol" CssClass="globalButton butonUyeOl" /></div>
              
            </div>
        </div>
        <div class="validate">
        <asp:ValidationSummary ID="ValidationSummary1" CssClass="validationsummary" runat="server" />
        </div>
    </div>

    
</asp:Content>



