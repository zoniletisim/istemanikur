﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="menu.ascx.cs" Inherits="uye_menu" %>
    <script src="../Resources/js/jquery.combobox.js" type="text/javascript"></script>
<div class="root"></div>
<div class="sayfaSol">
    <div class="title">
        Hesabım</div>
    <div class="sayfaMenu" id="menu">
        <ul>
            <li><a href="UyelikBilgileri.aspx">Üyelik Bilgileri</a></li>
            <li><a href="SifreDegistir.aspx">Şifre Değiştir</a></li>
            <li><a href="Siparislerim.aspx">Siparişlerim</a></li>
            <li><a href="AdresListe.aspx">Adres Listesi</a></li>
            <li><a href="hediyeCekleri.aspx">Hediye Çeklerim</a></li>
            <%--<li><a href="arkadasinaOner.aspx">Arkadaşına Öner</a></li>--%>
            <%--<li><a href="../sepet.aspx">Sepetim</a></li>--%>
            <li><a href="Cikis.aspx">Çıkış</a></li>
        </ul>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        var url = window.location.pathname,
        urlRegExp = new RegExp(url.replace(/\/$/, '') + "$"); 
        $('.sayfaMenu ul li a').each(function () {
            if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
                $(this).addClass('active');
            }
        });
    });
</script>