﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="UyeGirisi.aspx.cs" Inherits="uye_UyeGirisi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="uyeGirisContainer">
        <div class="uyeGirisForm">
        <div class="uyeGirisFormHeader">
        <div class="uyeGirisFormHeaderTitle">Giriş Yap</div>
        </div>
        <div class="formContent" style="padding-left:41px;">
        <table>
              <tr style="height:45px;">
                <td>Email:</td>
                <td><div class="uyeGirisFormUser"><asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Text="<img src='../Resouces/img/Exclamation.gif' title='Email Adresinizi Giriniz.' />" runat="server" ControlToValidate="txtUser" ErrorMessage="Email Adresinizi Giriniz."></asp:RequiredFieldValidator></div></td>
            </tr>
               <tr style="height:45px;">
                <td>Şifre:</td>
                <td> <div class="uyeGirisFormPass"><asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Text="<img src='../Resouces/img/Exclamation.gif' title='Şifresnizi Giriniz.' />" runat="server" ControlToValidate="txtPassword" ErrorMessage="Şifre Giriniz."></asp:RequiredFieldValidator></div></td>
            </tr>
        </table>
   
     <div class="uyeGirisFormSubmit"><asp:Button ID="Button1" runat="server" Text="Giriş" CssClass="globalButton butonGiris" onclick="Button1_Click" /></div>
     </div>
       
     </div>
        <asp:Panel ID="Panel1" Visible="false" runat="server">
            <div class="validateUyeGirisi">
                <asp:Label ID="lblMessage" CssClass="validationsummary" runat="server" Text=""></asp:Label>
            </div>  
        </asp:Panel>
     </div>
  
</asp:Content>


