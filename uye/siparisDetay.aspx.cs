﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class uye_siparisDetay : System.Web.UI.Page
{
    public SiparisModel siparis = new SiparisModel();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            SiparisRepositories rep = new SiparisRepositories();

            if (Request.QueryString["sNo"] != null)
            {
                Zon_Kullanici u = (Zon_Kullanici)Session["user"];
                if (u != null)
                    siparis = rep.getUserSiparis(Convert.ToInt32(u.ID), Convert.ToInt32(Request.QueryString["sNo"]));
            }
        }
        else
            Response.Redirect("../default.aspx");

    }
}