﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class sifreHatirlat : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    ZonDBEntities db = new ZonDBEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["rid"] != null)
        {
           
            string aktivasyon = Request.QueryString["rid"];
            var mail = db.Zon_Kullanici.Where(z => z.emailAktivasyon == aktivasyon).FirstOrDefault();

            if (mail != null)
            {
                pnlEmail.Visible = false;
                pnlsifre.Visible = true;
            }

            else
            {
                Panel2.Visible = true;
                lblMessage.Text = "Hata Oluştu.";
            }
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["rid"] == null)
        {
            pnlsifre.Visible = false;
            pnlEmail.Visible = true;
            var user = rep.rePassword(txtUser.Text);
            if (user.result == true)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('" + user.message + "', 'Şifremi Unuttum',"+ "function (result) {if (result) {window.location.href = 'anasayfa.aspx'}});</script>");
            }
            else
            {
                Panel1.Visible = true;
                ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('" + user.message + "', 'Şifremi Unuttum', '');</script>");
                txtUser.Focus();
            }
        }

        

    }
    protected void btnSifreKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtPassword.Text != "" && txtPassword2.Text != "")
            {
                if (txtPassword.Text == txtPassword2.Text)
                {
                    string aktivasyon = Request.QueryString["rid"];
                    var mail = db.Zon_Kullanici.Where(z => z.emailAktivasyon == aktivasyon).FirstOrDefault();
                    var user = rep.changePassWord(mail.Email, mail.Password, txtPassword.Text);
                    if (user.result)
                    {
                        ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('Şifreniz başarıyla değiştirilmiştir.', 'Şifremi Unuttum'," + "function (result) {if (result) {window.location.href = 'uyelik.aspx'}});</script>");
                    }
                    else
                        ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('" + user.message + "', 'Şifremi Unuttum', '');</script>");
                }

                else
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> jAlert('Girmiş olduğunuz şifreler aynı değil', 'Şifremi Unuttum', '');</script>");
                    
                }
            }

            
        }
        catch (Exception ex)
        {

        }

    }
}