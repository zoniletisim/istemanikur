﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;

/// <summary>
/// Summary description for SepetModel
/// </summary>
public class SepetModel
{
    public List<View_SepetListe> sepetListe { get; set; }
    //public List<View_KendiTasiniYaratSepet> KendiTasiniYaratSepet { get; set; }
    public decimal urunToplam;
    public decimal kargoFiyati;
    public decimal kargoHesaplananFiyati;
    public decimal genelToplam;
    public decimal genelToplamTaksitli;
    public string teslimatSure;
    public decimal indirimliFiyat;
    public decimal hediyeCekTutar;

}