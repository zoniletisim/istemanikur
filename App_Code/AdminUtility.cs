﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// AdminUtility
/// admin de kullanılan bazı kullanışlı fonksiyonlar
/// </summary>
public class AdminUtility
{
	public AdminUtility()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// Clear form elements in a panel
    /// </summary>
    /// <param name="panel">Panel</param>
    public static void ClearPanelForm(Panel panel) {
        foreach (var item in panel.Controls)
        {
            if (item.GetType().Name == "TextBox") {
                (item as TextBox).Text = "";
            }
            //else if (item.GetType().Name == "DropDownList") {
            //    (item as DropDownList).SelectedIndex = 0;
            //}
            else if (item.GetType().Name == "CheckBox") {
                (item as CheckBox).Checked = false;
            }
            else if (item.GetType().Name == "HiddenField") {
                (item as HiddenField).Value = "";
            }
            else if (item.GetType().Name == "CKEditorControl")
            {
                (item as CKEditor.NET.CKEditorControl).Text = "";
            }
            else if (item.GetType().Name == "ListBox") {
                foreach (var _item in (item as ListBox).Items)
                {
                    (_item as ListItem).Selected = false;
                }
            }
        }
    }
    public static string SuccessAdd = "Kayıt işlemi başarıyla gerçekleşti";
    public static string SucessUpdate = "Güncelleme işlemi başarıyla gerçekleşti";
    public static string Fail = "Hata meydana geldi. ";
    public static string SucessDelete = "Kayıt başarıyla silindi";
    /// <summary>
    /// DropDownList'e seçiniz ekler
    /// </summary>
    /// <param name="drp">DropDownList</param>
    /// <remarks></remarks>
    public static void InsertSelectToDropDownList(DropDownList drp) {
        drp.Items.Insert(0, new ListItem("Seçiniz.", "-1"));
    }

    /// <summary>
    /// Bütün notification kutularını kapatır
    /// </summary>
    /// <param name="master">Master page</param>
    public static void closeBox(MasterPage master)
    {
        HtmlGenericControl div = (HtmlGenericControl)master.FindControl("div_attention");
        HtmlGenericControl div2 = (HtmlGenericControl)master.FindControl("div_error");
        HtmlGenericControl div3 = (HtmlGenericControl)master.FindControl("div_inform");
        HtmlGenericControl div4 = (HtmlGenericControl)master.FindControl("div_success");
        div.Attributes.CssStyle.Add("display", "none");
        div2.Attributes.CssStyle.Add("display", "none");
        div3.Attributes.CssStyle.Add("display", "none");
        div4.Attributes.CssStyle.Add("display", "none");
    }

    /// <summary>
    /// Attention kutusunu açar
    /// </summary>
    /// <param name="master">masterpage</param>
    /// <param name="message">mesaj</param>
    public static void setAttentionBox(MasterPage master, string message)
    {
        HtmlGenericControl div = (HtmlGenericControl)master.FindControl("div_attention");
        div.Attributes.CssStyle.Add("display", "");
        HtmlGenericControl lit = (HtmlGenericControl)master.FindControl("attention");
        lit.InnerText = message;
    }
    /// <summary>
    /// Error kutusunu açar
    /// </summary>
    /// <param name="master">masterpage</param>
    /// <param name="message">mesaj</param>
    public static void setErrorBox(MasterPage master, string message)
    {
        HtmlGenericControl div = (HtmlGenericControl)master.FindControl("div_error");
        div.Attributes.CssStyle.Add("display", "");
        HtmlGenericControl lit = (HtmlGenericControl)master.FindControl("error");
        lit.InnerText = message;
    }
    /// <summary>
    /// Inform kutusunu açar
    /// </summary>
    /// <param name="master">masterpage</param>
    /// <param name="message">mesaj</param>
    public static void setInformBox(MasterPage master, string message)
    {
        HtmlGenericControl div = (HtmlGenericControl)master.FindControl("div_inform");
        div.Attributes.CssStyle.Add("display", "");
        HtmlGenericControl lit = (HtmlGenericControl)master.FindControl("inform");
        lit.InnerText = message;
    }
    /// <summary>
    /// Success kutusunu açar
    /// </summary>
    /// <param name="master">masterpage</param>
    /// <param name="message">mesaj</param>
    public static void setSuccessBox(MasterPage master, string message)
    {
        HtmlGenericControl div = (HtmlGenericControl)master.FindControl("div_success");
        div.Attributes.CssStyle.Add("display", "");
        HtmlGenericControl lit = (HtmlGenericControl)master.FindControl("success");
        lit.InnerText = message;
    }

    /// <summary>
    /// Upload dosya
    /// </summary>
    /// <param name="upload">Upload Controlu</param>
    /// <param name="path">Yolu</param>
    /// <returns>ResultAction</returns>
    public static ResultAction UploadFile(FileUpload upload,string path)
    {
        ResultAction _result = new ResultAction();
        Boolean fileOK = false;
        String _path = HttpContext.Current.Server.MapPath(path);

        if (upload.HasFile)
        {
            if (!File.Exists(_path + upload.FileName))
            {
                String fileExtension =
                    System.IO.Path.GetExtension(upload.FileName).ToLower();

                String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".bmp", ".jpg" };
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = true;
                    }
                }
                if (!fileOK)
                {
                    _result.message = "Dosya türü uygun değil";
                }
            }
            else
            {
                fileOK = false;
                _result.message = "Bu dosya zaten var";
            }
        }
        else {
            _result.message = "Dosya seçmelisin";
        }
        if (fileOK)
        {
            try
            {
                upload.PostedFile.SaveAs(_path
                    + upload.FileName);
                _result.result = true;
                _result.message = path
                    + upload.FileName;
                return _result;
            }
            catch (Exception ex)
            {
                _result.result = false;
                _result.message = ex.Message;

                return _result;
            }
        }
        else
        {
            _result.result = false;
            return _result;
        }
    }
}
