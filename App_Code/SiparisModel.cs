﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
/// <summary>
/// Summary description for SiparisModel
/// </summary>
public class SiparisModel
{
    public List<Zon_Siparis> siparis { get; set; }
    public List<Zon_Siparis_Detay> siparisDetay { get; set; }
    public List<siparisUrun> urun { get; set; }
    //public List<View_Urun_Tas> urunTas { get; set; }
    public List<siparisList> siparisLists { get; set; }
    public Zon_Siparis seciliSiparis { get; set; }
    public Zon_Musteri_Adresleri teslimatAdres { get; set; }
    public Zon_Musteri_Adresleri faturaAdres { get; set; }
    public string siparisDurum{get;set;}
    public string siparisNo { get; set; }
    public string banka { get; set; }
    public string taksit { get; set; }
    public decimal UrunToplam { get; set; }
    public decimal hediyeCekTutar { get; set; }
    public decimal AraToplam { get; set; }
}

public class siparisUrun
{
    public int urunID { get; set; }
    public string Kod { get; set; }
    public string Baslik { get; set; }
    public decimal Fiyat { get; set; }
    public string Thumb2ImageUrl { get; set; }

}

public class siparisList
{
    public int ID { get; set; }
    public string SiparisNO { get; set; }
    public DateTime EklenmeTarihi { get; set; }
    public decimal ToplamTutar { get; set; }
    public string Aciklama { get; set; }
  
}