﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;

/// <summary>
/// Summary description for TanimRepositories
/// </summary>
public class TanimRepositories
{
	ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
    public TanimRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}

    #region Şirket Tanımı

    public ResultAction ActionSirket(string id, string baslik, string mode, string aktif,string tip)
    {
        try
        {
            Zon_Sirket s = null;
            if (mode.Equals("add"))
            {
                s = new Zon_Sirket();
            }
            else
            {
                int cId = Convert.ToInt32(id);
                s = db.Zon_Sirket.Where(z => z.Id == cId).FirstOrDefault();
            }

            s.SiretAd = baslik;
            s.Aktif = aktif;
            s.Tip =int.Parse(tip);
            if (mode == "add")
                db.AddToZon_Sirket(s);

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    public Zon_Sirket GetSirket(int pId)
    {
        return db.Zon_Sirket.Where(z => z.Id == pId).FirstOrDefault();
    }

    public List<Zon_Sirket> GetSirketList()
    {
        return db.Zon_Sirket.Where(z => z.Aktif != "3").OrderBy(x => x.SiretAd).ToList();
    }

    public ResultAction deleteRow(int id)
    {
        try
        {
            var s = db.Zon_Sirket.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
    #endregion

    #region Ofis Tanımı

    public ResultAction ActionOfis(string id,string kategori,string baslik, string mode, string aktif,string ofisAdres)
    {
        try
        {
            Zon_Ofis s = null;
            if (mode.Equals("add"))
            {
                s = new Zon_Ofis();
            }
            else
            {
                int cId = Convert.ToInt32(id);
                s = db.Zon_Ofis.Where(z => z.Id == cId).FirstOrDefault();
            }
            s.SirketId =int.Parse(kategori);
            s.OfisAd = baslik;
            s.Aktif = aktif;
            s.Adres = ofisAdres;
            if (mode == "add")
                db.AddToZon_Ofis(s);

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    public Zon_Ofis GetOfis(int pId)
    {
        return db.Zon_Ofis.Where(z => z.Id == pId).FirstOrDefault();
    }

    public List<Zon_Ofis> GetOfisList()
    {
        return db.Zon_Ofis.Where(z => z.Aktif != "3").OrderBy(x => x.OfisAd).ToList();
    }


    public ResultAction deleteRowOfis(int id)
    {
        try
        {
            var s = db.Zon_Ofis.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
    #endregion

    #region Hizmet Türü Tanımı

    public ResultAction ActionHizmet(string id, string baslik,string fiyat, string mode, string aktif,string kategori,string aciklama)
    {
        try
        {
            Zon_HizmetTur s = null;
            if (mode.Equals("add"))
            {
                s = new Zon_HizmetTur();
            }
            else
            {
                int cId = Convert.ToInt32(id);
                s = db.Zon_HizmetTur.Where(z => z.Id == cId).FirstOrDefault();
            }

            s.HizmetAd = baslik;
            if (!string.IsNullOrEmpty(fiyat))
                s.Fiyat = decimal.Parse(fiyat);

            s.Aktif = aktif;
            s.Aciklama = aciklama;
            s.KategoriId = int.Parse(kategori);
            if (mode == "add")
                db.AddToZon_HizmetTur(s);

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    public Zon_HizmetTur GetHizmet(int pId)
    {
        return db.Zon_HizmetTur.Where(z => z.Id == pId).FirstOrDefault();
    }

    public List<Zon_HizmetTur> GetHizmetList()
    {
        return db.Zon_HizmetTur.Where(z => z.Aktif != "3").OrderBy(x => x.HizmetAd).ToList();
    }


    public ResultAction deleteRowHizmet(int id)
    {
        try
        {
            var s = db.Zon_HizmetTur.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
    #endregion

    #region Rezervasyon Saat Tanımı

    public ResultAction ActionSaat(string id, string basSaat, string BitSaat, string mode, string aktif)
    {
        try
        {
            Zon_Rezerve_Saat s = null;
            if (mode.Equals("add"))
            {
                s = new Zon_Rezerve_Saat();
            }
            else
            {
                int cId = Convert.ToInt32(id);
                s = db.Zon_Rezerve_Saat.Where(z => z.Id == cId).FirstOrDefault();
            }

            string [] tempBasSaat=basSaat.Split(':');
            string [] tempBitSaat=BitSaat.Split(':');

            DateTime baslangic=new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,int.Parse(tempBasSaat[0]),int.Parse(tempBasSaat[1]),0);
            DateTime bitis = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, int.Parse(tempBitSaat[0]), int.Parse(tempBitSaat[1]), 0);

            s.BaslangicSaat = baslangic;
            s.BitisSaat = bitis;

            s.Aktif = aktif;

            if (mode == "add")
                db.AddToZon_Rezerve_Saat(s);

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    public Zon_Rezerve_Saat GetSaat(int pId)
    {
        return db.Zon_Rezerve_Saat.Where(z => z.Id == pId).FirstOrDefault();
    }

    public List<Zon_Rezerve_Saat> GetSaatList()
    {
        return db.Zon_Rezerve_Saat.Where(z => z.Aktif != "3").ToList();
    }


    public ResultAction deleteRowSaat(int id)
    {
        try
        {
            var s = db.Zon_Rezerve_Saat.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
    #endregion

    
}