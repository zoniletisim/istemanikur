﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
using System.Transactions;
/// <summary>
/// Summary description for SayfaRepositories
/// </summary>
public class SayfaRepositories
{
    ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
	public SayfaRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}

    public ResultAction ActionSayfa(string pId,string sayfaid, string kategori, string dil, string baslik, string kisaIcerik, string icerik, string dokuman, string resim,string banner,string bannerUrl, string fotoGaleriId, string url,bool newTab, bool menuclik, bool menuvisible,string tarih, string aktif, int tip, string mode, string path,string kaynak)
    {
        try
        {
            Zon_Sayfa s = null;
            Zon_SayfaIcerik i = null;
            using (TransactionScope trans = new TransactionScope())
            {
                if (mode.Equals("add"))
                {
                    s = new Zon_Sayfa();
                    i = new Zon_SayfaIcerik();
                }
                else if (mode.Equals("addL"))
                {
                    i = new Zon_SayfaIcerik();
                }
                else
                {
                    int cId = Convert.ToInt32(pId);
                    int dilId = Convert.ToInt32(dil);
                    int pSayfaId = Convert.ToInt32(sayfaid);
                    i = db.Zon_SayfaIcerik.Where(x => x.SayfaId == pSayfaId && x.DilId==dilId).FirstOrDefault();
                    s = db.Zon_Sayfa.Where(z => z.Id == i.SayfaId).FirstOrDefault();
                }

                if (mode != "addL")
                {
                    if (tip == 1)
                    {
                        if (kategori == "")
                            s.ParentId = -1;
                        else
                            s.ParentId = int.Parse(kategori);
                    }
                    else
                        i.OzelMenu =int.Parse(kategori);



                    s.clLevel = 0;
                    if (tarih != "")
                        s.EklemeTarih = Convert.ToDateTime(tarih);
                    else
                        s.EklemeTarih = DateTime.Now;
                    s.Sira = 1;
                    s.Aktif = Convert.ToBoolean(1);

                    if (mode == "add")
                        db.AddToZon_Sayfa(s);
                
                i.SayfaId = s.Id;
                }

                if (mode == "addL")
                {
                    if(sayfaid!="")
                    i.SayfaId =int.Parse(sayfaid);
                }
                if (dil != "") 
                    i.DilId = int.Parse(dil);
              
                i.Baslik = baslik;
                i.KisaIcerik = kisaIcerik;
                i.Icerik = icerik;
                i.YeniSekme = newTab;
                i.Kaynakca = kaynak;
                if (fotoGaleriId != "")
                    i.ResimGaleriId = Convert.ToInt32(fotoGaleriId);
                else
                    i.ResimGaleriId = null;

                if (resim != "")
                {
                    string[] resimTh = resim.Split('/');
                    i.Resim = resim;
                    i.ResimTh = "/" + resimTh[1] + "/" + resimTh[2] + "/_thumbs/" + resimTh[3] + "/" + resimTh[4] + "/" + resimTh[5];
                }
                if (banner != "")
                    i.BannerImage = banner;  

                i.BannerImageUrl = bannerUrl;
                i.StaticUrl = url;
                i.MenuClick = menuclik;
                i.MenuVisible = menuvisible;

                i.Tip = tip;
                if(mode=="update")
                    i.GüncellemeTarih = DateTime.Now;

                if(!string.IsNullOrEmpty(tarih))
                    i.EklemeTarih = DateTime.Parse(tarih);
                else
                    i.EklemeTarih = DateTime.Now;

                if (mode == "add" || mode=="addL")
                {
                    
                    db.AddToZon_SayfaIcerik(i);
                }

                i.Aktif = aktif;
                db.SaveChanges();
                trans.Complete();

            }

           
            //if (!mode.Equals("add"))
            //{
            //    int cId = Convert.ToInt32(pId);
            //    i = db.Zon_SayfaIcerik.Where(x => x.Id == cId).Single();
            //    i.Id = cId;
            //}
            //else
            //    i = new Zon_SayfaIcerik();

            ////t.EditorAd = ad;
            ////t.EditorSoyad = soyad;
            ////if (!resim.Equals(""))
            ////{
            ////    t.EditorResim = path + resim;
            ////    t.EditorResimTh = path + resimTh;
            ////}


            //if (mode.Equals("add"))
            //    db.AddToZon_SayfaIcerik(i);

            //db.SaveChanges();

            _result.result = true;
            _result.id = i.Id;

            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public List<View_Sayfalar> getSayfaKategori()
    {
        List<View_Sayfalar> k = (from item in db.View_Sayfalar where item.Tip==(int)Constants.safyaIcerikTip.sayfa select item).ToList();

        return k;

    }

    public Zon_SayfaIcerik GetSayfa(int pId,int tip,int dilid)
    {
        if (dilid == 0)
            dilid = 1;

        return db.Zon_SayfaIcerik.Where(z => z.SayfaId==pId && z.Tip==tip && z.DilId==dilid ).FirstOrDefault();
    }
    
    public string getUstKategori(int pId)
    {
        var u=db.Zon_Sayfa.Where(z => z.Id == pId).FirstOrDefault();
        return u.ParentId.ToString();
    }
    
    //public List<Zon_ResimGaleri_Kategori> getFotoGaleri()
    //{
    //    List<Zon_ResimGaleri_Kategori> k = (from item in db.Zon_ResimGaleri_Kategori where item.Aktif==true  select item ).ToList();
    //    return k;

    //}

    //public ResultAction saveEmailBulten(string email)
    //{
    //    try
    //    {
    //        Zon_EmailBulten k = new Zon_EmailBulten();

    //        k.Email = email;
    //        k.Tarih = DateTime.Now;
    //        db.AddToZon_EmailBulten(k);
    //        db.SaveChanges();

    //        _result.result = true;
    //        return _result;
    //    }
    //    catch (Exception ex)
    //    {
    //        _result.result = false;
    //        _result.message = ex.Message;
    //        logger.Error(ex);
    //        return _result;
    //    }
    //}

    //public ResultAction deleteRowEmailBulten(int id)
    //{
    //    try
    //    {
    //        var s = db.Zon_EmailBulten.Where(z => z.Id == id).FirstOrDefault();
    //        db.DeleteObject(s);
    //        db.SaveChanges();

    //        _result.result = true;
    //        return _result;
    //    }
    //    catch (Exception ex)
    //    {
    //        _result.result = false;
    //        _result.message = ex.Message;
    //        logger.Error(ex);
    //        return _result;
    //    }
    //}
    
    //public ResultAction deleteRowIletisim(int id)
    //{
    //    try
    //    {
    //        var s = db.Zon_Iletisim.Where(z => z.Id == id).FirstOrDefault();
    //        db.DeleteObject(s);
    //        db.SaveChanges();

    //        _result.result = true;
    //        return _result;
    //    }
    //    catch (Exception ex)
    //    {
    //        _result.result = false;
    //        _result.message = ex.Message;
    //        logger.Error(ex);
    //        return _result;
    //    }
    //}

    public ResultAction deleteRowHaber(int id)
    {
        try
        {
            var s = db.Zon_SayfaIcerik.Where(z => z.Id == id).FirstOrDefault();
            db.DeleteObject(s);
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public List<Zon_SayfaIcerik> getSayfaList(int tip,string dilid)
    {
        int dil = int.Parse(dilid);
        return db.Zon_SayfaIcerik.Where(z=>z.Tip==tip && z.Aktif!="3" && z.DilId==dil).OrderBy(z => z.Sira).ToList();
    }

    //public List<Zon_EmailBulten> getEmailBultenList()
    //{
    //    return db.Zon_EmailBulten.OrderByDescending(z => z.Tarih).ToList();
    //}

    public List<Zon_Iletisim> getIletisimList()
    {
        return db.Zon_Iletisim.OrderByDescending(z => z.Tarih).ToList();
    }

    public Zon_SayfaIcerik GetSayfaLanguage(int pId,int dilId, int tip)
    {
        Zon_SayfaIcerik i = null;


        i = db.Zon_SayfaIcerik.Where(z => z.SayfaId == pId && z.Tip == tip && z.DilId == dilId).FirstOrDefault();

        return i;
    }

    public Zon_SayfaIcerik getTrBaslik(int id)
    {
        var b = db.Zon_SayfaIcerik.Where(z => z.SayfaId == id && z.DilId==1).FirstOrDefault();
        return b;
    }

    public Zon_SayfaIcerik GetSayfaFront(int pId, int tip, int dilid)
    {
        Zon_SayfaIcerik sayfa = (from item in db.Zon_SayfaIcerik where item.Id == pId select item).FirstOrDefault();

        return db.Zon_SayfaIcerik.Where(z => z.SayfaId == sayfa.SayfaId && z.Tip == tip && z.DilId == dilid).FirstOrDefault();
    }

    public string pageName(string name,int dilid ) 
    {
        string ret="";
        var r = db.Zon_SayfaIcerik.Where(z => z.StaticUrl == name && z.DilId==dilid).FirstOrDefault();
        ret = r.Baslik;
        return ret;
    }

    public ResultAction deleteRow(int id)
    {
        try
        {
            var s = db.Zon_SayfaIcerik.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }


  
}

