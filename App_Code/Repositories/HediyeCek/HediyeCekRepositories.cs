﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
using System.Collections;
using System.Data.Objects;
/// <summary>
/// Summary description for HediyeCekRepositories
/// </summary>
public class HediyeCekRepositories
{
    ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
	public HediyeCekRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}
    #region GrupTanım
    public Zon_MusteriGrup GetGrup(int pId)
    {
        return db.Zon_MusteriGrup.Where(z => z.Id == pId).FirstOrDefault();
    }
    public List<Zon_MusteriGrup> GetGrupList()
    {
        return db.Zon_MusteriGrup.Where(z=>z.Aktif=="a").ToList();
    }

    public ResultAction deleteRowGrup(int id)
    {
        try
        {
            var s = db.Zon_MusteriGrup.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "d";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public ResultAction ActionGrup(string id, string baslik, string mode, string aktif)
    {
        try
        {
            Zon_MusteriGrup s = null;
            if (mode.Equals("add"))
            {
                s = new Zon_MusteriGrup();
            }
            else
            {
                int cId = Convert.ToInt32(id);
                s = db.Zon_MusteriGrup.Where(z => z.Id == cId).FirstOrDefault();
            }

            s.Ad = baslik;
            s.Aktif = aktif;

            if (mode == "add")
                db.AddToZon_MusteriGrup(s);

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    #endregion

    #region Grup_Müşteri
    public Zon_Musteri_Grup GetGrupMusteri(int pId)
    {
        return db.Zon_Musteri_Grup.Where(z => z.Id == pId).FirstOrDefault();
    }
    public ResultAction deleteRowGrupMusteri(int id)
    {
        try
        {
            var s = db.Zon_Musteri_Grup.Where(z => z.Id == id).FirstOrDefault();
           
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public ResultAction ActionGrupMusteri(string mode, string grupId, string MusteriId)
    {
        try
        {
            Zon_Musteri_Grup s = null;
            List<Zon_Musteri_Grup> g = new List<Zon_Musteri_Grup>();
            string[] array = MusteriId.Split(',');
            
            if(mode=="update")
            {
                int cId = Convert.ToInt32(grupId);
               // int mId = Convert.ToInt32(MusteriId);
                g = db.Zon_Musteri_Grup.Where(z => z.GrupId == cId).ToList();
                if (g.Count>0)
                {
                    db.ExecuteStoreCommand("delete from Zon_Musteri_Grup where GrupId= " + cId );
                    db.SaveChanges();
                }
                
            }

            foreach (var i in array)
            {
                s = new Zon_Musteri_Grup();
                s.GrupId = int.Parse(grupId);
                s.MusteriId = int.Parse(i);
                db.AddToZon_Musteri_Grup(s);
            }

            
           

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    #endregion

    #region HediyeÇekiOluştur

    public string GenerateKod()
    {

        string kod = "";
        kod = DateTime.Now.Day + RandomPassword.Generate(4).ToUpper() + DateTime.Now.Hour + DateTime.Now.Second;
        while (true)
        {
            if (CheckKod(kod))
            {
                break;
            }
            else
            {
                kod = DateTime.Now.Day + RandomPassword.Generate(4).ToUpper() + DateTime.Now.Hour + DateTime.Now.Second;
            }
        }
        return kod;

    }

    public bool CheckKod(string kod)
    {
        try
        {
            var kods = db.Zon_HediyeCek.Where(a => a.Kod == kod).ToList();

            if (kods.Count == 0)
            {
                return true;
            }
            else { return false; }
        }
        catch (Exception)
        {
            throw;
        }
    }

    public ResultAction AddNewHediyeCeki(string not, string baslangic, string bitis, string indirim, string minimum, int userid, string seriNo,int tip,int randevuId,string tutarTip)
    {
        try
        {
            Zon_HediyeCek hediye = new Zon_HediyeCek();
            hediye.Aciklama = not;
            hediye.BaslamaTarih = Convert.ToDateTime(baslangic);
            if (randevuId == 0)
            {
                hediye.Tutar = Convert.ToDecimal(indirim);
                hediye.TutarTip = int.Parse(tutarTip);
            }
            else
            {
                hediye.TutarTip = getTutarTip(tip);
                hediye.Tutar = tutarHesapla(randevuId, tip);
            }
            hediye.Kullanim = "0";
            hediye.MinTutar = Convert.ToDecimal(minimum);
            hediye.BitisTarih = Convert.ToDateTime(bitis);
            hediye.Aktif = "1";
            hediye.MusteriId = userid;
            hediye.Tip = tip;
            hediye.SeriNo = seriNo;
            hediye.Kod = GenerateKod();
            hediye.OlusturmaTarih = DateTime.Now;
            db.AddToZon_HediyeCek(hediye);
            db.SaveChanges();

            Mailer.hediyeCekMail(hediye, userid);

            _result.result = true;
            _result.data = hediye;

            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public int? getTutarTip(int cekTip)
    {
        int tutarTip=1;
        Zon_HediyeCek_Ayar h = db.Zon_HediyeCek_Ayar.Where(z => z.CekTip == cekTip).FirstOrDefault();
        if (h != null)
            tutarTip = h.TutarTip.Value;

        return tutarTip;
    }

    public decimal? tutarHesapla(int randevuId, int tip)
    {
        decimal tutar=0;
        Zon_HediyeCek_Ayar h = null;
        if (randevuId != 0)
        {
            Zon_Randevu r = db.Zon_Randevu.Where(z => z.Id == randevuId).FirstOrDefault();

            h = db.Zon_HediyeCek_Ayar.Where(z => z.CekTip == tip).FirstOrDefault();

            if (h != null)
            {
                if (h.TumSirketler.Value)
                {
                    tutar = h.Tutar.Value;
                }
                else
                {
                    if (h.SirketId == r.SirketId)
                    {
                        tutar = h.Tutar.Value;
                    }
                }
            }
        }
        else
        { 
            
        }
        return tutar;

    }
    
    #endregion

    public List<Zon_HediyeCek> getHediyeCekList(int userId)
    {
        DateTime dt = DateTime.Today;
        //object dt2 = EntityFunctions.TruncateTime(DateTime.Now);

        string strSQL = "select * from Zon_HediyeCek where MusteriId=" + userId + " and cast(CONVERT(varchar, BitisTarih, 112) as datetime)>='" + dt.ToString("yyyyMMdd") + "'";
        List<Zon_HediyeCek> list = db.ExecuteStoreQuery<Zon_HediyeCek>(strSQL).ToList();

        //var a = db.Zon_HediyeCek.Where(z => z.MusteriId == userId && EntityFunctions.TruncateTime(z.BitisTarih) >= dt).ToList();

        return list;
    }

    public List<Zon_HediyeCek> getHediyeCekAllList()
    {

       return db.Zon_HediyeCek.ToList();

    }

    public Zon_HediyeCek getHediyeCek(int userId,int tip)
    {
        return db.Zon_HediyeCek.Where(z => z.MusteriId == userId && z.Tip==tip).FirstOrDefault();
    }

    public ResultAction checkHediyeCeki(string hediyeCekKod, int userId)
    {
        _result.result = true;
        Zon_HediyeCek cek = null;
        odemeSecModel cart = new odemeSecModel();
        OdemeRepositories o = new OdemeRepositories();
        Zon_Kullanici CurrentUser = (Zon_Kullanici)HttpContext.Current.Session["user"];
        try
        {
            if (CurrentUser != null)
            {
                string strSQL = "select * from View_SepetListe where SessionId = '" + HttpContext.Current.Session.SessionID + "'";
                List<View_SepetListe> list = db.ExecuteStoreQuery<View_SepetListe>(strSQL).ToList();

                cart.sepetListe = list;
                cart.genelToplam =o.findCartTotal(list, Constants.EticaretOdemeTipi.PESIN,1,1);
                if (!hediyeCekKod.Trim().Equals(""))
                {

                    cek = (from item in db.Zon_HediyeCek
                           where item.Kod == hediyeCekKod.Trim()
                           select item).FirstOrDefault();

                }
                if (cek != null)
                {
                    if (HttpContext.Current.Session["hediyeCekKod"] == null)
                    {
                        if (isValidDateHediyeCek(cart.genelToplam, hediyeCekKod))
                        {
                            if (isValidTotalHediyeCek(cart.genelToplam, hediyeCekKod))
                            {
                                cart.hediyeCekTutar = tutarHesapla(cart.genelToplam,cek.TutarTip.Value,(decimal)cek.Tutar);
                                cart.genelToplam = cart.genelToplam - tutarHesapla(cart.genelToplam, cek.TutarTip.Value, (decimal)cek.Tutar);
                                HttpContext.Current.Session["hediyeCekKod"] = cek.Kod;
                                _result.message = cart.hediyeCekTutar + ":" + cart.genelToplam;
                                _result.result = true;

                            }
                            else
                            {
                                _result.message = "Bu Hediye Çeki " + cek.MinTutar.Value.ToString("N") + " TL ve üzeri alışverişlerde geçerlidir.<br/> Hediye çekinizi kullanabilmeniz için sepetinize <br/>" + (cek.MinTutar - cart.genelToplam).Value.ToString("N") + " TL'lik ürün ekleyebilirsniz.";
                                HttpContext.Current.Session["hediyeCekKod"] = null;
                                _result.result = false;
                            }
                        }
                        else
                        {
                            _result.message = "Hediye Çekinizin Son Kullanma Tarihi Geçmiş.";
                            HttpContext.Current.Session["hediyeCekKod"] = null;
                            _result.result = false;
                        }

                    }
                    else
                    {
                        _result.message = "Siparişlerde birden fazla hediye çeki kullanılamaz.";
                        _result.result = false;
                    }
                }
                else
                {
                    _result.message = "Geçersiz Hediye Çeki.";
                    HttpContext.Current.Session["hediyeCekKod"] = null;
                    _result.result = false;
                }

            }
            else
            {
                _result.message = "Hediye çeklerinizi kullanabilmek için lütfen üye girişi yapınız."; 
                //ViewState["Error"] = "Hediye çeklerinizi kullanabilmek için lütfen üye girişi yapınız.";
                _result.result = false;
            }

            return _result;

        }
        catch (Exception ex)
        {
            _result.result = false;
            logger.Error(ex);
            return _result;
        }


    }

    public decimal tutarHesapla(decimal toplam, int tip, decimal indirim)
    {
        if (tip == 1)
            toplam = (toplam * indirim) / 100;

        else
            toplam = indirim;

        return toplam;

    }

    public bool isValidDateHediyeCek(decimal sepetToplam, string hediyeCekKod)
    {

        bool ret = false;
        string strSQL = "select * from Zon_HediyeCek where Kod='" + hediyeCekKod + "' and Kullanim='0'  and cast(CONVERT(varchar, BitisTarih, 112) as datetime)>='" + DateTime.Today.ToString("yyyyMMdd") + "'";
        Zon_HediyeCek u =  db.ExecuteStoreQuery<Zon_HediyeCek>(strSQL).FirstOrDefault();

        
        if (u != null)
            ret = true;

        else
            ret = false;

        return ret;
    }

    public bool isValidTotalHediyeCek(decimal sepetToplam, string hediyeCekKod)
    {

        bool ret = false;
        Zon_HediyeCek u = (from item in db.Zon_HediyeCek
                           where item.Kod == hediyeCekKod && item.Kullanim == "0" && item.MinTutar <= sepetToplam
                                   select item).FirstOrDefault();
        if (u != null)
            ret = true;

        else
            ret = false;

        return ret;
    }

    public ResultAction deleteHediyeCek(int id)
    {
        try
        {
            Zon_HediyeCek cek = db.Zon_HediyeCek.Where(z => z.Id == id).FirstOrDefault();
            if (cek != null)
            {
                db.DeleteObject(cek);
                db.SaveChanges();
                _result.result = true;
                _result.message = "Kayıt silindi.";
            }
            else
            {
                _result.message = "Kayıt Bulunamadı.";
                _result.result = false;
            }
            return _result;
        }
        catch (Exception ex)
        {

            _result.message = "Hata Oluştu.";
            _result.result = false;
            logger.Error(ex);
            return _result;
        }
    }

    public List<Zon_Sirket> GetSirket()
    {
        return db.Zon_Sirket.Where(z => z.Aktif == "1").ToList();
    }

    public ArrayList GetGrupMusteriList(Array list)
    {
        ArrayList lst=new ArrayList();

        foreach (var i in list)
        {
            int id = int.Parse(i.ToString());
            var grupList = db.Zon_Musteri_Grup.Where(z => z.GrupId == id).ToList();
            foreach (var g in grupList)
                lst.Add(g.MusteriId.ToString());
        }

        return lst;
    }

    public ArrayList GetSirketMusteriList(Array list)
    {
        ArrayList lst = new ArrayList(); 

        foreach (var i in list)
        {
            int id = int.Parse(i.ToString());
            var grupList = db.Zon_Kullanici.Where(z => z.SirketId == id).ToList();
            foreach (var g in grupList)
                lst.Add(g.ID.ToString());
        }

        return lst;
    }

    public bool siparisSayiSirketKontrol(int randevuid,int cekTip)
    {
        bool ret = false;
        
        Zon_Randevu r=db.Zon_Randevu.Where(z=>z.Id==randevuid).FirstOrDefault();

        Zon_HediyeCek_Ayar h = db.Zon_HediyeCek_Ayar.Where(z =>z.CekTip==cekTip).FirstOrDefault();

        if (cekTip == (int)Constants.HediyeCekTip.siparisSayisi)
        {

            if (h != null)
            {
                if (h.TumSirketler.Value)
                    ret = true;
                else
                {
                    if (h.SirketId == r.SirketId)
                        ret = true;
                }
            }
        }
        return ret;
    }

    #region Çek Ayar
   
    public Zon_HediyeCek_Ayar getHediyeCekAyar(int id)
    {
        return db.Zon_HediyeCek_Ayar.Where(z => z.Id == id).FirstOrDefault();
    }
    
    public ResultAction deleteRowCekAyar(int id)
    {
        try
        {
            var s = db.Zon_HediyeCek_Ayar.Where(z => z.Id == id).FirstOrDefault();
            db.DeleteObject(s);
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public ResultAction ActionCekAyar(string id,string sirket,string cekTip,string tutar,string tutarTip,bool chkTumSirket, string mode, string aktif)
    {
        try
        {
            Zon_HediyeCek_Ayar s = null;
            if (mode.Equals("add"))
            {
                s = new Zon_HediyeCek_Ayar();
            }
            else
            {
                int cId = Convert.ToInt32(id);
                s = db.Zon_HediyeCek_Ayar.Where(z => z.Id == cId).FirstOrDefault();
            }
            if(!string.IsNullOrEmpty(sirket))
                s.SirketId = int.Parse(sirket);

            if (!string.IsNullOrEmpty(cekTip))
                s.CekTip = int.Parse(cekTip);

            if (!string.IsNullOrEmpty(tutar))
                s.Tutar = decimal.Parse(tutar);

            if (!string.IsNullOrEmpty(tutarTip))
                s.TutarTip = int.Parse(tutarTip);

            s.TumSirketler = chkTumSirket;

            s.Aktif = aktif;

            if (mode == "add")
                db.AddToZon_HediyeCek_Ayar(s);

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }
    #endregion
}