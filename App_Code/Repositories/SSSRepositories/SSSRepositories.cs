﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;

/// <summary>
/// Summary description for SSSRepositories
/// </summary>
public class SSSRepositories
{
    ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
	public SSSRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}

    public ResultAction ActionSSS(string id, string baslik, string icerik,string mode,string aktif, int sira)
    {
        try
        {
            Zon_SSS s = null;
            if (mode.Equals("add"))
            {
                s = new Zon_SSS();
            }
            else
            {
                int cId = Convert.ToInt32(id);
                s = db.Zon_SSS.Where(z => z.Id == cId).FirstOrDefault();
            }

            s.Baslik = baslik;
            s.Icerik = icerik;
            s.Aktif = Convert.ToBoolean(aktif);
            s.Sira = sira;

            if (mode == "add")
                db.AddToZon_SSS(s);

            db.SaveChanges();

            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    public Zon_SSS GetResimGaleri(int pId)
    {
        return db.Zon_SSS.Where(z => z.Id == pId).FirstOrDefault();
    }

    public List<Zon_SSS> GetSSSList()
    {
        return db.Zon_SSS.Where(z => z.Aktif == true).OrderBy(x=>x.Sira).ToList();
    }
}