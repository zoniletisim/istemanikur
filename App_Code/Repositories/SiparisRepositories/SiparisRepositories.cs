﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
/// <summary>
/// SiparisRepositories
/// Sipariş database işlemleri
/// </summary>
public class SiparisRepositories
{
	ZonDBEntities _db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
    public SiparisRepositories()
	{
		//
		// TODO: Add constructor logic here
		//
        _db = new ZonDBEntities();
        _result = new ResultAction();
	}

    /// <summary>
    /// Sipariş bilgisi
    /// </summary>
    /// <param name="id">sipariş id'si</param>
    /// <returns>dimara_Siparisler</returns>
    public Zon_Siparis GetSiparis(int id)
    {
        try
        {
            return _db.Zon_Siparis.Where(a => a.ID == id).Single();
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// Aylık sipariş sayısı (Rapor)
    /// </summary>
    /// <param name="mouth">Ay</param>
    public void GetSiparisForMouthReport(int mouth) {
        try
        {
            int days = DateTime.DaysInMonth(DateTime.Now.Year, mouth);
            List<int> _days = new List<int>();
            List<int> sayi = new List<int>();

            for (int i = 1; i < days; i++)
            {
                _days.Add(1);
                DateTime _dt = new DateTime(DateTime.Now.Year,DateTime.Now.Month,i);
                sayi.Add((from _w in _db.Zon_Siparis
                          where _w.EklenmeTarihi == _dt
                              select _w).Count());
            }


        }
        catch (Exception ex)
        {
            logger.Error(ex);
            throw;
        }
    }

    /// <summary>
    /// Tüm siparişler
    /// </summary>
    /// <returns>List dimara_Siparisler</returns>
    public List<Zon_Siparis> GetAllSiparisler()
    {
        try
        {
            return _db.Zon_Siparis.ToList();
        }
        catch (Exception)
        {
            
            throw;
        }
    }

    /// <summary>
    /// Tüm sipariş durumları
    /// </summary>
    /// <returns>List dimara_Siparis_Durumlari</returns>
    //public List<dimara_Siparis_Durumlari> GetAllSiparisDurumlari()
    //{
    //    try
    //    {
    //        return _db.dimara_Siparis_Durumlari.ToList();
    //    }
    //    catch (Exception)
    //    {

    //        throw;
    //    }
    //}

    /// <summary>
    /// Sipariş durumu güncellem
    /// </summary>
    /// <param name="siparisid">sipariş no'su</param>
    /// <param name="notifyMusteri">müşteri bilgilendirme durumu</param>
    /// <param name="_durum">sipariş durumu</param>
    /// <param name="aciklama">durumla alakalı açıklama</param>
    /// <returns>ResultAction</returns>
    //public ResultAction AddNewSiparisDurum(int siparisid, int notifyMusteri, string _durum, string aciklama) {
    //    try
    //    {
    //        dimara_Siparis_Siparis_Durumlari durum = new dimara_Siparis_Siparis_Durumlari();

    //        durum.Aciklama = aciklama;
    //        durum.EklenmeTarihi = DateTime.Now;
    //        durum.Notification = notifyMusteri;
    //        durum.SiparisDurumID = Convert.ToInt32(_durum);
    //        durum.SiparisID = siparisid;

    //        _db.AddTodimara_Siparis_Siparis_Durumlari(durum);
    //        _db.SaveChanges();

    //        _result.result = true;
    //        _result.id = durum.ID;

    //        return _result;
    //    }
    //    catch (Exception ex)
    //    {
    //        _result.message = ex.Message;
    //        _result.result = false;
    //        logger.Error(ex);
    //        return _result;
    //    }
    //}

    public SiparisModel GetUserSipariss(int userId)
    {
        
            SiparisModel s = new SiparisModel();
            string strSQL = "select s.ID, s.SiparisNO, s.EklenmeTarihi, s.ToplamTutar, i.Aciklama  from Zon_Siparis s inner join Zon_Siparis_Durumlari i on s.Durumu=i.ID where s.MusteriID=" + userId;
            s.siparisLists = _db.ExecuteStoreQuery<siparisList>(strSQL).ToList();
           
            return s;
      
       
    }

    public SiparisModel getUserSiparis(int userID,int siparisID)
    {
        Zon_HediyeCek hediyecek=new Zon_HediyeCek();
        SiparisModel s = new SiparisModel();
        HediyeCekRepositories cekRep = new HediyeCekRepositories();
        try
        {
            
            s.siparis = (from si in _db.Zon_Siparis
                         where si.MusteriID == userID
                         select si).ToList();

            s.seciliSiparis = (from si in _db.Zon_Siparis
                               where si.MusteriID == userID && si.ID == siparisID
                               select si).FirstOrDefault();
           
            string strSQL = "select s.ID, s.SiparisNO, s.EklenmeTarihi, s.ToplamTutar, i.Aciklama  from Zon_Siparis s inner join Zon_Siparis_Durumlari i on s.Durumu=i.ID where s.MusteriID=" + userID;
            s.siparisLists = _db.ExecuteStoreQuery<siparisList>(strSQL).ToList();
            var x = s.siparisLists.Where(z => z.ID == siparisID).FirstOrDefault();
            s.siparisDurum = x.Aciklama;
            s.siparisNo = x.SiparisNO;

            //_db.dimara_Siparisler.OrderBy(z => z.EklenmeTarihi).Where(x => x.MusteriID == userID).ToList();

            //strSQL = "select distinct w.urunID,w.Kod, w.Baslik, w.Fiyat, w.Thumb2ImageUrl from View_Urun w inner join dimara_Siparis_Urunleri i on w.urunID=i.UrunID where i.SiparisID=" + siparisID;
            //s.urun = _db.ExecuteStoreQuery<siparisUrun>(strSQL).ToList();
            //s.UrunToplam = s.urun.Sum(z => z.Fiyat);


            s.siparisDetay = (from item in _db.Zon_Siparis_Detay
                              where item.SiparisID == siparisID
                              select item).ToList();

            if (!string.IsNullOrEmpty(s.seciliSiparis.HediyeCeki))
            {

                hediyecek = (from h in _db.Zon_HediyeCek
                             where h.Kod == s.seciliSiparis.HediyeCeki
                             select h).FirstOrDefault();

                if (hediyecek != null)
                    s.hediyeCekTutar = cekRep.tutarHesapla(s.seciliSiparis.AraToplam.Value, hediyecek.Tip.Value, hediyecek.Tutar.Value);
            }

            //strSQL = "select t.* from Zon_Siparis_Detay u inner join View_Urun_Tas t on u.UrunID=t.urunID where u.SiparisID=" + siparisID;
            //s.urunTas = _db.ExecuteStoreQuery<View_Urun_Tas>(strSQL).ToList();
            s.teslimatAdres = (from item in _db.Zon_Musteri_Adresleri
                               where item.ID == s.seciliSiparis.TeslimatAdresID
                               select item).FirstOrDefault();
            s.faturaAdres = (from item in _db.Zon_Musteri_Adresleri
                             where item.ID == s.seciliSiparis.FaturaAdresID
                             select item).FirstOrDefault();


            return s;
        }
        catch (Exception)
        {
            return s;
           
        }
        
    }

    public odemeSecModel getUserBasket(int userId, string sessionId, string teslimatAdres, string faturaAdres)
    {
        odemeSecModel o = new odemeSecModel();
        RandevuRepositories u = new RandevuRepositories();
        int sessionTeslimatAdresId = 0, sessionFaturaAdresId = 0;
        try
        {
            if (!string.IsNullOrEmpty(teslimatAdres))
                int.TryParse(teslimatAdres, out sessionTeslimatAdresId);

            if (!string.IsNullOrEmpty(faturaAdres))
                int.TryParse(faturaAdres, out sessionFaturaAdresId);

            if (sessionTeslimatAdresId > 0)
                o.teslimatAdres = _db.Zon_Musteri_Adresleri.Where(a => a.MusteriID == userId && a.ID == sessionTeslimatAdresId).FirstOrDefault();

            if (sessionFaturaAdresId > 0)
                o.faturaAdres = _db.Zon_Musteri_Adresleri.Where(a => a.MusteriID == userId && a.ID == sessionFaturaAdresId).FirstOrDefault();

            o.sepetListe = _db.View_SepetListe.Where(z => z.SessionId == sessionId).ToList();
            o.urunToplam = u.findCartTotal(o.sepetListe, Constants.EticaretOdemeTipi.PESIN);

            o.kargoFiyati = Constants.KargoUcreti;
            if (o.urunToplam >= Constants.MinSiparisTutarForKargo)
                o.kargoHesaplananFiyati = 0;
            else
                o.kargoHesaplananFiyati = o.kargoFiyati;

            o.genelToplam = o.urunToplam + o.kargoHesaplananFiyati;
            //if (taksitId != 0)
            //{
            //    o.genelToplam =findCartTotal(o.sepetListe, o, Constants.EticaretOdemeTipi.TAKSITLI, bankaID, taksitId) + o.kargoHesaplananFiyati;
            //    o.vadeFarki = o.genelToplam - o.urunToplam;

            //}
            //if (HttpContext.Current.Session["hediyeCekKod"] != null)
            //{
            //    string kod = HttpContext.Current.Session["hediyeCekKod"].ToString();
            //    dimara_Hediye_Cekleri cek = new dimara_Hediye_Cekleri();
            //    cek = (from item in _db.dimara_Hediye_Cekleri
            //           where item.Kod == kod
            //           select item).FirstOrDefault();

            //    o.hediyeCekTutar = (decimal)cek.Indirim;
            //    o.genelToplam = o.genelToplam - (decimal)cek.Indirim;
            //}
            //o.bankaVade = (from item in _db.View_Banka_Vade
            //               where item.BankaId == bankaID && item.TaksitId == taksitId
            //               select item).FirstOrDefault();

            //o.TaksitSayi = o.bankaVade.TaksitSayi.Value;

            o.OrderId = userId.ToString().PadLeft(6, '0') + DateTime.Now.Ticks.ToString();


            //if (o.sepetListe.Count > 0)
            //{
            //    var topSure = o.sepetListe.Distinct().Max(z => z.TeslimatSuresi);
            //    var MaxDate = (from d in o.sepetListe select d.TeslimatSuresi).Max();
            //    if (MaxDate.Value == 1)
            //        o.teslimatSure = "Hemen Teslim";
            //    else if (MaxDate.Value == 2)
            //        o.teslimatSure = "2-3 İş Gününde Kargoya Teslim";
            //    else if (MaxDate.Value == 3)
            //        o.teslimatSure = "3-4 İş Gününde Kargoya Teslim";
            //    else if (MaxDate.Value == 4)
            //        o.teslimatSure = "7-10 İş Gününde Kargoya Teslim";
            //}

            _result.result = true;
            return o;
        }
        catch (Exception ex)
        {
            _result.message = ex.Message;
            _result.result = false;
            logger.Error(ex);
            return o;
        }

    }
    public decimal findCartTotal(List<View_SepetListe> list, odemeSecModel o, Constants.EticaretOdemeTipi odemeTip, int banka, int taksit)
    {
        decimal total = 0;
        foreach (View_SepetListe r in list)
        {
            decimal fiyat = 0;
            if (odemeTip == Constants.EticaretOdemeTipi.PESIN)
            {
                if (r.MiktarYuzde != null)
                {
                    fiyat = (r.Fiyat.Value * (100 - r.MiktarYuzde.Value)) / 100;
                }
                else
                    fiyat = r.Fiyat.Value;
               
            }
            else if (odemeTip == Constants.EticaretOdemeTipi.TAKSITLI)
            {
                
                        fiyat = r.Fiyat.Value;

               
            }
            else
                throw new Exception("Ödeme tipi desteklenmiyor.");


            total += fiyat;

        }

        total = Math.Round(total, 2);
        return total;
    }  




    public string adres(int ofis,Zon_Kullanici u)
    {
        var ofisAdres = _db.Zon_Ofis.Where(z => z.Id == ofis).FirstOrDefault();

        if (!string.IsNullOrEmpty(ofisAdres.Adres))
            return ofisAdres.Adres;
        else
        {
            var UserAdres = _db.Zon_Musteri_Adresleri.Where(z => z.MusteriID == u.ID).FirstOrDefault();
            return UserAdres.Adres;
        }
        
    }
}