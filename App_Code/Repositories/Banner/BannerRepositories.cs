﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;

/// <summary>
/// Summary description for BannerRepositories
/// </summary>
public class BannerRepositories
{
    ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
	public BannerRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}

    public ResultAction ActionBanner(string id, string sayfa, string baslik, HttpFileCollection resimler, string icerik, string mode, string aktif, string dil, int bayiId)
    {
        string path = "/App_themes/Uploads/images/Banner/";
        string pathTh1 = "/App_themes/Uploads/images/Banner/";
        string FileNameOrg = string.Empty;
        string FileNameTh1 = string.Empty;
        string FileNameTh2 = string.Empty;
        System.Drawing.Image orjinalFoto = null;
        try
        {
            Zon_Banner s = null;
            if (mode.Equals("add"))
                s = new Zon_Banner();

            else
            {
                int cId = Convert.ToInt32(id);
                int pSayfaId = Convert.ToInt32(sayfa);
                int pDilId = Convert.ToInt32(dil);
                s = db.Zon_Banner.Where(z => z.SayfaId == pSayfaId && z.DilId == pDilId).FirstOrDefault();
            }
            

            s.Baslik = baslik;
            s.Icerik = icerik;
            s.DilId = int.Parse(dil);
            s.Aktif = aktif;
            if (resimler != null)
            {
                HttpFileCollection fileCollection = resimler;
                HttpPostedFile postedFile;

                for (int i = 0; i < fileCollection.Count; i++)
                {
                    postedFile = fileCollection[i];
                    if (postedFile.ContentLength > 0)
                    {
                        orjinalFoto = System.Drawing.Image.FromStream(postedFile.InputStream);
                        string[] flName = postedFile.FileName.Split('.');
                        FileNameOrg = flName[0] + "_Org" + DateTime.Now.Ticks + "." + flName[1];
                        FileNameTh1 = flName[0] + "_Thumb1" + DateTime.Now.Ticks + "." + flName[1];
                        Utility.boyutlandir(orjinalFoto, 150, FileNameTh1, pathTh1);

                        string savedFileName = path + FileNameOrg;
                        postedFile.SaveAs(HttpContext.Current.Server.MapPath(savedFileName));

                        s.Resim = savedFileName;
                        s.ResimTh = pathTh1 + FileNameTh1;
                    }
                }
            }

            if (mode == "add")
                db.AddToZon_Banner(s);

            db.SaveChanges();

            if (sayfa == "")
                s.SayfaId = s.Id;
            else
                s.SayfaId = int.Parse(sayfa);
            db.SaveChanges();


            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }

    public Zon_Banner GetBanner(int pId, int dil)
    {
        return db.Zon_Banner.Where(z => z.SayfaId == pId && z.DilId == dil).FirstOrDefault();
    }

    public List<Zon_Banner> GetBannerList()
    {
        return db.Zon_Banner.Where(z => z.Aktif == "1").OrderBy(x => x.Sira).ToList();
    }

    public ResultAction deleteRowBanner(int id)
    {
        try
        {
            var s = db.Zon_Banner.Where(z => z.Id == id).FirstOrDefault();
            db.DeleteObject(s);
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public Zon_Banner GetSayfaLanguage(int pId, int dilId)
    {
        Zon_Banner i = db.Zon_Banner.Where(z => z.SayfaId == pId && z.DilId == dilId).FirstOrDefault();

        return i;
    }

    public Zon_Banner getTrBaslik(int id)
    {
        var b = db.Zon_Banner.Where(z => z.SayfaId == id && z.DilId == 1).FirstOrDefault();
        return b;
    }

    
}