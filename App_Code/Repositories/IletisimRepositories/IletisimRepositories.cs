﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;

/// <summary>
/// Summary description for IletisimRepositories
/// </summary>
public class IletisimRepositories
{
    ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
	public IletisimRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}

    public ResultAction saveIletisimForm(string ad, string email, string tel, string konu,string not)
    {
        try
        {
            Zon_Iletisim k = new Zon_Iletisim();

            k.Ad = ad;
            k.Email = email;
            k.Tel = tel;
            k.Konu = konu;
            k.Mesaj = not;
            k.Tarih = DateTime.Now;
            k.Okundu = false;
            db.AddToZon_Iletisim(k);
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
    public ResultAction saveHenuzyok(string ad, string soyad, string email, string sirket, string iletisimKisi, string iletisimTel)
    {
        try
        {
            Zon_HenuzYok k = new Zon_HenuzYok();

            k.Ad = ad;
            k.Soyad = soyad;
            k.Email = email;
            k.Sirket = sirket;
            k.IletisimKisi = iletisimKisi;
            k.İletisimTel = iletisimTel;
            k.Tarih = DateTime.Now;
            db.AddToZon_HenuzYok(k);
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
}