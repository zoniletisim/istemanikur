﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.Transactions;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Web.Security;
using System.Web.UI;

public class OdemeRepositories :Control
{
	ZonDBEntities _db;
   
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
    HttpSessionState Session = HttpContext.Current.Session;
    public string message;
    public string apiTransId;
    public string apiResponse;
    public string apiResponseText;
    public string apiAuthCode;
    public string apiHostRefNum;
    public string mdStatus;
    public long logicalref;
    public long orderref;
   
    public OdemeRepositories()
    {
        _db = new ZonDBEntities();
        _result = new ResultAction();
    }
    
    public odemeSecModel getUserBasket(int userId, string sessionId)
    {
        odemeSecModel o = new odemeSecModel();
        RandevuRepositories u = new RandevuRepositories();
        HediyeCekRepositories hediyeCekRep = new HediyeCekRepositories();
        try
        {

            o.sepetListe = _db.View_SepetListe.Where(z => z.SessionId == sessionId).ToList();
            o.urunToplam = u.findCartTotal(o.sepetListe, Constants.EticaretOdemeTipi.PESIN);

            o.kargoFiyati = Constants.KargoUcreti;
            if (o.urunToplam >= Constants.MinSiparisTutarForKargo)
                o.kargoHesaplananFiyati = 0;
            else
                o.kargoHesaplananFiyati = o.kargoFiyati;

            o.genelToplam = o.urunToplam + o.kargoHesaplananFiyati;

            //if (taksitId != 0)
            //{
            //    o.genelToplam = findCartTotal(o.sepetListe, o, Constants.EticaretOdemeTipi.TAKSITLI, bankaID, taksitId) + o.kargoHesaplananFiyati;
            //    o.vadeFarki = o.genelToplam - o.urunToplam;
            //}

            //if (HttpContext.Current.Session["hediyeCekKod"] != null)
            //{
            //    string kod = HttpContext.Current.Session["hediyeCekKod"].ToString();
            //    Zon_HediyeCek cek = new Zon_HediyeCek();
            //    cek = (from item in _db.Zon_HediyeCek
            //           where item.Kod == kod
            //           select item).FirstOrDefault();

            //    o.hediyeCekTutar = hediyeCekRep.tutarHesapla(o.genelToplam, cek.TutarTip.Value, (decimal)cek.Tutar);
            //    o.genelToplam = o.genelToplam - o.hediyeCekTutar;

            //}

            //o.bankaVade = (from item in _db.View_Banka_Vade
            //               where item.BankaId == bankaID && item.TaksitId == taksitId
            //               select item).FirstOrDefault();

            //o.TaksitSayi = o.bankaVade.TaksitSayi.Value;

            o.OrderId = userId.ToString().PadLeft(6, '0') + DateTime.Now.Ticks.ToString();

           
            //if (o.sepetListe.Count > 0)
            //{
            //    //var topSure = o.sepetListe.Distinct().Max(z => z.TeslimatSuresi);
            //    //var MaxDate = (from d in o.sepetListe select d.TeslimatSuresi).Max();
            //    if (MaxDate.Value == 1)
            //        o.teslimatSure = "Hemen Teslim";
            //    else if (MaxDate.Value == 2)
            //        o.teslimatSure = "2-3 İş Gününde Kargoya Teslim";
            //    else if (MaxDate.Value == 3)
            //        o.teslimatSure = "3-4 İş Gününde Kargoya Teslim";
            //    else if (MaxDate.Value == 4)
            //        o.teslimatSure = "7-10 İş Gününde Kargoya Teslim";
            //}

            _result.result = true;
            return o;
        }
        catch (Exception ex)
        {
            _result.message = ex.Message;
            _result.result = false;
            logger.Error(ex);
            return o;
        }
        
    }

    public decimal findCartTotal(List<View_SepetListe> list, Constants.EticaretOdemeTipi odemeTip,int banka,int taksit)
    {

        decimal total = 0;
        foreach (View_SepetListe r in list)
        {
            decimal fiyat=0;
            
            if (odemeTip == Constants.EticaretOdemeTipi.PESIN)
            {

                if (r.MiktarYuzde != null)
                {
                    fiyat = (r.Fiyat.Value * (100 - r.MiktarYuzde.Value)) / 100;
                    fiyat = Math.Ceiling(fiyat);
                }
                else
                    fiyat = r.Fiyat.Value;

            }
            else if (odemeTip == Constants.EticaretOdemeTipi.TAKSITLI)
            {
              fiyat = r.Fiyat.Value;
            }
            else
                throw new Exception("Ödeme tipi desteklenmiyor.");

            
            total += fiyat;

        }

        if (Session["hizmetIndirimTutar"] != null)
        {
            total = total - decimal.Parse(Session["hizmetIndirimTutar"].ToString());
        }

        total = Math.Round(total, 2);
        
        return total;

    }

    public ResultAction odeme(NameValueCollection postForm, string teslimatAdres, string faturaAdres)
    {
        _result.result = false;
        odemeSecModel m = new odemeSecModel();
        Zon_Kullanici u=null;
        Zon_HediyeCek cek = null;
        int _bankaId=0,_taksitId=0;
        bool islem = false;
        decimal toplam = 0;
        HediyeCekRepositories hediyeCekRep = new HediyeCekRepositories();
        try
        {
            string banka = "1";
            string taksit = "0";
            string kartTip = postForm["ctl00$ContentPlaceHolder1$ddKartTip"];
            string kartIsim = postForm["ctl00$ContentPlaceHolder1$txtKartIsim"];
            string kartNo = postForm["ctl00$ContentPlaceHolder1$txtKartNo1"] + postForm["ctl00$ContentPlaceHolder1$txtKartNo2"] + postForm["ctl00$ContentPlaceHolder1$txtKartNo3"] + postForm["ctl00$ContentPlaceHolder1$txtKartNo4"];
            string sonKulTarihAy = postForm["ctl00$ContentPlaceHolder1$ddAy"];
            string sonKulTarihYil = postForm["ctl00$ContentPlaceHolder1$ddYil"];
            string cvc2 = postForm["ctl00$ContentPlaceHolder1$txtCvc"];
            string tcno = postForm["ctl00$ContentPlaceHolder1$txtTcKimlik"];
            if(Session["User"]!=null)
                 u=(Zon_Kullanici)Session["User"];

           
            if (banka !=null)
                int.TryParse(banka, out _bankaId);

            if (taksit != null)
                int.TryParse(taksit, out _taksitId);

            m = getUserBasket(u.ID, Session.SessionID);
            m.bankaId = _bankaId;
            m.taksitId = _taksitId;
            m.kartSahibi = kartIsim;
            m.KartSKAy = sonKulTarihAy;
            m.KartSKYil = sonKulTarihYil;
            m.KartGuvenlikNo = cvc2;
            m.KartNo = kartNo;
            m.KartTipi = kartTip;
            m.tckn = tcno;
            toplam = m.genelToplam;
            if(!string.IsNullOrEmpty(tcno)){
                u = _db.Zon_Kullanici.Where(z => z.ID == u.ID).FirstOrDefault();
                u.TcNo = tcno;
                _db.SaveChanges();
            }
            if (HttpContext.Current.Session["hediyeCekKod"] != null)
            {
                string kod = HttpContext.Current.Session["hediyeCekKod"].ToString();
               
                cek = (from item in _db.Zon_HediyeCek
                       where item.Kod == kod
                       select item).FirstOrDefault();
                if (cek != null)
                {
                    if (hediyeCekRep.isValidTotalHediyeCek(m.genelToplam, kod))
                    {
                        m.hediyeCekTutar = hediyeCekRep.tutarHesapla(m.genelToplam, cek.TutarTip.Value, (decimal)cek.Tutar);
                        toplam = m.genelToplam - m.hediyeCekTutar;
                        if (toplam <= 0)
                            m.genelToplam = 0;
                    }
                }
            }


            if (toplam > 0)
                islem = processOrder(m, _bankaId, _taksitId, teslimatAdres, faturaAdres);
            else
                islem = processOrder(m, faturaAdres);

            if (islem)
            {
                _result.result = true;
                _result.data = ViewState["orderID"];
            }
            else
            {
                _result.result = false;
                _result.data = ViewState["errorMsj"];
            }

            return _result;
        }
        catch (Exception ex)
        {
            _result.message = ex.Message;
            _result.result = false;
            logger.Error(ex);
            return _result;
            
        }
       
    }


    //Sipariş Toplamı 0 ise sanal pos işlemi yapılmıyor.
    private bool processOrder(odemeSecModel m, string faturaAdres)
    {
        bool isOdemeOk = false;
        long newOrderRef;
        Zon_Kullanici user = null;
        Zon_HediyeCek cek = null;
        HediyeCekRepositories hediyeCekRep = new HediyeCekRepositories();
        try
        {
            //dimara_Hediye_Cekleri cek = new dimara_Hediye_Cekleri();
            Zon_Kullanici u = (Zon_Kullanici)Session["User"];
            if (u != null)
            {
                string strSQL = "select * from View_SepetListe where SessionId= '" + Session.SessionID + "'";
                List<View_SepetListe> cart = _db.ExecuteStoreQuery<View_SepetListe>(strSQL).ToList();
                if (cart != null)
                {
                    #region Odeme
                    Zon_Siparis o = new Zon_Siparis();

                    string hediyeNot = Session["BasketNote"] != null ? Session["BasketNote"].ToString() : "";
                    if (hediyeNot.Length > 800)
                        hediyeNot = hediyeNot.Substring(0, 800);

                    o.HediyeNotu = hediyeNot;
                    o.IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

                    decimal kartToplam = (decimal)findCartTotal(cart, Constants.EticaretOdemeTipi.PESIN, 1, 1);
                    decimal kargo = kartToplam >= Constants.MinSiparisTutarForKargo ? 0 : Constants.KargoUcreti;
                    o.AraToplam = kartToplam;

                    if (m.TaksitSayi > 1)
                        kartToplam = (decimal)findCartTotal(cart, Constants.EticaretOdemeTipi.TAKSITLI, 1, 1);

                    if (HttpContext.Current.Session["hediyeCekKod"] != null)
                    {
                        string kod = HttpContext.Current.Session["hediyeCekKod"].ToString();

                        cek = (from item in _db.Zon_HediyeCek
                               where item.Kod == kod
                               select item).FirstOrDefault();
                        if (cek != null)
                        {
                            if (hediyeCekRep.isValidTotalHediyeCek(kartToplam, kod))
                            {
                                m.hediyeCekTutar = hediyeCekRep.tutarHesapla( o.AraToplam.Value, cek.TutarTip.Value, (decimal)cek.Tutar);
                                m.genelToplam = 0;
                                kartToplam = 0;
                                o.HediyeCeki = kod;
                            }
                        }
                    }


                    o.KargoTutar = kargo;
                    o.ToplamTutar = kartToplam + kargo;
                    o.OdemeTuru = "Ücretsiz";
                    o.FaturaAdres = faturaAdres;
                    o.TeslimatAdres = faturaAdres;
                    o.Taksit = m.TaksitSayi;
                    o.Banka = m.bankaId.ToString();
                    o.MusteriID = u.ID;
                    o.EklenmeTarihi = DateTime.Now;
                    string str = FormsAuthentication.HashPasswordForStoringInConfigFile(m.OrderId, "MD5").Substring(0, 8);
                    o.SiparisNO = str;
                    o.KartSahibi = m.kartSahibi;
                    //o.KartNO = m.KartNo.Substring(12, 4);
                    o.Durumu = 6;
                    o.vadeFarki = m.vadeFarki;
                    _db.AddToZon_Siparis(o);
                    #endregion

                    #region odemeDetay

                    foreach (var i in cart)
                    {

                        strSQL = "select * from Zon_Siparis_Detay ";
                        strSQL += " where UrunID = " + i.UrunId + " and SaatId=" + i.SaatId;

                        var siparisDetay = _db.ExecuteStoreQuery<Zon_Siparis_Detay>(strSQL).FirstOrDefault();

                        if (siparisDetay != null)
                        {
                            // string temp = string.IsNullOrWhiteSpace(i.OzelNot) ? i.Aciklama : i.OzelNot;
                            Zon_Sepet sepetsil = (from item in _db.Zon_Sepet
                                                  where item.Id == i.Id
                                                  select item).FirstOrDefault();

                            _db.Zon_Sepet.DeleteObject(sepetsil);
                            _db.SaveChanges();
                            throw new StokTukendiException("Seçili randevu başka bir kullanıcı tarafından satın alınmıştır.");
                        }

                        Zon_Siparis_Detay d = new Zon_Siparis_Detay();

                        if (i.indirimId != null)
                            d.UrunIndirimID = i.indirimId;
                        if (i.MiktarYuzde != null)
                        {
                            d.IndirimMiktar = i.MiktarYuzde;
                            d.IndirimliFiyat = Math.Ceiling((i.Fiyat.Value * (100 - i.MiktarYuzde.Value)) / 100);
                        }

                        // d.KAZANILAN_PUAN = 0;

                        d.Adet = 1;
                        d.BirimFiyat = i.Fiyat;
                        d.ToplamFiyat = i.Fiyat;
                        //d.Tip = 1;
                        d.SiparisID = o.ID;//siparis tablosunun id si
                        d.HizmetId = i.HizmetId;
                        d.UrunID = i.UrunId;
                        d.SaatId = i.SaatId;
                        m.RandevuId=i.UrunId.Value;

                        _db.AddToZon_Siparis_Detay(d);

                        Zon_Rezerve rezerve = new Zon_Rezerve();
                        rezerve.UrunId = i.UrunId;
                        rezerve.SaatId = i.SaatId;
                        rezerve.HizmetTip = i.HizmetId;
                        rezerve.Tarih = i.Tarih;

                        _db.AddToZon_Rezerve(rezerve);

                        Zon_Sepet lineToDelete = (from item in _db.Zon_Sepet
                                                  where item.Id == i.Id
                                                  select item).FirstOrDefault();

                        _db.Zon_Sepet.DeleteObject(lineToDelete);

                        //user = _db.Zon_Kullanici.Where(z => z.ID == u.ID).FirstOrDefault();
                        //user.TcNo = m.tckn;
                    }
                    #endregion

                   
                    using (TransactionScope trans = new TransactionScope(TransactionScopeOption.Suppress))
                    {

                        _db.SaveChanges();
                        newOrderRef = o.ID;
                       

                        m.genelToplam = kartToplam + kargo;

                        if (cek != null)
                        {
                            cek.Kullanim = "1";
                            cek.SiparisId = o.ID;
                            //cek.MusteriId = user.ID;
                        }

                       

                        _db.SaveChanges();
                        if (siparisSayiHediyeCekKontrol(o.MusteriID.Value, m.RandevuId))
                        {
                            HediyeCekRepositories cekrep = new HediyeCekRepositories();
                            //cekrep.AddNewHediyeCeki("5 siparis ver altıncısı bizden", DateTime.Now.ToString(), DateTime.Now.AddMonths(3).ToString(), "15", "0", (int)o.MusteriID, DateTime.Now.Ticks.ToString(), (int)Constants.HediyeCekTip.siparisSayisi, m.RandevuId, "0");
                        }
                       
                        trans.Complete();
                     

                        ViewState.Add("orderID", newOrderRef);
                        Mailer.informOrder(newOrderRef, _db, "");
                        Session["hediyeCekKod"] = null;
                        Session["BasketNote"] = null;
                        return true;

                    }
                }
                else
                    return false;
            }
            else
                return false;
        }
        catch (Exception ex)
        {
            ViewState.Add("errorMsj", ex.InnerException);
            return false;
        }
    } 

    //Sanal Pos ile ödeme işlemi
    private bool processOrder(odemeSecModel m, int banka, int taksit, string teslimatAdres, string faturaAdres)
    {
        bool isOdemeOk = false;
        long newOrderRef;
        Zon_Kullanici user=null;
        Zon_HediyeCek cek = null;
        HediyeCekRepositories hediyeCekRep = new HediyeCekRepositories();
        try
        {
            //dimara_Hediye_Cekleri cek = new dimara_Hediye_Cekleri();
            Zon_Kullanici u = (Zon_Kullanici)Session["User"];
            if (u != null)
            {
                string strSQL = "select * from View_SepetListe where SessionId= '" + Session.SessionID+"'";
                List<View_SepetListe> cart = _db.ExecuteStoreQuery<View_SepetListe>(strSQL).ToList();
                if (cart != null)
                {



                    #region Odeme
                   Zon_Siparis o = new Zon_Siparis();

                   string hediyeNot = Session["BasketNote"] != null ? Session["BasketNote"].ToString() : "";
                    if (hediyeNot.Length > 800)
                        hediyeNot = hediyeNot.Substring(0, 800);

                    o.HediyeNotu = hediyeNot;
                    o.IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

                    decimal kartToplam = (decimal)findCartTotal(cart, Constants.EticaretOdemeTipi.PESIN, banka, taksit);
                    decimal kargo = kartToplam >= Constants.MinSiparisTutarForKargo ? 0 : Constants.KargoUcreti;
                    o.AraToplam = kartToplam;

                    if (m.TaksitSayi > 1)
                        kartToplam = (decimal)findCartTotal(cart, Constants.EticaretOdemeTipi.TAKSITLI, banka, taksit);

                    if (HttpContext.Current.Session["hediyeCekKod"] != null)
                    {
                        string kod = HttpContext.Current.Session["hediyeCekKod"].ToString();

                         cek = (from item in _db.Zon_HediyeCek
                               where item.Kod == kod
                               select item).FirstOrDefault();
                        if (cek != null)
                        {
                            if (hediyeCekRep.isValidTotalHediyeCek(kartToplam, kod))
                            {
                                m.hediyeCekTutar = hediyeCekRep.tutarHesapla(m.genelToplam,cek.TutarTip.Value,(decimal)cek.Tutar);
                                m.genelToplam = m.genelToplam - m.hediyeCekTutar;
                                kartToplam = kartToplam - m.hediyeCekTutar;
                                o.HediyeCeki = kod;
                            }
                        }
                    }
                    if (Session["hizmetIndirimTutar"] != null)
                    { 
                        m.hizmetIndirim=decimal.Parse(Session["hizmetIndirimTutar"].ToString());
                        o.HizmetIndirim=m.hizmetIndirim;
                    }

                    o.KargoTutar = kargo;
                    o.ToplamTutar = kartToplam + kargo;
                    o.OdemeTuru = "Kredi Kartı";
                    o.FaturaAdres = faturaAdres;
                    o.TeslimatAdres = teslimatAdres;
                    o.Taksit = m.TaksitSayi;
                    o.Banka = m.bankaId.ToString();
                    o.MusteriID = u.ID;
                    o.EklenmeTarihi = DateTime.Now;
                    string str = FormsAuthentication.HashPasswordForStoringInConfigFile(m.OrderId, "MD5").Substring(0,8);
                    o.SiparisNO=str;
                    o.KartSahibi = m.kartSahibi;
                    o.KartNO = m.KartNo.Substring(12, 4);
                    o.Durumu = 6;
                    o.vadeFarki = m.vadeFarki;
                    _db.AddToZon_Siparis(o);
                    #endregion

                    #region odemeDetay

                    foreach (var i in cart)
                    {
                       
                       strSQL = "select * from Zon_Siparis_Detay ";
                        strSQL += " where UrunID = " + i.UrunId +" and SaatId=" +i.SaatId;

                       var siparisDetay = _db.ExecuteStoreQuery<Zon_Siparis_Detay>(strSQL).FirstOrDefault();

                        if (siparisDetay!=null)
                        {
                           // string temp = string.IsNullOrWhiteSpace(i.OzelNot) ? i.Aciklama : i.OzelNot;
                            Zon_Sepet sepetsil = (from item in _db.Zon_Sepet
                                                      where item.Id == i.Id
                                                      select item).FirstOrDefault();

                            _db.Zon_Sepet.DeleteObject(sepetsil);
                            _db.SaveChanges();
                            throw new StokTukendiException("Seçili randevu başka bir kullanıcı tarafından satın alınmıştır.");
                        }

                        Zon_Siparis_Detay d = new Zon_Siparis_Detay();
                       
                        if(i.indirimId!=null)
                            d.UrunIndirimID = i.indirimId;
                        if (i.MiktarYuzde != null)
                        {
                            d.IndirimMiktar = i.MiktarYuzde;
                            d.IndirimliFiyat =Math.Ceiling((i.Fiyat.Value  *(100 - i.MiktarYuzde.Value)) / 100);
                        }
                            
                        // d.KAZANILAN_PUAN = 0;

                        d.Adet = 1;
                        d.BirimFiyat = i.Fiyat;
                        d.ToplamFiyat = i.Fiyat;
                        //d.Tip = 1;
                        d.SiparisID = o.ID;//siparis tablosunun id si
                        d.HizmetId = i.HizmetId;
                        d.UrunID = i.UrunId;
                        d.SaatId = i.SaatId;
                        m.RandevuId = i.UrunId.Value;

                        _db.AddToZon_Siparis_Detay(d);

                        Zon_Rezerve rezerve = new Zon_Rezerve();
                        rezerve.UrunId = i.UrunId;
                        rezerve.SaatId = i.SaatId;
                        rezerve.HizmetTip = i.HizmetId;
                        rezerve.Tarih = i.Tarih;

                        _db.AddToZon_Rezerve(rezerve);

                        Zon_Sepet lineToDelete = (from item in _db.Zon_Sepet
                                                     where item.Id == i.Id
                                                     select item).FirstOrDefault();

                        _db.Zon_Sepet.DeleteObject(lineToDelete);

                        //user = _db.Zon_Kullanici.Where(z => z.ID == u.ID).FirstOrDefault();
                        //user.TcNo = m.tckn;
                    }
                    #endregion
                    
                    cctran c = null;
                    using (TransactionScope trans = new TransactionScope())
                    {
                       
                        _db.SaveChanges();
                        newOrderRef = o.ID;
                        #region Odeme

                        m.genelToplam = kartToplam + kargo;


                        isOdemeOk = KrediKartIslem(m);

                        #region CreateCCTrans
                        c = new cctran();
                        c.anapara = o.ToplamTutar;
                        c.authcode = apiAuthCode;
                        c.cardholder = m.kartSahibi;
                        c.cyphcode = "";
                        c.date = DateTime.Now;
                        c.hostrefnum = apiHostRefNum;
                        c.maskedpan = m.KartNo.Substring(12, 4);
                        c.mderrmsg = apiResponseText;
                        c.mdstatus = mdStatus;

                        if (isOdemeOk)
                            c.orderref = (int)o.ID;

                        c.reportnet = 0;
                        c.reportrate = 0;
                        c.response = apiResponse;
                        c.taksit_sayi = m.TaksitSayi;
                        c.toplam = o.ToplamTutar;
                        c.transid = apiTransId;
                        c.trcurr = 0;
                        c.trnet = 0;
                        c.trrate = 0;
                        c.userRef = u.ID;
                        c.vposref = m.bankaId;
                        c.vtaksitref = 0;
                        #endregion
                        _db.AddTocctrans(c);


                        #endregion
                       
                        if (isOdemeOk)
                        {
                            if (cek != null)
                            {
                                cek.Kullanim = "1";
                                cek.SiparisId = o.ID;
                                //cek.MusteriId = user.ID;
                            }
                           

                            _db.SaveChanges();

                            if (siparisSayiHediyeCekKontrol(u.ID, m.RandevuId))
                            {
                                HediyeCekRepositories cekrep = new HediyeCekRepositories();
                                //cekrep.AddNewHediyeCeki("5 siparis ver altıncısı bizden", DateTime.Now.ToString(), DateTime.Now.AddMonths(3).ToString(), "15", "0", (int)o.MusteriID, DateTime.Now.Ticks.ToString(), (int)Constants.HediyeCekTip.siparisSayisi, m.RandevuId, "0");
                            }
                            trans.Complete();
                            ViewState.Add("orderID", newOrderRef);
                            //StringWriter sozlesmeW = new StringWriter();
                            //HttpContext.Current.Server.Transfer("mesafeliSozlesme.aspx?posBank=" + m.bankaId + "&Taksit="+m.taksitId);
                            Mailer.informOrder(newOrderRef, _db, "");
                            Session["hediyeCekKod"] = null;
                            Session["BasketNote"] = null;
                            return true;
                        }
                        
                        else
                        {
                            if (c != null)
                            {
                                using (TransactionScope ccTrans = new TransactionScope(TransactionScopeOption.Suppress))
                                {
                                    try
                                    {
                                        _db.AddTocctrans(c);

                                        _db.SaveChanges();

                                        ccTrans.Complete();
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error("Başarısız KK işlemi:", ex);
                                    }
                                }
                            }
                            ViewState.Add("errorMsj", message);
                            return false;
                        }
                    }
                }
                else
                    return false;
            }
            else
                return false;
        }
        catch (Exception ex)
        {
            ViewState.Add("errorMsj", ex.Message);
            return false;
        }
       
        
           
        
    }

    private bool siparisSayiHediyeCekKontrol(int userId, int randevuid)
    {
        bool ret = false;
        HediyeCekRepositories cekRep = new HediyeCekRepositories();
        if (cekRep.siparisSayiSirketKontrol(randevuid, (int)Constants.HediyeCekTip.siparisSayisi))
        {
            Zon_SiparisSayi s = _db.Zon_SiparisSayi.Where(z => z.UserId == userId).FirstOrDefault();
            if (s != null)
                s.OrderCount++;

            else
            {

                s = new Zon_SiparisSayi();
                s.OrderCount = 1;
                s.UserId = userId;
                s.CompanyId = randevuid;
                _db.AddToZon_SiparisSayi(s);
               
            }
            _db.SaveChanges();
            Zon_SiparisSayi s2 = _db.Zon_SiparisSayi.Where(z => z.UserId == userId).FirstOrDefault();

            if (s2.OrderCount >= 5)
            {
                s2.OrderCount = 0;
                ret = true;
            }
            _db.SaveChanges();
           
        }

        return ret;
    }

    private bool siparisSayiKontrol(int p,ZonDBEntities context)
    {
        bool ret = false;
        int siparis = context.Zon_Siparis.Where(z => z.MusteriID == p).ToList().Count();

        if (siparis % 5 == 0)
        {
            //HediyeCekRepositories cek = new HediyeCekRepositories();
            //cek.AddNewHediyeCeki("5 siparis ver altıncısı bizden", DateTime.Now.ToString(), DateTime.Now.AddMonths(3).ToString(), "15", "0", p, DateTime.Now.Ticks.ToString(), (int)Constants.HediyeCekTip.siparisSayisi,0,"0");
            //ret = true;
        }

        return ret;

    }

    public bool isValidHediyeCek(decimal sepetToplam, string hediyeCekKod)
    {

        bool ret = false;
        Zon_HediyeCek u = (from item in _db.Zon_HediyeCek
                                   where item.Kod == hediyeCekKod && item.Kullanim == "0" && item.MinTutar <= sepetToplam && item.BitisTarih > DateTime.Now
                                   select item).FirstOrDefault();
        if (u != null)
            ret = true;

        else
            ret = false;

        return ret;
    }
    
    private int getActiveFaturaAdresRef()
    {
        Zon_Kullanici u = (Zon_Kullanici)Session["User"];
        int sessionFaturaAdresId = 0;

        if (Session["faturaAdresId"] != null)
            int.TryParse(Session["faturaAdresId"].ToString(), out sessionFaturaAdresId);
        return sessionFaturaAdresId;
    }
    
    private int getActiveTeslimatAdresRef()
    {
        Zon_Kullanici u = (Zon_Kullanici)Session["User"];
        int sessionFaturaAdresId = 0;

        if (Session["faturaAdresId"] != null)
            int.TryParse(Session["faturaAdresId"].ToString(), out sessionFaturaAdresId);
        return sessionFaturaAdresId;
    }

    private bool KrediKartIslem(odemeSecModel model)
    {
        bool ret = false;
        try
        {

                if (model.bankaId == 1)//işbankası
                    ret = createXMLandMakeApiCall_EST("600101103", "HIPERVISION", "Mn+mk+ch3", "https://www.fbwebpos.com/servlet/cc5ApiServer", model, model.genelToplam);
                   // ret = createXMLandMakeApiCall_EST("700100000", "ISBANKAPI", "ISBANK07", "https://testsanalpos.est.com.tr/servlet/cc5ApiServer", model, model.genelToplam);
                //else if (model.bankaId == 2)//yapıkredi
                //    ret = createXMLandMakeApiCall_POSNET("", "", "", "", "http://setmpos.ykb.com/PosnetWebService/XML", model, model.genelToplam);
                //else if (model.bankaId == 3)//vakıf
                //    ret = createXMLandMakeApiCall_VAKIF("", "", "", "", "https://subesiz.vakifbank.com.tr/vpos724v3/", model, model.genelToplam);
                //else if (model.bankaId == 5)//tebbonus
                //    ret = createXMLandMakeApiCall_EST("", "", "", "https://testsanalpos.est.com.tr/servlet/cc5ApiServer", model, model.genelToplam);
                //else if (model.bankaId == 6)//hsbc-advantage
                //{
                //    model.KartNo = createTicketCall_HSBC("", "https://vpostest.advantage.com.tr/servlet/cardgate", model);
                //    ret = createXMLandMakeApiCall_EST("", "", "", "https://vpostest.advantage.com.tr/servlet/cc5ApiServer", model, model.genelToplam);
                //}
                //else if(model.bankaId == 7)//akbank
                //    ret = createXMLandMakeApiCall_EST("", "", "", "https://testsanalpos.est.com.tr/servlet/cc5ApiServer", model, model.genelToplam);
                //else if (model.bankaId == 999)//diğer kartlarda İŞTEN çekiyoruz.
                //    ret = createXMLandMakeApiCall_EST("700100000", "ISBANKAPI", "ISBANK07", "https://testsanalpos.est.com.tr/servlet/cc5ApiServer", model, model.genelToplam);

            //if (model.bankaId == 1)//işbankası
            //    ret = createXMLandMakeApiCall_EST("700100000", "ISBANKAPI", "ISBANK07", "https://testsanalpos.est.com.tr/servlet/cc5ApiServer", model, model.genelToplam);
            //else if (model.bankaId == 2)//yapıkredi
            //    ret = createXMLandMakeApiCall_POSNET("6784335371", "67469171", "sogancilar", "Kutu3792", "https://www.posnet.ykb.com/PosnetWebService/XML", model, model.genelToplam);
            //else if (model.bankaId == 3)//vakıf
            //    ret = createXMLandMakeApiCall_VAKIF("000020461", "VP002030", "0001", "25782678", "https://subesiz.vakifbank.com.tr/vpos724v3/", model, model.genelToplam);
            //else if (model.bankaId == 4)//finans
            //    ret = createXMLandMakeApiCall_EST("", "", "", "https://www.fbwebpos.com/servlet/cc5ApiServer", model, model.genelToplam);
            //else if (model.bankaId == 5)//teb
            //    ret = createXMLandMakeApiCall_EST("", "", "", "https://sanalpos.teb.com.tr/servlet/cc5ApiServer", model, model.genelToplam);
            //else if (model.bankaId == 6)//hsbc-advantage
            //{
            //    model.KartNo = createTicketCall_HSBC("", "https://vpos.advantage.com.tr/servlet/cardgate", model);
            //    ret = createXMLandMakeApiCall_EST("", "", "", "https://vpos.advantage.com.tr/servlet/cc5ApiServer", model, model.genelToplam);
            //}
            //else if (model.bankaId == 7)//akbank
            //    ret = createXMLandMakeApiCall_EST("100793792", "soganapi", "KUTU3792", "https://www.sanalakpos.com/servlet/cc5ApiServer", model, model.genelToplam);
            //else if (model.bankaId == 999)//diğer kartlarda İŞTEN çekiyoruz.
            //    ret = createXMLandMakeApiCall_EST("700100000", "ISBANKAPI", "ISBANK07", "https://testsanalpos.est.com.tr/servlet/cc5ApiServer", model, model.genelToplam);

        }
        catch (Exception ex)
        {
            ret = false;
        }
        finally
        {

        }

        return ret;
    }

    private bool createXMLandMakeApiCall_EST(string clientId, string apiusername, string apipassword, string apigate, odemeSecModel model, decimal sipTutar)
    {
        bool ret = false;

        ePayment.cc5payment mycc5pay = new ePayment.cc5payment();
        mycc5pay.host = apigate;
        mycc5pay.name = apiusername;
        mycc5pay.password = apipassword;
        mycc5pay.clientid = clientId;
        mycc5pay.orderresult = 0;
        mycc5pay.oid = model.OrderId;
        mycc5pay.cardnumber = model.KartNo;
        mycc5pay.expmonth = model.KartSKAy;
        mycc5pay.expyear = model.KartSKYil;
        mycc5pay.cv2 = model.KartGuvenlikNo;
        mycc5pay.subtotal = sipTutar.ToString();
        mycc5pay.currency = "949";
        mycc5pay.chargetype = "Auth";
        if (model.TaksitSayi > 1)
            mycc5pay.taksit = model.TaksitSayi.ToString();

        String Result1 = mycc5pay.processorder();

        String Procreturncode = mycc5pay.procreturncode;
        String ErrMsg = mycc5pay.errmsg;
        String Oid1 = mycc5pay.oid;
        String GroupId = mycc5pay.groupid;
        String appr1 = mycc5pay.appr;
        String refno = mycc5pay.refno;
        String transid = mycc5pay.transid;
        String Extra = mycc5pay.Extra("BONUSABONUS");
        String Extra2 = mycc5pay.Extra("TRXDATE");
        String HostMsg = mycc5pay.Extra("HOSTMSG");

        apiTransId = transid;
        apiResponse = mycc5pay.appr;
        apiResponseText = mycc5pay.errmsg;

        if (apiResponseText.IndexOf("HOSTMSG") >= 0)
            apiResponseText += "-" + HostMsg;

        if (apiResponseText.Length > 500)
            apiResponseText = apiResponseText.Substring(0, 500);

        apiAuthCode = mycc5pay.code;
        apiHostRefNum = mycc5pay.refno;
        mdStatus = Result1;

        if (Result1.Equals("1") && "Approved".Equals(apiResponse))
        {
            ret = true;
        }
        else
        {
            ret = false;
            message = apiResponseText;
        }

        return ret;
    }

    private bool createXMLandMakeApiCall_POSNET(string clientId, string terminalId, string apiusername, string apipassword, string apigate, odemeSecModel model, decimal sipTutar)
    {
        bool ret = false;

        _PosnetDotNetModule.C_Posnet mycc5pay = new _PosnetDotNetModule.C_Posnet();

        mycc5pay.SetURL(apigate);
        mycc5pay.SetMid(clientId);
        mycc5pay.SetTid(terminalId);
        //if (!String.IsNullOrEmpty(Request.Form.Get("cmbVadaaKampanya")))
        //    mycc5pay.SetKOICode(Request.Form.Get("cmbVadaaKampanya"));

        ret = mycc5pay.DoSaleTran(model.KartNo
            , model.KartSKYil.Substring(2) + model.KartSKAy.PadLeft(2, '0')
            , model.KartGuvenlikNo
            , "YKBCMDN_0000" + DateTime.Now.ToString("yyMMddHHmmss")
            , ((int)(sipTutar * 100)).ToString()
            , "YT"
            , model.TaksitSayi == 0 ? "00" : model.TaksitSayi.ToString()
            , null
            , null);

        if (ret)
        {
            String Procreturncode = mycc5pay.GetResponseCode();
            String ErrMsg = mycc5pay.GetResponseText();
            String transid = mycc5pay.GetHostlogkey();
            String Result1 = mycc5pay.GetApprovedCode();

            apiTransId = transid;
            apiResponse = mycc5pay.GetResponseCode();
            apiResponseText = mycc5pay.GetResponseText();
            apiAuthCode = mycc5pay.GetAuthcode();
            apiHostRefNum = mycc5pay.GetHostlogkey();

            if (Result1.Equals("1"))
            {
                ret = true;
                /*Çok kritik sakın bu text i değiştirme */
                apiResponse = "Approved";
                /****************************************/
            }
            else
            {
                ret = false;
                message = apiResponseText;
            }
        }
        else
            message = "Bağlantı Kurulamıyor.";

        return ret;
    }

    private bool createXMLandMakeApiCall_VAKIF(string clientId, string terminalId, string apiusername, string apipassword, string apigate, odemeSecModel model, decimal sipTutar)
    {
        bool ret = false;
        byte[] b = new byte[1500];
        string sonuc;

        //GET mesaji oldugu icin bosluklar + ile doldurulmustur. provizyonMesaji vpos724v3.doc dokumaninda belirtilen formata uygun
        //olarak olusturulmali ve donen mesaj ayni dokumana uygun olarak parcalanmalidir. 
        //string provizyonMesaji="UYGUN FORMATTA OTORIZASYON MESAJI";
        string provizyonMesaji = "?kullanici=" + apiusername;
        provizyonMesaji += "&sifre=" + apipassword;
        provizyonMesaji += "&islem=PRO";
        provizyonMesaji += "&uyeno=" + clientId;
        provizyonMesaji += "&posno=" + terminalId;
        provizyonMesaji += "&kkno=" + model.KartNo;
        provizyonMesaji += "&gectar=" + model.KartSKYil.Substring(2) + model.KartSKAy.PadLeft(2, '0');
        provizyonMesaji += "&cvc=" + model.KartGuvenlikNo;
        provizyonMesaji += "&tutar=" + ((long)(sipTutar * 100)).ToString().PadLeft(12, '0');
        provizyonMesaji += "&provno=000000";
        provizyonMesaji += "&taksits=" + model.TaksitSayi.ToString().PadLeft(2, '0');
        provizyonMesaji += "&islemyeri=I";
        provizyonMesaji += "&uyeref=" + model.OrderId;
        provizyonMesaji += "&vbref=0";
        provizyonMesaji += "&khip=" + HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        provizyonMesaji += "&xcip=V8N5PDVRXI";

        b.Initialize();

        Console.WriteLine("Provizyon Mesaji:" + provizyonMesaji);

        b = Encoding.ASCII.GetBytes(provizyonMesaji);

        WebRequest h1 = (WebRequest)HttpWebRequest.Create(apigate + provizyonMesaji);
        h1.Method = "GET";

        Stream dataStream;
        WebResponse response = h1.GetResponse();
        dataStream = response.GetResponseStream();
        StreamReader reader = new StreamReader(dataStream);
        string responseFromServer = reader.ReadToEnd();
        reader.Close();
        dataStream.Close();
        response.Close();

        XmlDocument doc = new XmlDocument();
        doc.LoadXml(responseFromServer);

        string kod = doc.SelectSingleNode("Cevap/Msg/Kod").InnerText;
        if (kod.Equals("00"))
            ret = true;
        else
            ret = false;

        if (ret)
        {
            apiTransId = doc.SelectSingleNode("Cevap/Msg/UyeRef").InnerText;
            apiResponse = doc.SelectSingleNode("Cevap/Msg/Status").InnerText;
            apiResponseText = doc.SelectSingleNode("Cevap/Msg/Mesaj").InnerText;
            apiAuthCode = "";
            apiHostRefNum = doc.SelectSingleNode("Cevap/Msg/VBRef").InnerText;

            apiResponse = "Approved";
        }
        else
            message = doc.SelectSingleNode("Cevap/Msg/Mesaj").InnerText;

        return ret;
    }

    private string createTicketCall_HSBC(string clientId, string ticketgate, odemeSecModel model)
    {
        System.Net.HttpWebResponse resp = null;
        string ret = "";
        try
        {
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(ticketgate);

            string postData = "ClientId=" + clientId;
            postData += "&Pan=" + model.KartNo;
            postData += "&cv2=" + model.KartGuvenlikNo;
            postData += "&Ecom_Payment_Card_ExpDate_Year=" + model.KartSKYil.Substring(2);
            postData += "&Ecom_Payment_Card_ExpDate_Month=" + model.KartSKAy.PadLeft(2, '0');

            /*if (model.TaksitSayi > 1)
            {
                postData += "&txtype=taksit";
                postData += "&taksit=" + model.TaksitSayi;
            }
            else
            {
                postData += "&txtype=sale";
            }
            */

            byte[] postdatabytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(postData);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postdatabytes.Length;
            System.IO.Stream requeststream = request.GetRequestStream();
            requeststream.Write(postdatabytes, 0, postdatabytes.Length);
            requeststream.Close();

            resp = (System.Net.HttpWebResponse)request.GetResponse();
            System.IO.StreamReader responsereader = new System.IO.StreamReader(resp.GetResponseStream(), System.Text.Encoding.GetEncoding("ISO-8859-9"));

            ret = responsereader.ReadToEnd(); //Gelen xml string olarak alındı.

            int valIndex = ret.IndexOf(model.KartNo.Substring(0, 6));

            int lastIndex = ret.IndexOf("\"", valIndex);

            ret = ret.Substring(valIndex, lastIndex - valIndex);

        }
        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            if (resp != null)
                resp.Close();
        }

        return ret;


    }


    

}