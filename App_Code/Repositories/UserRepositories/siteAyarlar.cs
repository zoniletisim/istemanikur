﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;

/// <summary>
/// Summary description for siteAyarlar
/// </summary>
public class siteAyarlar
{
    ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
	public siteAyarlar()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}
    public ResultAction ActionAyar(string keyword, string mailAdres, string mailsifre, string mailServer, string bilgiMailAdres, string Adres, string tel, string fax, string facebook, string twitter, string pinterest, string googleAnalytics,string googleMaps) 
    {
        try
        {
            Zon_Ayarlar a = (from item in db.Zon_Ayarlar where item.Id==1 select item).FirstOrDefault();

            a.Meta = keyword;
            a.MailAdres = mailAdres;
            a.MailServer = mailServer;
            a.MailSifre = mailsifre;
            a.BilgiMailAdres = bilgiMailAdres;
            a.IletisimAdres = Adres;
            a.IletisimTel = tel;
            a.IletisimFax = fax;
            a.FaceBook = facebook;
            a.Twitter = twitter;
            a.Pinterest = pinterest;
            a.googleAnalytics = googleAnalytics;
            a.googleMaps = googleMaps;

            db.SaveChanges();
            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public Zon_Ayarlar GetAyar()
    {
        return db.Zon_Ayarlar.Where(z => z.Id == 1).FirstOrDefault();
    }
}