﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;

public class Roles
{

    public Roles()
    {

    }

    /// <summary>
    /// Role varmı diye kontrol ediyor. Ekleme işleminde kullanılıyor.
    /// </summary>
    /// <param name="role">Eklenecek rol ismi</param>
    /// <returns></returns>
    public static bool RoleExists(string role)
    {
        ZonDBEntities _db = new ZonDBEntities();
        var varmi = _db.Zon_Roles.Where(a => a.Role == role).ToList();

        if (varmi.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Yeni rol oluşturma işlemi
    /// </summary>
    /// <param name="_role">rol ismi</param>
    public static void CreateRole(string _role)
    {
        ZonDBEntities _db = new ZonDBEntities();
        Zon_Roles role = new Zon_Roles();
        role.Role = _role;
        role.CanInsert = false;
        role.CanDelete = false;
        role.CanUpdate = false;
        _db.AddToZon_Roles(role);
        _db.SaveChanges();

    }

    /// <summary>
    /// Kullanıcı rolünü değiştirme işlemi
    /// </summary>
    /// <param name="username">kullanıcı adı</param>
    /// <param name="_role">rolü</param>
    public static void ChangeRole(string username, string _role)
    {
        ZonDBEntities _db = new ZonDBEntities();

        UserRepositories rep = new UserRepositories();
        var user = rep.GetUser(username);
        user.RoleID = Roles.GetRole(_role).ID;
        rep.UpdateUser(username, user);

    }

    /// <summary>
    /// Tüm rolleri listeleyen fonksiyon
    /// </summary>
    /// <returns>List Zon_Roles</returns>
    public static List<Zon_Roles> GetAllRoles()
    {
        ZonDBEntities _db = new ZonDBEntities();
        return _db.Zon_Roles.ToList();
    }
    /// <summary>
    /// Rol
    /// </summary>
    /// <param name="role">alınacak rol</param>
    /// <returns>Zon_Roles</returns>
    public static Zon_Roles GetRole(string role)
    {
        try
        {
            ZonDBEntities _db = new ZonDBEntities();
            Zon_Roles _role = (from w in _db.Zon_Roles
                                 where w.Role == role
                                 select w).Single();
            return _role;
        }
        catch (Exception)
        {

            throw;
        }
    }
    /// <summary>
    /// Kualnıcının rolünü veren işlem
    /// </summary>
    /// <param name="username">kullanıcı adı</param>
    /// <returns>Zon_Roles</returns>
    public static Zon_Roles GetRoleForUser(string username)
    {
        try
        {
            ZonDBEntities _db = new ZonDBEntities();
            Zon_Roles role = (from w in _db.Zon_Kullanici
                                where w.Email == username
                                select w.Zon_Roles).Single();
            return role;
        }
        catch (Exception)
        {

            throw;
        }
    }
}