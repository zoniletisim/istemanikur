﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
using System.IO;
using System.Collections.Specialized;
using System.Text;
/// <summary>
/// Summary description for UserRepositories
/// </summary>
public class UserRepositories
{
    ZonDBEntities db ;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
	public UserRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}
    public ResultAction ValidateMusteri(string email, string password)
    {
        try
        {
            var musteri = db.Zon_Kullanici.Where(a => a.Email == email && a.Password == password).FirstOrDefault();

            if (musteri!= null)
            {
                _result.result = true;
                _result.data = musteri;

                musteri.SonGiris = DateTime.Now;
                musteri.IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                db.SaveChanges();

                return _result;
            }
            else
            {
                _result.result = false;
                _result.message = "Email veya şifre hatalı";
                return _result;
            }
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }


    public ResultAction AddNewKullanici(string id, string ad, string soyad, string email,string tel, string password, string tarih, string role,string aktif, string mode,int UserType,string bayiAdi)
    {
        try
        {
            Zon_Kullanici u=null;
            if(mode=="add")
                u = new Zon_Kullanici();
            else
            {
                int cId = Convert.ToInt32(id);
                u = db.Zon_Kullanici.Where(z=>z.ID==cId).FirstOrDefault();
            }
                u.Ad = ad;
                u.Soyad = soyad;
                u.Email = email;
                u.Password = password;
                u.Soyad = soyad;
                //u.Tarih = Convert.ToDateTime(tarih);
                u.Aktif = aktif;
                u.IsPanelUser = true;
                if (UserType == 1)
                    u.RoleID = Convert.ToInt32(role);
                else
                    u.RoleID = 3;
                if (UserType == 1)//1 yönetici grubu, 2 bayi grubu
                    u.GrupID = 1;
                else
                    u.GrupID = 2;

                u.Onay = true;
                u.IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                u.Telefon = tel;

                if (mode.Equals("add"))
                {

                    u.Tarih = DateTime.Now;
                    db.AddToZon_Kullanici(u);
                }
                db.SaveChanges();

                _result.result = true;
                _result.id = u.ID;

                return _result;
            
            
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public ResultAction saveFriends(int userId, string email,string icerik)
    {
        try
        {
            if (!emailsKontrol(email, userId))
            {
                Zon_ArkadasinaOner a = new Zon_ArkadasinaOner();
                a.ArkadasEmail = email;
                a.KullaniciId = userId;
                a.GonderimTarih = DateTime.Now;
                a.Icerik = icerik;
                a.UyeOlduMu = "0";
                a.Hash = RandomString(24);
                db.AddToZon_ArkadasinaOner(a);
                db.SaveChanges();
                Mailer.postFriendsInvitationMail(email, userId,a.Hash);


                _result.data = a;
                _result.result = true;
                return _result;
            }
            else
            {
                _result.result = false;
                _result.message = email+" adresi sistemimize kayıtlı.";
                return _result;
            }
            

        }
        catch (Exception ex)
        {

            _result.result = false;
            _result.message = "İşlem sırasında hata oluştu.";
            logger.Error(ex);
            return _result;

        }
    }

    private bool emailsKontrol(string email,int userId)
    {
        bool ret = false;

        Zon_Kullanici user = db.Zon_Kullanici.Where(z => z.Email == email.Trim()).FirstOrDefault();
        Zon_Kullanici sessionUser=db.Zon_Kullanici.Where(z=>z.Email==email && z.ID==userId).FirstOrDefault();
        Zon_ArkadasinaOner o = db.Zon_ArkadasinaOner.Where(z => z.ArkadasEmail == email && z.KullaniciId == userId).FirstOrDefault();
        if (user != null && sessionUser != null)
            ret = true;
        if (o != null)
            ret = true;

        return ret;
    }

    public List<Zon_Kullanici> GetAllUsers()
    {
        try
        {
            return db.Zon_Kullanici.Where(a => (bool)a.IsPanelUser).ToList();
        }
        catch (Exception)
        {
            throw;
        }
    }
    public List<Zon_Kullanici> GetMusteriList()
    {
        try
        {
            return db.Zon_Kullanici.ToList();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static Zon_Roles GetRole(string role)
    {
        try
        {
            ZonDBEntities db = new ZonDBEntities();
            Zon_Roles _role = (from w in db.Zon_Roles
                                  where w.Role == role
                                  select w).Single();
            return _role;

        }
        catch (Exception)
        {

            throw;
        }
    }

    public bool canDelete(string username)
    {
        try
        {
            var userrole = Roles.GetRoleForUser(username);

            bool isDelete = false;
            if ((bool)userrole.CanDelete)
            {
                isDelete = true;
            }

            return isDelete;
        }
        catch (Exception)
        {
            throw;
        }
    }


    /// <summary>
    /// Kullanıcının güncelleme iznini kontrol eder
    /// </summary>
    /// <param name="username">kullanıcı adı</param>
    /// <returns>bool</returns>
    public bool canUpdate(string username)
    {
        try
        {
            var userrole = Roles.GetRoleForUser(username);

            bool isDelete = false;
            if ((bool)userrole.CanUpdate)
            {
                isDelete = true;
            }

            return isDelete;
        }
        catch (Exception)
        {
            throw;
        }
    }
    /// <summary>
    /// Kullanıcının yeni kayıt ekleme iznini kontrol eder
    /// </summary>
    /// <param name="username">kullanıcı adı</param>
    /// <returns>bool</returns>
    public bool canInsert(string username)
    {
        try
        {
            var userrole = Roles.GetRoleForUser(username);

            bool isDelete = false;
            if ((bool)userrole.CanInsert)
            {
                isDelete = true;
            }

            return isDelete;
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Role izin durumu ekle
    /// </summary>
    /// <param name="role">role</param>
    /// <param name="canDelete">silme izni</param>
    /// <param name="canUpdate">güncelleme izni</param>
    /// <param name="canInsert">ekleme izni</param>
    /// <returns>ResultAction</returns>
    public ResultAction UpdateRolePermission(string role, bool canDelete, bool canUpdate, bool canInsert)
    {
        try
        {
            var varmi = db.Zon_Roles.Where(a => a.Role == role).Single();

            varmi.CanDelete = canDelete;
            varmi.CanInsert = canInsert;
            varmi.CanUpdate = canUpdate;

            db.SaveChanges();
            _result.result = true;
            _result.id = varmi.ID;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    /// <summary>
    /// Kullanıcı güncelleme
    /// </summary>
    /// <param name="username">kullanıcı adı (email)</param>
    /// <param name="member">kullanıcı bilgisi</param>
    /// <returns>ResultAction</returns>
    public ResultAction UpdateUser(string username, Zon_Kullanici member)
    {
        try
        {

            var user = db.Zon_Kullanici.Where(a => a.Email == username).Single();
            user = member;
            db.SaveChanges();

            _result.result = true;

            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }
    public Zon_Kullanici GetUser(string username)
    {
        try
        {
            return db.Zon_Kullanici.Where(a => a.Email == username).Single();
        }
        catch (Exception)
        {

            throw;
        }
    }

    public Zon_Kullanici GetUser(int id)
    {
        try
        {
            return db.Zon_Kullanici.Where(a => a.ID == id).FirstOrDefault();
        }
        catch (Exception)
        {

            throw;
        }
    }

    public ResultAction deleteRowUser(int id)
    {
        try
        {
            var s = db.Zon_Kullanici.Where(z => z.ID == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
    public ResultAction ActionMusteri(string id, string ad,string soyad, string email, string tel, string sirket, string ofis,string unvan,string dogumTarih,string ozelTarih,string cocukDurum,string NeredeManikur,string renkTercih,string markaTercih,string tirnakTeshis,string notlar, string aktif, string mode,string tckn,string onay)
    {
        try
        {
            Zon_Kullanici u = null;
            if (mode == "add")
                u = new Zon_Kullanici();
            else
            {
                int cId = Convert.ToInt32(id);
                u = db.Zon_Kullanici.Where(z => z.ID == cId).FirstOrDefault();
            }
            u.Ad = ad;
            u.Soyad = soyad;
            u.Email = email;
            u.Telefon = tel;
            if(!string.IsNullOrEmpty(sirket))
                u.SirketId = int.Parse(sirket);
            if (!string.IsNullOrEmpty(ofis))
                u.OfisId = int.Parse(ofis);
            u.Unvan = unvan;
            if (!string.IsNullOrEmpty(dogumTarih))
                u.DogumTarihi =DateTime.Parse(dogumTarih);
            u.OzelTarih= ozelTarih;
            u.CocukDurum = cocukDurum;
            u.NeredeManikur = NeredeManikur;
            u.RenkTercih = renkTercih;
            u.MarkaTercih = markaTercih;
            u.TirnakTeshis = tirnakTeshis;
            u.Notlar = notlar;
            u.TcNo = tckn;
            u.Aktif = aktif;
            if (!string.IsNullOrEmpty(onay))
                u.Onay = bool.Parse(onay);

            if (mode.Equals("add"))
                db.AddToZon_Kullanici(u);

            db.SaveChanges();

            _result.result = true;
            _result.id = u.ID;

            return _result;


        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public ResultAction CheckUser(string userName, string Password) ///uye/UyeGirisi.aspx
    {
        try
        {
            var user = (from item in db.Zon_Kullanici
                        where item.Email == userName && item.Password == Password 
                        select item).FirstOrDefault();

            if (user != null)
            {
                if (user.Onay == true)
                {
                    _result.result = true;
                    _result.data = user;
                }
                else
                {
                    KullaniciAktivasyon(user.ID);
                    _result.result = false;
                    _result.message = "<b> Hesabınız aktif durumda değil.</b><br/><br/> Hesabınızı aktif hale getirmek için email <br /> adresinize gelen aktivasyon mailindeki linke tıklayınız.";

                }
            }
            else
            {
                _result.result = false;
                _result.message = "Kullanıcı Adı veya Şifreniz Hatalı. ";
            }

            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = "Sunucularımızda hata meydana geldi <br /> daha sonra tekrar deneyin.";
            logger.Error(ex);
            return _result;
        }

    }

    public ResultAction rePassword(string userName)
    {
        string hash = "";
        try
        {
            var user = (from item in db.Zon_Kullanici
                        where item.Email == userName 
                        select item).FirstOrDefault();

            if (user != null)
            {

              hash=RandomString(128);
              user.emailAktivasyon = hash;
              db.SaveChanges();
              Mailer.postForgotPass(user.Email,hash);
                _result.result = true;
                _result.data = user;
                _result.message = "Şifre değiştirme linkiniz email adresinize gönderilecektir.";
            }
            else
            {
                _result.result = false;
                _result.message = "Email adresi hatalı.";
            }

            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = "Sunucularımızda hata meydana geldi <br /> daha sonra tekrar deneyin.";
            logger.Error(ex);
            return _result;
        }

    }

    public string RandomString(int size)
    {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        char ch;
        for (int i = 0; i < size; i++)
        {
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
            builder.Append(ch);
        }
        return builder.ToString();
    }

    public void userLastLoginUpdate(string userName, string Password)///uye/UyeGirisi.aspx
    {
        try
        {
            var user = (from item in db.Zon_Kullanici
                        where item.Email == userName && item.Password == Password
                        select item).FirstOrDefault();

            if (user != null)
            {
                user.SonGiris = DateTime.Now;
                user.IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                db.SaveChanges();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);

        }

    }

    public ResultAction emailKontrol(string email)
    {
        try
        {
            var u =db.Zon_Kullanici.Where(z => z.Email == email).FirstOrDefault();
            if (u != null)
                _result.result = false;
            else
                _result.result = true;

            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            logger.Error(ex);
            return _result;

        }
    }

    public ResultAction saveUser(NameValueCollection postForm)
    {
        try
        {
            string ad = postForm["ctl00$ContentPlaceHolder1$txtAd"];
            string soyad = postForm["ctl00$ContentPlaceHolder1$txtSoyad"];
            string email = postForm["ctl00$ContentPlaceHolder1$txtEmail"];
            string sifre = postForm["ctl00$ContentPlaceHolder1$txtSifre"];
            string tel = postForm["ctl00$ContentPlaceHolder1$txtTel"];
            string sirket = postForm["ctl00$ContentPlaceHolder1$ddlSirket"];
            string ofis = postForm["ctl00$ContentPlaceHolder1$ddlOfis"];
            string musteriSirket = postForm["ctl00$ContentPlaceHolder1$txtSirket"];
            string KampanyaEmail = postForm["ctl00$ContentPlaceHolder1$chkBilgi"];

            var checkMail = emailKontrol(email);
            if (checkMail.result)
            {
                Zon_Kullanici m = new Zon_Kullanici();
                m.Ad = ad;
                m.Soyad = soyad;
                m.Email = email;
                m.Password = sifre;
                m.Telefon = tel;
                if (!string.IsNullOrEmpty(sirket))
                    m.SirketId = int.Parse(sirket);
                if (!string.IsNullOrEmpty(ofis))
                     m.OfisId = int.Parse(ofis);
                m.RoleID = 3;
                m.Aktif = "1";
                m.Onay = false;
                m.SonGiris = DateTime.Now;
                m.Tarih = DateTime.Now;
                if(KampanyaEmail=="on")
                    m.KampanyaEmail = true;
                else
                    m.KampanyaEmail = false;

                db.AddToZon_Kullanici(m);
                db.SaveChanges();
                _result.result = true;
                _result.data = m;
                _result.id = m.ID;

                if (!string.IsNullOrEmpty(musteriSirket) && sirket == "999")
                {
                    Zon_Musteri_Sirket s = new Zon_Musteri_Sirket();
                    s.SirketAd = musteriSirket;
                    s.Tarih = DateTime.Now;
                    s.KullaniciId = m.ID;
                    db.AddToZon_Musteri_Sirket(s);
                    db.SaveChanges();
                }
            }
            else
            {
                _result.result = false;
                _result.message = "Bu mail kullanılıyor.";
            }
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = "Beklenmedik bir hata oluştu.";
            logger.Error(ex);
            return _result;
        }

    }

    public ResultAction UserUpdate(string ad, string soyad, string email, string tel, string dogumtarihi, string cinsiyet, string sirket, string ofis, string sessionEmail)// /uye/UyelikBilgileri.aspx
    {
        try
        {
            var user = (from item in db.Zon_Kullanici
                        where item.Email == sessionEmail
                        select item).FirstOrDefault();

            if (user != null)
            {
                user.Ad = ad;
                user.Soyad = soyad;
                user.Email = email;
                user.Telefon = tel;
                if (dogumtarihi != "")
                    user.DogumTarihi = Convert.ToDateTime(dogumtarihi);
                user.Cinsiyet = cinsiyet;
                //user.SirketId =int.Parse(sirket);
                //user.OfisId = int.Parse(ofis);
                db.SaveChanges();
                _result.data = GetUser(email);
                _result.result = true;

            }
            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public ResultAction changePassWord(string email, string oldPass, string newPass)
    {


        try
        {
            var user = (from item in db.Zon_Kullanici
                        where item.Email == email && item.Password == oldPass
                        select item).FirstOrDefault();

            if (user != null)
            {
                user.Password = newPass;
                db.SaveChanges();
                _result.result = true;
            }
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    } // uye/Sfredegistir.aspx

    public List<Zon_Musteri_Adresleri> GetAdressList(int userId)
    {
        try
        {
            return db.Zon_Musteri_Adresleri.Where(a => a.MusteriID == userId).ToList();
        }
        catch (Exception)
        {

            throw;
        }

    }

    public Zon_Musteri_Adresleri GetAdress(int userId, string aId)
    {
        try
        {
            int adresid = Convert.ToInt32(aId);
            return db.Zon_Musteri_Adresleri.Where(a => a.MusteriID == userId && a.ID == adresid).FirstOrDefault();

        }
        catch (Exception)
        {

            throw;
        }

    }

    public ResultAction deleteUserAdres(int userID, int adresId)
    {
        try
        {
            var adres = db.Zon_Musteri_Adresleri.Where(a => a.MusteriID == userID && a.ID == adresId).FirstOrDefault();
            if (adres != null)
            {
                db.DeleteObject(adres);
                db.SaveChanges();
                _result.result = true;
                _result.message = "Kayıt Silindi.";
            }
            else
            {
                _result.result = false;
                _result.message = "Kayıt Bulunamadı.";
            }
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = "Kayıt silinirken hata oluştu lütfen daha sonra deneyin.";
            logger.Error(ex);
            return _result;
        }

    }

    public ResultAction UserAdres(string email, string adresTip, string adresTanim, string adres, string postaKodu, string sehir, string ilce,
       string adresID, string islemTip, string faturaTip, string kimlikNo, string firmaUnvan, string vergiDaire, string vergiNo, string cepTel, string sabitTel)
    {
        try
        {
            int adresid = Convert.ToInt32(adresID);
            Zon_Musteri_Adresleri a = null;
            var user = db.Zon_Kullanici.Where(x => x.Email == email).FirstOrDefault();
            if (islemTip == "update")
                a = db.Zon_Musteri_Adresleri.Where(z => z.MusteriID == user.ID && z.ID == adresid).FirstOrDefault();
            else
                a = new Zon_Musteri_Adresleri();

            if (user != null)
            {
                a.MusteriID = user.ID;
                a.AdresTipi = Convert.ToInt32(adresTip);
                a.Adres = adres;
                a.PostaKodu = postaKodu;
                a.Tanim = adresTanim;
                a.Sehir = sehir;
                a.Ilce = ilce;
                a.TCNo = kimlikNo;
                a.VergiDairesi = vergiDaire;
                a.VergiNo = vergiNo;
                a.FirmaUnvan = firmaUnvan;
                a.FaturaTip = faturaTip;
                a.TelSabit = sabitTel;
                a.TelCep = cepTel;
                if (islemTip == "insert")
                    db.AddToZon_Musteri_Adresleri(a);
                db.SaveChanges();
                _result.result = true;
            }
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }


    public ResultAction KullaniciAktivasyon(int id)
    {
        string hash = "";
        try
        {
            var user = (from item in db.Zon_Kullanici
                        where item.ID==id
                        select item).FirstOrDefault();

            if (user != null)
            {

                hash = RandomString(128);
                user.AktivasyonKey = hash;
                db.SaveChanges();
                Mailer.postAktivasyonMail(user.Email, hash);
                _result.result = true;
                _result.data = user;
            }
            else
            {
                _result.result = false;
                _result.message = "Kullanıcı Adı veya Şifreniz Hatalı.";
            }

            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = "Sunucularımızda hata meydana geldi, daha sonra tekrar deneyin.";
            logger.Error(ex);
            return _result;
        }

    }

    public ResultAction AktivasyonKontrol(string aktivasyon)
    {

        try
        {
            Zon_Kullanici u= db.Zon_Kullanici.Where(z => z.AktivasyonKey == aktivasyon).FirstOrDefault();
            if (u != null)
            {
                u.Onay = true;
                db.SaveChanges();
                _result.result = true;
                _result.data = u;
                
            }
            else
            {
                _result.result = false;
                _result.message = "Hata Oluştu.";
            }

            return _result;
        }
        catch (Exception ex)
        {

            _result.result = false;
            _result.message = "Sunucularımızda hata meydana geldi<br/> daha sonra tekrar deneyin.";
            logger.Error(ex);
            return _result;
        }
    }

    public List<cctran> sanalPosList()
    {
       
        return db.cctrans.OrderByDescending(z=>z.date).ToList();

    }

    public List<Zon_ArkadasinaOner> getFriendsInvitationList(int p)
    {
        return db.Zon_ArkadasinaOner.Where(z => z.KullaniciId == p).ToList();
    }

    public void updateUserFriends(string p)
    {
        HediyeCekRepositories cek = new HediyeCekRepositories();
        try
        {
            Zon_ArkadasinaOner a = new Zon_ArkadasinaOner();
            if (!string.IsNullOrEmpty(p))
            {
                a = db.Zon_ArkadasinaOner.Where(z => z.Hash == p).FirstOrDefault();
                a.UyeOlduMu = "1";
                a.KayitTarih = DateTime.Now;
                db.SaveChanges();
            }

            Zon_Kullanici u = db.Zon_Kullanici.Where(z => z.ID == a.Id).FirstOrDefault();

            int friendsCount = db.Zon_ArkadasinaOner.Where(z => z.KullaniciId == u.ID && z.UyeOlduMu=="1").Count();
            if (friendsCount % 3==0)
            {
              cek.AddNewHediyeCeki("Davet ettiğin arkadaşların hediye çeki kazandırdı.", DateTime.Now.ToString(), DateTime.Now.AddMonths(3).ToString(), "15", "0", u.ID, DateTime.Now.Ticks.ToString(), (int)Constants.HediyeCekTip.arkadaslarinaOner,0,"0");
            }



        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    public List<Zon_Kullanici> DogumGunuOlanlar()
    {

        List<Zon_Kullanici> user = new List<Zon_Kullanici>();

        try
        {
            user = db.Zon_Kullanici.Where(z => z.DogumTarihi.Value.Month == DateTime.Now.Month && z.Aktif=="1").ToList();
            return user;
        }
        catch (Exception ex)
        {
            
            throw;
        }

    }
}