﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
using System.Transactions;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for RandevuRepositories
/// </summary>
public class RandevuRepositories
{
	ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
    public RandevuRepositories()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}

    public ResultAction ActionRandevu(string id, string sirket, string ofis, List<string> hizmet, string tarih, string mode, string aktif, string aciklama, List<int> saatList,string indirim,string tolerans)
    {
        try
        {
            Zon_Randevu s = null;
            using (TransactionScope trans = new TransactionScope())
            {
                if (mode.Equals("add"))
                {
                    s = new Zon_Randevu();
                }
                else
                {
                    int cId = Convert.ToInt32(id);
                    s = db.Zon_Randevu.Where(z => z.Id == cId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(sirket))
                    s.SirketId = int.Parse(sirket);
                if (!string.IsNullOrEmpty(ofis))
                    s.OfisId = int.Parse(ofis);
                if (!string.IsNullOrEmpty(tarih))
                    s.Tarih = DateTime.Parse(tarih);

                s.Aciklama = aciklama;
                s.KayitTarih = DateTime.Now;
                s.Aktif = aktif;
                if(!string.IsNullOrEmpty(tolerans))
                    s.SaatSablonId = int.Parse(tolerans);
                else
                    s.SaatSablonId =0;


                if (mode == "add")
                    db.AddToZon_Randevu(s);

                db.SaveChanges();

                if (hizmet.Count > 0)
                {
                    List<Zon_Randevu_Hizmet> randevu=db.Zon_Randevu_Hizmet.Where(z=>z.RandevuId==s.Id).ToList();
                    if (randevu.Count > 0)
                    {
                        foreach(var r in randevu)
                            db.DeleteObject(r);

                        db.SaveChanges();
                    }
                    foreach (var list in hizmet)
                    {
                      Zon_Randevu_Hizmet  r = new Zon_Randevu_Hizmet();
                      r.HizmetId =int.Parse(list);
                      r.RandevuId = s.Id;
                      db.AddToZon_Randevu_Hizmet(r);
                      db.SaveChanges();
                    }

                }
                
                if (mode == "update")
                {
                    var saatListDb = db.Zon_Randevu_Saat.Where(z => z.RandevuId == s.Id).Select(z=>z.SaatId).ToList();
                    int varmi = 0;



                    if (saatListDb.Count > 0)
                    {
                        foreach (var saat in saatList) // listede var db de yok ise ekle
                        {
                            varmi = 0;

                            if (saatListDb.Contains(saat))
                                varmi = 1;

                            if (varmi == 0)
                                saveSaatUpdate(s.Id, saat);

                        }

                        foreach (var saatdb in saatListDb) // db de var listede yok ise sil
                        {
                            varmi = 0;

                            if (saatList.Contains(saatdb.Value))
                                varmi = 1;

                            if (varmi == 0)
                            {
                                db.ExecuteStoreCommand("delete from Zon_Randevu_Saat where RandevuId= " + s.Id + " and  SaatId=" + saatdb.Value);
                                var sepet = db.Zon_Sepet.Where(z => z.UrunId == s.Id && z.SaatId == saatdb).ToList();
                                if (sepet != null)
                                    db.ExecuteStoreCommand("delete from Zon_Sepet where UrunId= " + s.Id + " and  SaatId=" + saatdb.Value);


                                db.SaveChanges();
                            }

                        }
                    }

                    else
                    { 
                        if(saatList.Count>0)
                            saveSaatInsert(s.Id, saatList);
                    }
                }
                
                else
                    saveSaatInsert(s.Id, saatList);


                if (!string.IsNullOrEmpty(indirim) && indirim != "0")
                {

                    Zon_Indirim indirimVarmi = db.Zon_Indirim.Where(z => z.RandevuId == s.Id).FirstOrDefault();
                    if (indirimVarmi != null)
                        indirimVarmi.MiktarYuzde = int.Parse(indirim);
                    else
                    {
                        Zon_Indirim i = new Zon_Indirim();
                        i.MiktarYuzde = int.Parse(indirim);
                        i.RandevuId = s.Id;
                        i.Aktif = "a";
                        db.AddToZon_Indirim(i);
                    }
                    db.SaveChanges();
                }
                if (string.IsNullOrEmpty(indirim) || indirim == "0")
                { 
                    if(mode=="update")
                    {
                        Zon_Indirim i = db.Zon_Indirim.Where(z => z.RandevuId == s.Id).FirstOrDefault();
                        if (i != null)
                        {
                            db.DeleteObject(i);
                            db.SaveChanges();
                        }   


                    }
                }


                trans.Complete();
            }
            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }
    
    private void saveSaatUpdate(int id, int items)
    {
        try
        {
                Zon_Randevu_Saat o = new Zon_Randevu_Saat();
                o.RandevuId = id;
                o.SaatId = items;
                db.AddToZon_Randevu_Saat(o);
                db.SaveChanges();
          
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);


        }

    }
    
    private void saveSaatInsert(int id, List<int> items)
    {
        try
        {
            foreach (var item in items)
            {
                Zon_Randevu_Saat o = new Zon_Randevu_Saat();
                o.RandevuId = id;
                o.SaatId = item;
                db.AddToZon_Randevu_Saat(o);
                db.SaveChanges();
            }
           

        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);


        }
    }
    
    public Zon_Randevu GetRandevu(int pId)
    {
        return db.Zon_Randevu.Where(z => z.Id == pId).FirstOrDefault();
    }

    public List<Zon_Randevu> GetRandevuList()
    {
        return db.Zon_Randevu.Where(z => z.Aktif == "1").ToList();
    }

    public ResultAction deleteRowRandevu(int id)
    {
        try
        {
            var s = db.Zon_Randevu.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }

    public List<object> categories(TreeView t1,string mode,string rndId)
    {
        string durum = "";
        int randevuid = 0;
        List<object> liste = new List<object>();
        try
        {
            randevuid = int.Parse(rndId);

            var cats = db.Zon_Rezerve_Saat.Where(z => z.Aktif == "1").OrderBy(z => z.BaslangicSaat).ToList();
           

            foreach (var item in cats)
            {
                var rezeryasyon = db.Zon_Rezerve.Where(z => z.UrunId == randevuid && z.SaatId==item.Id).FirstOrDefault();
                if (rezeryasyon != null)
                    durum = "<div style='width:300px'><div style='float:left;width:70px;padding:6px 0 0 6px'>" + string.Format("{0:t}", item.BaslangicSaat) + "-" + string.Format("{0:t}", item.BitisSaat) + "</div>" + "<a onclick='return bosGoster();' id='bosGoster' href='Randevu.aspx?action=saatBos&pid=" + rndId + "&saatId=" + item.Id + "' style='float:left;width:70px;padding:6px 0 0 6px;color:red'  class='aktif'>Boş Göster</a></div>";
                else
                    durum = "<div style='width:300px'><div style='float:left;width:70px;padding:6px 0 0 6px'>" + string.Format("{0:t}", item.BaslangicSaat) + "-" + string.Format("{0:t}", item.BitisSaat) + "</div>" + "<a onclick='return doluGoster();' id='doluGoster' href='Randevu.aspx?action=saatDolu&pid=" + rndId + "&saatId=" + item.Id + "' style='float:left;width:70px;padding:6px 0 0 6px' class='aktif'>Dolu Göster</a></div>";


                TreeNode node = null;
                if (mode == "update")
                    node = new TreeNode(durum);
                else
                    node = new TreeNode("<div style='width:300px'><div style='float:left;width:70px;padding:6px 0 0 6px'>" + string.Format("{0:t}", item.BaslangicSaat) + "-" + string.Format("{0:t}", item.BitisSaat) + "</div>");
                node.ShowCheckBox = true;
                node.SelectAction = TreeNodeSelectAction.None;
                node.Value = item.Id.ToString();
                node.Expanded = true;

                t1.Nodes.Add(node);
                //sub(item.Id, node);

            }
            
            return liste;
        }
        catch (Exception ex)
        {

            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return liste;
        }
        
    }

    public Zon_Indirim getRandevuIndirim(string rId)
    {
        int id = int.Parse(rId);

        return db.Zon_Indirim.Where(z => z.RandevuId == id).FirstOrDefault();

    }

    //####################################################################################################################/

    #region Randevu Seçimi

    public  Dictionary<string, string> GetSirket()
    {
        int temp = 0;
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<string, string> sirketList = new Dictionary<string, string>();
        //var query = db.Zon_Sirket.Where(z => z.Aktif == "1" && z.Tip==1).OrderBy(z => z.SiretAd).ToList();
        var q2 = (from s in db.Zon_Sirket
                  join r in db.Zon_Randevu on s.Id equals r.SirketId
                  where s.Aktif=="1" && s.Tip==1 && r.Aktif=="1"
                  orderby s.SiretAd
                  select s).ToList();

        if (q2.Count > 0)
        {
            foreach (var i in q2)
            {
                if (temp != i.Id)
                {
                    temp = i.Id;
                    sirketList.Add(i.Id.ToString(), i.SiretAd.ToString());
                }
            };
        }
        return sirketList;
    }
    public Dictionary<string, string> GetSirketUyelik()
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<string, string> sirketList = new Dictionary<string, string>();
        var query = db.Zon_Sirket.Where(z => z.Aktif == "1").OrderBy(z => z.SiretAd).ToList();

        foreach (var i in query)
        {
            sirketList.Add(i.Id.ToString(), i.SiretAd.ToString());
        };
        return sirketList;
    }
    public  Dictionary<string, string> GetOfis(int pSirketId)
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<string, string> ofisList = new Dictionary<string, string>();
        var query = db.Zon_Ofis.Where(z => z.Aktif == "1" && z.SirketId==pSirketId).OrderBy(z => z.OfisAd).ToList();

        foreach (var i in query)
        {
            ofisList.Add(i.Id.ToString(), i.OfisAd.ToString());
        };
        return ofisList;
    }

    public Dictionary<string, string> GetOfis()
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<string, string> ofisList = new Dictionary<string, string>();
        var query = db.Zon_Ofis.Where(z => z.Aktif == "1").OrderBy(z => z.OfisAd).ToList();

        foreach (var i in query)
        {
            ofisList.Add(i.Id.ToString(), i.OfisAd.ToString());
        };
        return ofisList;
    }

    public Dictionary<string, string> GetRandevuTarih(int pSirketId,int pOfisId)
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<string, string> randevulistTarih = new Dictionary<string, string>();
        var query = db.Zon_Randevu.Where(z => z.Aktif == "1" && z.SirketId == pSirketId && z.OfisId==pOfisId).OrderBy(z => z.Tarih).ToList();

        foreach (var i in query)
        {
            randevulistTarih.Add(i.Id.ToString(), string.Format("{0:d MMMM yyyy dddd}", i.Tarih));
        };
        return randevulistTarih;
    }

    public Zon_Randevu rezervasyonId(int sirketId,int ofisId,DateTime tarih)
    {
        Zon_Randevu r = db.Zon_Randevu.Where(z => z.SirketId == sirketId && z.OfisId == ofisId && z.Tarih == tarih && z.Aktif=="1").FirstOrDefault();
        return r;
    }


    public List<Zon_Rezerve_Saat> rezervasyonSaat(int randevuId)
    {

        var saat=(from r in db.Zon_Randevu_Saat 
                      join s in db.Zon_Rezerve_Saat on r.SaatId equals s.Id
                      where r.RandevuId==randevuId
                      select s).OrderBy(z=>z.BaslangicSaat.Value.Hour).ThenBy(z=>z.BaslangicSaat.Value.Minute).ToList();
        List<Zon_Rezerve_Saat> tolereSaat = new List<Zon_Rezerve_Saat>();
        var randevu=db.Zon_Randevu.Where(z=>z.Id==randevuId).FirstOrDefault();


        if (saat.Count > 0)
        {
            if (randevu.SaatSablonId != 0 && randevu.SaatSablonId != null)
            {
                foreach (var s in saat)
                {
                   s.BaslangicSaat= s.BaslangicSaat.Value.AddMinutes((int)randevu.SaatSablonId);
                   s.BitisSaat= s.BitisSaat.Value.AddMinutes((int)randevu.SaatSablonId);

                    tolereSaat.Add(s);
                }
            }
          
        }
        if (randevu.SaatSablonId != 0 && randevu.SaatSablonId != null)
            return tolereSaat.OrderBy(z => z.BaslangicSaat.Value.Hour).ThenBy(z => z.BaslangicSaat.Value.Minute).ToList();
        else
            return saat;
    }
    public List<Zon_Randevu_Hizmet> hizmetTur(int rId)
    {
        return db.Zon_Randevu_Hizmet.Where(z => z.RandevuId==rId).ToList();
    }



    #endregion


    #region Sepete Ekle

    public ResultAction ActionBasket(string sepetId, int uyeId, string sessionId, int randevuId, string saatId , string hizmetId,string mode)
    {
        try
        {
           
            Zon_Sepet s = null;
            using (TransactionScope trans = new TransactionScope())
            {
                 if (mode.Equals("add"))
                {
                    s = new Zon_Sepet();
                }
                else
                {
                    int cId = Convert.ToInt32(sepetId);
                    s = db.Zon_Sepet.Where(z => z.Id == cId).FirstOrDefault();
                }
                s.UyeId = uyeId;
                s.SessionId = sessionId;
                s.Miktar = 1;
                s.UrunId = randevuId;
                if (!string.IsNullOrEmpty(saatId))
                    s.SaatId = int.Parse(saatId);
                if (!string.IsNullOrEmpty(hizmetId))
                    s.HizmetId = int.Parse(hizmetId);

                s.Tarih = DateTime.Now;
                if (mode == "add")
                    db.AddToZon_Sepet(s);

                db.SaveChanges();

                

                trans.Complete();
            }
            _result.result = true;
            _result.id = s.Id;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }

    }
    #endregion


    public SepetModel getSepet(string sessionID)//sepet.aspx
    {
        SepetModel urun = new SepetModel();
        string hizmetIds = "";
        List<View_SepetListe> sepet = new List<View_SepetListe>();
        HediyeCekRepositories hediyeRep = new HediyeCekRepositories();
        try
        {
            string strSQL = "select * from View_SepetListe where SessionId= '" + sessionID + "'";
            urun.sepetListe = db.ExecuteStoreQuery<View_SepetListe>(strSQL).ToList();

            int flag = 0;
            if (urun.sepetListe.Count > 0)
            {
                foreach (var s in urun.sepetListe.OrderBy(z => z.HizmetId))
                {

                    var randevu = db.Zon_Randevu.Where(z => z.Id == s.UrunId).FirstOrDefault();
                    var hizmetIndirim = db.Zon_Hizmet_Indirim.Where(z => z.SirketId == randevu.SirketId).FirstOrDefault();
                    if (hizmetIndirim != null)
                        hizmetIds += s.HizmetId;

                    if (randevu != null)
                    {
                        if (randevu.SaatSablonId != 0 && randevu.SaatSablonId != null)
                        {
                            var saat = db.Zon_Rezerve_Saat.Where(z => z.Id == s.SaatId).FirstOrDefault();
                            if (saat != null)
                            {
                                s.BaslangicSaat = s.BaslangicSaat.Value.AddMinutes((int)randevu.SaatSablonId);
                                s.BitisSaat = s.BitisSaat.Value.AddMinutes((int)randevu.SaatSablonId);
                                sepet.Add(s);
                                flag = 1;
                            }
                        }
                    }
                }
            }


            if (flag == 1)
                urun.sepetListe = sepet;



            //foreach (var s in urun.sepetListe)
            //{ 
            //    RandevuRepositories r=new RandevuRepositories();
            //    int randevuid =r.rezervasyonId(s.SirketId.Value,s.OfisId.Value,s.Tarih.Value).Id;
            //}

            urun.urunToplam = findCartTotal(urun.sepetListe, Constants.EticaretOdemeTipi.PESIN);
            //urun.kargoFiyati = Constants.KargoUcreti;
            //if (urun.urunToplam >= Constants.MinSiparisTutarForKargo)
            //    urun.kargoHesaplananFiyati = 0;
            //else
            //   urun.kargoHesaplananFiyati = urun.kargoFiyati;


            urun.genelToplam = urun.urunToplam;
            urun.genelToplamTaksitli = findCartTotal(urun.sepetListe, Constants.EticaretOdemeTipi.TAKSITLI) + urun.kargoHesaplananFiyati;
            if (HttpContext.Current.Session["hediyeCekKod"] != null)
            {
                string kod = HttpContext.Current.Session["hediyeCekKod"].ToString();
                Zon_HediyeCek cek = (from item in db.Zon_HediyeCek
                                     where item.Kod == kod
                                     select item).FirstOrDefault();
                if (cek != null)
                {
                    if (hediyeRep.isValidTotalHediyeCek(urun.genelToplam, kod))
                    {
                        if (hediyeRep.isValidDateHediyeCek(urun.genelToplam, kod))
                        {

                            urun.hediyeCekTutar = tutarHesapla(urun.urunToplam, cek.TutarTip.Value, (decimal)cek.Tutar);
                            decimal toplam = urun.genelToplam - tutarHesapla(urun.urunToplam,cek.TutarTip.Value,(decimal)cek.Tutar);
                            if (toplam > 0)
                                urun.genelToplam = urun.genelToplam - tutarHesapla(urun.urunToplam, cek.TutarTip.Value, (decimal)cek.Tutar);
                            else
                                urun.genelToplam = 0;
                            HttpContext.Current.Session["hediyeCekKod"] = cek.Kod;
                        }
                        else
                        {
                            HttpContext.Current.Session["hediyeCekKod"] = null;
                        }
                    }
                    else
                    {
                        //ViewState["Error"] = "Bu Hediye Çeki " + cek.MinTutar + " TL ve üzeri alışverişlerde geçerlidir. Hediye çekinizi kullanabilmeniz için sepetinize " + (cek.MinTutar - urun.genelToplam) + " TL'lik ürün ekleyebilirsniz.";
                        HttpContext.Current.Session["hediyeCekKod"] = null;
                    }
                }
            }
            if (urun.sepetListe.Count > 1) // sepette iki hizmet varsa indirim için kontrol et
            {
                HttpContext.Current.Session["hizmetIndirimTutar"] = null;
                getHizmetIndirimKontrol(hizmetIds, urun, db);
                
            }

            return urun;
        }
        catch (Exception ex)
        {

            return urun;
        }
        
    }

    public static bool getHizmetIndirimKontrol(string hizmetIds, SepetModel urun,ZonDBEntities db)
    {
        bool ret = false;
        List<Zon_Hizmet_Indirim> indirim = db.Zon_Hizmet_Indirim.ToList();
        string hizmetIndirimId = "";
        foreach (var i in indirim)
        {
            if (i.Hizmet1Id > i.Hizmet2Id)
                hizmetIndirimId = i.Hizmet2Id.ToString() + i.Hizmet1Id.ToString();
            else if (i.Hizmet2Id > i.Hizmet1Id)
                hizmetIndirimId = i.Hizmet1Id.ToString() + i.Hizmet2Id.ToString();
            else
                hizmetIndirimId = i.Hizmet1Id.ToString() + i.Hizmet2Id.ToString();

            if (hizmetIndirimId == hizmetIds)
            {
                ret = true;
                decimal toplam = urun.genelToplam - tutarHesapla(urun.genelToplam, i.IndirimTip.Value, i.Indirim.Value);
                HttpContext.Current.Session["hizmetIndirimTutar"] = urun.genelToplam - toplam;
                if (toplam > 0)
                    urun.genelToplam = toplam;
                else
                    urun.genelToplam = 0;

               
            }
        }

        return ret;
    }

    public static decimal tutarHesapla(decimal toplam,int tip,decimal indirim)
    {
        if (tip == 1)
            toplam = (toplam * indirim) / 100;

        else
            toplam = indirim;

        return toplam;

    }

    public decimal findCartTotal(List<View_SepetListe> list, Constants.EticaretOdemeTipi odemeTip)
    {
        decimal total = 0;
        foreach (View_SepetListe r in list)
        {
            decimal fiyat = 0; 
           
                if (odemeTip == Constants.EticaretOdemeTipi.PESIN)
                {
                    if (r.MiktarYuzde != null)
                    {
                        fiyat = (r.Fiyat.Value*(100-r.MiktarYuzde.Value))/100;
                        fiyat = Math.Ceiling(fiyat);
                    }
                    else
                        fiyat = r.Fiyat.Value;

                }
                else if (odemeTip == Constants.EticaretOdemeTipi.TAKSITLI)
                    fiyat = r.Fiyat.Value;
                else
                    throw new Exception("Ödeme tipi desteklenmiyor.");


           
           

            total += fiyat;
        }

        total = Math.Round(total, 2);
        
        return total;
    }

    public ResultAction SepetSil(string id)
    {
        try
        {
            int pid = 0;
            if (id != null || id != "")
                pid = Convert.ToInt32(id);

            var urun = db.Zon_Sepet.Where(a => a.Id == pid).FirstOrDefault();

            db.DeleteObject(urun);
            db.SaveChanges();

            _result.result = true;

            return _result;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            _result.message = ex.Message;
            _result.result = false;
            return _result;

        }
    }


    #region Hizmet İndirim

    public Zon_Hizmet_Indirim getHizmetIndirim(int id)
    {
        return db.Zon_Hizmet_Indirim.Where(z => z.Id == id).FirstOrDefault();
    }


    public static string getSirketAd(int id)
    {
        ZonDBEntities db = new ZonDBEntities();
        return db.Zon_Sirket.Where(z => z.Id == id).FirstOrDefault().SiretAd;
    }

    public static string getHizmetAd(int id)
    {
        ZonDBEntities db = new ZonDBEntities();
        return db.Zon_HizmetTur.Where(z => z.Id == id).FirstOrDefault().HizmetAd;
    }

    public ResultAction actionHizmetIndirim(string id,string mode,string sirket,string hizmet1,string hizmet2,string indirim,string indirimTip,string aktif)
    {
        try
        {
            Zon_Hizmet_Indirim s = null;
            
            if (mode.Equals("add"))
            {
                s = new Zon_Hizmet_Indirim();
            }
            else
            {
                int cId = int.Parse(id);
                s = db.Zon_Hizmet_Indirim.Where(z => z.Id == cId).FirstOrDefault();
            }

            if (!string.IsNullOrEmpty(sirket))
                s.SirketId = int.Parse(sirket);
            if (!string.IsNullOrEmpty(hizmet1))
                s.Hizmet1Id = int.Parse(hizmet1);
            if (!string.IsNullOrEmpty(hizmet2))
                s.Hizmet2Id = int.Parse(hizmet2);
            if (!string.IsNullOrEmpty(indirim))
                s.Indirim = decimal.Parse(indirim);

            s.IndirimTip =int.Parse(indirimTip);
           
            s.Aktif = aktif;

            if (mode == "add")
                db.AddToZon_Hizmet_Indirim(s);

            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            _result.result = false;
            return _result;
        }


    }

    public ResultAction deleteRowIndirim(int id)
    {
        try
        {
            var s = db.Zon_Hizmet_Indirim.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
    #endregion

}