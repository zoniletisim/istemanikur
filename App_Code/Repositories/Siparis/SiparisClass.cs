﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
using System.Transactions;

/// <summary>
/// Summary description for SiparisClass
/// </summary>
public class SiparisClass
{
	
	ZonDBEntities db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
    public SiparisClass()
	{
        db = new ZonDBEntities();
        _result = new ResultAction();
	}

    //public ResultAction ActionRandevu(string id, string sirket, string ofis,List<string> hizmet,string tarih, string mode,string aktif)
    //{
    //    try
    //    {
    //        Zon_Randevu s = null;
    //        using (TransactionScope trans = new TransactionScope())
    //        {
    //            if (mode.Equals("add"))
    //            {
    //                s = new Zon_Randevu();
    //            }
    //            else
    //            {
    //                int cId = Convert.ToInt32(id);
    //                s = db.Zon_Randevu.Where(z => z.Id == cId).FirstOrDefault();
    //            }

    //            if (!string.IsNullOrEmpty(sirket))
    //                s.SirketId = int.Parse(sirket);
    //            if (!string.IsNullOrEmpty(ofis))
    //                s.OfisId = int.Parse(ofis);
    //            if (!string.IsNullOrEmpty(tarih))
    //                s.Tarih = DateTime.Parse(tarih);

    //            s.KayitTarih = DateTime.Now;
    //            s.Aktif = aktif;
    //            if (mode == "add")
    //                db.AddToZon_Randevu(s);

    //            db.SaveChanges();

    //            if (hizmet.Count > 0)
    //            {
    //                List<Zon_Randevu_Hizmet> randevu=db.Zon_Randevu_Hizmet.Where(z=>z.RandevuId==s.Id).ToList();
    //                if (randevu.Count > 0)
    //                {
    //                    foreach(var r in randevu)
    //                        db.DeleteObject(r);
    //                    db.SaveChanges();
    //                }
    //                foreach (var list in hizmet)
    //                {
    //                  Zon_Randevu_Hizmet  r = new Zon_Randevu_Hizmet();
    //                  r.HizmetId =int.Parse(list);
    //                  r.RandevuId = s.Id;
    //                  db.AddToZon_Randevu_Hizmet(r);
    //                  db.SaveChanges();
    //                }

    //            }
              
    //            trans.Complete();
    //        }
    //        _result.result = true;
    //        _result.id = s.Id;
    //        return _result;
    //    }
    //    catch (Exception ex)
    //    {
    //        _result.result = false;
    //        _result.message = ex.Message;
    //        logger.Error(ex);
    //        return _result;
    //    }

    //}

    public Zon_Siparis GetSiparis(int pId)
    {
        return db.Zon_Siparis.Where(z => z.ID == pId).FirstOrDefault();
    }

    public List<Zon_Siparis_Detay> GetSiparisDetay(int pId)
    {
        return db.Zon_Siparis_Detay.Where(z => z.SiparisID == pId).ToList();
    }
    public List<View_Siparis> GetSiparisDetayView(int pId)
    {
        return db.View_Siparis.ToList();
    }
    public ResultAction deleteRowRandevu(int id)
    {
        try
        {
            var s = db.Zon_Randevu.Where(z => z.Id == id).FirstOrDefault();
            s.Aktif = "3";
            db.SaveChanges();

            _result.result = true;
            return _result;
        }
        catch (Exception ex)
        {
            _result.result = false;
            _result.message = ex.Message;
            logger.Error(ex);
            return _result;
        }
    }
}