﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Globalization;
/// <summary>
/// Summary description for BasePageFront
/// </summary>
public class BasePageFront:Page
{
	public BasePageFront()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public const string LanguageDropDownName = "ctl00$ddlLanguage";
    public const string PostBackEventTarget = "__EVENTTARGET";

    protected override void InitializeCulture()
    {
        string dil="";
        if(Session["dil"]!=null)
            dil=Session["dil"].ToString();

        if (!string.IsNullOrEmpty(dil))
        {

            string selectedValue = getdilId();

                switch (dil)
                {
                    case "1": SetCulture("tr-TR", "tr-TR"); 
                        break;
                    case "2": SetCulture("en-US", "en-US"); 
                        break;
                    case "3": SetCulture("ru-RU", "ru-RU");
                        break;
                    default: break;
                }
            
        }

        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];

        }
        else
        {
            SetCulture("tr-TR", "tr-TR");
            
        }

        base.InitializeCulture();
    }

    public string getdilId()
    {
        string id = "";
        if (Session["dil"] == null)
            id = "1";
        else if (Session["dil"].ToString() == "tr")
            id = "1";
        else if (Session["dil"].ToString() == "en")
            id = "2";

        return id;

    }

    protected void SetCulture(string name, string locale)
    {
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(name);
        Thread.CurrentThread.CurrentCulture = new CultureInfo(locale);

        Session["MyUICulture"] = Thread.CurrentThread.CurrentUICulture;
        Session["MyCulture"] = Thread.CurrentThread.CurrentCulture;
    }
}