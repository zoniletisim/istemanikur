﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ResultAction
/// </summary>
public class ResultAction
{
    public int id { get; set; }
    public string message { get; set; }
    public bool result { get; set; }
    public object data { get; set; }
}