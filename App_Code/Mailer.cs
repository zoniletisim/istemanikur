﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using log4net;
using System.Threading;
using System.Collections;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Net;
using System.Web.Services;

/// Summary description for Mailer
/// </summary>
public class Mailer:Control
{
	ZonDBEntities _db;
    ResultAction _result;
    public static ILog logger = LogManager.GetLogger("default");
    public static List<Zon_Siparis_Detay> urun;
    public static string userMail = "";
    public static string receipent = "";
    public static string sendEmailAddress = "";
    public static string sendEmailPassword = "";
    public static string mailServer = "";
    
    HttpContext context = HttpContext.Current;
    public static string sifremiUnutumTemp = "";
    public static string siparisBilgileri = HttpContext.Current.Server.MapPath("~/MailTemplates/siparisBilgileri.html");
    public static string sifremiUnuttum = HttpContext.Current.Server.MapPath("~/MailTemplates/sifremiUnuttum.html");
    public static string uyelikAktivasyon = HttpContext.Current.Server.MapPath("~/MailTemplates/uyelikAktivasyon.html");
    public static string iletisim = HttpContext.Current.Server.MapPath("~/MailTemplates/iletisim.html");
    public static string tanitimMail = HttpContext.Current.Server.MapPath("~/MailTemplates/tanitimMail.html");
    public static string hediyeCekMailTemplate = HttpContext.Current.Server.MapPath("~/MailTemplates/0912Mailing/iste_manikur_hediye_ceki.html"); //public static string hediyeCekMailTemplate= HttpContext.Current.Server.MapPath("~/MailTemplates/hediyeCekMail.html");
    public static string ornek = "siparisBilgileri.html";
    public Mailer()
	{

        _db = new ZonDBEntities();
        _result = new ResultAction();
	}


    public static void informOrder(long orderId, ZonDBEntities dbContext, string sozlesme)
    {
        try
        {
            Thread t = new Thread(new ParameterizedThreadStart(processInformOrder));
            t.IsBackground = true;
            ArrayList arr = new ArrayList();
            arr.Add(orderId);
            arr.Add(dbContext);
            arr.Add(sozlesme);
            t.Start(arr);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    public static void processInformOrder(object prms)
    {
        long orderId = (long)((ArrayList)prms)[0];
        //byte[] sozleme = (byte[])((ArrayList)prms)[4];
        string html = "";
        int state = 0;
        Zon_HediyeCek cek = null;
        try
        {

            //BinaryContentResult sz = new BinaryContentResult(sozleme, "application/pdf");
            //PdfReader reader = new PdfReader(sozleme);
           // MemoryStream ms = new MemoryStream(sozleme);
            //PdfStamper stamper = new PdfStamper(reader, ms);
            //stamper.Close(); 
            //PdfViewController pdf = new PdfViewController();
            //byte[] iade=pdf.sozlesmePdf("", "Sozlesme", sozlesme());

            ZonDBEntities orderObjectContext = (ZonDBEntities)((ArrayList)prms)[1];
            string sozlesme = (string)((ArrayList)prms)[2];
            Zon_Siparis o = (from item in orderObjectContext.Zon_Siparis
                        where item.ID == orderId
                        select item).FirstOrDefault();

            if (o == null)
                throw new Exception("Randevu bulunamıyor.");

            state = 1;

            List<Zon_Siparis_Detay> d = (from de in orderObjectContext.Zon_Siparis_Detay
                                     where de.SiparisID == orderId
                                               select de).ToList<Zon_Siparis_Detay>();

            state = 2;

            Zon_Kullanici u = (from item in orderObjectContext.Zon_Kullanici
                     where item.ID == o.MusteriID
                     select item).FirstOrDefault();

            state = 3;

            //Zon_Musteri_Adresleri t = (from item in orderObjectContext.Zon_Musteri_Adresleri
            //               where item.ID == o.TeslimatAdresID
            //               select item).FirstOrDefault();

            state = 4;

            //Zon_Musteri_Adresleri f = (from item in orderObjectContext.Zon_Musteri_Adresleri
            //               where item.ID == o.FaturaAdresID 
            //               select item).FirstOrDefault();

            state = 5;

            Zon_Ayarlar ayar = (from item in orderObjectContext.Zon_Ayarlar
                                    select item).FirstOrDefault();

            if (o.HediyeCeki != null)
            {
                cek = (from item in orderObjectContext.Zon_HediyeCek
                       where item.Kod == o.HediyeCeki
                       select item).FirstOrDefault();
            }
            state = 6;

            receipent = ayar.BilgiMailAdres;
            sendEmailAddress = ayar.MailAdres;
            sendEmailPassword = ayar.MailSifre;
            mailServer = ayar.MailServer;
            userMail = u.Email.ToString();
           // userMail = "arif@hipervision.com";

            state = 7;


           // html = getUserSendMail(o, d, u, t, f, cek, orderId, itemObjectContext, orderObjectContext, userObjectContext, state);
            html = getUserSendMail(o, d, u, orderId,  orderObjectContext,  state,sozlesme,cek);
            string flag = "1";
            postMail(html, flag,  o.ID);
            flag = "2";
            html = getDimaraSendMail(o, d, u, orderId,  orderObjectContext, state,cek);
            postMail(html, flag,  o.ID);

        }
        catch (Exception ex)
        {
            logger.Error("Mail Sorunu OrderId=" + orderId + ",processState=" + state, ex);
        }
        finally
        {

        }
    }

    private static string getDimaraSendMail(Zon_Siparis o, List<Zon_Siparis_Detay> d, Zon_Kullanici u, long orderId, ZonDBEntities orderObjectContext, int state,Zon_HediyeCek cek)
    {
        string temp = "";
        string html = "";
        float araToplam = 0;
        HediyeCekRepositories cekRep = new HediyeCekRepositories();
        try
        {
            state = 17;
            string title = "<b>iştemanikür Web Site Yeni Randevu Bilgileri</b>";
            string message = "Aşağıdaki bilgiler doğrultusunda yeni bir randevu alındı.";
            html = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\"><title><b>iştemanikür</b></title></head><body><table style=\"width: 700px;\"><tbody><tr><td style=\"vertical-align: top; text-align: left; height: 200px;\"><font face=\"Arial,Geneva,Helvetica,Swiss,SunSans-Regular\" color=\"#C6314D\" size=\"2\"><table width='700' border='0' align=\"center\" cellpadding='0' cellspacing='0' style=\"border-bottom: 4px solid #C6314D; background-color: rgb(255, 255, 255); height: 70px;\"><tbody><tr><td nowrap><h3>" + title + "</h3></td></tr></tbody></table><br>";
            html += "</font><font face=\"Arial,Geneva,Helvetica,Swiss,SunSans-Regular\" color=\"#C6314D\" size='2'><strong><font color=\"#C6314D\"></font></strong></font><br><font face=\"Arial,Geneva,Helvetica,Swiss,SunSans-Regular\" size='2'><br>" + message + "<br><br><b>Müşteri:</b>&nbsp;" + u.Ad + " " + u.Soyad + "</a></b>";
            html += "<br/><b>Müşteri İletişim:</b>&nbsp;" + u.Telefon;
            state = 18;
            html += "<br/><b>Randevu No:</b>&nbsp;" + o.SiparisNO;
            html += "<br/><b>Randevu Tarihi:</b>&nbsp;" + o.EklenmeTarihi.ToString();
            html += "<br/><b>Randevu Toplamı:</b>&nbsp;" + string.Format("{0:N}", o.ToplamTutar) + " TL";
            state = 19;

               

                
                temp = " - Peşin";

                state = 20;
            
          

            html += "<br/><b>Ödeme Tipi:</b> Kredi Kartı";

            if (!String.IsNullOrEmpty(o.HediyeNotu))
                html += "<br/><br/><font color='red'><b>MÜŞTERİ NOTU:</b>&nbsp;" + o.HediyeNotu + "</font>";

            html += "<br></font><font face=\"Arial,Helvetica\" size='2'></font><font face=\"Arial,Helvetica\" size='2'><br><font color=\"#C6314D\">";
            html += "</font></font></td></tr></tbody></table><br/><br/>";
            html += "<table width='700' border='0' cellpadding='2' cellspacing='2' style=\"font-size:12px;font-family: Arial; background-color: rgb(255, 255, 255); height: 70px;\"><tbody>";
            html += "<tr style=\"color:#333;\" ><th>Servis</th> <th nowrap>Tarih</th><th>Saat </th><th>Firma</th><th>Ofis</th><th style=\"text-align:right;\" align=\"right\">Toplam(Kdv Dahil)</th></tr>";
            state = 21;
            foreach (Zon_Siparis_Detay line in d)
            {

                Zon_Randevu i = (from item in orderObjectContext.Zon_Randevu
                                 where item.Id == line.UrunID
                                 select item).FirstOrDefault();

                Zon_HizmetTur h = (from item in orderObjectContext.Zon_HizmetTur
                                   where item.Id == line.HizmetId
                                   select item).FirstOrDefault();
                Zon_Rezerve_Saat s = (from item in orderObjectContext.Zon_Rezerve_Saat
                                      where item.Id == line.SaatId
                                      select item).FirstOrDefault();


                

                string fiyat="";

                if(line.IndirimliFiyat!=null)
                    fiyat=string.Format("{0:N2}", line.IndirimliFiyat) ;
                else
                    fiyat = string.Format("{0:N2}", line.ToplamFiyat);

                html += "<tr><td>" + h.HizmetAd + "</td><td height='25'>" + string.Format("{0:d MMMM yyyy dddd}", i.Tarih) + "</td>";
                if(i.SaatSablonId!=null)
                    html += "<td>" + string.Format("{0:t}", s.BaslangicSaat.Value.AddMinutes((int)i.SaatSablonId)) + " - " + string.Format("{0:t}", s.BitisSaat.Value.AddMinutes((int)i.SaatSablonId)) + "</td>";
                else
                    html += "<td>" + string.Format("{0:t}", s.BaslangicSaat.Value) + " - " + string.Format("{0:t}", s.BitisSaat.Value) + "</td>";
                html += "<td>" + i.Zon_Ofis.Zon_Sirket.SiretAd + "</td><td>" + i.Zon_Ofis.OfisAd + "</td>";
                html += "<td align=\"right\" style=\"text-align:right;\">" + fiyat + " TL</td></tr>";
                
                if(line.IndirimliFiyat!=null)
                    araToplam += (float)line.IndirimliFiyat;
                else
                    araToplam += (float)line.BirimFiyat;
            }
            state = 22;

            //html += "<tr style=\"margin-top:15px ;\"><td align=\"right\" colspan=\"3\" style=\"padding:5px 0;text-align:right\">";
            //html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Ara Toplam:</b></span>";
            //html += "</td> <td style=\"padding:0;text-align:right;\"><b><span style=\"font-size:12px;color:#333\">";
            //html += araToplam.ToString("N2") + " TL</span></td></tr>";

            if (o.vadeFarki > 0)
            {
                html += "<tr><td align=\"right\" colspan=\"5\" style=\"padding:5px 0;text-align:right\">";
                html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Vade Farkı:</b></span>";
                html += "</td> <td style=\"padding:0;text-align:right;\"><b><span style=\"font-size:12px;color:#333\">";
                html += string.Format("{0:N2}", o.vadeFarki) + " TL</span></td></tr>";
            }

            if (cek != null)
            {
                html += "<tr><td align=\"right\" colspan=\"5\" style=\"padding:5px 0;text-align:right\">";
                html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Hediye Çeki:</b></span>";
                html += "</td> <td style=\"padding:0;text-align:right;\"><b><span style=\"font-size:12px;color:#333\">";
                html += string.Format("{0:N2}",cekRep.tutarHesapla(o.AraToplam.Value,cek.Tip.Value, cek.Tutar.Value)) + " TL</span></td></tr>";
            }
            if (o.HizmetIndirim != null)
            {
                html += "<tr><td align=\"right\" colspan=\"5\" style=\"padding:5px 0;text-align:right\">";
                html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>2. Hizmet İndirimi:</b></span>";
                html += "</td> <td style=\"padding:0;text-align:right;\"><b><span style=\"font-size:12px;color:#333\">";
                html += string.Format("{0:N2}", o.HizmetIndirim) + " TL</span></td></tr>";
            }
            //html += "<tr><td align=\"right\" colspan=\"3\" style=\"padding:5px 0;text-align:right\">";
            //html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Kargo Ücreti:</b></span>";
            //html += "</td> <td style=\"padding:0;text-align:right;\"><b><span style=\"font-size:12px;color:#333\">";
            //html += string.Format("{0:N2}",o.KargoTutar) + " TL</span></td></tr>";

            html += "<tr><td align=\"right\" colspan=\"5\" style=\"padding:5px 0;text-align:right\">";
            html += "<span style=\"font-size:12px;color:#8B161C;text-align:right\"><b>Genel Toplam :</b></span></p>";
            html += "</td> <td style=\"padding:0;text-align:right;\"><b><span style=\"font-size:12px;color:#333\">";
            html += string.Format("{0:N2}", o.ToplamTutar) + " TL</span></td></tr>";

            state = 23;
            html += "</tbody></table><br/><br/>";
            html += "<table border=\"0\" width=\"700\"><tr>";

            html += "<td style=\"width:330px;\"><b>Servis Adresi:</b></td>";
            html += "<td><b>Fatura Adresi:</b></td>";
            html += "</tr>";
            html += "<tr><td>" + u.Ad + " " + u.Soyad + "</td>";
            html += "<td>" + u.Ad + " " + u.Soyad + "</td></tr>";
            html += "<tr><td>" + o.TeslimatAdres+ "</td>";
            html += "<td>" + o.FaturaAdres+ "</td></tr>";
       
            html += "<tr><td>" + u.Telefon+ "</td>";
            html += "<td>" + u.Telefon + "</td></tr>";
           

            state = 24;

            //html += "<tr><td>&nbsp;</td>";
            //if(!string.IsNullOrEmpty(f.FirmaUnvan))
            //    html += "<td>Firma:" + f.FirmaUnvan + "</td></tr>";
            //html += "<tr><td>&nbsp;</td>";
            //if (!string.IsNullOrEmpty(f.VergiDairesi))
            //    html += "<td>Vergi D:" + f.VergiDairesi + "</td></tr>";
            //html += "<tr><td>&nbsp;</td>";
            //if (!string.IsNullOrEmpty(f.VergiNo))
            //    html += "<td>Vergi No:" + f.VergiNo + "</td></tr>";
            //html += "<tr><td>&nbsp;</td>";
            //if (!string.IsNullOrEmpty(f.TCNo))
            //    html += "<td>Tc No:" + f.TCNo + "</td></tr>";


            html += "</table>";
            html += "</body></html>";
            state = 25;
            return html;
        }
        catch (Exception ex)
        {

            logger.Error("siparisOzetState:" + state);
            throw ex;
        }

    }

    //private static string getUrunTas(int _urunID, ZonDBEntities orderObjectContext)
    //{
    //    string taslar = "";

    //    var taslist = (from item in orderObjectContext.View_Urun_Tas
    //                   where item.urunID == _urunID
    //                   select item).ToList();

    //    if (taslist != null)
    //    {
    //        foreach (var t in taslist)
    //        {
    //            taslar = t.Agirlik.ToString();
    //            if (t.Maden == 1)
    //                taslar += " ct";

    //            taslar += " " + t.Deger;


    //        }
    //    }
    //}

    public static string getUserSendMail(Zon_Siparis o, List<Zon_Siparis_Detay> d, Zon_Kullanici u, long orderId, ZonDBEntities orderObjectContext, int state, string sozlesme,Zon_HediyeCek cek)
    {
        string html = "";
        string ofis="";
        string sirket="";
        string ofisAciklama="";
        float araToplam = 0;
        HediyeCekRepositories cekRep = new HediyeCekRepositories();
        try
        {
            state = 8;
            html = "<div align=\"center\">";
            html += "<table border=\"0\" cellpadding=\"0\"  cellspacing=\"0\" style=\"width:722px;font-size:12px;font-family: Arial;\" ><tbody><tr><td style=\"padding:0cm 0cm 0cm 0cm\">";
            html += "<div align=\"center\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:722px;font-size:12px;font-family:Arial;\"><tbody>";
            html += "<td style=\"background:white;padding:9.0pt 9.0pt 9.0pt 9.0pt\"><p style=\"margin-bottom:12.0pt;text-align:justify\">";
            html += "<span style=\"font-size:14px;color:#0760B9\"></span></p>";
            html += "<p style=\"margin-bottom:12.0pt;text-align:justify\"><span style=\"font-size:13px;color:#1C2B32\"><b>Merhaba  " + u.Ad + " " + u.Soyad + "</b>,<br />";
            state = 9;
            html += "<br /><b>iştemanikür'ü<b> tercih ettiğiniz için teşekkür ederiz.<br />";
            html += "<br /><b style='font-size:18px'> " + o.SiparisNO + "</b> numaralı randevunuzun detayı aşağıdaki gibidir. </span></p>";
            
            ////////// kart bilgileri///////////////////
            string temp2 = "";
           
             temp2 = " - Peşin";

            state = 20;

            html += "<table cellpadding='0' cellspacing='0' border='0' width='600' style='font-family: Arial,Helvetica,sans-serif;margin: 0 0 20px'><tbody><tr>";
            html+="<td style='font-family: Arial,Helvetica,sans-serif; font-size: 12px; width: 290px;vertical-align: top'><p style='font-family: Arial,Helvetica,sans-serif; font-size: 12px; margin: 10px 0 20px'><span style='font-weight: bold; color: #282828'>Ödeme Şekli</span><br>";
            html+="<span style='color: #7d7d7d'>Kredi Kartı ile Ödeme"+temp2+" </span></p>";
           // html+="<span style='color: #7d7d7d'>"+temp+"</span></p>";
            html += "<p style='font-family: Arial,Helvetica,sans-serif; font-size: 12px; margin: 15px 0 3px'><span style='font-weight: bold; color: #282828'>Tahsil Edilen Toplam Tutar</span><br><span style='color: #3b3b3b; font-size: 16px'>" + string.Format("{0:N}", o.ToplamTutar) + "</span>";
            html+="<span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></p></td><td style='vertical-align: top'></td></tr></tbody></table>";
            ////////////////////////////////////
            state = 10;
            html += "<br/><br/> <table width='550' border='0' cellpadding='0' cellspacing='0' style=\"font-size:12px;font-family: Arial; background-color: rgb(255, 255, 255); height: 70px;\">";
            html += "<thead><tr><th height='20' style='font-family: Arial,Helvetica,sans-serif; font-size: 12px;text-align: left; font-weight: bold; color: #1a1a1a; width: 130px; padding: 0 0 4px;border-bottom: solid 1px #d3d3d3'>Servis</th>";
            html +="<th height='20' style='font-family: Arial,Helvetica,sans-serif; font-size: 12px;text-align: left; font-weight: bold; color: #1a1a1a; width: 190px; padding: 0 0 4px;border-bottom: solid 1px #d3d3d3'>Tarih</th>";
            html += "<th height='20' style='font-family: Arial,Helvetica,sans-serif; font-size: 12px;text-align: left; font-weight: bold; color: #1a1a1a; width: 130px; padding: 0 0 4px;border-bottom: solid 1px #d3d3d3'>Saat</th>";
            html += "<th height='20' style='font-family: Arial,Helvetica,sans-serif; font-size: 12px;text-align: right; font-weight: bold; color: #1a1a1a; width: 130px; padding: 0 0 4px;border-bottom: solid 1px #d3d3d3'>Toplam Fiyat</th></tr></thead><tbody>";
            state = 11;
            foreach (Zon_Siparis_Detay line in d)
            {

                Zon_Randevu i = (from item in orderObjectContext.Zon_Randevu
                                        where item.Id == line.UrunID
                                        select item).FirstOrDefault();
                  
                Zon_HizmetTur h = (from item in orderObjectContext.Zon_HizmetTur
                                   where item.Id == line.HizmetId
                                   select item).FirstOrDefault();
                Zon_Rezerve_Saat s = (from item in orderObjectContext.Zon_Rezerve_Saat
                                   where item.Id == line.SaatId
                                   select item).FirstOrDefault();
                
                ofis=i.Zon_Ofis.OfisAd;
                ofisAciklama=i.Zon_Ofis.Aciklama;
                sirket=i.Zon_Ofis.Zon_Sirket.SiretAd;


                string fiyat = "";

                if (line.IndirimliFiyat != null)
                    fiyat = string.Format("{0:N2}", line.IndirimliFiyat);
                else
                    fiyat = string.Format("{0:N2}", line.ToplamFiyat);

                //html += "<tr><td style='font-family: Arial,Helvetica,sans-serif; font-size: 12px; text-align: left;color: #1a1a1a; width: 360px; padding: 10px 0; border-bottom: solid 1px #d3d3d3;vertical-align: top'>";
                //html += "<table><tbody><tr><td style='vertical-align: top'></td>";
                //html += "<td style='vertical-align: top'><span style='font-weight: bold;font-size:12px;color: #555'>" + i.Baslik + "</span><br>";
                //html += "<div><p style='color: #818181; font-size: 11px; margin: 0;color:#4D9186; padding: 0'>" + yuzukTas + "</p>";
                //html += "<p style='color: #818181; font-size: 10px; margin: 0; padding: 0'>" + yuzukOlcu + "</p></div></td></tr></tbody></table></td>";
                //html += "<td style='font-family: Arial,Helvetica,sans-serif; text-align: left; padding: 10px 0;border-bottom: solid 1px #d3d3d3;vertical-align: top'><span style='color: #555'>" + i.ModelNo + "</span></td>";
                //html += "<td style='font-family: Arial,Helvetica,sans-serif; text-align: right; padding: 10px 0;border-bottom: solid 1px #d3d3d3; vertical-align: top'>";
                //html += "<span style='font-family: Arial,Helvetica,sans-serif; color: #3b3b3b; font-size: 16px'>" + string.Format("{0:N2}", line.BirimFiyat) + "</span><span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></td></tr>";
                html += "<tr>";
                html += "<td style='font-family: Arial,Helvetica,sans-serif; text-align: left; padding: 10px 0;border-bottom: solid 1px #d3d3d3;vertical-align: top'>" + h.HizmetAd + "</td>";
                html += "<td style='font-family: Arial,Helvetica,sans-serif; text-align: left; padding: 10px 0;border-bottom: solid 1px #d3d3d3;vertical-align: top'>" + string.Format("{0:d MMMM yyyy dddd}", i.Tarih) + "</td>";
                if(i.SaatSablonId!=null)
                    html += "<td style='font-family: Arial,Helvetica,sans-serif; text-align: left; padding: 10px 0;border-bottom: solid 1px #d3d3d3;vertical-align: top'>" + string.Format("{0:t}", s.BaslangicSaat.Value.AddMinutes((int)i.SaatSablonId)) + " - " + string.Format("{0:t}", s.BitisSaat.Value.AddMinutes((int)i.SaatSablonId)) + "</td>";
                else
                    html += "<td style='font-family: Arial,Helvetica,sans-serif; text-align: left; padding: 10px 0;border-bottom: solid 1px #d3d3d3;vertical-align: top'>" + string.Format("{0:t}", s.BaslangicSaat.Value) + " - " + string.Format("{0:t}", s.BitisSaat.Value) + "</td>";
                html += "<td style='font-family: Arial,Helvetica,sans-serif; text-align: right; padding: 10px 0;border-bottom: solid 1px #d3d3d3;vertical-align: top'><span style='font-family: Arial,Helvetica,sans-serif; color: #3b3b3b; font-size: 16px'>" +fiyat+ "</span><span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></td>";
                html += "</tr>";


                   //html += "<tr><td height='25'>" + i.Baslik + yuzukOlcu + "</td><td >" + i.ModelNo + "</td>";
                   //html += "<td align=\"right\" style=\"text-align:right;\">" + string.Format("{0:N2}", line.BirimFiyat) + " TL</td></tr>";
                
                //else if (line.Tip == 2)
                //{
                //    dimara_Tas_Tasarim i = (from item in orderObjectContext.dimara_Tas_Tasarim
                //                            where item.Id == line.TasId
                //                            select item).FirstOrDefault();

                //    html += "<tr><td>" + i.Baslik1 + "</td><td >" + i.ModelNo1 + "</td>";
                //    html += "<td align=\"right\" style=\"text-align:right;\">" + string.Format("{0:N2}", line.BirimFiyat) + " TL</td></tr>";

                //}
                //else if (line.Tip == 3)
                //{
                //    dimara_Maden_Tasarim i = (from item in orderObjectContext.dimara_Maden_Tasarim
                //                              where item.Id == line.MonturID
                //                              select item).FirstOrDefault();

                //    html += "<tr><td>" + i.Baslik + "</td><td >" + i.ModelNo + "</td>";
                //    html += "<td align=\"right\" style=\"text-align:right;\">" + string.Format("{0:N2}", line.BirimFiyat) + " TL</td></tr>";

                //}

                araToplam += (float)line.BirimFiyat;
            }

            state = 12;

            //html += "<tr style=\"margin-top:15px ;\"><td align=\"right\" colspan=\"3\" style=\"padding:5px 0;text-align:right\">";
            //html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Ara Toplam:</b></span></td>";
            //html += "<td style='padding: 0; text-align: right;'><span style='font-size: 12px; color: #333'><b>";
            //html += "<span style='font-family: Arial,Helvetica,sans-serif;color: #3b3b3b; font-size: 16px'>" + araToplam.ToString("N2");
            //html+="<span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></b></span></td></tr>";
            
            //html += "</td> <td style=\"padding:0;text-align:right;\"><b><span style=\"font-size:12px;color:#333\">";
            //html += araToplam.ToString("N2") + " TL</span></td></tr>";


            if (o.vadeFarki > 0)
            {
                html += "<tr><td align=\"right\" colspan=\"3\" style=\"padding:5px 0;text-align:right\">";
                html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Vade Farkı:</b></span></td>";
                html += "<td style='padding: 0; text-align: right;'><span style='font-size: 12px; color: #333'><b>";
                html += "<span style='font-family: Arial,Helvetica,sans-serif;color: #3b3b3b; font-size: 16px'>" + string.Format("{0:N2}", o.vadeFarki);
                html += "<span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></b></span></td></tr>";
            
            }


            if (cek != null)
            {
                html += "<tr><td align=\"right\" colspan=\"3\" style=\"padding:5px 0;text-align:right\">";
                html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Hediye Çeki:</b></span></td>";
                html += "<td style='padding: 0; text-align: right;'><span style='font-size: 12px; color: #333'><b>";
                html += "<span style='font-family: Arial,Helvetica,sans-serif;color: #3b3b3b; font-size: 16px'>" + string.Format("{0:N2}", cekRep.tutarHesapla(o.AraToplam.Value, cek.Tip.Value, cek.Tutar.Value));
                html += "<span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></b></span></td></tr>";
            }
            if (o.HizmetIndirim != null)
            {
                html += "<tr><td align=\"right\" colspan=\"3\" style=\"padding:5px 0;text-align:right\">";
                html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>2. Hizmet İndirimi:</b></span></td>";
                html += "<td style='padding: 0; text-align: right;'><span style='font-size: 12px; color: #333'><b>";
                html += "<span style='font-family: Arial,Helvetica,sans-serif;color: #3b3b3b; font-size: 16px'>" + string.Format("{0:N2}",o.HizmetIndirim) ;
                html += "<span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></b></span></td></tr>";
            }


            //html += "<tr><td align=\"right\" colspan=\"2\" style=\"padding:5px 0;text-align:right\">";
            //html += "<span style=\"font-size:12px;color:#000;text-align:right\"><b>Kargo Ücreti:</b></span></td>";
            //html += "<td style='padding: 0; text-align: right;'><span style='font-size: 12px; color: #333'><b>";
            //html += "<span style='font-family: Arial,Helvetica,sans-serif;color: #3b3b3b; font-size: 16px'>" + string.Format("{0:N2}", o.KargoTutar);
            //html += "<span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></b></span></td></tr>";

            html += "<tr><td align=\"right\" colspan=\"3\" style=\"padding:5px 0;text-align:right\">";
            html += "<span style=\"font-size:12px;color:#8B161C;text-align:right\"><b>Genel Toplam :</b></span></p></td>";

            html += "<td style='padding: 0; text-align: right;'><span style='font-size: 12px; color: #333'><b>";
            html += "<span style='font-family: Arial,Helvetica,sans-serif;color: #3b3b3b; font-size: 16px'>" + string.Format("{0:N2}", o.ToplamTutar);
            html += "<span style='font-family: Arial,Helvetica,sans-serif;font-size: 11px'>&nbsp;TL</span></b></span></td></tr>";

            state = 13;
            html += "</tbody></table>";
            html += "<p  style=\"margin-bottom:6.0pt;text-align:justify\"><br />";
            html += "<br /><b>Servis Bilgileri </b><br/>Firma:"+ sirket +" " +ofis +"<br />Adres: " + o.TeslimatAdres;
            if (ofisAciklama != "")
                html += "<br /><b Not: </b>" + ofisAciklama;

            state = 14;
            html += "<b>Fatura adresi: </b>" +o.FaturaAdres +  "<br /><br />";
            //html += "<b>Kargo Firması: </b>" +Constants.kargoFirma + "<br /><br />";
            state = 15;
            html += "Daha fazla bilgi ve sorularınız i&ccedil;in web sitemizde bulunan &#39;İletişim&#39; b&ouml;l&uuml;m&uuml;nden bize ulaşabilirsiniz.<br /><br />";
            html += "<br /><br />Saygılarımızla,<br /><b>iştemanikür</b></span></p></td></tr><tr><td style=\"padding:0cm 0cm 0cm 0cm\"><p >";
            html += "</p></td></tr></tbody></table></div></td></tr><tr><td>";
            html += "<p >&nbsp;</p></td></tr></tbody></table></div>";
            html += sozlesme;
            state = 16;
            return html;
        }
        catch (Exception ex)
        {
            logger.Error("siparisOzetState:" + state);
            throw ex;
        }
    }

    private static void postMail(string html, string flag, int p)
    {
        string subject;
        try
        {
           
            MailMessage mailToUser = new MailMessage();
            if (flag.Equals("1"))
                subject = "iştemanikür Randevu Bilgileriniz.";
            else
                subject = "iştemanikür Yeni Randevu";
            MailDefinition message = new MailDefinition();
            message.BodyFileName = siparisBilgileri;
            message.IsBodyHtml = true;
            message.From = sendEmailAddress;
            message.Subject = subject;

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<%icerik%>", html);
            if (flag.Equals("1"))
            {
                mailToUser = message.CreateMailMessage(userMail, replacements, new LiteralControl());
            }
            else
            {
                mailToUser = message.CreateMailMessage("arif@hipervision.com", replacements, new LiteralControl());
            }
            mailToUser.From = new MailAddress(sendEmailAddress,"iştemanikür");

           
               // mailToUser.Body = html;


            if (flag.Equals("2"))
            {
                string[] rList = receipent.Split(new char[] { ';' });
                foreach (string s in rList)
                    mailToUser.To.Add(s);
            }
            mailToUser.BodyEncoding = System.Text.Encoding.UTF8;
            // mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.UTF8;
            SmtpClient client = new SmtpClient(mailServer, 587);
            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(sendEmailAddress, sendEmailPassword);
            //ms.Position = 0;
            //Attachment attch = new Attachment(ms, new System.Net.Mime.ContentType("application/pdf"));
            //ContentDisposition disposition = attch.ContentDisposition;
            //disposition.FileName = "Sozlesme-" + orderId + ".pdf";

            //mailToUser.Attachments.Add(attch);



            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            client.Send(mailToUser);
        }
        catch (Exception)
        {
            
            throw;
        }
       
    }
    
    public static void postMail(string ad, string soyad, string email, string konu, string mesaj, string siparisNo)
    {
        try
        {
            MailMessage mailToMe = new MailMessage("arif@hipervision.com", "arif@hipervision.com", ad + " " + soyad, "Gönderen:" + ad + " " + soyad + "<br><br> Konu:" + konu + "<br><br> Mesaj:" + mesaj + "<br><br> Email Adresi:" + email + "<br><br> Randevu Numarası:" + siparisNo);
            mailToMe.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            mailToMe.IsBodyHtml = true;
            mailToMe.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            MailMessage mailToUser = new MailMessage("arif@hipervision.com", email, "iştemanikür",
                "Sayın " + ad + " " + soyad + "<br><br>" + "&nbsp;&nbsp;Talebiniz kayıda alınmıştır.Konuyla İlgili En kısa süre içerisinde bilgilendirileceksiniz."
                + "<br><br><a href=\"http://www.istemanikür.com\"><b>iştemanikür</b></a>");
            mailToUser.BodyEncoding = System.Text.Encoding.UTF8;
            mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.UTF8;

            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential("arif@hipervision.com", "Aarif123");
            SmtpClient client = new SmtpClient("mail.hipervsion.com", 587);

            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            client.Send(mailToMe);
            client.Send(mailToUser);

        }
        catch (Exception ex)
        {
            logger.Error("Mail Sorunu =" + ex);
            throw ex;
        }


    }
    
    public static void postForgotPass(string email,string hash)
    {
        string html = "";
        try
        {
           
            ZonDBEntities userObjectContext = new ZonDBEntities();
            Zon_Kullanici u = (from item in userObjectContext.Zon_Kullanici
                               where item.Email == email
                               select item).FirstOrDefault();
            Zon_Ayarlar ayar = (from item in userObjectContext.Zon_Ayarlar
                                select item).FirstOrDefault();
            html += "<div><span style='font-size:10pt;font-family:Trebuchet MS'><strong>Sayın "+ u.Ad +" "+u.Soyad +",</strong>&nbsp;</span><br><br></div>";
            html+="<div><span style='font-size: 10pt; font-family: Trebuchet MS'><div class='im'>Şifrenizi yeniden belirlemek için aşağıdaki linke tıklamanız yeterlidir.<br></div>";
            html+="<a href='https://www.istemanikur.com/sifreHatirlat.aspx?rid=";
            html += hash + "'";
            html +=" target='_blank'>https://www.istemanikur.com/sifreHatirlat.aspx?rid=";
            html += hash+ "</a>";
            html+="</span><br><br></div>";
            MailMessage mailToUser = new MailMessage();
            MailDefinition message = new MailDefinition();
            message.BodyFileName = sifremiUnuttum;
            message.IsBodyHtml = true;
            message.From = u.Email;
            message.Subject = "iştemanikür Üyelik Servisi";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<%icerik%>", html);
           
            mailToUser = message.CreateMailMessage(u.Email, replacements, new LiteralControl());

            mailToUser.From = new MailAddress(ayar.MailAdres,"iştemanikür");
            mailToUser.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            // mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            SmtpClient client = new SmtpClient(ayar.MailServer, 587);
            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(ayar.MailAdres,ayar.MailSifre );
            //ms.Position = 0;
            //Attachment attch = new Attachment(ms, new System.Net.Mime.ContentType("application/pdf"));
            //ContentDisposition disposition = attch.ContentDisposition;
            //disposition.FileName = "Sozlesme-" + orderId + ".pdf";

            //mailToUser.Attachments.Add(attch);



            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            client.Send(mailToUser);


        }
        catch (Exception ex)
        {
            logger.Error("Mail Sorunu =" + ex);
            throw ex;
        }


    }

    public static void postAktivasyonMail(string email, string hash)
    {
        string html = "";
        try
        {

            ZonDBEntities userObjectContext = new ZonDBEntities();
            Zon_Kullanici u = (from item in userObjectContext.Zon_Kullanici
                               where item.Email == email
                               select item).FirstOrDefault();
            Zon_Ayarlar ayar = (from item in userObjectContext.Zon_Ayarlar
                                select item).FirstOrDefault();
            html += "<div><span style='font-size:10pt;font-family:Trebuchet MS'><strong>Sayın " + u.Ad + " " + u.Soyad + ",</strong>&nbsp;</span><br><br></div>";
            html += "<div><span style='font-size: 10pt; font-family: Trebuchet MS'><div class='im'>Üyelik başvurunuz başarı ile alındı. Üyeliğinizi aktif etmek için aşağıdaki linke tıklamanız yeterlidir.<br></div>";
            html += "<a href='https://www.istemanikur.com/aktivasyon.aspx?rid=";
            html += hash + "'";
            html += " target='_blank'>https://www.istemanikur.com/aktivasyon.aspx?rid=";
            html += hash + "</a>";
            html += "</span><br><br></div>";
            MailMessage mailToUser = new MailMessage();
            MailDefinition message = new MailDefinition();
            message.BodyFileName = sifremiUnuttum;
            message.IsBodyHtml = true;
            message.From = u.Email;
            message.Subject = "iştemanikür hesabınızı aktive edin";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<%icerik%>", html);

            mailToUser = message.CreateMailMessage(u.Email, replacements, new LiteralControl());

            mailToUser.From = new MailAddress(ayar.MailAdres,"iştemanikür");
            mailToUser.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            // mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            SmtpClient client = new SmtpClient(ayar.MailServer, 587);
            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(ayar.MailAdres, ayar.MailSifre);
            //ms.Position = 0;
            //Attachment attch = new Attachment(ms, new System.Net.Mime.ContentType("application/pdf"));
            //ContentDisposition disposition = attch.ContentDisposition;
            //disposition.FileName = "Sozlesme-" + orderId + ".pdf";

            //mailToUser.Attachments.Add(attch);



            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            client.Send(mailToUser);


        }
        catch (Exception ex)
        {
            logger.Error("Mail Sorunu =" + ex);
            throw ex;
        }


    }

    public static void postFriendsInvitationMail(string email,int gonderenId,string hash)
    {
        string html = "";
        try
        {

            ZonDBEntities userObjectContext = new ZonDBEntities();
            Zon_Kullanici u = (from item in userObjectContext.Zon_Kullanici
                               where item.ID == gonderenId
                               select item).FirstOrDefault();
            Zon_Ayarlar ayar = (from item in userObjectContext.Zon_Ayarlar
                                select item).FirstOrDefault();
            html += "<div><span style='font-size:10pt;font-family:Trebuchet MS'><strong>Sayın " + u.Ad + " " + u.Soyad + ",</strong>&nbsp;</span><br><br></div>";
            html += "<div><span style='font-size: 10pt; font-family: Trebuchet MS'><div class='im'>Üyelik başvurunuz başarı ile alındı. Üyeliğinizi aktif etmek için aşağıdaki linke tıklamanız yeterlidir.<br></div>";
            html += "<a href='https://www.istemanikur.com/uyelik.aspx?rid=";
            html += hash + "'";
            html += " target='_blank'>https://www.istemanikur.com/uyelik.aspx?rid=";
            html += hash + "</a>";
            html += "</span><br><br></div>";
            MailMessage mailToUser = new MailMessage();
            MailDefinition message = new MailDefinition();
            message.BodyFileName = sifremiUnuttum;
            message.IsBodyHtml = true;
            message.From = email;
            message.Subject = u.Ad+" "+u.Soyad +" sizi iştemanikür'e davet ediyor.";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("<%icerik%>", html);

            mailToUser = message.CreateMailMessage(email, replacements, new LiteralControl());

            mailToUser.From = new MailAddress(ayar.MailAdres, "iştemanikür");
            mailToUser.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            // mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            SmtpClient client = new SmtpClient(ayar.MailServer, 587);
            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(ayar.MailAdres, ayar.MailSifre);
            //ms.Position = 0;
            //Attachment attch = new Attachment(ms, new System.Net.Mime.ContentType("application/pdf"));
            //ContentDisposition disposition = attch.ContentDisposition;
            //disposition.FileName = "Sozlesme-" + orderId + ".pdf";

            //mailToUser.Attachments.Add(attch);



            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            client.Send(mailToUser);


        }
        catch (Exception ex)
        {
            logger.Error("Mail Sorunu =" + ex);
            throw ex;
        }


    }

    public static string sendOutlookInvitationViaICSFile(eAppointmentMail objApptEmail)
    {
        try
        {
           
            ZonDBEntities userObjectContext = new ZonDBEntities();
            Zon_Ayarlar ayar = (from item in userObjectContext.Zon_Ayarlar
                                select item).FirstOrDefault();

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress(ayar.MailAdres);
            msg.To.Add(new MailAddress(objApptEmail.Email));
            msg.Subject ="test";
            msg.Body = "İştemanikür Randevu";
            StringBuilder str = new StringBuilder();
            str.AppendLine("BEGIN:VCALENDAR");
            str.AppendLine("PRODID:-//" + objApptEmail.Email);
            str.AppendLine("VERSION:2.0");
            str.AppendLine("METHOD:REQUEST");
            str.AppendLine("BEGIN:VEVENT");

            str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", objApptEmail.StartDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
            str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", (objApptEmail.EndDate - objApptEmail.StartDate).Minutes.ToString()));
            str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", objApptEmail.EndDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
            //str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", objApptEmail.StartDate.ToString()));
            //str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
            //str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", objApptEmail.EndDate.ToString()));
            str.AppendLine("LOCATION:" + objApptEmail.Location);
            str.AppendLine(string.Format("DESCRIPTION:{0}", objApptEmail.Body));
            str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", objApptEmail.Body));
            str.AppendLine(string.Format("SUMMARY:{0}", objApptEmail.Subject));
            str.AppendLine(string.Format("ORGANIZER:", "asdasdasd"));
            str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));
            str.AppendLine("BEGIN:VALARM");
            str.AppendLine("TRIGGER:-PT15M");
            str.AppendLine("ACTION:DISPLAY");
            str.AppendLine("DESCRIPTION:Reminder");
            str.AppendLine("END:VALARM");
            str.AppendLine("END:VEVENT");
            str.AppendLine("END:VCALENDAR");
            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
            ct.Parameters.Add("method", "REQUEST");
            //ct.Parameters.Add("name", "Meeting.ics");
            AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
            msg.AlternateViews.Add(avCal);
            SmtpClient sc = new SmtpClient(ayar.MailServer, 587);
            System.Net.NetworkCredential nc = new System.Net.NetworkCredential(ayar.MailAdres, ayar.MailSifre);
            sc.Credentials = nc;
            try
            {
                sc.Send(msg);
                return "Success";
            }
            catch
            {
                return "Fail";
            }
        }
        catch { }
        return string.Empty;
    }

    public static bool teaserMail(string email,string subject,string resim)
    {
        string html = "";
        bool ret = false;
        try
        {

            ZonDBEntities userObjectContext = new ZonDBEntities();
            //Zon_Kullanici u = (from item in userObjectContext.Zon_Kullanici
            //                   where item.Email == email
            //                   select item).FirstOrDefault();
            Zon_Ayarlar ayar = (from item in userObjectContext.Zon_Ayarlar
                                select item).FirstOrDefault();

            MailMessage mailToUser = new MailMessage();
            MailDefinition message = new MailDefinition();
            message.BodyFileName = tanitimMail;
            message.IsBodyHtml = true;
            message.From = email;
            message.Subject =subject;
            resim = "http://istemanikur.com" + resim;
            ListDictionary replacements = new ListDictionary();
            replacements.Add("<%Resim%>", resim);

            mailToUser = message.CreateMailMessage(email, replacements, new LiteralControl());

            mailToUser.From = new MailAddress(ayar.MailAdres,"iştemanikür");
            mailToUser.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            // mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            SmtpClient client = new SmtpClient(ayar.MailServer, 587);
            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(ayar.MailAdres, ayar.MailSifre);
            //ms.Position = 0;
            //Attachment attch = new Attachment(ms, new System.Net.Mime.ContentType("application/pdf"));
            //ContentDisposition disposition = attch.ContentDisposition;
            //disposition.FileName = "Sozlesme-" + orderId + ".pdf";

            //mailToUser.Attachments.Add(attch);



            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            client.Send(mailToUser);
            ret = true;
            return ret;

        }
        catch (Exception ex)
        {
            logger.Error("Mail Sorunu =" + ex);
            ret = false;
            return ret;
        }


    }

    public static bool hediyeCekMail(Zon_HediyeCek cek,int userId)
    {
        string html = "";
        bool ret = false;
        try
        {
            ZonDBEntities userObjectContext = new ZonDBEntities();

            Zon_Kullanici u = (from item in userObjectContext.Zon_Kullanici
                               where item.ID == userId
                               select item).FirstOrDefault();
            
            Zon_Ayarlar ayar = (from item in userObjectContext.Zon_Ayarlar
                                select item).FirstOrDefault();

            string icerik ="Hediye çekiniz " + cek.BitisTarih.Value.ToShortDateString()+" tarihine kadar geçerlidir.";
            MailMessage mailToUser = new MailMessage();
            MailDefinition message = new MailDefinition();
            message.BodyFileName = hediyeCekMailTemplate;
            message.IsBodyHtml = true;
            message.From = u.Email;
            message.Subject = "Size özel hediye çekiniz hesabınızda.";
            Dictionary<string, string> replacements = new Dictionary<string, string>();
            replacements.Add("<%icerik%>", icerik);
            
            replacements.Add("<%name%>", u.Ad + " " + u.Soyad);
            replacements.Add("<%hediyecek_kod%>", cek.Kod.ToString());
            
            if (cek.TutarTip.HasValue)
            {
                if (cek.TutarTip == 2)
                {
                    replacements.Add("<%tutar%>", cek.Tutar.ToString());
                    replacements.Add("<%tip%>", "TL");
                }
                else
                {
                    replacements.Add("<%tutar%>", ((int)cek.Tutar).ToString());
                    replacements.Add("<%tip%>", "%");
                }
            }
            else
            {
                replacements.Add("<%tutar%>", cek.Tutar.ToString());
                replacements.Add("<%tip%>", "TL");
            }
            

            mailToUser = message.CreateMailMessage(u.Email, replacements, new LiteralControl());

            mailToUser.From = new MailAddress(ayar.MailAdres, "iştemanikür");
            mailToUser.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            // mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            SmtpClient client = new SmtpClient(ayar.MailServer, 587);
            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(ayar.MailAdres, ayar.MailSifre);
            //ms.Position = 0;
            //Attachment attch = new Attachment(ms, new System.Net.Mime.ContentType("application/pdf"));
            //ContentDisposition disposition = attch.ContentDisposition;
            //disposition.FileName = "Sozlesme-" + orderId + ".pdf";

            //mailToUser.Attachments.Add(attch);



            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            client.Send(mailToUser);
            ret = true;
            return ret;

        }
        catch (Exception ex)
        {
            logger.Error("Mail Sorunu =" + ex);
            ret = false;
            return ret;
        }


    }
    
   
}

public class eAppointmentMail
{
    public string Name { set; get; }
    public string Email { set; get; }
    public string Location { set; get; }
    public DateTime StartDate { set; get; }
    public DateTime EndDate { set; get; }
    public string Subject { set; get; }
    public string Body { set; get; }
}