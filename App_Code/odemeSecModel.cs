﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZonDBModel;
using System.ComponentModel;
/// <summary>
/// Summary description for odemeSecModel
/// </summary>
public class odemeSecModel
{
    public Zon_Musteri_Adresleri teslimatAdres { get; set; }
    public Zon_Musteri_Adresleri faturaAdres { get; set; }
    public string teslimatAdresVal { get; set; }
    public string faturaAdresVal { get; set; }
    public decimal urunToplam;
    public decimal kargoFiyati;
    public decimal kargoHesaplananFiyati;
    public decimal genelToplam;
    public decimal genelToplamTaksitli;
    public decimal vadeFarki;
    public string teslimatSure;
    public decimal hediyeCekTutar;
    public decimal hizmetIndirim { get; set; }
    public List<View_SepetListe> sepetListe { get; set; }
    public int RandevuId{ get; set; }

    public string OrderId { get; set; }

    public int bankaId { get; set; }
    public int taksitId { get; set; }
    public int TaksitSayi { get; set; }
    public string kartSahibi;
    public string KartGuvenlikNo { get; set; }
    public string KartTipi { get; set; }
    public string KartSKAy { get; set; }
    public string KartSKYil { get; set; }
    public string KartNo { get; set; }
    public string tckn { get; set; }
   

}