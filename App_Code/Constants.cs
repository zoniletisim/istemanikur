﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using ZonDBModel;
using System.Xml;
using System.Xml.Linq;
/// <summary>
/// Summary description for Constants
/// </summary>
public class Constants
{
    public static bool dilSecenekleri = false;
    public static decimal KargoUcreti = 0;
    public static decimal MinSiparisTutarForKargo = 0;
    public static int pageCount = 30;

    public static string URLCorrect(string val)
    {
        string Temp = val;

        Temp = val.ToLower();
        Temp = Temp.Replace("-", ""); Temp = Temp.Replace(" ", "-");
        Temp = Temp.Replace("ç", "c"); Temp = Temp.Replace("ğ", "g");
        Temp = Temp.Replace("ı", "i"); Temp = Temp.Replace("ö", "o");
        Temp = Temp.Replace("ş", "s"); Temp = Temp.Replace("ü", "u");
        Temp = Temp.Replace("\"", ""); Temp = Temp.Replace("/", "");
        Temp = Temp.Replace("(", ""); Temp = Temp.Replace(")", "");
        Temp = Temp.Replace("{", ""); Temp = Temp.Replace("}", "");
        Temp = Temp.Replace("%", ""); Temp = Temp.Replace("&", "");
        Temp = Temp.Replace("+", ""); Temp = Temp.Replace(".", "-");
        Temp = Temp.Replace("?", ""); Temp = Temp.Replace(",", "");

        //Temp = Regex.Replace(Temp, @"[^a-zA-Z0-9-]", " ");
        //Temp = Regex.Replace(Temp, @"\s+", " ");
      

        return Temp;

    }

    public enum safyaIcerikTip
    {
        sayfa = 1,
        haber = 2,
    }

    public enum KullaniciTip
    {
        admin = 1,
        bayi = 3,
    }

    public enum HediyeCekTip
    {
        admin = 1,
        arkadaslarinaOner = 2,
        dogumGunu= 3,
        siparisSayisi = 4,
    }

    public static string  HediyeCekTipText(int id)
    {
        string ret = "";

        switch (id)
        {
            case 1: ret = "Admin"; break;
            case 2: ret = "Arkadaslarina Öner"; break;
            case 3: ret = "Doğum Günü"; break;
            case 4: ret = "Randevu Sayısı"; break;
        }

        return ret;
    }

    public static string TutarTip(int id)
    {
        string ret = "";

        switch (id)
        {
            case 1: ret = "Yüzde(%)"; break;
            case 2: ret = "TL"; break;
        }

        return ret;
    }

    //public static Dictionary<string, string> getDil()
    //{
    //    Dictionary<string, string> list = new Dictionary<string, string>();
    //   ZonDBEntities db = new ZonDBEntities();
    //    var banka = (from item in db.Zon_Dil
    //                where item.Aktif==true
    //                 select item).ToList();
    //    foreach (var b in banka)
    //    {
    //        list.Add(b.DilId.ToString(), b.Aciklama );
    //    }
    //    return list;
    //}
    public static Dictionary<string, string> GetSehir()
    {
        Dictionary<string, string> sehirList = new Dictionary<string, string>();
        XmlDocument doc = new XmlDocument();
        doc.Load(HttpContext.Current.Server.MapPath("~/Resources/StaticData/sehir.xml"));
        XmlNodeList list = doc.SelectNodes("dataroot/sehir");
        foreach (XmlNode n in list)
        {
            sehirList.Add(n.SelectSingleNode("id").InnerText, n.SelectSingleNode("sehir").InnerText);
        }

        return sehirList;
    }

    public static Dictionary<string, string> GetIlce(string ilId)
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<string, string> sehirList = new Dictionary<string, string>();
        XDocument doc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/StaticData/ilce.xml"));
        var query = from p in doc.Element("dataroot").Elements("r")
                    where p.Element("t").Value == ilId
                    orderby p.Element("s").Value
                    select p;
        foreach (var i in query)
        {
            sehirList.Add(i.Element("v").Value, i.Element("s").Value);
        };
        return sehirList;
    }

    public static Dictionary<string, string> GetIlFilter(string ilId)
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<string, string> sehirList = new Dictionary<string, string>();
        XDocument doc = XDocument.Load(HttpContext.Current.Server.MapPath("~/Resources/StaticData/sehir.xml"));
        var query = from p in doc.Element("dataroot").Elements("sehir")
                    where p.Element("id").Value == ilId
                    orderby p.Element("sehir").Value
                    select p;
        foreach (var i in query)
        {
            sehirList.Add(i.Element("id").Value, i.Element("sehir").Value);
        };
        return sehirList;
    }

    public static Dictionary<string, string> GetKrediKartTip()
    {
        Dictionary<string, string> list = new Dictionary<string, string>();
        list.Add("0", "Seçiniz..");
        list.Add("1", "Visa");
        list.Add("2", "MasterCard");
        return list;
    }

    public static Dictionary<string, string> GetKrediKartSkAy()
    {
        Dictionary<string, string> list = new Dictionary<string, string>();

        for (int i = 1; i <= 12; i++)
            list.Add(i.ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0'));

        return list;
    }

    public static Dictionary<string, string> GetKrediKartSkYil()
    {
        Dictionary<string, string> list = new Dictionary<string, string>();

        for (int i = DateTime.Now.Year; i <= DateTime.Now.Year + 10; i++)
            list.Add(i.ToString(), i.ToString());

        return list;
    }

    public enum EticaretOdemeTipi
    {
        PESIN = 0,
        TAKSITLI = 1,
    }

    public static string getUserNameAndSurname(int userId)
    {
        ZonDBEntities db = new ZonDBEntities();
        Zon_Kullanici u = db.Zon_Kullanici.Where(z => z.ID == userId).FirstOrDefault();

        return u.Ad + " " + u.Soyad;
    }
   
}

[Serializable()]
public class StokTukendiException : System.Exception
{
    public StokTukendiException() : base() { }
    public StokTukendiException(string message) : base(message) { }
    public StokTukendiException(string message, System.Exception inner) : base(message, inner) { }

    protected StokTukendiException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) { }
}