﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Globalization;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : Page
{
    public BasePage()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public const string LanguageDropDownName = "ctl00$ddlLanguage";
    public const string PostBackEventTarget = "__EVENTTARGET";
   
    protected override void InitializeCulture()
    {
       
        if (!string.IsNullOrEmpty(Request[PostBackEventTarget]))
        {
            string controlID = Request[PostBackEventTarget];
            if (controlID == LanguageDropDownName)
            {
                string selectedValue = Request.Form[Request[PostBackEventTarget]].ToString();

                switch (selectedValue)
                {
                    case "0": SetCulture("tr-TR", "tr-TR"); Session["dilID"] = "0";
                        break;
                    case "1": SetCulture("en-US", "en-US"); Session["dilID"] = "1";
                        break;

                    default: break;
                }
            }
        }

        if (Session["MyUICulture"] != null && Session["MyCulture"] != null)
        {
            Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["MyUICulture"];
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["MyCulture"];

        }
        else
        {
            SetCulture("tr-TR", "tr-TR");
            Session["dilID"] = "0";
        }

        base.InitializeCulture();
    }


    
    protected void SetCulture(string name, string locale)
    {
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(name);
        Thread.CurrentThread.CurrentCulture = new CultureInfo(locale);
       
        Session["MyUICulture"] = Thread.CurrentThread.CurrentUICulture;
        Session["MyCulture"] = Thread.CurrentThread.CurrentCulture;
    }
}