﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitemasterpage.master" AutoEventWireup="true" CodeFile="sifreHatirlat.aspx.cs" Inherits="sifreHatirlat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<div class="mainContainer">
<script>
    $(document).ready(function () {
        $(".uyelikSignupContainer").find('.uyelikSozlesmesi').fancybox({
            'width': 800,
            'height': 600,
            'autoScale': false,
            'transitionIn': 'none',
            'overlayOpacity': 0.5,
            'transitionOut': 'none',
            'centerOnScroll': true,
            'type': 'iframe'

        });
    });
</script>
	<div class="uyelikContainer">
        <div class="uyelikBanner"></div>
        <div class="uyelikShadow"></div>
        <asp:Panel ID="pnlEmail" runat="server">
        
        <div class="uyelikLoginContainer">
        	<div class="uyelikLoginTitle">Şifremi Unuttum</div>	
            <div class="uyelikLoginText">Daha önce belirlemiş olduğunuz e-posta adresiniz ve şifreniz ile giriş yapınız. Eğer üye değilseniz sağ taraftaki formu kullanarak hemen ücretsiz üye olabilirsiniz.</div>
            <asp:TextBox ID="txtUser" CssClass="loginFormInput" placeholder="E-posta adresinizi giriniz." runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="loginValidate" ValidationGroup="login" Text="<img src='../Resources/images/Exclamation.gif' title='Email Adresinizi Giriniz.' />" runat="server" ControlToValidate="txtUser" ErrorMessage="Email Adresinizi Giriniz."></asp:RequiredFieldValidator>
        
            <%--<asp:TextBox ID="txtPassword" ValidationGroup="login" TextMode="Password" CssClass="loginFormInput" placeholder="Şifrenizi giriniz." runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="loginValidate" ValidationGroup="login" Text="<img src='../Resources/images/Exclamation.gif' title='Şifresnizi Giriniz.' />" runat="server" ControlToValidate="txtPassword" ErrorMessage="Şifre Giriniz."></asp:RequiredFieldValidator>
           --%>
           
              <%--<asp:CheckBox ID="chkBeniHatirla"  ClientIDMode="Static" class="loginFormCheckbox" runat="server" Text="Beni hatırla" />--%>
           <%-- <a class="forgotPassword" style="color: red;text-decoration: none;">Şifremi unuttum!</a>--%>
            <asp:Button ID="Button1" ValidationGroup="login"  runat="server" Text="" CssClass="uyelikLoginButton" onclick="Button1_Click" />

             <asp:Panel ID="Panel1" Visible="false" runat="server">
            <div class="validateUyeGirisi">
                <asp:Label ID="lblMessage" CssClass="validationsummary" runat="server" Text=""></asp:Label>
            </div>  
        </asp:Panel>
        </div>
        </asp:Panel>

         <asp:Panel ID="pnlsifre" runat="server" Visible="false">
       
        <div class="uyelikLoginContainer">
        	<div class="uyelikLoginTitle">Şifremi Unuttum</div>	
            <div class="uyelikLoginText">Daha önce belirlemiş olduğunuz e-posta adresiniz ve şifreniz ile giriş yapınız. Eğer üye değilseniz sağ taraftaki formu kullanarak hemen ücretsiz üye olabilirsiniz.</div>
            <%--<asp:TextBox ID="TextBox1" CssClass="loginFormInput" placeholder="E-posta adresinizi giriniz." runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="loginValidate" ValidationGroup="login" Text="<img src='../Resources/images/Exclamation.gif' title='Email Adresinizi Giriniz.' />" runat="server" ControlToValidate="txtUser" ErrorMessage="Email Adresinizi Giriniz."></asp:RequiredFieldValidator>
        --%>
            <asp:TextBox ID="txtPassword" ValidationGroup="login" TextMode="Password" CssClass="loginFormInput" placeholder="Şifrenizi giriniz." runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="loginValidate" ValidationGroup="login2" Text="<img src='../Resources/images/Exclamation.gif' title='Şifresnizi Giriniz.' />" runat="server" ControlToValidate="txtPassword" ErrorMessage="Şifre Giriniz."></asp:RequiredFieldValidator>
          <asp:TextBox ID="txtPassword2" ValidationGroup="login" TextMode="Password" CssClass="loginFormInput" placeholder="Şifrenizi tektrar giriniz." runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="loginValidate" ValidationGroup="login2" Text="<img src='../Resources/images/Exclamation.gif' title='Şifresnizi Giriniz.' />" runat="server" ControlToValidate="txtPassword" ErrorMessage="Şifre Giriniz."></asp:RequiredFieldValidator>
            <asp:Label ID="Label2" runat="server" Visible="false" Text=""></asp:Label>
              <%--<asp:CheckBox ID="chkBeniHatirla"  ClientIDMode="Static" class="loginFormCheckbox" runat="server" Text="Beni hatırla" />--%>
           <%-- <a class="forgotPassword" style="color: red;text-decoration: none;">Şifremi unuttum!</a>--%>
            <asp:Button ID="btnSifreKaydet" ValidationGroup="login2"  runat="server" Text="" 
                CssClass="uyelikLoginButton" onclick="btnSifreKaydet_Click" />

             <asp:Panel ID="Panel2" Visible="false" runat="server">
            <div class="validateUyeGirisi">
                <asp:Label ID="Label1" CssClass="validationsummary" runat="server" Text=""></asp:Label>
            </div>  
        </asp:Panel>
        </div>
      </asp:Panel>
    </div>
      <div style="clear:both"></div>
       <div style="display: none">
    <div id="popupRandevu">
    	<span class="popupRandevuTitle">Kullanım ve Üyelik Sözleşmesi </span>
      Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.


    
    </div>
	</div>
    <script src="Resources/js/jquery.placeholder.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('input, textarea').placeholder();

    });
       

 
  </script>
</asp:Content>



