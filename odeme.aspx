﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="odeme.aspx.cs" Inherits="odeme" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="mainContainer">
	<div class="odemeContainer">
        <div class="randevuTop">
        	<a class="randevuTopRandevuPassive" href="randevu.aspx">RANDEVU</a>
            <a class="randevuTopOdeme" href="odeme.aspx">ÖDEME</a>
            <div class="randevuTopOnayPassive">RANDEVU ONAY</div>
        </div>
        <div class="shadow"></div>
          <%if (sepetUrun.sepetListe.Count > 0)
            { %>
        <div class="odemeBasket">
        	<div class="items">
            <div style="display: block;text-align: right;margin-bottom: 11px;margin-right: 6px;" >
                <table width="319" style="float:left;margin-right:20px">
                    <tr>
                    <td>Hediye Çeki:</td>
                    <td><asp:TextBox ID="txtCekKod" ClientIDMode="Static" Width="100" Height="20" runat="server"></asp:TextBox></td>
                    <td style="width:50px;"><div class="hediyeCeki cekButon">Kullan</div></td>
                    <td><div class="hediyeCekiList cekButon">Çeklerim</div></td>
                    </tr>
                    </table>
                </div>
              <div style="display: block;text-align: right;margin-bottom: 11px;margin-right: 6px;" >
                 <a href="?Not=" class="SepetNotEkle globalButton addNoteMainBasket" title="Not Ekleyin">Randevunuza Not Ekleyin</a>
                </div>
            <div class="title">
            <table width="960">
            <tr>
                <td style="width:120px">Hizmet</td>
                <td style="width:120px">Randevu Günü</td>
                <td style="width:120px">Randevu Saati</td>
                <td style="width:100px">Firma</td>
                <td style="width:100px">Ofis</td>
                <td style="width:70px">Fiyat</td>
            </tr>
            </table>
            </div>
            <%
                decimal indirimliFiyat = 0;
                decimal toplamTutar = 0;
                
                foreach (var s in sepetUrun.sepetListe)
                {
                    ofisId = s.OfisId.Value;
                    %>
                <div class="item">
                    <div class="basketurun"><%=s.HizmetAd%></div>
                    <div class="basketGun"><%=string.Format("{0:d MMMM yyyy dddd}", s.Tarih)%></div>
                    <div class="basketSaat"><%=string.Format("{0:t}", s.BaslangicSaat)%> - <%=string.Format("{0:t}", s.BitisSaat)%></div>
                    <div class="basketFirma"><%=s.SiretAd%></div>
                    <div class="basketOfis"><%=s.OfisAd%></div>
                    <div class="basketPrice">
                    <%if (s.MiktarYuzde != null)
                      {
                          indirimliFiyat = ((decimal)s.Fiyat *(100-Convert.ToDecimal(s.MiktarYuzde))) / 100;
                           %>
                           <%
                          if (indirimliFiyat != 0)
                          {
                              %>
                              <span style="color:#A8A6A6;text-decoration:line-through"> <%=s.Fiyat%> TL</span>
                            <%=Math.Ceiling(indirimliFiyat).ToString("N2")%> TL
                              <%
                          }   
                             %>
                            
                     <%}else{ %>
                          <%=s.Fiyat%> TL
                    <%} %>
                    </div>
                 
                    <div class="delete">
                       <a href="?del=1&pid=<%=s.Id%>" class="deleteMainBasket SepetSil basketIconClose" title="Sepetten Sil"></a>
                    </div> 

                </div>
                <% 
                } %>
              
               <%-- <div class="item">
                    
                    <div class="basketCheck"><input name="" type="checkbox" value="" /></div>
                    <div class="basketurun">Makyaj</div>
                    <div class="basketGun">18 Haziran 2013, Çrş</div>
                    <div class="basketSaat">07:45-08:00</div>
                    <div class="basketFirma">Unilever</div>
                    <div class="basketOfis">Ümraniye</div>
                    <div class="basketPrice">15TL</div>
                 
                    <div class="delete"> 
                       <a href="?del=1&pid=" class="deleteMainBasket SepetSil basketIconClose" title="Sepetten Sil"></a>
                    </div> 

                </div>--%>
                <div class="sepetToplamTutar">
                <ul style="width: 180px;">
                <li style="float:right;margin-top:5px;padding-top:5px;font-size: 14px;display:none" id="liHediyeCek">
                <div class="ceksil"></div>
                    <div class="text"><b>HEDİYE ÇEKİ: </b></div>
                    <div class="value hediyeCekVal" ><span></span> TL </div>
                     
                    </li>
                 <%if (Session["hediyeCekKod"] != null){%>
                    <li style="float:right;margin-top:5px;padding-top:5px;font-size: 14px;">
                     <div class="ceksil"></div>
                    <div class="text"><b>HEDİYE ÇEKİ: </b></div>
                    <div class="value"><span> <%=sepetUrun.hediyeCekTutar.ToString("N2")%></span> TL </div>
                    
                    </li>
                     <%} %>
                 <%if (Session["hizmetIndirimTutar"] != null){%>
                    <li style="float:right;margin-top:5px;padding-top:5px;font-size: 14px;">
                    <div class="text"><b>2. HİZMET İNDİRİMİ: </b></div>
                    <div class="value"><span> <%=Session["hizmetIndirimTutar"]%></span> TL </div>
                    
                    </li>
                    <%} %>
                    <%
                if (sepetUrun.genelToplam != 0)
                {
                    %>
                     <li style="float:right;margin-top:5px;padding-top:5px;font-size: 14px;" class="siparisOzetirw3"><div class="text"><b>TOPLAM: </b></div>
                        <div class="value totalPrice"> <span> <%=sepetUrun.genelToplam.ToString("N2")%></span> TL </div>
                    </li>
                    <%
                }
                         %>
                    
                </ul>
            </div>

            </div>		
        </div>
      
        <div class="cardContainer">
        <%if(sepetUrun.genelToplam!=0){ %>
        <table class="kartForm">
                      <tr>
                      <td>
                      <div class="mastercard mastercardPassive"><img src="resources/images/mastercard.jpg" /></div>
                      <div class="visa visaPassive"><img src="resources/images/visa.jpg" /></div> 
							</td>                      
                      
                      </tr>
                        <tr style="height:32px;float: left;margin-top: 80px;margin-left: 21px;position:relative;">
                            <td colspan="4">
                            <div class="kartNoError" style="display:none;position:absolute;color:Red;font-size:11px;font-weight:bold;top:-17px;">Kart numarasını eksiksiz giriniz.</div>
                            <div style="margin-right: 10px;float:left; height: 29px;width: 60px;" class="ui-form">
                          <asp:TextBox ID="txtKartNo1" CssClass="numeric" AutoCompleteType="Disabled"  Width="44" MaxLength="4" ClientIDMode="Static" style="height:12px;"    runat="server"></asp:TextBox></div>
                            <div style="margin-right: 9px;float:left; height: 29px;width: 60px;" class="ui-form">
                            <asp:TextBox  ID="txtKartNo2" CssClass="numeric" AutoCompleteType="Disabled" Width="44" MaxLength="4" ClientIDMode="Static" style="height:12px;"  runat="server"></asp:TextBox></div>
                            <div style="margin-right: 9px;float:left; height: 29px;width: 60px;" class="ui-form">
                         <asp:TextBox  ID="txtKartNo3" CssClass="numeric" AutoCompleteType="Disabled" Width="44" MaxLength="4" ClientIDMode="Static"  style="height:12px;"  runat="server"></asp:TextBox></div>
                            <div style="margin-right: 9px;float:left; height: 29px;width: 60px;" class="ui-form">
                          <asp:TextBox  ID="txtKartNo4" CssClass="numeric" AutoCompleteType="Disabled" Width="44" MaxLength="4" ClientIDMode="Static" style="height:12px;"   runat="server"></asp:TextBox></div>
                     <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="None" ForeColor="red" runat="server" ValidationGroup="cardNo" ControlToValidate="txtKartNo1" ErrorMessage="Kart numarasını eksiksiz giriniz."></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="None" ForeColor="red" runat="server" ValidationGroup="cardNo" ControlToValidate="txtKartNo2" ErrorMessage="Kart numarasını eksiksiz giriniz."></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="None" ForeColor="red" runat="server" ValidationGroup="cardNo" ControlToValidate="txtKartNo3" ErrorMessage="Kart numarasını eksiksiz giriniz."></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="None" ForeColor="red" runat="server" ValidationGroup="cardNo" ControlToValidate="txtKartNo4" ErrorMessage="Kart numarasını eksiksiz giriniz."></asp:RequiredFieldValidator>
                     <asp:ValidationSummary ID="ValidationSummary1"  ValidationGroup="cardNo" ShowSummary="true"   ForeColor="Red" DisplayMode="SingleParagraph" HeaderText="aaaaaaaaaaaa" runat="server" /></div>
                          --%> </td>
                        </tr>
                        <tr style="height:27px;width: 367px;float: left;margin-left: 48px;">
                            <td colspan="4">
                            <div style="margin-right:5px;float:left;"><div class="kartTarih" style="float:left;margin-right:3px;">
                            <asp:DropDownList ID="ddAy" CssClass="odemeSelect2" runat="server"></asp:DropDownList></div></div>
                            <div style="margin-right:5px;float:left;"><div class="kartTarih" style="float:left;margin-right:3px;">
                            <asp:DropDownList ID="ddYil" CssClass="odemeSelect2" runat="server"></asp:DropDownList></div></div>
                            </td>
                           
                        </tr>
                         <tr style="height:45px;float: left;width: 275px;margin-top: 8px;margin-left: 19px;">
                            
                            <td colspan="4">
                            <div class="ui-form"><asp:TextBox ID="txtKartIsim" ClientIDMode="Static" style="font-size:11px;" AutoCompleteType="None" Width="260" Height="14"  runat="server"></asp:TextBox></div>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1"  CssClass="validateOdeme"  Display="Dynamic" ForeColor="red" runat="server" ControlToValidate="txtKartIsim" ErrorMessage="Kart Üzerindeki İsmi Giriniz."></asp:RequiredFieldValidator></td>--%>
                                <div class="kartIsim" style="display:none;color:Red;font-size:11px;font-weight:bold;top:-17px;">Kart Üzerindeki İsmi Giriniz.</div>
                        </tr>
                        
                         <tr style="height:45px;position:absolute;margin-left: 590px;margin-top: 67px;*margin-top: -138px; *margin-left: 543px;">
                            
                           <td colspan="4" class="ieTdOdeme"><div class="ui-form"> 
                           <asp:TextBox ID="txtCvc" ClientIDMode="Static"  CssClass="numeric"   Width="27" Height="12" MaxLength="3" runat="server"></asp:TextBox></div>
                         <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" CssClass="validateOdeme"  Display="Dynamic" ForeColor="red" runat="server" ControlToValidate="txtCvc" ErrorMessage="Kartınızın CVC2 numarasını giriniz. "></asp:RequiredFieldValidator>--%>
                         <div class="kartCvc" style="display:none;color:Red;font-size:11px;font-weight:bold;top:-17px;">Kartınızın CVC2 numarasını giriniz.</div>
                           </td>
                        </tr>
                       </table>
                       <%} %>
                        <div class="sozlesmeRw">
                        <asp:CheckBox ID="chkSozlesme" ClientIDMode="Static"  runat="server" /><span> <a href="#" style="font-size:12px" id="mesafeliSozlesme" > Mesafeli satış sözleşmesini</a>  okudum kabul ediyorum.</span>
                    </div>
                    <div style="float:right;margin-right:20px;">
                    <table>
                    <tr>
                    
                    <%
                if (sepetUrun.genelToplam != 0)
                {
                    %>
                    <td>TC Kimlik No:</td>
                    <td>
                    <div class="ui-form"><asp:TextBox ID="txtTcKimlik" ClientIDMode="Static"  CssClass="numeric"   Width="200" Height="12" MaxLength="11" runat="server"></asp:TextBox></div>
                    <span style="font-size:11px;"> Fatura bilgileri için.</span>
                   <div class="tcNoError" style="display:none;color:Red;font-size:11px;font-weight:bold;top:-17px;">TC Kimlik numarınızı giriniz.</div>
                    </td>
                    <%
                }
                         %>
                    
                    
                    </tr>
                    </table>
                    
                    </div>
                    <div class="odemeyiTamamla">
                   <asp:Button ID="btnBitir" ClientIDMode="Static" runat="server" Text=""  CssClass="odemeTamamlaButton"  />
                  <asp:Button ID="btnBitir2" ClientIDMode="Static" runat="server" Text="" style="display:none;"  CssClass="odemeTamamlaButton"  />
                </div>
        </div>
        <%}
            else { %>
            <div class="sepetBos" style="width:960px;text-align:center;float:left;margin-top:40px;" >
            Lütfen randevu sayfasından seçim yapınız.
            </div>
            <%} %>
     </div> 
     </div>
     <div id="dialog-confirm" title="" style="display:none;">
	<p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 70px 0;"></span>
        <span id="dialog-text" style="font-size:13px;font-weight:bolder;height:110px"></span>
    </p>
</div>  
    <asp:HiddenField ID="sepetToplam" runat="server" ClientIDMode="Static" />
    <script src="/Resources/js/jquery.blockUI.1.js" type="text/javascript"></script>
    <script src="/Resources/js/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    <script src="/Resources/js/jquery.autotab-1.1b.js" type="text/javascript"></script>
    <script src="/Resources/js/jquery.numeric.js" type="text/javascript"></script>
    <script src="/Resources/js/facebox.js?v=2" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
    if($('#sepetToplam').val()=="0")
    {
        $('table.kartForm').css('display','none');
        $('.odemeContainer .cardContainer').css('background-image','none');
        $('#btnBitir').css('display','none');
        $('#btnBitir2').css('display','block');
    }

     $("#txtTcKimlik").mask("99999999999");
        $("#txtKartNo1").keypress(function (e) {
            var kartNo = $(this).val();
            var charCode = !e.charCode ? e.which : e.charCode;
            if (kartNo.length == 0) {
                if (charCode == 52) {
                    $('.visa').removeClass('visaPassive');
                    $('.mastercard').addClass('mastercardPassive');
                }
                else if (charCode == 53) {
                    $('.mastercard').removeClass('mastercardPassive');
                    $('.visa').addClass('visaPassive');
                }
            }
        });
        $('#txtKartNo1, #txtKartNo2, #txtKartNo3,#txtKartNo4').autotab_magic();
        $(".numeric").keypress(function (e) {
            if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
        });

        $("#mesafeliSozlesme").click(function () {
            var d = new Date();
            jQuery.facebox({ ajax: "/mesafeliSozlesme.aspx?v="+d+"&ofisid="+<%=ofisId %>,
                loadingImage: '/Resources/images/loading.gif',
                closeImage: '/Resources/images/sepetClose.png'
            });
            //    $('#mesafeliSozlesme').bind('click', function () { doruk_faceboxOpened(this); });
            doruk_faceboxOpened(this);
        });

      
    });



    function confirmDelete() {
        return window.confirm("Servis sepetten çıkarılacak.\nEmin misiniz?");
    }
    $('.deleteMainBasket').click(function () {
        linkHref = $(this).attr('href');
        jConfirm('Servis sepetinizden silinecek, işlemi onaylıyor musunuz?', null, function (result) {if (result) {window.location = linkHref;}
        });
        return false;
    });

    var doruk_prevScrollOffset = false;
    function doruk_faceboxOpened(sender) {
        scrollWindowToTop(sender);
        $('#facebox_overlay').bind('click', function () { doruk_destroyFacebox(this); });
    }

    function scrollWindowToTop(sender) {

        var offset = 0;
        doruk_prevScrollOffset = $(window).scrollTop();
        $('html,body').stop();
        $('html,body').animate({ scrollTop: offset }, 'slow');

    }

    function doruk_faceboxClosed() {
        $('html,body').stop();
        $('html,body').animate({ scrollTop: doruk_prevScrollOffset }, 'slow');
    }

    function doruk_destroyFacebox(sender) {
        $('#facebox').find('.close_image').click();
    }

    sfHover = function () {
        //var sfEls = document.getElementById("navbar").getElementsByTagName("li");
        var sfEls = $('#navbar li');
        for (var i = 0; i < sfEls.length; i++) {
            sfEls[i].onmouseover = function () {
                this.className += " hover";
            }
            sfEls[i].onmouseout = function () {
                this.className = this.className.replace(new RegExp(" hover\\b"), "");
            }
        }
    }
    if (window.attachEvent) window.attachEvent("onload", sfHover);

    if ($.attrFn) { $.attrFn.text = true; }


    $('#btnBitir').click(function () {
    if ($('#txtKartIsim').val() == "" || $('#txtCvc').val() == "" || $('#txtKartNo1').val() == "" || $('#txtKartNo2').val() == "" || $('#txtKartNo3').val() == "" || $('#txtKartNo4').val() == "" || $('#txtTcKimlik').val() == "") 
    {
        if ($('#txtKartNo1').val() == "") {
            $('.kartNoError').css("display", "block");
        }

        else {
            $('.kartNoError').css("display", "none");
        }
        if ($('#txtKartIsim').val() == "") {
            $('.kartIsim').css("display", "block");
           
        }
         else {
            $('.kartIsim').css("display", "none");
        }
        if ($('#txtCvc').val() == "") {
            $('.kartCvc').css("display", "block");
           
        }
         else {
            $('.kartCvc').css("display", "none");
        }
        if ($('#txtTcKimlik').val() == "") {
            $('.tcNoError').css("display", "block");
        }
         else {
            $('.tcNoError').css("display", "none");
        }
          return false;
        }
        else {
            if ($('#chkSozlesme').is(':checked') == false) {
                jAlert('Lütfen Satış Sözleşmesini Onaylayın', 'Ödeme', '');
                return false;
            }
            $("#dialog-confirm").css("display", "");
            $("#dialog-confirm").attr("title", "Servis Onayı");
            $("#dialog-text").html(buildOnayText());
            $("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                minHeight: 100,
                height: 200,
                width: 300,
                buttons: {
                    "Evet": function () {
                        $(this).dialog("close");
                        $.blockUI({
                            message: 'Ödeme İşlemi Gerçekleştiriliyor..',
                            css: { border: 'none', padding: '15px', backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', opacity: 0.5, color: '#fff' }
                        });
                        $('#form1').submit();
                    },
                    "Hayır": function () {
                        $(this).dialog("close");
                        return false;
                    }
                }
            });
            return false;
           
        }
    });
     

    function buildOnayText() {
        var str = "Servis Toplamı : ";
        str += $('.totalPrice span').text() + '<br/><br/>';
        str += 'Ödeme Şekli : Peşin' + '<br/><br/><br/>';
        str += 'Onaylıyor musunuz?';
        return str;

    }

    $('.addNoteMainBasket').click(function () {
        linkHref = $(this).attr('href');
        jPrompt('Randevunuza not eklemek için aşağıadaki alanı doldurunuz.', "<%=Session["BasketNote"]%>", "Randevu Notu", function (result) {
            if (result) {
                window.location = linkHref+result;
            }
        });
        return false;
    });

    $('.hediyeCeki').click(function () {
        var cekKod = $('#txtCekKod').val();
            if (cekKod != '') {
                $.ajax({
                    type: 'POST',
                    url: 'odeme.aspx/hediyeCeki',
                    data: "{kod:'" + cekKod + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d["durum"] == "0") {
                            jAlert(result.d["cekTutar"], 'Bilgi', '');
                        }
                        else {
                        $('#liHediyeCek').css('display','block');
                            $('.hediyeCekVal span').html(result.d["cekTutar"]);
                            $('.totalPrice span').html(result.d["toplamTutar"]);
                            $('div[class ^= "sepetToplamTutar"]').effect("highlight", {}, 1500);
                            $('#txtCekKod').val("");
                            if(result.d["toplamTutar"]=="0,00")
                            {
                                $('#sepetToplam').val(result.d["toplamTutar"]);
                                $('table.kartForm').css('display','none');
                                $('.odemeContainer .cardContainer').css('background-image','none');
                                $('#btnBitir').css('display','none');
                                $('#btnBitir2').css('display','block');
                            }
                        }

                    },
                    error: function () {
                        //alert('Talep esnas&#305;nda sorun olu&#351;tu. Yeniden deneyin');
                    }
                });
        }
        return false;
    });

   

    $('.hediyeCekiList').click(function () {
        var dialog = document.createElement("div");
        $(dialog).hide().load("hediyeceklist.aspx", function (data, textStatus) {
            $(this).dialog({
                width: 600,
                height: 400,
                title: "Hediye Çekleriniz",
                buttons: {
                    "Tamam": function () {
                        $(this).dialog("close");
                        $('#txtCekKod').val($('input[name=hediyeCek]:checked').val());
                        // $('#hediyeCek').submit();
                    },
                    "İptal": function () {
                        $(this).dialog("close");
                    }
                },
                modal: true
            });
        });
    });

     $('.ceksil').click(function () {
        $.ajax({
            type: 'POST',
            url: 'odeme.aspx/cekSil',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $('#txtCekKod').val("");
                window.location.href="/odeme.aspx";
            },
            error: function () {
                //alert('Talep esnas&#305;nda sorun olu&#351;tu. Yeniden deneyin');
            }
        });
    });

 $('#btnBitir2').click(function () {
            if ($('#chkSozlesme').is(':checked') == false) {
                jAlert('Lütfen Satış Sözleşmesini Onaylayın', 'Ödeme', '');
                return false;
            }
            if ($('#txtTcKimlik').val() == "") {
            $('.tcNoError').css("display", "block");
            return false;
        }
         else {
            $('.tcNoError').css("display", "none");
        }
  });
</script>
</asp:Content>















