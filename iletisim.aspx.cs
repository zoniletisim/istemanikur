﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using ZonDBModel;
using System.Web.Services;
using log4net;

public partial class iletisim : System.Web.UI.Page
{
    public static ILog logger = LogManager.GetLogger("default");
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string saveForm(string ad, string email, string tel, string konu, string not)
    {
        string r = "0";
        try
        {
            if (email != "")
            {
                IletisimRepositories rep = new IletisimRepositories();
                var a = rep.saveIletisimForm(ad, email, tel, konu, not);
                if (a.result == true)
                {
                    r = "Mesajınız Başarıyla Gönderildi.";
                    mailGonder(ad, email, tel, konu, not);
                }
            }

            return r;
        }
        catch (Exception)
        {
            return r;
            throw;

        }
    }

    private static void mailGonder(string ad, string email, string tel, string konu, string not)
    {
        ZonDBEntities db = new ZonDBModel.ZonDBEntities();
        try
        {
            string temp = "";

            if (konu == "1")
                temp = "Bayi İstek";
            else if (konu == "2")
                temp = "Teknik Destek";
            else if (konu == "3")
                temp = "Şikayet";
            else
                temp = "Diğer";

            Zon_Ayarlar a = db.Zon_Ayarlar.FirstOrDefault();

            //MailMessage mailToUser = new MailMessage(a.MailAdres, "arif@hipervision.com", "Kale 7/24 İletişim Formu",
            //                    "&nbsp;Kale 7/24 websitesinden <strong>İletişim Formu</strong> doldurulmuştur.<br><br><strong>Kullanıcı Bilgileri</strong> <br><br> Ad Soyad: " + ad + " " + soyad + "<br>Email: " + email + "<br>Telefon : " + tel + "<br>Konu: " + temp + "<br>Not :" + not + "<br><br><br><br><br><br>");

            MailMessage mailToUser = new MailMessage();
            mailToUser.Body = "İştemanikür web sitesinden <strong>İletişim Formu</strong> doldurulmuştur.<br><br><strong>Kullanıcı Bilgileri</strong> <br><br> Ad Soyad: " + ad + "<br>Email: " + email + "<br>Telefon : " + tel + "<br>Konu: " + konu + "<br>Not :" + not + "<br><br><br><br><br><br>";
            mailToUser.IsBodyHtml = true;
            mailToUser.From = new MailAddress(a.MailAdres);

            mailToUser.Subject = "İştemanikür İletişim Formu";

            string receipent = a.BilgiMailAdres;
            string[] rList = receipent.Split(new char[] { ';' });
            foreach (string s in rList)
                mailToUser.To.Add(s);

            mailToUser.To.Add("arif@hipervision.com");

            mailToUser.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            mailToUser.IsBodyHtml = true;
            mailToUser.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");

            System.Net.NetworkCredential userInfo = new System.Net.NetworkCredential(a.MailAdres, a.MailSifre);
            SmtpClient client = new SmtpClient(a.MailServer, 587);

            client.UseDefaultCredentials = false;
            client.Credentials = userInfo;
            //client.Send(mailToMe);
            client.Send(mailToUser);
        }
        catch (Exception ex)
        {
            logger.Error(ex);

        }



    }
}