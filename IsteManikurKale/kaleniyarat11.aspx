﻿<%@ Page Title="" Language="C#" MasterPageFile="kendiKalenMasterPage.master" EnableEventValidation="false"  AutoEventWireup="true" CodeFile="kaleniyarat11.aspx.cs" Inherits="kendiKaleniYarat_kaleniyarat11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style>
.kaleniYaratContainer .redContainer .redContainerTitle {
font-family: 'ErasDemi';
color: #333;
font-size: 18px;
width: 550px;
text-align: center;
margin-left: 112px;
margin-top: 127px;

}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="redContainerTitle">Lütfen aşağıdaki formu doldurup,
iletişim bilgilerinizi bizimle paylaşınız.</div>
<div class="formWrapper" style="margin-top:33px;font-size: 12px;">
    <%--<div class="formLabel">Ad     :</div><asp:TextBox ID="txtAd" class="blackButton" runat="server"></asp:TextBox>
    <div class="formLabel">Soyad  :</div><asp:TextBox ID="txtSoyad" class="blackButton" runat="server"></asp:TextBox>
    <div class="formLabel">E-Mail :</div><asp:TextBox ID="txtEmail" class="blackButton" runat="server"></asp:TextBox>
    <div class="formLabel">Tel    :</div><asp:TextBox ID="txtTel" class="blackButton" runat="server"></asp:TextBox>--%>
    <table cellpadding="0" cellspacing="0" border="0" style="margin-left: 116px;width: 550px;">
    <tr>
        <td style="width:106px;"><div class="formLabel">Ad-Soyad:</div></td>
        <td style="width: 174px;"><asp:TextBox ID="txtAd" class="blackButton" Font-Size="14px" runat="server"></asp:TextBox></td>


     
      
        <td><asp:TextBox ID="txtSoyad" class="blackButton" Font-Size="14px" runat="server"></asp:TextBox></td>
         <td>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="txtAd" ForeColor="White" ErrorMessage="Adınızı Giriniz !"></asp:RequiredFieldValidator>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="txtSoyad" ForeColor="White" ErrorMessage="Soyadnızı Giriniz !"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td><div class="formLabel">E-Mail:</div></td>
        <td><asp:TextBox ID="txtEmail" class="blackButton"  Font-Size="14px" runat="server"></asp:TextBox> </td>
         <td><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ControlToValidate="txtEmail" ForeColor="White" ErrorMessage="Email Giriniz !"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regexEmailValid" Display="Dynamic" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White" ControlToValidate="txtEmail" ErrorMessage="Geçersiz email adresi!"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td><div class="formLabel">Tel:</div></td>
        <td><asp:TextBox ID="txtTel" class="blackButton" Font-Size="14px" ClientIDMode="Static" runat="server"></asp:TextBox> </td>
         <td><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ControlToValidate="txtTel" ForeColor="White" ErrorMessage="Telefon Giriniz !"></asp:RequiredFieldValidator>
        </td>
    </tr>
      <tr>
        <td><div class="formLabel">İl-İlçe:</div></td>
        <td> 
        <div class="blackButton"> 
        <asp:DropDownList ID="ddlSehir"  runat="server" ClientIDMode="Static" 
                onselectedindexchanged="ddlSehir_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
            </div>
        </td>
          
        
        <td> 
         <div class="blackButton"> 
         <asp:DropDownList ID="ddlIlce"  runat="server" ClientIDMode="Static" 
                 onselectedindexchanged="ddlIlce_SelectedIndexChanged">
            </asp:DropDownList>
            </div>
        </td>
         <td>
         <asp:RequiredFieldValidator InitialValue="0" ID="RequiredFieldValidator5" Display="Dynamic"  runat="server" ForeColor="White" ControlToValidate="ddlSehir" ErrorMessage="İl Seçiniz !"></asp:RequiredFieldValidator>
         <asp:RequiredFieldValidator InitialValue="-1" ID="Req_ID" Display="Dynamic" ForeColor="White"  runat="server" ControlToValidate="ddlIlce" ErrorMessage="İlçe Seçiniz !"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td><div class="formLabel">Kale 7/24’ü nerede gördünüz ?:</div></td>
        <td> 
         <div class="blackButton"> 
         <asp:DropDownList ID="ddlAnket"  runat="server" ClientIDMode="Static"   >
         <asp:ListItem Value="-">Seçiniz</asp:ListItem>
         <asp:ListItem Value="İnternet">İnternet</asp:ListItem>
         <asp:ListItem Value="Gazete">Gazete</asp:ListItem>
         <asp:ListItem Value="Dergi">Dergi</asp:ListItem>
         <asp:ListItem Value="Radyo">Radyo</asp:ListItem>
         <asp:ListItem Value="Mail">Mail</asp:ListItem>
         <asp:ListItem Value="SMS">SMS</asp:ListItem>
         <asp:ListItem Value="Sosyal medya">Sosyal medya</asp:ListItem>
          <asp:ListItem Value="Kale Çalışan Kampanyası">Kale Çalışan Kampanyası</asp:ListItem>
             <asp:ListItem Value="brosur-calismasi">Broşür Çalışması</asp:ListItem>
  
            </asp:DropDownList>
            </div>
        </td>
        <td>
         <!-- added brosur -->
            <div class="blackButton brosurHolder" style="display:none;">
                <asp:DropDownList ID="ddlBrosur"  runat="server" ClientIDMode="Static"   >
                    <asp:ListItem Value="-">Seçiniz</asp:ListItem>
                    <asp:ListItem value="Avcılar">Avcılar saha çalışması</asp:ListItem>
                    <asp:ListItem value="Küçükçekmece">Küçükçekmece saha çalışması</asp:ListItem>
                    <asp:ListItem value="Fatih">Fatih saha çalışması</asp:ListItem>
                    <asp:ListItem value="Beşiktaş">Beşiktaş saha çalışması</asp:ListItem>
                    <asp:ListItem value="Bahçelievler">Bahçelievler saha çalışması</asp:ListItem>
                    <asp:ListItem value="Gaziosmanpaşa">Gaziosmanpaşa saha çalışması</asp:ListItem>
                    <asp:ListItem value="Beylikdüzü">Beylikdüzü saha çalışması</asp:ListItem>
                    <asp:ListItem value="Esenyurt">Esenyurt saha çalışması</asp:ListItem>
                    <asp:ListItem value="Beyoğlu">Beyoğlu saha çalışması</asp:ListItem>
                    <asp:ListItem value="Şişli">Şişli saha çalışması</asp:ListItem>
                    <asp:ListItem value="Eyüp">Eyüp saha çalışması</asp:ListItem>
                    <asp:ListItem value="Kadıköy">Kadıköy saha çalışması</asp:ListItem>
                    <asp:ListItem value="Maltepe">Maltepe saha çalışması</asp:ListItem>
                    <asp:ListItem value="Üsküdar">Üsküdar saha çalışması</asp:ListItem>
                    <asp:ListItem value="Ataşehir">Ataşehir saha çalışması</asp:ListItem>
                    <asp:ListItem value="Ümraniye">Ümraniye saha çalışması</asp:ListItem>
                </asp:DropDownList>
            </div>
        </td>
    </tr>
    </table>
      <div style="font-family: arial;font-size: 11px;display: block;margin-left: 166px;margin-top: 63px;width: 410px;">
      
        *Çalışma saatlerimiz 7:30 ile 17;30 arasındadır , 17;30'dan sonra gelen
        taleplere bir sonraki resmi iş günü irtibata geçilecektir.
        </div>
</div>    
    
<a href="kaleniyarat10.aspx" class="goBack"></a>
<asp:Button ID="Button1" runat="server" ClientIDMode="Static" class="goContinue" Text="Button" 
        onclick="Button1_Click" />
           <script src="../resources/js/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
        <script type="text/javascript">
            function alert() {
                jAlert("Belirtilen noktaya hizmetimiz bulunmamaktadır.", "Bilgi", "");
            }
            jQuery(function ($) {
                $("#txtTel").mask("999 9999999");

                $("#ddlAnket").change(function () {
                    if ($(this).val() == "brosur-calismasi") {
                        $(".brosurHolder").show();
                    }
                    else {
                        $(".brosurHolder").hide();
                    }
                });
            });
//            $(document).ready(function () {
//                //$("#ddTaksit").combobox(); 
//                $("#ddlSehir").change(function () {
//                    $("#ddlIlce").html("");
//                    $("#loadingB").css("display", "");
//                    $.ajax({
//                        type: "POST",
//                        url: "kaleniyarat11.aspx/ilceGetir",
//                        data: "{id: '" + $('#ddlSehir').val() + "'}",
//                        contentType: "application/json; charset=ISO-8859-15",
//                        dataType: "json",
//                        success: function (response) {
//                            BindCityddl(response.d)
//                            $("#loadingB").css("display", "none");

//                        }
//                    });
//                });
//            });
//            function BindCityddl(msg) {
//                $("#ddlIlce").html("");
//                $("#ddlIlce").append($("<option></option>").val(-1).html("SEÇİNİZ.."));
//                $("#ddlIlce").append($("<option></option>").val(0).html("MERKEZ"));
//                $.each(msg, function () {
//                    $("#ddlIlce").append($("<option></option>").val(this['id']).html(this['aciklama']));
//                });
//            }

            $("#ddlIlce").change(function () {
                //   $("#loadingB").css("display", "");
                $.ajax({
                    type: "POST",
                    url: "kaleniyarat11.aspx/ilceAktifKontrol",
                    data: "{id: '" + $('#ddlIlce').val() + "'}",
                    contentType: "application/json; charset=ISO-8859-15",
                    dataType: "json",
                    success: function (response) {
                        if (response.d) {
                            $('#Button1').css("display", "none");

                            parent.alert();

                        }
                        else {

                            $('#Button1').css("display", "block");
                        }
                        // $("#loadingB").css("display", "none");
                    }
                });

            });
</script>
</asp:Content>




