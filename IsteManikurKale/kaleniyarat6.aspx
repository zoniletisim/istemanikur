﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kendiKaleniYarat/kendiKalenMasterPage.master" AutoEventWireup="true" CodeFile="kaleniyarat6.aspx.cs" Inherits="kendiKaleniYarat_kaleniyarat6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style>
.redContainer .blackButton {
width: 105px;
height: 30px;
background: url(../resources/images/common/kaleniyaratSubmitBlack.png);
display: block;
text-align: center;
line-height: 32px;
text-decoration: none;
color: white;
font-family: arial;
font-size: 16px;
float: left;
margin-top: 42px;
border: none;
outline: none;
margin-left: 310px;
	}
.redContainer .blackButton:focus { 
border:none; 
outline: none;
}	
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="redContainerTitle">Giriş koridoru hariç koridor sayısı?</div>
    
     <asp:TextBox ID="txtVal" ClientIDMode="Static" class="blackButton numeric" runat="server"></asp:TextBox>
    <a href="kaleniyarat5.aspx" class="goBack"></a>
    <input id="Button2" type="button" class="goContinue" value="" />

    <script type="text/javascript">
        $('#Button2').click(function () {
            if ($('#txtVal').val() == "")
                alert("Lütfen Giriş Yapın..");
            else
                $('#form1').submit();
        });
        $(document).ready(function () {
            $('#txtVal').focus();
        });
      </script>
</asp:Content>


