// jQuery Alert Dialogs Plugin
//
// Version 1.0
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 29 December 2008
//
// Visit http://abeautifulsite.net/notebook/87 for more information
//
// Usage:
//		jAlert( message, [title, callback] )
//		jConfirm( message, [title, callback] )
//		jPrompt( message, [value, title, callback] )
// 
// History:
//
//		1.00 - Released (29 December 2008)
//
// License:
// 
//		This plugin is licensed under the GNU General Public License: http://www.gnu.org/licenses/gpl.html
//

// BURADA jSuccess diye bir link eklendi. Bunun yeni sürümlerde dikkatli taşınması gerekiyor. 
// 13.10.2010 - jSuccess eklendi - Positive 

(function($) {
	
	$.alerts = {
		
		// These properties can be read/written by accessing $.alerts.propertyName from your scripts at any time
		
		verticalOffset: -75,                // vertical offset of the dialog from center screen, in pixels
		horizontalOffset: 0,                // horizontal offset of the dialog from center screen, in pixels/
		repositionOnResize: true,           // re-centers the dialog on window resize
		overlayOpacity: .40,                // transparency level of overlay
		overlayColor: '#000',               // base color of overlay
		draggable: true,                    // make the dialogs draggable (requires UI Draggables plugin)
		okButton: '&nbsp;Tamam&nbsp;',         // text for the OK button
		cancelButton: '&nbsp;Vazgeç&nbsp;', // text for the Cancel button
		yesButton: '&nbsp;Evet&nbsp;', // text for the Cancel button
		noButton: '&nbsp;Hayır&nbsp;', // text for the Cancel button
		SaveButton: '&nbsp;Kaydet&nbsp;', // text for the Cancel button
        dialogClass: null,                  // if specified, this class will be applied to all dialogs
		
		// Public methods
		
		alert: function(message, title, callback) {
			if( title == null ) title = 'Hata!';
			$.alerts._show(title, message, null, 'alert', function(result) {
				if( callback ) callback(result);
			});
		},

		success: function(message, title, callback) {
			if( title == null ) title = 'Onay!';
			$.alerts._show(title, message, null, 'success', function(result) {
				if( callback ) callback(result);
			});
		},
		
		confirm: function(message, title, callback) {
			if( title == null ) title = 'Onay Penceresi';
			$.alerts._show(title, message, null, 'confirm', function(result) {
				if( callback ) callback(result);
			});
		},
			
		prompt: function(message, value, title, callback) {
			if( title == null ) title = 'Onay Penceresi';
			$.alerts._show(title, message, value, 'prompt', function(result) {
				if( callback ) callback(result);
			});
},
hediyeCeki: function (message, value, title, callback) {
    if (title == null) title = 'Onay Penceresi';
    $.alerts._show(title, message, value, 'hediyeCeki', function (result) {
        if (callback) callback(result);
    });
},
        editor: function (message, value, title, callback) {
            if (title == null) title = 'Onay Penceresi';
            $.alerts._show(title, message, value, 'editor', function (result) {
                if (callback) callback(result);
            });
        },
		stockalert: function(message, value, title, callback) {
			if( title == null ) title = 'Stokta Yok';
			$.alerts._show(title, message, value, 'stockalert', function(result) {
				if( callback ) callback(result);
			});
		},
		
		// Private methods
		
		_show: function(title, msg, value, type, callback) {
			
			$.alerts._hide();
			$.alerts._overlay('show');
			
			$("BODY").append(
			  '<div id="popup_container">' +
			    '<h1 id="popup_title"></h1>' +
			    '<div id="popup_content">' +
			      '<div class="popup_message"></div>' +
				'</div>' +
			  '</div>');
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);
			
			// IE6 Fix
			var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed'; 
			
			$("#popup_container").css({
				position: pos,
				zIndex: 99999,
				padding: 0,
				margin: 0
			});
			
			$("#popup_title").text(title);
			$("#popup_content").addClass(type);
			$(".popup_message").text(msg);
			$(".popup_message").html( $(".popup_message").text().replace(/\n/g, '<br />') );
			
			$("#popup_container").css({
				minWidth: $("#popup_container").outerWidth(),
				maxWidth: $("#popup_container").outerWidth()
			});
			
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			
			switch( type ) {
				case 'alert':
					$(".popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '"  class="button" id="popup_ok" title="" /></div>');
					$('#popup_container').addClass("jAlert");
					$("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					$("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
				break;
				case 'success':
					$(".popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '"  class="button" id="popup_ok" title="" /></div>');
					$('#popup_container').addClass("jSuccess");
					$("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					$("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
				break;
				case 'confirm':
					$(".popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.yesButton + '" id="popup_ok" class="button" title="" alt="" /> <input type="button" value="' + $.alerts.noButton + '"  class="button" id="popup_cancel" title="" /></div>');
					$('#popup_container').addClass("jConfirm");
					$("#popup_ok").click( function() {
						$.alerts._hide();
						if( callback ) callback(true);
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback(false);
					});
					$("#popup_ok").focus();
					$("#popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
				break;
                    case 'prompt':
                        $(".popup_message").append('<br /><br /><center><div class="ui-form"> <textarea style="width:275px;height:120px;"   id="popup_prompt" ></textarea></div></center> ').after('<div id="popup_panel"><input type="button" value="' + $.alerts.SaveButton + '" id="popup_ok" class="button" title="" alt="" /> <input type="button" value="' + $.alerts.cancelButton + '"  class="button" id="popup_cancel" title="" /></div>');
                        $("#popup_container").addClass("jConfirm");
					$("#popup_ok").click( function() {
						var val = $("#popup_prompt").val();
						$.alerts._hide();
						if( callback ) callback( val );
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$("#popup_prompt, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if( value ) $("#popup_prompt").val(value);
					$("#popup_prompt").focus().select();
					break;
	case 'hediyeCeki':
	    $(".popup_message").append('<br /><br/><span style="font-size:11px">Hediye Çeki Kodu:</span> <input type="text" style="width:150px;font-size:11px" id="popup_prompt" /> ').after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" class="button" title="" alt="" /> <input type="button" value="' + $.alerts.cancelButton + '"  class="button" id="popup_cancel" title="" /></div>');
	    $("#popup_container").addClass("jConfirm");
	    $("#popup_ok").click(function () {
	        var val = $("#popup_prompt").val();
	        $.alerts._hide();
	        if (callback) callback(val);
	    });
	    $("#popup_cancel").click(function () {
	        $.alerts._hide();
	        if (callback) callback(null);
	    });
	    $("#popup_prompt, #popup_ok, #popup_cancel").keypress(function (e) {
	        if (e.keyCode == 13) $("#popup_ok").trigger('click');
	        if (e.keyCode == 27) $("#popup_cancel").trigger('click');
	    });
	    if (value) $("#popup_prompt").val(value);
	    $("#popup_prompt").focus().select();
	    break;
	        case 'editor':
	            $(".popup_message").append('<br /><br /><span style="font-size:11px">Email:</span><div class="ui-form"> <input style="width:300px" type="text" id="baslik" ></div><br/><br/> <span style="font-size:11px">Mesaj:</span><div class="ui-form"> <textarea style="width:300px"   id="popup_prompt" ></textarea></div> ').after('<div id="popup_panel"><input type="button" value="' + $.alerts.SaveButton + '" id="popup_ok" class="button" title="" alt="" /> <input type="button" value="' + $.alerts.cancelButton + '"  class="button" id="popup_cancel" title="" /></div>');
	            $("#popup_container").addClass("jConfirm");
	            $("#popup_ok").click(function () {
	                var val = $("#popup_prompt").val();
	                var val2 = $("#baslik").val();
	                $.alerts._hide();
	                if (callback) callback(val2+"+"+val);
	            });
	            $("#popup_cancel").click(function () {
	                $.alerts._hide();
	                if (callback) callback(null);
	            });
	            $("#popup_prompt, #popup_ok, #popup_cancel").keypress(function (e) {
	                if (e.keyCode == 13) $("#popup_ok").trigger('click');
	                if (e.keyCode == 27) $("#popup_cancel").trigger('click');
	            });
	            if (value) $("#popup_prompt").val(value);
	            $("#baslik").focus().select();
	            break;
				case 'stockalert':
					$("#popup_container").addClass("stockalert");
					$("#popup_container").find("h1").before('<span class="icon ir"></span><a class="popupClose ir" id="popupClose"  href="#">Kapat</a>');
					$(".popup_message").after('<div class="popup"><input type="button" value="Alışverişe Devam" id="popup_ok" class="button" title="" /></div>');
					$("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					$("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
					$("#popupClose").click(function(e) {
						$("#popup_ok").trigger('click');
					});
				break;
			}
			
			// Make draggable
			if( $.alerts.draggable ) {
				try {
					$("#popup_container").draggable({ handle: $("#popup_title") });
					$("#popup_title").css({ cursor: 'move' });
				} catch(e) { /* requires jQuery UI draggables */ }
			}
		},
		
		_hide: function() {
			$("#popup_container").remove();
			$.alerts._overlay('hide');
			$.alerts._maintainPosition(false);
		},
		
		_overlay: function(status) {
			switch( status ) {
				case 'show':
					$.alerts._overlay('hide');
					$("BODY").append('<div id="popup_overlay" class="popup_overlay"></div>');
					$("#popup_overlay").css({
						position: 'absolute',
						zIndex: 99998,
						top: '0px',
						left: '0px',
						width: '100%',
						height: $(document).height()
					});
				break;
				case 'hide':
					$("#popup_overlay").remove();
				break;
			}
		},
		
		_reposition: function() {
			var top = (($(window).height() / 2) - ($("#popup_container").outerHeight() / 2)) + $.alerts.verticalOffset;
			var left = (($(window).width() / 2) - ($("#popup_container").outerWidth() / 2)) + $.alerts.horizontalOffset;
			if( top < 0 ) top = 0;
			if( left < 0 ) left = 0;
			
			// IE6 fix
			if( $.browser.msie && parseInt($.browser.version) <= 6 ) top = top + $(window).scrollTop();
			
			$("#popup_container").css({
				top: top + 'px',
				left: left + 'px'
			});
			$("#popup_overlay").height( $(document).height() );
		},
		
		_maintainPosition: function(status) {
			if( $.alerts.repositionOnResize ) {
				switch(status) {
					case true:
						$(window).bind('resize', function() {
							$.alerts._reposition();
						});
					break;
					case false:
						$(window).unbind('resize');
					break;
				}
			}
		}
		
	}
	
	// Shortuct functions
	jAlert = function(message, title, callback) {
		$.alerts.alert(message, title, callback);
	}

	jStockAlert = function(message, value, title, callback) {
		$.alerts.stockalert(message, value, title, callback);
	}
	
	jSuccess = function(message, title, callback) {
		$.alerts.success(message, title, callback);
	}
	
	jConfirm = function(message, title, callback) {
		$.alerts.confirm(message, title, callback);
}
jEditor = function (message,value, title, callback) {
    $.alerts.editor(message,value, title, callback);
}
		
	jPrompt = function(message, value, title, callback) {
		$.alerts.prompt(message, value, title, callback);
	}
jHediyeCeki = function (message, value, title, callback) {
    $.alerts.hediyeCeki(message, value, title, callback);
}
})(jQuery);