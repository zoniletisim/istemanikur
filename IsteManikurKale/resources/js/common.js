// JavaScript Document

function trace(msg) {
	if(console)
		console.log(msg);
	else alert(msg);
}

function removePx(str) {
	str = str + "";
	str = str.toLowerCase();
	while(str.indexOf("px") > 0)
		str = str.replace("px", "");
	str = parseInt(str);
	return str;
}

$(document).ready(function() {
   
    bindPlaceHolders();
    var a = getParameterByName('hash')
    if (a == "haber7")
    {


        $(".haber7Trigger").fancybox({
            fitToView: false,
            width: '480px',
            height: '585px',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            parent: $('form:first')
        }).trigger('click');
    }
	
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function bindPlaceHolders(input) {

	if(input) {
		bindSinglePlaceHolder(input);	
	} else {
		$("input[type=text]").each(function(index, element) {
			bindSinglePlaceHolder(element);
		});
	}
	
	function bindSinglePlaceHolder(element) {
		var placeHolder = $(element).attr('plc');
		var originalColor = $(element).css('color');
		var plcColor = $(element).attr('plcColor');
		if(typeof plcColor == 'undefined')
			plcColor = originalColor;

		if(typeof placeHolder != 'undefined') {
			
			if($(element).val() == '') {
				$(element).val(placeHolder);
				$(element).css('color', plcColor);
			}
			
			$(element).focus(function(){
				if($(element).val() == placeHolder) {
					$(element).val('');
					$(element).css('color', originalColor);
				}
			});
			
			$(element).blur(function(){
				if($(element).val() == '') {
					$(element).val(placeHolder);
					$(element).css('color', plcColor);
				}
			});
		}
	}
}

function disableScroll(disabled) {
	if(disabled) {
		$('body').bind('mousewheel DOMMouseScroll', function(e) { return false; });
		return;
	}
	$('body').unbind('mousewheel DOMMouseScroll');
}