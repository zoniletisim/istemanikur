// JavaScript Document

function Sprite(args) {	
	this.container 		= args.container;
	this.imagePath 		= args.imagePath;
	this.framesCount 	= args.framesCount;
	this.fps			= args.fps;
	this.looped			= args.looped;
	
	this.width			= args.width;
	this.height			= args.height;
	
	this.frameInterval 	= Math.floor(1000 / this.fps);
	this.currentFrame 	= 0;
	this.timer			= false;
	this.backgroundSet	= false;
	this.tickOffset 	= 1;
	
	$(this.container).css('width', this.width);
	$(this.container).css('height', this.height);
	
	if(typeof args.beginFrame != "undefined")
		this.renderFrame(args.beginFrame);
}

Sprite.prototype.play = function () {
	this.gotoAndPlay(this.currentFrame);
}

Sprite.prototype.pause = function () {
	clearInterval(this.timer);
}

Sprite.prototype.setLoop = function (loopedIn) {
	this.looped = loopedIn;
}

Sprite.prototype.gotoAndPlay = function (whatFrame) {
	if(this.tickOffset > 0)
		var endFrame = 0;//this.framesCount;
	else var endFrame = 0;
	this.playInterval(whatFrame, endFrame, false);
	/*
	var me = this;
	
	me.pause();
	me.renderFrame(whatFrame);
	
	this.timer = setInterval(
		function() {
			var nextFrame = me.currentFrame + me.tickOffset;
			if(!me.looped && (nextFrame >= me.framesCount || nextFrame < 0))
				me.pause();
			me.renderFrame(nextFrame);
		}, me.frameInterval
	);
	*/
}

Sprite.prototype.playInterval = function (startFrame, endFrame, loopedIn) {

	var me = this;
//trace("startFrame: " + startFrame);	
//trace("endFrame: " + endFrame);
	me.pause();
	me.renderFrame(startFrame);
	
	this.timer = setInterval(
		function() {
//			var nextFrame = (me.currentFrame + me.tickOffset) % me.framesCount;
			var nextFrame = me.modulo(me.currentFrame + me.tickOffset);
//trace("endFrame: " + endFrame);	
//trace("nextFrame: " + nextFrame);
			me.renderFrame(nextFrame);
	
			if(me.currentFrame == endFrame) {
				me.pause();
				if(loopedIn)
					me.playInterval(startFrame, endFrame, loopedIn);	
			}
			
		}, me.frameInterval
	);
	
}

Sprite.prototype.animateToFrame = function (args) {
	this.pause();
	
	var endFrame = args.endFrame;
	
	var me = this;
	var framesToPlay = new Array();
	
	var increment = me.tickOffset;
	
	if(args.shortestPath) {
		if(endFrame > me.currentFrame)	
			increment = 1;
		else increment = -1;
	}
	
	for(var i = me.currentFrame; i != endFrame; i += increment) {
		i = me.modulo(i);
//		i %= me.framesCount;
		framesToPlay.push(i);	
	}
	framesToPlay.push(endFrame);	
	
	framesToPlay.reverse();
//trace(me.frameInterval);
	this.timer = setInterval(
		function() {
			var nextFrame = framesToPlay.pop();
//			trace('nextFrame is: ' + nextFrame);
			me.renderFrame(nextFrame);
	
			if(framesToPlay.length <= 0) {
				me.pause();
				if(args.onFinish)
					args.onFinish();	
			}
			
		}, me.frameInterval
	);
}

Sprite.prototype.reverseAnimation = function (revIn) {

	if(typeof revIn == 'undefined')
		this.tickOffset *= -1;
	else if(revIn)
		this.tickOffset = -1;
	else this.tickOffset = 1;
	
}

Sprite.prototype.modulo = function (val) {
	val += 2 * this.framesCount;
	val %= this.framesCount;
	return val;
}

Sprite.prototype.renderFrame = function (whatFrame) {
	whatFrame = this.modulo(whatFrame);

	var offsetX = -1 * (whatFrame * this.width);
	$(this.container).css('background-position', offsetX + 'px 0px');

	var fullPath = 'url(' + this.imagePath + ')';
	
	if(!this.backgroundSet) {
		var cmd = "$(this.container).css('background-image', 'url(" + this.imagePath + ")');"
		eval(cmd);
		this.backgroundSet = true;
	}
		
	this.currentFrame = whatFrame;
}