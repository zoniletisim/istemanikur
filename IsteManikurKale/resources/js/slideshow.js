$slideshow = {
    context: false,
    tabs: false,
    timeout:4000,   // time before next slide appears (in ms)
    slideSpeed: 500,   // time it takes to slide in each slide (in ms)
    tabSpeed: 500,      // time it takes to slide in each slide (in ms) when clicking through tabs
    fx: 'scrollLeft',   // the slide effect to use
 

    init: function() {
        // set the context to help speed up selectors/improve performance
        this.context = $('#slideshow');
        
        // set tabs to current hard coded navigation items
        this.tabs = $('ul.slides-nav li', this.context);
        
        // remove hard coded navigation items from DOM 
        // because they aren't hooked up to jQuery cycle
        this.tabs.remove();
        
        // prepare slideshow and jQuery cycle tabs
        this.prepareSlideshow();
    },
    
    prepareSlideshow: function() {
        $('div.slides > ul', $slideshow.context).cycle({
           fx: $slideshow.fx,
            autostop: $slideshow.autostop,
            timeout: $slideshow.timeout, 
            speed: $slideshow.slideSpeed, 
            fastOnEvent: $slideshow.tabSpeed, 
            pager: $("ul.slides-nav", $slideshow.context), 
            pagerAnchorBuilder: $slideshow.prepareTabs, 
            before: $slideshow.activateTab
//pauseOnPagerHover: 1, 
//pause: true 
        });    
     $("#resim1").click(function(){
         $("div.slides > ul", $slideshow.context).cycle('pause');
     });
     $("#resim2").click(function () {
         $("div.slides > ul", $slideshow.context).cycle('pause');
     });     
    },
    
    prepareTabs: function(i, slide) {
        return $slideshow.tabs.eq(i);
    },

  

    activateTab: function(currentSlide, nextSlide) {
        var activeTab = $('a[href="#' + nextSlide.id + '"]', $slideshow.context);
        
        if(activeTab.length) {
            $slideshow.tabs.removeClass('on');
            activeTab.parent().addClass('on');
        }            
    }            
};


$(function() {
    $('body').addClass('js');
    $slideshow.init();
});


