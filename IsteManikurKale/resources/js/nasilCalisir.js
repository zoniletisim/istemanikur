function NasilCalisir(containerIn) {
	this.container = containerIn;
	this.speed = 1.5;
	this.init();
}

NasilCalisir.prototype.init = function() {
	var me = this;
	$(me.container).html('<div class="animationsWrapper">\
                <div class="aBottomText bottomText1"></div>\
                <div class="aBottomText bottomText2"></div>\
                <div class="aBottomText bottomText3"></div>\
                <div class="aBottomText bottomText4"></div>\
                <div class="aBottomText bottomText5"></div>\
                <div class="aBottomText bottomText6"></div>\
                <div class="aBall ball1"></div>\
                <div class="aBall ball2"></div>\
                <div class="aBall ball3"></div>\
                <div class="aBall ball4"></div>\
                <div class="aBall ball5"></div>\
                <div class="aBall ball6"></div>\
                <div class="aLine line1"></div>\
                <div class="aLine line2"></div>\
                <div class="aLine line3"></div>\
                <div class="aLine line4"></div>\
                <div class="aLine line5"></div>\
                <div class="aRightText rightText1"></div>\
                <div class="aRightText rightText2"></div>\
                <div class="aRightText rightText3"></div>\
                <div class="aRightText rightText4"></div>\
                <div class="aRightText rightText5"></div>\
            </div> <!-- End animationsWrapper -->\
            <div class="topWrapper">\
                <div class="playButton"></div>\
            </div> <!-- End topWrapper -->\
	');
	$(me.container).find('.playButton').click(function(){me.beginAnimations();});
}

NasilCalisir.prototype.beginAnimations = function() {
	var me = this;
	$(me.container).find('.topWrapper').remove();
//	$(me.container).find('.bottomText').css('background-image', 'url(../resources/images/nasilCalisir/bottomText1.png)');
	me.showButtomText('.bottomText1');
	$(me.container).find('.bottomText').fadeIn();
	$(me.container).find('.ball1').fadeIn();
	setTimeout(function(){me.nextStep(1);}, 3000 * me.speed);
}

NasilCalisir.prototype.nextStep = function(whichStep) {
	var me = this;
	if(whichStep == 1) {
		me.drawLine('.line1');
		return;
	}
	
	if(whichStep == 2) {
//		$(me.container).find('.bottomText').css('background-image', 'url(../resources/images/nasilCalisir/bottomText2.png)');
		me.showButtomText('.bottomText2');
		$(me.container).find('.ball2').fadeIn();
		setTimeout(function(){me.drawLine('.line2');}, 3000 * me.speed);
		return;
	}
	
	if(whichStep == 3) {
//		$(me.container).find('.bottomText').css('background-image', 'url(../resources/images/nasilCalisir/bottomText3.png)');
		me.showButtomText('.bottomText3');
		$(me.container).find('.ball3').fadeIn();
		setTimeout(function(){me.drawLine('.line3');}, 3000 * me.speed);
		return;
	}
	
	if(whichStep == 4) {
//		$(me.container).find('.bottomText').css('background-image', 'url(../resources/images/nasilCalisir/bottomText4.png)');
		me.showButtomText('.bottomText4');
		$(me.container).find('.ball4').fadeIn();
		setTimeout(function(){me.drawLine('.line4');}, 3000 * me.speed);
		return;
	}
	
	if(whichStep == 5) {
//		$(me.container).find('.bottomText').css('background-image', 'url(../resources/images/nasilCalisir/bottomText5.png)');
		me.showButtomText('.bottomText5');
		$(me.container).find('.ball5').fadeIn();
		setTimeout(function(){me.drawLine('.line5');}, 3000 * me.speed);
		return;
	}
	
	if(whichStep == 6) {
//		$(me.container).find('.bottomText').css('background-image', 'url(../resources/images/nasilCalisir/bottomText6.png)');
		me.showButtomText('.bottomText6');
		$(me.container).find('.ball6').fadeIn();
		me.showRightTexts();
		setTimeout(function(){me.init();}, 4000 * me.speed);
//		trace('finito');
		return;
	}
	
}

NasilCalisir.prototype.showButtomText = function(whichBottomText) {
	var me = this;
	
	setTimeout(function(){
		$(me.container).find('.aBottomText').fadeOut();
		$(me.container).find(whichBottomText).fadeIn();
	}, 500 * me.speed);
	
}

NasilCalisir.prototype.showRightTexts = function() {
	var me = this;
	
	setTimeout(function(){$(me.container).find('.rightText1').show('slide', { direction: 'right' }, 'fast');}, 200 * me.speed);
	setTimeout(function(){$(me.container).find('.rightText2').show('slide', { direction: 'right' }, 'fast');}, 400 * me.speed);
	setTimeout(function(){$(me.container).find('.rightText3').show('slide', { direction: 'right' }, 'fast');}, 600 * me.speed);
	setTimeout(function(){$(me.container).find('.rightText4').show('slide', { direction: 'right' }, 'fast');}, 800 * me.speed);
	setTimeout(function(){$(me.container).find('.rightText5').show('slide', { direction: 'right' }, 'fast');}, 1000 * me.speed);
	

}

NasilCalisir.prototype.drawLine = function(whichLine) {
	var me = this;
	var line = $(me.container).find(whichLine);
	if($(line).length <= 0)
		return;
	
	var drawTime = 1000  * me.speed;
	if(whichLine == '.line1')
		var args = {width: 122, height: 95, time: drawTime, direction: 'x', animationComplete: function(){me.nextStep(2);}};
	else if(whichLine == '.line2')
		var args = {width: 166, height: 163, time: drawTime, direction: 'x', animationComplete: function(){me.nextStep(3);}};
	else if(whichLine == '.line3')
		var args = {width: 669, height: 194, time: drawTime, direction: 'x', animationComplete: function(){me.nextStep(4);}};
	else if(whichLine == '.line4')
		var args = {width: 2, height: 23, time: drawTime, direction: 'y', animationComplete: function(){me.nextStep(5);}};
	else if(whichLine == '.line5')
		var args = {width: 2, height: 28, time: drawTime, direction: 'y', animationComplete: function(){me.nextStep(6);}};
		
//	trace(whichLine);
	$(line).css('width', 0);
	$(line).css('height', 0);
	$(line).show();
	
	if(args.direction == 'x') {
		$(line).css('height', args.height);
		$(line).animate({width: args.width}, args.time, args.animationComplete);
	} else if(args.direction == 'y') {
		$(line).css('width', args.width);
		$(line).animate({height: args.height}, args.time, args.animationComplete);
	}
}