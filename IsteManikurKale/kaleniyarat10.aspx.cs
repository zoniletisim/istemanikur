﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class kendiKaleniYarat_kaleniyarat10 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["TehlikeNoktasi"] != null)
        {
            if (Session["TehlikeNoktasi"].ToString() == "0")
            {
                alarmPaneli.InnerHtml = "Alarm Paneli:";
                alarmPaneliNumber.InnerHtml = "1";
                Session.Add("alarmPaneli", "1");
                tusTakimi.InnerHtml = "Tuş Takımı:";
                tusTakimiNumber.InnerHtml = "1";
                Session.Add("tusTakimi", "1");
                DahiliSiren.InnerHtml = "Dahili Siren:";
                DahiliSirenNumber.InnerHtml = "1";
                Session.Add("DahiliSiren", "1");
                HariciSiren.InnerHtml = "Harici Siren:";
                HariciSirenNumber.InnerHtml = "1";
                Session.Add("HariciSiren", "1");
                ManyetikKontak.InnerHtml = "Manyetik Kontak:";
                ManyetikKontakNumber.InnerHtml = "1";
                Session.Add("ManyetikKontak", "1");
                Pir.InnerHtml = "Pır:";
                PirNumber.InnerHtml = "1";
                Session.Add("Pir", "1");

              
            }
            else
            {
                int adim5 = Session["adim5"] != null ? Convert.ToInt32(Session["adim5"].ToString()) : 0;
                int adim6 = Session["adim6"] != null ? Convert.ToInt32(Session["adim6"].ToString()) : 0;
                int adim7 = Session["adim7"] != null ? Convert.ToInt32(Session["adim7"].ToString()) : 0;
                int adim8 = Session["adim8"] != null ? Convert.ToInt32(Session["adim8"].ToString()) : 0;
                alarmPaneli.InnerHtml = "Alarm Paneli:";
                alarmPaneliNumber.InnerHtml = "1" ;
                tusTakimi.InnerHtml = "Tuş Takımı:";
                tusTakimiNumber.InnerHtml = "1";
                DahiliSiren.InnerHtml = "Dahili Siren:";
                DahiliSirenNumber.InnerHtml = "1";
                HariciSiren.InnerHtml = "Harici Siren:";
                HariciSirenNumber.InnerHtml = "1";
                ManyetikKontak.InnerHtml = "Manyetik Kontak:";
                ManyetikKontakNumber.InnerHtml = (1+adim5).ToString() ;
                Pir.InnerHtml = "Pır:";
                PirNumber.InnerHtml = (1+adim6).ToString();
                GazDedektor.InnerHtml = "Gaz Dedektörü:";
                GazDedektorN.InnerHtml = (adim7).ToString();
                DumanDedektor.InnerHtml = "Duman Dedektörü:";
                DumanDedektorN.InnerHtml = (adim8).ToString();

                Session.Add("alarmPaneli", "1");
                Session.Add("tusTakimi", "1");
                Session.Add("DahiliSiren", "1");
                Session.Add("HariciSiren", "1");
                Session.Add("ManyetikKontak", (1 + adim5).ToString());
                Session.Add("Pir", (1+adim6).ToString());
                Session.Add("GazDedektor", (adim7).ToString());
                Session.Add("DumanDedektor", (adim8).ToString());
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("kaleniyarat11.aspx");
    }
}