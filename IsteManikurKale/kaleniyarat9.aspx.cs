﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class kendiKaleniYarat_kaleniyarat9 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (radioKablolu.Checked)
                Session.Add("SistemTercihi", "kablolu");
            else
                Session.Add("SistemTercihi", "kablosuz");

            Response.Redirect("kaleniyarat10.aspx");
        }
    }
}