﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class kendiKaleniYarat_kaleniyarat8 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (txtVal.Text != "")
            {
                Session.Add("adim8", txtVal.Text);

                Response.Redirect("kaleniyarat9.aspx");

            }
        }
    }
}