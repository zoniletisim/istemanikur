﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class kendiKaleniYarat_kaleniyarat7 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            if (txtVal.Text != "")
            {
                Session.Add("adim7", txtVal.Text);

                Response.Redirect("kaleniyarat8.aspx");

            }
        }
    }
}