﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class kendiKaleniYarat_kaleniyarat4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string temp = "";
        if (Page.IsPostBack)
        {
            if (radioVar.Checked)
            {
                Session.Add("TehlikeNoktasi", "Var");
                temp = "kaleniyarat5.aspx";
            }
            else
            {
                Session.Add("TehlikeNoktasi", "Yok");
                temp = "kaleniyarat9.aspx";
            }

            Response.Redirect(temp, false);
        }
    }
}