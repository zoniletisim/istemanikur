﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class siteMasterPageDefault : System.Web.UI.MasterPage
{
    SayfaRepositories rep = new SayfaRepositories();
    public List<View_Sayfalar> sayfa = new List<View_Sayfalar>();
    ZonDBEntities db = new ZonDBEntities();
    public string pageID = "0";
    public string page = "0";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["id"] != null)
            pageID = Request.QueryString["id"];

        page = Request.Url.AbsolutePath;

        sayfa = db.View_Sayfalar.Where(z => z.Aktif == "1").OrderBy(z => z.Sira).ToList();
    }
}
