﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class hediyeCekList : System.Web.UI.Page
{
    HediyeCekRepositories rep = new HediyeCekRepositories();
    public List<Zon_HediyeCek> cekList = new List<Zon_HediyeCek>();
    protected void Page_Load(object sender, EventArgs e)
    {
        Zon_Kullanici u = null;
        if (Session["user"] != null)
        {
            if (!Page.IsPostBack)
            {
                u = (Zon_Kullanici)Session["user"];
                cekList = rep.getHediyeCekList(u.ID);
            }
        }
        else
            Response.Redirect("../default.aspx", false);
    }
}