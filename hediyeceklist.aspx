﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="hediyeCekList.aspx.cs" Inherits="hediyeCekList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <form id="hediyeCek" runat="server">
    <div class="usermain-middle" style="width:550px;min-height:200px;border:none;">
  <%--   <div class="title">Hediye Çekleri</div>--%>
        <div class="usermain-kullaniciBilgi" style="margin-top:5px;">
         <div class="ui-form">
           <div class="adres-content-col" style="margin:5px 0 0 0px">
           <h3 style="margin-left:0px;font-weight:bold;">Aktif Hediye Çekleriniz</h3>
             <br />

            <div class="siparis-table">
           <table width="570" class="c">
           <tr style="background:#ffe3dc;height:30px;font-size:13px;text-align:center;font-weight:bold;color:#999">
                <td></td>
                <td style="border-right:1px solid #fff;text-align:center">Hediye Çeki Kodu</td>
                <td style="border-right:1px solid #fff;text-align:center">Tutarı</td>
                <td style="border-right:1px solid #fff;text-align:center">Son Kullanma Tarihi</td>
                <td style="border-right:1px solid #fff;text-align:center">Min Sipariş Tutarı</td>
              
           </tr>
           <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
           <tr style="background:#efefef;height:2px;">
                <td colspan="6"></td>
           </tr>
            <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
          <%var teslimat=cekList.Where(x=>x.Kullanim=="0").ToList();
            if (teslimat.Count > 0)
            {
                foreach (var a in teslimat)
                {
                   
                        
                    %>
           <tr  id="tr<%=a.Id %>" onclick="rowClick('<%=a.Id %>')"  style="background:#F4F4F4;color:#fff;height:30px;font-size:12px;text-align:center;border-bottom:1px solid #fff;color:#333;cursor:pointer;">
                 <td> <input id="radio<%=a.Id%>" type="radio" name="hediyeCek" value="<%=a.Kod %>"/></td>
                <td style="border-right:1px solid #fff;border-bottom:1px solid #fff"><%=a.Kod%></td>
                <%if(a.TutarTip==2){ %>
                <td style="border-right:1px solid #fff;border-bottom:1px solid #fff"><%=a.Tutar.Value.ToString("N")%> TL</td>
                <%}else{ %>
                <td style="border-right:1px solid #fff;border-bottom:1px solid #fff"> %<%=(int)a.Tutar%>  </td>
                <%} %>
                <td style="border-right:1px solid #fff;border-bottom:1px solid #fff"><%=a.BitisTarih.Value.ToShortDateString()%></td>
                <td style="border-bottom:1px solid #fff"><%=a.MinTutar.Value.ToString("N")%> TL ve Üzeri Alışverişlerde Geçerlidir.</td>
           </tr>
           <%
                    
                }
            } %>
            <%else
            { %>
            <tr>
                <td colspan="4"><span style="font-size:12px">Kayıtlı Aktif Hediye Çekiniz Yok.</span></td>
            </tr>
            <%} %>
       </table> 
       </div> 

       <br /><br /><br />
        <%--<h3 style="margin-left:0px"><b>Kullanılmış Hediye Çekleriniz</b></h3>
             <br />--%>
       <%--<div class="siparis-table">
           <table width="700">
           <tr style="background:#ffe3dc;height:30px;font-size:13px;text-align:center;font-weight:bold;color:#999">
                <td style="border-right:1px solid #fff;text-align:center">Hediye Çeki Kodu</td>
                <td style="border-right:1px solid #fff;text-align:center">Tutarı</td>
                <td style="border-right:1px solid #fff;text-align:center">Son Kullanma Tarihi</td>
                <td style="border-right:1px solid #fff;text-align:center">Min Sipariş Tutarı</td>
              
           </tr>
           <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
           <tr style="background:#efefef;height:2px;">
                <td colspan="6"></td>
           </tr>
            <tr style="background:#fff;height:5px;">
                <td colspan="6"></td>
           </tr>
          <%var teslimat2=cekList.Where(x=>x.Kullanim.Value==true).ToList();
            if (teslimat2.Count > 0)
            {
                foreach (var a in teslimat2)
                {%>
           <tr style="background:#F4F4F4;color:#fff;height:30px;font-size:12px;text-align:center;border-bottom:1px solid #fff;color:#333">
                <td style="border-right:1px solid #fff"><%=a.Kod%></td>
                <td style="border-right:1px solid #fff"><%=a.Indirim.Value.ToString("N")%> TL</td>
                <td style="border-right:1px solid #fff"><%=a.SonTarih.Value.ToString()%></td>
                <td><%=a.MinTutar.Value.ToString("N")%> TL ve Üzeri Alışverişlerde Geçerlidir.</td>
           </tr>
           <%}
            } %>
            <%else
            { %>
            <tr>
                <td colspan="4"><span style="font-size:12px">Kayıtlı Kullanılmış Hediye Çekiniz Yok.</span> </td>
            </tr>
            <%} %>
       </table> 
       </div>--%>
           </div>
        </div>
     </div>
    </div>
    </form>
</body>
<script type="text/javascript">
    function rowClick(id) {
        $('#radio' + id).attr('checked', 'checked');
        $('#tr' + id).removeClass("selectedTr");
        $('#tr' + id).addClass("selectedTr");
    }
</script>
</html>



