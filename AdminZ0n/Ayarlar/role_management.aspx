﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/AdminMasterPage.master" AutoEventWireup="true" CodeFile="role_management.aspx.cs" Inherits="AdminZ0n_UserSettings_role_management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        $(function () {
            $("#settings").addClass("current");
            $("#kullanicilar").addClass("current");

            $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="title" Runat="Server">
    Kullan&#305;c&#305; Rolleri Yönetimi
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <asp:Panel ID="pnlAddRole" runat="server" Visible="false">
        <fieldset>
            <p>
                <label>Role</label>
                <asp:TextBox ID="txtRole" runat="server"></asp:TextBox>
            </p>
            <!--<p>
                <label>Tan&#305;m&#305;</label>
                <asp:TextBox ID="txtDesc" runat="server"></asp:TextBox>
            </p>-->
            <p>
                <asp:LinkButton CssClass="button" ID="btnAdd" runat="server" OnClick="btnAdd_Click">Kaydet</asp:LinkButton>
                <asp:LinkButton CssClass="button" ID="btnExit" runat="server" OnClick="btnExit_Click">Ç&#305;k</asp:LinkButton>
            </p>
        </fieldset>
    </asp:Panel>
    <asp:Panel ID="pnlListe" runat="server">
        <p>
            <asp:LinkButton CssClass="button" ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" >Yeni Role Ekle</asp:LinkButton>
        </p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="EntityDataSource1">
            <Columns>
                <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role" />
                
                <asp:CheckBoxField DataField="CanDelete" HeaderText="CanDelete" SortExpression="CanDelete" />
                <asp:CheckBoxField DataField="CanUpdate" HeaderText="CanUpdate" SortExpression="CanUpdate" />
                <asp:CheckBoxField DataField="CanInsert" HeaderText="CanInsert" SortExpression="CanInsert" />
                <asp:CommandField ButtonType="Image" CancelImageUrl="~/AdminZ0n/resources/images/icons/cross_circle.png" DeleteImageUrl="~/AdminZ0n/resources/images/icons/cross.png" EditImageUrl="~/AdminZ0n/resources/images/icons/pencil.png" ShowDeleteButton="True" ShowEditButton="True" UpdateImageUrl="~/AdminZ0n/resources/images/icons/tick_circle.png" />
            </Columns>
        </asp:GridView>
        <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=ZonDBEntities" DefaultContainerName="ZonDBEntities" EnableDelete="True" EnableFlattening="False" EnableInsert="True" EnableUpdate="True" EntitySetName="Zon_Roles"></asp:EntityDataSource>
    </asp:Panel>
</asp:Content>

