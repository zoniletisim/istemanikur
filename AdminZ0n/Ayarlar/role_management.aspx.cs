﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminZ0n_UserSettings_role_management : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlAddRole.Visible = false;
        pnlListe.Visible = true;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        AdminUtility.closeBox(Master);
        try
        {
            if (!Roles.RoleExists(txtRole.Text))
            {
                Roles.CreateRole(txtRole.Text);

                AdminUtility.setInformBox(Master, AdminUtility.SuccessAdd);

                EntityDataSource1.DataBind();
                GridView1.DataBind();

                pnlAddRole.Visible = false;
                pnlListe.Visible = true;
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + "zaten var");
            }
        }
        catch (Exception ex)
        {

            AdminUtility.setErrorBox(Master, AdminUtility.Fail + ex.Message);
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        pnlAddRole.Visible = true;
        pnlListe.Visible = false;
    }
}