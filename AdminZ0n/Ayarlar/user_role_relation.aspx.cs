﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class AdminZ0n_UserSettings_user_role_relation : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UserRepositories urep = new UserRepositories();
        if (!IsPostBack)
        {
            drpRoller.DataSource = Roles.GetAllRoles();
            drpRoller.DataTextField = "Role";
            drpRoller.DataValueField = "Role";
            drpRoller.DataBind();
            AdminUtility.InsertSelectToDropDownList(drpRoller);

            drpKullanicilar.DataSource = urep.GetAllUsers();
            drpKullanicilar.DataTextField = "Email";
            drpKullanicilar.DataValueField = "Email";
            drpKullanicilar.DataBind();

            AdminUtility.InsertSelectToDropDownList(drpKullanicilar);
        }
    }
    protected void drpKullanicilar_SelectedIndexChanged(object sender, EventArgs e)
    {
        var role = Roles.GetRoleForUser(drpKullanicilar.SelectedValue);
        //if (role.Length > 0)
        drpRoller.SelectedIndex = drpRoller.Items.IndexOf(drpRoller.Items.FindByValue(role.Role));
    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        //var rl = Roles.GetRoleForUser(User.Identity.Name);
        Roles.ChangeRole(drpKullanicilar.SelectedValue, drpRoller.SelectedValue);

        //Roles.AddUserToRole(drpKullanıcılar.SelectedValue, drpRoller.SelectedValue);
    }
}