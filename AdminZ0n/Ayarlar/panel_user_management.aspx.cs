﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class AdminZ0n_UserSettings_add_panel_user : BasePage
{
    UserRepositories rep = new UserRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpRole.DataSource = Roles.GetAllRoles();
            drpRole.DataTextField = "Role";
            drpRole.DataValueField = "ID";
            drpRole.DataBind();
        }
    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        AdminUtility.closeBox(Master);
        string mode = hdnAction.Value;

        try
        {
            var member = rep.AddNewKullanici(hdnID.Value, txtAd.Text,txtSoyad.Text, txtEmail.Text,txtTel.Text, txtPassword.Text, DateTime.Now.Date.ToShortDateString(), drpRole.SelectedValue,drpStatu.SelectedValue,mode,1,"");
            if (member.result)
            {
                EntityDataSource1.DataBind();
                GridView1.DataBind();

                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlAddUser.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";

            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + member.message);
            }

        }
        catch (Exception ex)
        {
           
        }
    }
    protected void btnNewAdd_Click(object sender, EventArgs e)
    {
        pnlAddUser.Visible = true;
        pnlListe.Visible = false;
        hdnAction.Value = "add";
    }
    protected void btnCik_Click(object sender, EventArgs e)
    {
        pnlAddUser.Visible = false;
        pnlListe.Visible = true;
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlAddUser.Visible = true;
        hdnAction.Value = "update";
        hdnID.Value = GridView1.SelectedValue.ToString();
       var sf = rep.GetUser(Convert.ToInt32(GridView1.SelectedValue));

       txtAd.Text = sf.Ad;
       txtSoyad.Text = sf.Soyad;
       txtEmail.Text = sf.Email;
       txtTel.Text = sf.Telefon;
       txtPassword.Text = sf.Password;
       txtAgainPassword.Text = sf.Password;
       drpRole.SelectedValue = sf.RoleID.ToString();
       drpStatu.SelectedValue = sf.Aktif;


    }
}