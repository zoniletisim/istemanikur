﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminZ0n_Ayarlar_sitaAyar : BasePage
{
    siteAyarlar rep = new siteAyarlar();
    ResultAction result = new ResultAction();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            var a = rep.GetAyar();
             txtKeyword.Text=a.Meta;
            txtMailAdres.Text=a.MailAdres;
            txtMailSifre.Text=a.MailSifre;
            txtMailServer.Text=a.MailServer;
            txtBilgiMailAdres.Text=a.BilgiMailAdres;
            txtAdres.Text=a.IletisimAdres;
            txtTel.Text=a.IletisimTel;
            txtFax.Text=a.IletisimFax;
            txtFacebook.Text=a.FaceBook;
            txtTwitter.Text=a.Twitter;
            txtPinterest.Text = a.Pinterest;
            txtAnalytics.Text = a.googleAnalytics;
            ckIcerik.Text = a.googleMaps;
        }
        else
        {
            result = rep.ActionAyar(txtKeyword.Text,txtMailAdres.Text,txtMailSifre.Text,txtMailServer.Text,txtBilgiMailAdres.Text,txtAdres.Text,txtTel.Text,txtFax.Text,txtFacebook.Text,txtTwitter.Text,txtPinterest.Text,txtAnalytics.Text,ckIcerik.Text);


            if (result.result)
                AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);
            else
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
        }
    }
}