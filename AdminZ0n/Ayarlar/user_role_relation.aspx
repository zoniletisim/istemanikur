﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/AdminMasterPage.master" AutoEventWireup="true" CodeFile="user_role_relation.aspx.cs" Inherits="AdminZ0n_UserSettings_user_role_relation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        $(function () {
            $("#settings").addClass("current");
            $("#kullanicilar").addClass("current");

            $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
    Kullan&#305;c&#305; Role Tan&#305;mlar&#305;
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
    <fieldset>
        <p>
            <label>Kullan&#305;c&#305;lar</label>
            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="drpKullanicilar_SelectedIndexChanged" ID="drpKullanicilar" runat="server"></asp:DropDownList>
        </p>
        <p>
            <label>Roller</label>
            <asp:DropDownList ID="drpRoller" runat="server"></asp:DropDownList>
        </p>
        <p>
            <asp:Button OnClick="btnKaydet_Click" CssClass="button" ID="btnKaydet" runat="server" Text="Kaydet" />
        </p>
    </fieldset>
</asp:Content>



