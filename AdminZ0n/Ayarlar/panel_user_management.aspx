﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/AdminMasterPage.master" AutoEventWireup="true" CodeFile="panel_user_management.aspx.cs" Inherits="AdminZ0n_UserSettings_add_panel_user" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
       #main-content tbody tr.alt-row {
background: #f3f3f3;
}
    </style>
    <script type="text/javascript">

        $(function () {
            $("#settings").addClass("current");
            $("#kullanicilar").addClass("current");

            $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">
    <asp:ValidationSummary CssClass="notification error" ID="ValidationSummary1" runat="server" />
    <div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;">     Panel Kullan&#305;c&#305; Yönetimi</h3>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:Panel runat="server" ID="pnlAddUser" Visible="false">
        <table>
           
             <tr>
                <td><label>Kullan&#305;c&#305; Ad&#305;</label></td>
                <td><asp:TextBox ID="txtAd" runat="server"></asp:TextBox></td>
                <td><asp:RequiredFieldValidator ValidationGroup="add" ForeColor="Red" ControlToValidate="txtAd" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Email adresini giriniz."></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td><label>Kullan&#305;c&#305; Soyd&#305;</label></td>
                <td><asp:TextBox ID="txtSoyad" runat="server"></asp:TextBox></td>
                <td><asp:RequiredFieldValidator ValidationGroup="add" ForeColor="Red" ControlToValidate="txtSoyad" ID="RequiredFieldValidator5" runat="server" ErrorMessage="Email adresini giriniz."></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td><label>Email</label></td>
                <td><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                <td><asp:RequiredFieldValidator ValidationGroup="add" ForeColor="Red" ControlToValidate="txtEmail" ID="RequiredFieldValidator4" runat="server" ErrorMessage="Email adresini giriniz."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ValidationGroup="add" ForeColor="Red" ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Hatal&#305; e-mail adresi"></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td><label>Tel</label></td>
                <td><asp:TextBox ID="txtTel" runat="server"></asp:TextBox></td>
                <td></td>
                 </tr>
            <tr>
                <td><label>&#350;ifre</label></td>
                <td><asp:TextBox ID="txtPassword"  runat="server"></asp:TextBox></td>
                <td><asp:RequiredFieldValidator ValidationGroup="add"  ForeColor="Red" ControlToValidate="txtPassword" ID="RequiredFieldValidator2" runat="server" ErrorMessage="&#350;ifre giriniz."></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td><label>Tekrar &#350;ifre</label></td>
                <td><asp:TextBox ID="txtAgainPassword" runat="server" ></asp:TextBox></td>
                <td><asp:RequiredFieldValidator ValidationGroup="add" ForeColor="Red" ID="RequiredFieldValidator3" ControlToValidate="txtAgainPassword" runat="server" ErrorMessage="&#350;ifreyi tekrar giriniz."></asp:RequiredFieldValidator>
                <asp:CompareValidator ValidationGroup="add" ID="CompareValidator1" ForeColor="Red" ControlToCompare="txtPassword" ControlToValidate="txtAgainPassword" runat="server" ErrorMessage="&#350;ifreyi tekrar hatal&#305; girdiniz"></asp:CompareValidator></td>
            </tr>
            <tr>
                <td><label>
                  Rol:
                </label></td>
                <td><asp:DropDownList ID="drpRole" runat="server"></asp:DropDownList></td>
                <td></td>
            </tr>
            <tr>
                <td><label>Aktif:</label></td>
                <td> <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Aktif</asp:ListItem>
                    <asp:ListItem Value="0">Pasif</asp:ListItem>
                </asp:DropDownList>
                </td>
                <td></td>
            </tr>
            <tr>
               <td colspan="3"> <asp:Button ID="btnKaydet" ValidationGroup="add" runat="server" Text="Kaydet" CssClass="button" OnClick="btnKaydet_Click" />
                <asp:Button ID="btnCik" runat="server" Text="Ç&#305;k" CssClass="button" OnClick="btnCik_Click" /></td>
           </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlListe" runat="server">
        <p>
            <asp:Button ID="btnNewAdd" runat="server" Text="Yeni Kullan&#305;c&#305; Ekle" CssClass="button" OnClick="btnNewAdd_Click" />
        </p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="ID" DataSourceID="EntityDataSource1" 
         AllowPaging="True" AllowSorting="True" EnableViewState="False" 
          OnSelectedIndexChanged="GridView1_SelectedIndexChanged" 
         >
            <Columns>
                <asp:BoundField DataField="Ad" HeaderText="Ad" SortExpression="Ad" />
                <asp:BoundField DataField="Soyad" HeaderText="Soyad" SortExpression="Soyad" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Telefon" HeaderText="Telefon" SortExpression="Telefon" />
               
                <%--<asp:CheckBoxField DataField="Statu" HeaderText="Statu" SortExpression="Statu" />--%>
                <asp:BoundField DataField="SonGiris" HeaderText="SonGiris" SortExpression="SonGiris" />
               <%-- <asp:TemplateField HeaderText="RoleID" SortExpression="RoleID">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("RoleID") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Zon_Roles.Role") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                       <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Select" ImageUrl="~/AdminZ0n/resources/images/icons/pencil.png" Text="Select" />
                       <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" OnClientClick="if(confirm('silmek için onaylamal&#305;s&#305;n&#305;z.')){}else{return false;}" CommandName="Delete" ImageUrl="~/AdminZ0n/resources/images/icons/cross.png" Text="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:EntityDataSource ID="EntityDataSource1" AutoGenerateWhereClause="True" 
            runat="server" ConnectionString="name=ZonDBEntities" 
            DefaultContainerName="ZonDBEntities" EnableDelete="True" 
            EntitySetName="Zon_Kullanici" EnableFlattening="False">
            <WhereParameters>
                <asp:Parameter DefaultValue="True" Name="IsPanelUser" Type="Boolean" />
                <asp:Parameter DefaultValue="1" Name="GrupID" Type="Int32" />
            </WhereParameters>
        </asp:EntityDataSource>
        <br />
    </asp:Panel>
    <div class="clear"></div> <!-- End .clear -->
    </div></div>
</asp:Content>



