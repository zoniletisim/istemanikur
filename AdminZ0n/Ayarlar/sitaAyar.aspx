﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="sitaAyar.aspx.cs" Inherits="AdminZ0n_Ayarlar_sitaAyar" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script>
    $(function () {
        $("#settings").addClass("current");
        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
    });
</script>
 <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
   <div class="content-box">
    <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Ayarlar</h3>
    </div>
    <div class="content-box-content">
   
     <asp:Panel ID="pnlSayfaEkle" runat="server">
        <table>
        <tr>
            <td>Site Keyword</td>
            <td><asp:TextBox ID="txtKeyword" CssClass="text-input" TextMode="MultiLine" Width="95%" Height="70" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>İletişim Adres</td>
            <td><asp:TextBox ID="txtAdres" CssClass="text-input" Width="300" runat="server"></asp:TextBox></td>
        </tr>
         <tr >
            <td>İletişim Tel</td>
            <td>
                <asp:TextBox ID="txtTel" Width="300" ClientIDMode="Static" CssClass="text-input" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr >
            <td>İletişim Fax</td>
            <td>
                <asp:TextBox ID="txtFax" Width="300" ClientIDMode="Static" CssClass="text-input" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr >
            <td>Mail Adres </td>
            <td>
                <asp:TextBox ID="txtMailAdres" CssClass="text-input" ClientIDMode="Static" Width="300" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>Mail Server</td>
            <td><asp:TextBox ID="txtMailServer" CssClass="text-input" Width="500" runat="server"></asp:TextBox></td>
        </tr>
          <tr>
            <td>Mail Şifre</td>
         <td><asp:TextBox ID="txtMailSifre" CssClass="text-input" Width="500" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Bilgilendirme Mail Adresleri</td>
            <td><asp:TextBox ID="txtBilgiMailAdres" TextMode="MultiLine" CssClass="text-input" Width="500" runat="server"></asp:TextBox>
            <span style="color:Red;font-size:11px;">* Mail Adreslerini "Noktalı Virgül(;)" ile ayırınız.  </span>
            </td>
        </tr>
        <tr>
            <td>Facebook</td>
            <td><asp:TextBox ID="txtFacebook" CssClass="text-input" Width="300" runat="server"></asp:TextBox></td>
        </tr>
          <tr>
            <td>Twitter</td>
            <td><asp:TextBox ID="txtTwitter" CssClass="text-input date" Width="300" runat="server"></asp:TextBox></td>
        </tr>
         <tr>
            <td>Pinterest</td>
            <td><asp:TextBox ID="txtPinterest" CssClass="text-input date" Width="300" runat="server"></asp:TextBox></td>
        </tr>
         <tr>
            <td>Google Analytics </td>
            <td><CKEditor:CKEditorControl ID="txtAnalytics" CssClass="" ToolTip="" ClientIDMode="Static" runat="server" Height="100px"  Width="500px"></CKEditor:CKEditorControl></td>
        </tr>
         <tr>
            <td>Google Maps</td>
           <td><CKEditor:CKEditorControl ID="ckIcerik" CssClass="" ToolTip="" ClientIDMode="Static" runat="server" Height="100px"  Width="500px"></CKEditor:CKEditorControl></td>
        </tr>
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>

    </div></div>
</asp:Content>



