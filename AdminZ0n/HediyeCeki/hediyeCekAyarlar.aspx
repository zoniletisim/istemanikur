﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="hediyeCekAyarlar.aspx.cs" Inherits="AdminZ0n_HediyeCeki_hediyeCekAyarlar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#banner").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	    });
	    $(function () {
	        $("#list").sortable({
	            handle: '.handle',
	            update: function () {
	                var order = $('#list').sortable('serialize');
	                $.post("../service.aspx", { action: "ListBanner", item: order }, function (msg) {
	                    var json = $.parseJSON(msg);
	                    if (json.result) {

	                    }
	                });
	            }
	        });
	    });
	  
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Hediye Çek Ayarları</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Hediye Çeki Tipi</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Tutar</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Yayin<div id="ajaxloading" style="display:none; position:absolute;"><img src="/adminz0n/resources/images/ajax-loader.gif" /></div> </th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%
              ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
              
                  ayar = (from item in db.Zon_HediyeCek_Ayar
                            where item.Aktif != "3"
                            select item).ToList();

                  foreach (var item in ayar)
            {%>
             <tr>
                <td ><%=Constants.HediyeCekTipText(item.CekTip.Value)%></td>
                <td style="width: 30px;"><%=item.Tutar %> <%=Constants.TutarTip(item.TutarTip.Value)%> </td>
                 <td>
                <%if (item.Aktif=="1")
                    {%>
                    <input type="checkbox" id="chkTrue"  onclick="CallIsVisible(<%=item.Id%>,'cekAyar');" checked="checked"   />Aktif
                    <%} %>
                                  
                    <%else
                    { %>
                    <input type="checkbox" id="chkTrue" onclick="CallIsVisible(<%=item.Id%>,'cekAyar');" />Aktif
                    
                    <%} %>
                    </td>
                               
                    <td style="width: 20px;">
                   <a href="hediyeCekAyarlar.aspx?action=update&pid=<%=item.Id%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

</td>
                <td style="width: 20px;">
                <a href="bhediyeCekAyarlar.aspx?action=delete&pid=<%=item.Id%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <%--<asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
			      
       
		<th><input type="text" name="search_name" value="Baslik" style="" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_name" value="Kategori" class="search_init" style="width:120px" /></th>
		<th><input type="text" name="search_detail" value="Sira" style="display:none;" class="search_init" style="width:120px" /></th>

        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">

        <table>
        <tr>
        <td>Şirket</td>
        <td>
            <asp:DropDownList ID="ddlSirket" runat="server">
            </asp:DropDownList>
            <asp:CheckBox ID="chkTumsirket" Text="Tüm Şirketler" runat="server" />
        </td>
        </tr>

        <tr>
        <td>Hadiye Çeki Tipi</td>
        <td>
            <asp:DropDownList ID="ddlCekTip" runat="server">
            <asp:ListItem Value="2">Arkadaslarina Öner</asp:ListItem>
            <asp:ListItem Value="4">Randevu Sayısı</asp:ListItem>
            </asp:DropDownList>
        </td>
        </tr>
        <tr>
            <td>Tutar</td>
           <td><asp:TextBox ID="txtTutar" CssClass="text-input"   runat="server"></asp:TextBox>
             <asp:DropDownList ID="ddlTutarTip" runat="server" ClientIDMode="Static">
             <asp:ListItem Value="1">Yüzde(%)</asp:ListItem>
             <asp:ListItem Value="2">TL</asp:ListItem>
             </asp:DropDownList>
             <asp:Label ID="lblTrBaslik"  runat="server" Text=""></asp:Label>
           </td>
        </tr>
     
       
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="2">Yay&#305;nda De&#287;il</asp:ListItem>
                    <asp:ListItem Value="3">Silinebilir</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
    
    </div>
</div> 
</asp:Content>



