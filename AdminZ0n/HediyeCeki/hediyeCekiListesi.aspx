﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="hediyeCekiListesi.aspx.cs" Inherits="AdminZ0n_HediyeCeki_hediyeCekiListesi" %>

<asp:Content ID="Content4" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#hediyeCek").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	        $.configureBoxes();
	    });
	   
        </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Doğum Günü Hediye Çeki</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<%--<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> --%>
			<%--<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>--%>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnID" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Ad Soyad</th>
                 <th style="color:#000;text-align:center;cursor:pointer;">Baş.Tarih</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Bit. Tarih</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Tutar</th>
                <th style="color:#000;text-align:center;cursor:pointer;">MinTutar</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Kullanım</th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%
              foreach (var h in hediyeCek)
            {%>
             <tr>
                <td><%=Constants.getUserNameAndSurname(h.MusteriId.Value) %> </td>
                <td><%=h.BaslamaTarih.Value.ToShortDateString() %></td>
                <td><%=h.BitisTarih.Value.ToShortDateString() %></td>
                <%if (h.TutarTip == 1)
                  { %>
                <td>%<%=(int)h.Tutar.Value%></td>
                <%}
                  else
                  { %>
                <td><%=h.Tutar.Value%> TL</td>
                <%} %>
                <td><%=h.MinTutar %> TL</td>
                 <td><%=h.Kullanim=="0" ? "Kullanılmamiş" : "Kullanılmış"  %></td>
                    <td style="width: 20px;">
                   <a href="hediyeCekiListesi.aspx?action=update&pid=<%=h.Id%>"><img ID="imog" alt="Güncelle" title="Hediye Çeki Tanımla" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

</td>
                <td style="width: 20px;">
                <a href="hediyeCekiListesi.aspx?action=delete&pid=<%=h.Id%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Hediye Çekini Sil" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <%--<asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>

        <th></th>
        <th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  </div>
  </div>
  </asp:Content>

