﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class AdminZ0n_HediyeCeki_hediyeCekAyarlar : System.Web.UI.Page
{
    HediyeCekRepositories rep = new HediyeCekRepositories();
    ResultAction result = new ResultAction();
    public List<Zon_HediyeCek_Ayar> ayar = new List<Zon_HediyeCek_Ayar>();
    public Zon_Kullanici user = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
            user = (Zon_Kullanici)Session["user"];
        else
            Response.Redirect("/adminz0n/login.aspx");

        if (!Page.IsPostBack)
        {
            fillForm();
            //getLanguageFilterFill();
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.getHediyeCekAyar(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                txtTutar.Text = sf.Tutar.Value.ToString();
                drpStatu.SelectedValue = sf.Aktif.ToString();
                ddlCekTip.SelectedValue = sf.TutarTip.ToString();
                ddlSirket.SelectedValue = sf.SirketId.ToString();
                chkTumsirket.Checked = sf.TumSirketler.Value;

            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }
            
        }
    }


    private void getDelete(int p)
    {
        var d = rep.deleteRowCekAyar(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            int bayiID = 0;
            if (user.RoleID == (int)Constants.KullaniciTip.bayi)
                bayiID = user.ID;



            result = rep.ActionCekAyar(hdnID.Value,ddlSirket.SelectedValue,ddlCekTip.SelectedValue,txtTutar.Text,ddlTutarTip.SelectedValue,chkTumsirket.Checked, hdnAction.Value, drpStatu.SelectedValue);
            if (result.result)
            {
                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
   
    private void fillForm()
    {
        TanimRepositories repTanim = new TanimRepositories();
        ddlSirket.DataSource = repTanim.GetSirketList();
        ddlSirket.DataTextField = "SiretAd";
        ddlSirket.DataValueField = "Id";
        ddlSirket.DataBind();
    }

   
    
}