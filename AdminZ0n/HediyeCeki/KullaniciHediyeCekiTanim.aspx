﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="KullaniciHediyeCekiTanim.aspx.cs" Inherits="AdminZ0n_HediyeCeki_KullaniciHediyeCekiTanim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#hediyeCek").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	       var a=$.configureBoxes({ 
                box1View: 'box1View',
	            box2View: 'box2View'
               
	        });
             var b=$.configureBoxes({ 
                box1View: 'ddlGrup',
	            box2View: 'ddlGrup2',
	            to1: 'to12',
	            to2: 'to22',
	            allTo1: 'allTo12',
                allTo2: 'allTo22'
	        });
	        var b = $.configureBoxes({
	            box1View: 'ddlSirket',
	            box2View: 'ddlSirket2',
	            to1: 'to13',
	            to2: 'to23',
                allTo1: 'allTo13',
                allTo2: 'allTo23'

	        });
	    });
	   
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Şirket Tanımı</h3>
        <ul class="content-box-tabs">
            <%--<li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> 
			<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>--%>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnID" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 
  <asp:Panel ID="pnlSayfaEkle" runat="server">

      <h4>Müşteri Listesi</h4>
      <div class="rowElem dualBoxes">
                <div class="floatleft w40">
                <span style="font-size:15px;font-weight:bold"> Tüm Müşteri Listesi</span><br /><br />
                    <input type="text" id="box1Filter" class="boxFilter" placeholder="Filtre " /><br />
                    <asp:DropDownList ID="box1View" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple mus" style="height:300px;width:375px;">
                    </asp:DropDownList>
                    <br/>
                    <span id="box1Counter" class="countLabel"></span>
                         
                    <div class="displayNone"><select id="box1Storage"></select></div>
                </div>
                            
                <div class="floatleft dualControl">
                    <button id="to2" type="button" class="dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>
                    <button id="allTo2" type="button" class="dualBtn">&nbsp;&gt;&gt;&nbsp;</button><br />
                    <button id="to1" type="button" class="dualBtn mr5">&nbsp;&lt;&nbsp;</button>
                    <button id="allTo1" type="button" class="dualBtn">&nbsp;&lt;&lt;&nbsp;</button>
                </div>
                            
                <div class="floatright w40">
                 <span style="font-size:15px;font-weight:bold"> Seçilen Müşteri Listesi</span><br /><br />   
                    <input type="text" id="box2Filter" class="boxFilter" placeholder="Filtre" /><br />
                    <asp:DropDownList ID="box2View" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple mus" style="height:300px;width:375px;">
                    </asp:DropDownList>
                          
                           
                    <br/>
                    <span id="box2Counter" class="countLabel"></span>
                            
                    <div class="displayNone"><select id="box2Storage"></select></div>
                </div>
		    <div class="fix"></div>
            </div>
            <h4>Grup Listesi</h4>
        <div class="rowElem dualBoxes">
                <div class="floatleft w40">
                <span style="font-size:15px;font-weight:bold"> Tüm Grup Listesi</span><br /><br />   
                    <input type="text" id="Text1" class="boxFilter" placeholder="Filtre " /><br />
                    <asp:DropDownList ID="ddlGrup" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                    </asp:DropDownList>
                    <br/>
                    <span id="Span1" class="countLabel"></span>
                            
                    <div class="displayNone"><select id="Select1"></select></div>
                </div>
                            
                <div class="floatleft dualControl">
                    <button id="to22" type="button" class="dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>
                    <button id="allTo22" type="button" class="dualBtn">&nbsp;&gt;&gt;&nbsp;</button><br />
                    <button id="to12" type="button" class="dualBtn mr5">&nbsp;&lt;&nbsp;</button>
                    <button id="allTo12" type="button" class="dualBtn">&nbsp;&lt;&lt;&nbsp;</button>
                </div>
                            
                <div class="floatright w40">
                <span style="font-size:15px;font-weight:bold"> Seçilen Grup Listesi</span><br /><br />   
                    <input type="text" id="Text2" class="boxFilter" placeholder="Filter entries..." /><br />
                    <asp:DropDownList ID="ddlGrup2" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                    </asp:DropDownList>
                          
                           
                    <br/>
                    <span id="Span2" class="countLabel"></span>
                            
                    <div class="displayNone"><select id="Select2"></select></div>
                </div>
		    <div class="fix"></div>
            </div>
            <h4>Şirket Listesi</h4>
        <div class="rowElem dualBoxes">
                <div class="floatleft w40">
                <span style="font-size:15px;font-weight:bold"> Tüm Şirket Listesi</span><br /><br />   
                    <input type="text" id="Text3" class="boxFilter" placeholder="Filtre " /><br />
                    <asp:DropDownList ID="ddlSirket" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                    </asp:DropDownList>
                    <br/>
                    <span id="Span3" class="countLabel"></span>
                            
                    <div class="displayNone"><select id="Select3"></select></div>
                </div>
                            
                <div class="floatleft dualControl">
                    <button id="to23" type="button" class="dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>
                    <button id="allTo23" type="button" class="dualBtn">&nbsp;&gt;&gt;&nbsp;</button><br />
                    <button id="to13" type="button" class="dualBtn mr5">&nbsp;&lt;&nbsp;</button>
                    <button id="allTo13" type="button" class="dualBtn">&nbsp;&lt;&lt;&nbsp;</button>
                </div>
                            
                <div class="floatright w40">
                <span style="font-size:15px;font-weight:bold"> Seçilen Şirket Listesi</span><br /><br />   
                    <input type="text" id="Text4" class="boxFilter" placeholder="Filter entries..." /><br />
                    <asp:DropDownList ID="ddlSirket2" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                    </asp:DropDownList>
                          
                           
                    <br/>
                    <span id="Span4" class="countLabel"></span>
                            
                    <div class="displayNone"><select id="Select4"></select></div>
                </div>
		    <div class="fix"></div>
            </div>
       <table>
       
       <tr>
       <td style="width:140px;">Not:</td>
        <td>
            <asp:TextBox ID="txtNot" CssClass="required" ClientIDMode="Static" ToolTip="Hediye çeki notu giriniz" Width="500" TextMode="MultiLine" runat="server"></asp:TextBox>
            <label class="ui-state-error-text" style="display:none;" id="lbl_txtNot">Hediye çeki notu giriniz</label>
        </td>
        </tr>
         <tr>
           <td>Ba&#351;lang&#305;ç Tarihi:</td>
        <td>
          
            <asp:TextBox CssClass="date required" ClientIDMode="Static" ToolTip="Hediye çeki başlangıç tarihi giriniz" ID="txtBaslangic" runat="server"></asp:TextBox>
            <label class="ui-state-error-text" style="display:none;" id="lbl_txtBaslangic">Hediye çeki başlangıç tarihi giriniz</label>
        </td>
        </tr>
        <tr>
         <td>Biti&#351; Tarihi:</td>
        <td>
            <asp:TextBox ID="txtBitis" CssClass="date required" ClientIDMode="Static" ToolTip="Hediye çeki bitiş tarihi giriniz" runat="server"></asp:TextBox>
            <label class="ui-state-error-text" style="display:none;" id="lbl_txtBitis">Hediye çeki bitiş tarihi giriniz</label>
         </td>
        </tr>

        <tr>
         <td>&#304;ndirim Tutar&#305;:</td>
         <td>
            <asp:TextBox ID="txtIndirim" CssClass="required" ClientIDMode="Static" ToolTip="Hediye çeki indirim giriniz" runat="server"></asp:TextBox>
             <asp:DropDownList ID="ddlTutarTip" runat="server" ClientIDMode="Static">
             <asp:ListItem Value="1">Yüzde(%)</asp:ListItem>
             <asp:ListItem Value="2">TL</asp:ListItem>
             </asp:DropDownList>
            <label class="ui-state-error-text" style="display:none;" id="lbl_txtIndirim">Hediye çeki indirim giriniz</label>
         </td>
        </tr>
         <tr>
          <td>Minimum Sipari&#351; Tutar&#305;:</td>
         <td>
            <asp:TextBox ID="txtMinimumSiparis" CssClass="required" runat="server" ToolTip="Hediye çeki minimum sipariş tutarı giriniz" ClientIDMode="Static"></asp:TextBox>
            <label class="ui-state-error-text" style="display:none;" id="lbl_txtMinimumSiparis">Hediye çeki minimum sipariş tutarı giriniz</label>
        </td>
        </tr>
        
           <%-- <asp:Button OnClick="btnOlustur_Click" OnClientClick="if(!validate()){return false;}" CssClass="button" ID="btnOlustur" runat="server" Text="Kodlar&#305; Olu&#351;tur" />--%>
        </table>

        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center">
           <input id="btnKaydet" type="button" value="Kaydet" class="button" />
        <%--<asp:Button CssClass="button" OnClick="btnKaydet_Click"  ID="btnKaydet" runat="server" Text="Kaydet" />--%>
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
    <div class="clear">
    
    </div>
    </div>
</div> 
    <script src="../resources/scripts/jquery.blockUI.1.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnKaydet').click(function () {
            var supportIDs = "";
            var grupids = "";
            var sirkets = "";
            $("#box2View > option").each(function () {
                supportIDs += this.value + ",";
            });
            $("#ddlGrup2 > option").each(function () {
                grupids += this.value + ",";
            });
            $("#ddlSirket2 > option").each(function () {
                sirkets += this.value + ",";
            });


            supportIDs = supportIDs.slice(0, supportIDs.length - 1);
            grupids = grupids.slice(0, grupids.length - 1);
            sirkets = sirkets.slice(0, sirkets.length - 1);

            if ($('#hdnAction').val() == "update")
                grupid = $('#hdnID').val();
            else
                grupid = $('#ddlGrup').val();
            $.blockUI({
                message: 'Tanımlar Yapılıyor..',
                css: { border: 'none', padding: '15px', backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', opacity: 0.5, color: '#fff' }
            });
            $.ajax({
                type: "post",
                url: "KullaniciHediyeCekiTanim.aspx/saveCekList",
                data: "{musteriIds: '" + supportIDs + "',grupIds: '" + grupids + "',not: '" + $('#txtNot').val() + "',basTarih: '" + $('#txtBaslangic').val() + "',bitTarih: '" + $('#txtBitis').val() + "',tutar: '" + $('#txtIndirim').val() + "',minTutar: '" + $('#txtMinimumSiparis').val() + "',sirket: '" + sirkets + "',tutarTip: '" + $('#ddlTutarTip').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $.unblockUI();
                    if (response.d == "1")
                        alert('Kaydınız Başarıyla Yapıldı. ');
                    else
                        alert('İşlem sırasında hata oluştu.');
                   

                },
                error: function () {

                }
            });

        });

    });
    jQuery(function ($) {
        $.datepicker.regional['tr'] = {
            closeText: 'kapat',
            prevText: '&#x3c;geri',
            nextText: 'ileri&#x3e',
            currentText: 'bugün',
            monthNames: ['Ocak', '&#350;ubat', 'Mart', 'Nisan', 'May&#305;s', 'Haziran',
                'Temmuz', 'A&#287;ustos', 'Eylül', 'Ekim', 'Kas&#305;m', 'Aral&#305;k'],
            monthNamesShort: ['Oca', '&#350;ub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'A&#287;u', 'Eyl', 'Eki', 'Kas', 'Ara'],
            dayNames: ['Pazar', 'Pazartesi', 'Sal&#305;', 'Çar&#351;amba', 'Per&#351;embe', 'Cuma', 'Cumartesi'],
            dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
            dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
            weekHeader: 'Hf',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['tr']);
    });
    $(document).ready(function () {
        $("input.date").datepicker({ "dateFormat": "dd.mm.yy" });
    });
      
</script>
</asp:Content>



