﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class AdminZ0n_HediyeCeki_hediyeCekiListesi : System.Web.UI.Page
{
    public List<Zon_Kullanici> user = new List<Zon_Kullanici>();
    public List<Zon_HediyeCek> hediyeCek = new List<Zon_HediyeCek>();
    UserRepositories rep = new UserRepositories();
    HediyeCekRepositories cekRep = new HediyeCekRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {
        hediyeCek = cekRep.getHediyeCekAllList();
        if (!Page.IsPostBack)
        {
            int id = 0;
            if (Request.QueryString["action"] == "update")
            {
                if (Request.QueryString["pid"] != null)
                {
                    id = int.Parse(Request.QueryString["pid"]);

                    
                }

            }
            else if (Request.QueryString["action"] == "delete")
            {
                id = int.Parse(Request.QueryString["pid"]);
                var res = cekRep.deleteHediyeCek(id);
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> alert('" + res.message + "')</script>");

            }
        }
        

    }

    public string sirket(int id)
    {
        TanimRepositories t = new TanimRepositories();
        string ret = "-";
        if (id != null)
            ret = t.GetSirket(id).SiretAd;

        return ret;
    }


    public string dogumGunuHediyeCeki(int userid)
    {
        string ret = "Tanımlanmadı";

        Zon_HediyeCek cek = cekRep.getHediyeCek(userid, (int)Constants.HediyeCekTip.dogumGunu);

        if (cek != null)
            ret = "Tanımlı";

        return ret;
    } 
}