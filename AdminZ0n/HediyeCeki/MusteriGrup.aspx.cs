﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Web.Services;

public partial class AdminZ0n_HediyeCeki_MusteriGrup : System.Web.UI.Page
{
    HediyeCekRepositories rep = new HediyeCekRepositories();
    UserRepositories userrep = new UserRepositories();
    ResultAction result = new ResultAction();
    public List<Zon_Musteri_Grup> grupMusteri = new List<Zon_Musteri_Grup>();
    public List<Zon_MusteriGrup> grup = new List<Zon_MusteriGrup>();
    protected void Page_Load(object sender, EventArgs e)
    {
         
        if (!IsPostBack)
        {
            getMusteriList();
            getGrupList();
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.GetGrup(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                ddlGrup.SelectedValue = sf.Id.ToString();
                lblGrup.Visible = true;
                ddlGrup.Visible = false;
                lblGrup.Text = rep.GetGrup(sf.Id).Ad;
                getGrupList2();
            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }

        }
    }

    private void getGrupList2()
    {
        box2View.DataSource = GetMusteriList2();
        box2View.DataTextField = "Value";
        box2View.DataValueField = "Key";
        box2View.DataBind();
    }

    private void getGrupList()
    {
        ddlGrup.DataSource = rep.GetGrupList();
        ddlGrup.DataTextField = "Ad";
        ddlGrup.DataValueField = "Id";
        ddlGrup.DataBind();
    }

    private void getMusteriList()
    {
        box1View.DataSource = GetMusteriList();
        box1View.DataTextField = "Value";
        box1View.DataValueField = "Key";
        box1View.DataBind();
    }

    public  Dictionary<int, string> GetMusteriList()
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<int, string> list = new Dictionary<int, string>();
        var query = db.Zon_Kullanici.ToList();
        int grupid=0;
        
        if(Request.QueryString["pid"]!=null)
            grupid=int.Parse(Request.QueryString["pid"]);
        
        var musteriGrup = db.Zon_Musteri_Grup.Where(z => z.GrupId == grupid).ToList();

        foreach (var i in query)
        {
            Zon_Sirket s = db.Zon_Sirket.Where(z => z.Id == i.SirketId).FirstOrDefault();
            if (!musteriGrup.Any(x => x.MusteriId == i.ID))
            {
                if(s!=null)
                    list.Add(i.ID, i.Ad + " " + i.Soyad +" - "+ s.SiretAd);
                else
                    list.Add(i.ID, i.Ad + " " + i.Soyad);
            }
        };
        return list;
    }
    public Dictionary<int, string> GetMusteriList2()
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<int, string> list = new Dictionary<int, string>();
        var query = db.Zon_Kullanici.ToList();
        int grupid = 0;
        if (Request.QueryString["pid"] != null)
            grupid = int.Parse(Request.QueryString["pid"]);
        var musteriGrup = db.Zon_Musteri_Grup.Where(z => z.GrupId == grupid).ToList();
        foreach (var i in query)
        {
            Zon_Sirket s = db.Zon_Sirket.Where(z => z.Id == i.SirketId).FirstOrDefault();

            if (musteriGrup.Any(x => x.MusteriId == i.ID))
            {
                if (s != null)
                    list.Add(i.ID, i.Ad + " " + i.Soyad + " - " + s.SiretAd);
                else
                    list.Add(i.ID, i.Ad + " " + i.Soyad);
            }
        };
        return list;
    }
    private void getDelete(int p)
    {
        var d = rep.deleteRowGrup(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            List<object> lst = new List<object>();
            foreach (var item in box2View.Items)
            {
                var _item = (item as ListItem);
                
                var result = rep.ActionGrupMusteri(mode, ddlGrup.SelectedValue, _item.Value);
                if (result.result)
                {
                    //lst.Add(result.data);
                }
                
            }
            
            //if (result.result)
            //{
            //    if (mode.Equals("add"))
            //        AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
            //    else
            //        AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

            //    pnlListe.Visible = true;
            //    pnlSayfaEkle.Visible = false;
            //    hdnAction.Value = "";
            //    hdnID.Value = "";
            //}
            //else
            //{
            //    AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            //}

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
    [WebMethod]
    public static string saveMusteriGrup(string musteriIds, string grupId,string mode)
    {
        string r = "0";
        string[] array = null;
        try
        {
            if (mode == "")
                mode = "add";

                HediyeCekRepositories rep = new HediyeCekRepositories();
                rep.ActionGrupMusteri(mode, grupId, musteriIds);
                //if (a.result == true)
                //{
                //    r = "Mesajınız Başarıyla Gönderildi.";
                    
                //}
            

            return r;
        }
        catch (Exception)
        {
            return r;
            throw;

        }
    }
}