﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" EnableEventValidation="false"  CodeFile="MusteriGrup.aspx.cs" Inherits="AdminZ0n_HediyeCeki_MusteriGrup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#hediyeCek").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	        $.configureBoxes();
	    });
	   
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Şirket Tanımı</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<%--<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> --%>
			<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnID" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Şirket Adı</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Yayin<div id="ajaxloading" style="display:none; position:absolute;"><img src="/adminz0n/resources/images/ajax-loader.gif" /></div> </th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%
              ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
              grup = db.Zon_MusteriGrup.Where(z => z.Aktif != "d").ToList();
              foreach (var item in grup)
            {%>
             <tr>
                <td ><%=item.Ad%></td>
                 <td>
                <%if (item.Aktif=="a")
                    {%>
                    <input type="checkbox" id="chkTrue"  onclick="CallIsVisible(<%=item.Id%>,'banner');" checked="checked"   />Aktif
                    <%} %>
                                  
                    <%else
                    { %>
                    <input type="checkbox" id="chkTrue" onclick="CallIsVisible(<%=item.Id%>,'banner');" />Aktif
                    
                    <%} %>
                    </td>
                               
                    <td style="width: 20px;">
                   <a href="musterigrup.aspx?action=update&pid=<%=item.Id%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

</td>
                <td style="width: 20px;">
                <a href="musterigrup.aspx?action=delete&pid=<%=item.Id%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <%--<asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
			      
       
		
		<th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>

        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">

        <table>
        <tr>
            <td style="width:100px;">Grup Adı</td>
           <td>
           
               <asp:DropDownList ID="ddlGrup" ClientIDMode="Static" runat="server">
               </asp:DropDownList>
               <asp:Label ID="lblGrup" runat="server" Visible="false" Text="Label"></asp:Label>
           </td>
        </tr>
        </table>
        <div class="rowElem dualBoxes">
                    	<div class="floatleft w40">
                            <input type="text" id="box1Filter" class="boxFilter" placeholder="Filtre " /><br />
                            <asp:DropDownList ID="box1View" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                            </asp:DropDownList>
                            <br/>
                            <span id="box1Counter" class="countLabel"></span>
                            
                            <div class="displayNone"><select id="box1Storage"></select></div>
                        </div>
                            
                        <div class="floatleft dualControl">
                            <button id="to2" type="button" class="dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>
                            <button id="allTo2" type="button" class="dualBtn">&nbsp;&gt;&gt;&nbsp;</button><br />
                            <button id="to1" type="button" class="dualBtn mr5">&nbsp;&lt;&nbsp;</button>
                            <button id="allTo1" type="button" class="dualBtn">&nbsp;&lt;&lt;&nbsp;</button>
                        </div>
                            
                        <div class="floatright w40">
                            <input type="text" id="box2Filter" class="boxFilter" placeholder="Filter entries..." /><br />
                           <asp:DropDownList ID="box2View" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                            </asp:DropDownList>
                          
                           
                            <br/>
                            <span id="box2Counter" class="countLabel"></span>
                            
                            <div class="displayNone"><select id="box2Storage"></select></div>
                        </div>
					<div class="fix"></div>
                    </div>

        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center">
          <input id="btnKaydet" type="button" value="Kaydet" class="button" />
      <%--  <asp:Button CssClass="button" OnClick="btnKaydet_Click"  ID="btnKaydet" runat="server" Text="Kaydet" />--%>
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
    <div class="clear">
    
    </div>
    </div>
</div> 
<script type="text/javascript">
    $(document).ready(function () {
        $('#btnKaydet').click(function () {
            var supportIDs = "";
            var grupid = "";
            $("#box2View > option").each(function () {
                supportIDs += this.value + ",";
            });

            supportIDs = supportIDs.slice(0, supportIDs.length - 1);
            if ($('#hdnAction').val() == "update")
                grupid = $('#hdnID').val();
            else
                grupid = $('#ddlGrup').val();
            $.ajax({
                type: "post",
                url: "musterigrup.aspx/saveMusteriGrup",
                data: "{musteriIds: '" + supportIDs + "',grupId: '" + grupid + "',mode: '" + $('#hdnAction').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    alert('Kaydınız Başarıyla Yapıldı. ');

                },
                error: function () {

                }
            });

        });

    });

      
</script>
</asp:Content>




