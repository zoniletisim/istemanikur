﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class AdminZ0n_HediyeCeki_dogumGunu : System.Web.UI.Page
{
    public List<Zon_Kullanici> user = new List<Zon_Kullanici>();
    UserRepositories rep = new UserRepositories();
    HediyeCekRepositories cekRep = new HediyeCekRepositories();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int id=0;
            if (Request.QueryString["action"] == "update")
            { 
                if (Request.QueryString["pid"] != null)
                {
                    id = int.Parse(Request.QueryString["pid"]);

                   var res=cekRep.AddNewHediyeCeki("Doğum günü hediye çekiniz", DateTime.Now.ToString(), DateTime.Now.AddMonths(3).ToString(), "15", "0",id, DateTime.Now.Ticks.ToString(), (int)Constants.HediyeCekTip.dogumGunu,0,"0");
                    if(res.result)
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> alert('Hediye çeki başarıyla oluşturuldu.')</script>");
                    else
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> alert('Hata oluştu.')</script>");
                }
                
            }
            else if (Request.QueryString["action"] == "delete")
            {
                id = int.Parse(Request.QueryString["pid"]);
                var res=cekRep.deleteHediyeCek(id);
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> alert('"+res.message+"')</script>");

            }
        }
        user = rep.DogumGunuOlanlar();

    }

    public string sirket(int id)
    {
        TanimRepositories t = new TanimRepositories();
        string ret = "-";
        if (id != null)
            ret=t.GetSirket(id).SiretAd;

        return ret;
    }


    public string dogumGunuHediyeCeki(int userid)
    {
        string ret = "Tanımlanmadı";

        Zon_HediyeCek cek = cekRep.getHediyeCek(userid,(int)Constants.HediyeCekTip.dogumGunu);

        if (cek != null)
            ret = "Tanımlı";

        return ret;
    } 
     
}