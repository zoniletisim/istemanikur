﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Web.Services;
using System.Collections;

public partial class AdminZ0n_HediyeCeki_KullaniciHediyeCekiTanim : System.Web.UI.Page
{
    HediyeCekRepositories rep = new HediyeCekRepositories();
    UserRepositories userrep = new UserRepositories();
    ResultAction result = new ResultAction();
    public List<Zon_Musteri_Grup> grupMusteri = new List<Zon_Musteri_Grup>();
    public List<Zon_MusteriGrup> grup = new List<Zon_MusteriGrup>();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            getMusteriList();
            getGrupList();
            getSirket();
            if (Request.QueryString["action"] == "update")
            {
               
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.GetGrup(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                //ddlGrup.SelectedValue = sf.Id.ToString();
                //lblGrup.Visible = true;
                //ddlGrup.Visible = false;
                //lblGrup.Text = rep.GetGrup(sf.Id).Ad;
                getGrupList2();
            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }

        }
    }

    private void getGrupList2()
    {
        box2View.DataSource = GetMusteriList2();
        box2View.DataTextField = "Value";
        box2View.DataValueField = "Key";
        box2View.DataBind();
    }

    private void getGrupList()
    {
        ddlGrup.DataSource = rep.GetGrupList();
        ddlGrup.DataTextField = "Ad";
        ddlGrup.DataValueField = "Id";
        ddlGrup.DataBind();
    }
    private void getSirket()
    {
        ddlSirket.DataSource = rep.GetSirket();
        ddlSirket.DataTextField = "SiretAd";
        ddlSirket.DataValueField = "Id";
        ddlSirket.DataBind();
    }
    private void getMusteriList()
    {
        box1View.DataSource = GetMusteriList();
        box1View.DataTextField = "Value";
        box1View.DataValueField = "Key";
        box1View.DataBind();
    }

    public Dictionary<int, string> GetMusteriList()
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<int, string> list = new Dictionary<int, string>();
        var query = db.Zon_Kullanici.ToList();
        int grupid = 0;

        if (Request.QueryString["pid"] != null)
            grupid = int.Parse(Request.QueryString["pid"]);

        var musteriGrup = db.Zon_Musteri_Grup.Where(z => z.GrupId == grupid).ToList();

        foreach (var i in query)
        {
            Zon_Sirket s = db.Zon_Sirket.Where(z => z.Id == i.SirketId).FirstOrDefault();
            if (!musteriGrup.Any(x => x.MusteriId == i.ID))
            {
                if (s != null)
                    list.Add(i.ID, i.Ad + " " + i.Soyad + " - " + s.SiretAd);
                else
                    list.Add(i.ID, i.Ad + " " + i.Soyad);
            }
        };
        return list;
    }
    public Dictionary<int, string> GetMusteriList2()
    {
        ZonDBEntities db = new ZonDBEntities();
        Dictionary<int, string> list = new Dictionary<int, string>();
        var query = db.Zon_Kullanici.ToList();
        int grupid = 0;
        if (Request.QueryString["pid"] != null)
            grupid = int.Parse(Request.QueryString["pid"]);
        var musteriGrup = db.Zon_Musteri_Grup.Where(z => z.GrupId == grupid).ToList();
        foreach (var i in query)
        {
            Zon_Sirket s = db.Zon_Sirket.Where(z => z.Id == i.SirketId).FirstOrDefault();

            if (musteriGrup.Any(x => x.MusteriId == i.ID))
            {
                if (s != null)
                    list.Add(i.ID, i.Ad + " " + i.Soyad + " - " + s.SiretAd);
                else
                    list.Add(i.ID, i.Ad + " " + i.Soyad);
            }
        };
        return list;
    }
    private void getDelete(int p)
    {
        var d = rep.deleteRowGrup(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            List<object> lst = new List<object>();
            foreach (var item in box2View.Items)
            {
                var _item = (item as ListItem);

                //var result = rep.ActionGrupMusteri(mode, ddlGrup.SelectedValue, _item.Value);
                //if (result.result)
                //{
                //    //lst.Add(result.data);
                //}

            }

            //if (result.result)
            //{
            //    if (mode.Equals("add"))
            //        AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
            //    else
            //        AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

            //    pnlListe.Visible = true;
            //    pnlSayfaEkle.Visible = false;
            //    hdnAction.Value = "";
            //    hdnID.Value = "";
            //}
            //else
            //{
            //    AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            //}

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        
    }
    [WebMethod]
    public static string saveCekList(string musteriIds, string grupIds, string not, string basTarih, string bitTarih, string tutar, string minTutar,string sirket,string tutarTip)
    {
        string r = "0";
        string[] array = null;
        string[] arrayGrup = null;
        string[] arraySirket = null;
        ArrayList Musterilist = new ArrayList();

        ArrayList grupMusteri=null;
        ArrayList sirketMusteri=null;
        List<object> lst = new List<object>();
        HediyeCekRepositories rep = new HediyeCekRepositories();
        ResultAction res=new ResultAction();
        try
        {
            string seriNo = DateTime.Now.Ticks.ToString();

            arrayGrup = grupIds.Split(',');
            if(arrayGrup[0]!="")
                grupMusteri = rep.GetGrupMusteriList(arrayGrup);

            arraySirket = sirket.Split(',');
            if(arraySirket[0]!="")
                sirketMusteri = rep.GetSirketMusteriList(arraySirket);


            array = musteriIds.Split(',');
            foreach (var i in array)
            {
                if (i != "")
                    Musterilist.Add(i);
            }

            if (grupMusteri != null)
            {
                foreach (var i in grupMusteri)
                {
                    if (!Musterilist.Contains(i))
                        Musterilist.Add(i);
                }
            }
            if (sirketMusteri != null)
            {
                foreach (var i in sirketMusteri)
                {
                    if (!Musterilist.Contains(i))
                    {
                        if(i!="")
                            Musterilist.Add(i);
                    }
                }
            }

            foreach (var i in Musterilist)
                res = rep.AddNewHediyeCeki(not, basTarih, bitTarih, tutar, minTutar, int.Parse(i.ToString()), seriNo,(int)Constants.HediyeCekTip.admin,0,tutarTip);
            if (res.result == true)
            {
                lst.Add(res.data);

            }
            r = "1";
            return r;
        }
        catch (Exception ex)
        {
            return r;
            throw;

        }
    }
}