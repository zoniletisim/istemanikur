﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="dogumGunu.aspx.cs" Inherits="AdminZ0n_HediyeCeki_dogumGunu" %>



<asp:Content ID="Content4" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#hediyeCek").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	        $.configureBoxes();
	    });
	   
        </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Doğum Günü Hediye Çeki</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<%--<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> --%>
			<%--<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>--%>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnID" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Ad Soyad</th>
                 <th style="color:#000;text-align:center;cursor:pointer;">Dogum Tarih</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Şirket</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Hediye Çeki</th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%
              
              
              foreach (var item in user)
            {%>
             <tr>
                <td><%=item.Ad%> <%=item.Soyad%></td>
                 <td>
                 <%if (item.DogumTarihi != null)
                   { %>
                 <%=item.DogumTarihi.Value.ToShortDateString()%>
                 <%} %>
                 </td>
                 <td><%=sirket((int)item.SirketId)%></td>
                    <td><%=dogumGunuHediyeCeki(item.ID) %></td>           
                    <td style="width: 20px;">
                   <a href="dogumGunu.aspx?action=update&pid=<%=item.ID%>"><img ID="imog" alt="Güncelle" title="Hediye Çeki Tanımla" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

</td>
                <td style="width: 20px;">
                <a href="dogumGunu.aspx?action=delete&pid=<%=item.ID%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Hediye Çekini Sil" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <%--<asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>

        <th></th>
        <th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  <%--<asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">

        <table>
        <tr>
            <td style="width:100px;">Grup Adı</td>
           <td>
           
               <asp:DropDownList ID="ddlGrup" ClientIDMode="Static" runat="server">
               </asp:DropDownList>
               <asp:Label ID="lblGrup" runat="server" Visible="false" Text="Label"></asp:Label>
           </td>
        </tr>
        </table>
        <div class="rowElem dualBoxes">
                    	<div class="floatleft w40">
                            <input type="text" id="box1Filter" class="boxFilter" placeholder="Filtre " /><br />
                            <asp:DropDownList ID="box1View" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                            </asp:DropDownList>
                            <br/>
                            <span id="box1Counter" class="countLabel"></span>
                            
                            <div class="displayNone"><select id="box1Storage"></select></div>
                        </div>
                            
                        <div class="floatleft dualControl">
                            <button id="to2" type="button" class="dualBtn mr5 mb15">&nbsp;&gt;&nbsp;</button>
                            <button id="allTo2" type="button" class="dualBtn">&nbsp;&gt;&gt;&nbsp;</button><br />
                            <button id="to1" type="button" class="dualBtn mr5">&nbsp;&lt;&nbsp;</button>
                            <button id="allTo1" type="button" class="dualBtn">&nbsp;&lt;&lt;&nbsp;</button>
                        </div>
                            
                        <div class="floatright w40">
                            <input type="text" id="box2Filter" class="boxFilter" placeholder="Filter entries..." /><br />
                           <asp:DropDownList ID="box2View" ClientIDMode="Static" runat="server" multiple="multiple" class="multiple" style="height:300px;width:375px;">
                            </asp:DropDownList>
                          
                           
                            <br/>
                            <span id="box2Counter" class="countLabel"></span>
                            
                            <div class="displayNone"><select id="box2Storage"></select></div>
                        </div>
					<div class="fix"></div>
                    </div>

        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center">
          <input id="btnKaydet" type="button" value="button" class="Kaydet" />
    <asp:Button CssClass="button" OnClick="btnKaydet_Click"  ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>--%>
    <div class="clear">
    
    </div>
    </div>
</div> 

</asp:Content>



