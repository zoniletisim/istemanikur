﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminZ0n_SSS_sss : BasePage
{
    SSSRepositories rep = new SSSRepositories();
    ResultAction result = new ResultAction();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            result = rep.ActionSSS(hdnID.Value, txtBaslik.Text,ckIcerik.Text,hdnAction.Value, drpStatu.SelectedValue,1 );
            if (result.result)
            {
                EntityDataSource1.DataBind();
                GridView1.DataBind();

                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";

            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "update";
        hdnID.Value = GridView1.SelectedValue.ToString();
        var sf = rep.GetResimGaleri(Convert.ToInt32(GridView1.SelectedValue));

        txtBaslik.Text = sf.Baslik;
        ckIcerik.Text = sf.Icerik;
        drpStatu.SelectedValue = sf.Aktif.ToString();

    }
    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
    protected void btnSirala_Click(object sender, EventArgs e)
    {
        Sirala();
    }
    private void Sirala()
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = false;
        pnlSort.Visible = true;
        sayfaSirala.DataSource = rep.GetSSSList();
        sayfaSirala.DataBind();
    }
}