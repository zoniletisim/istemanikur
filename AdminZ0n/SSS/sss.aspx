﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="sss.aspx.cs" Inherits="AdminZ0n_SSS_sss" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">
    $(function () {
        $("#sss").addClass("current");
        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
    });

    $(function () {
        $("#list").sortable({
            handle: '.handle',
            update: function () {
                var order = $('#list').sortable('serialize');
                $.post("../service.aspx", { action: "ListSSS", item: order }, function (msg) {
                    var json = $.parseJSON(msg);
                    if (json.result) {

                    }
                });
            }
        });
    });
        </script>
 <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> <asp:Label ID="lblSayfa" runat="server" Text="SSS"></asp:Label></h3>
     <ul class="content-box-tabs">
						<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> 
						<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" /></li>
		</ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />
  <br /><br />
  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="Id" DataSourceID="EntityDataSource1" 
         OnSelectedIndexChanged="GridView1_SelectedIndexChanged" 
         AllowPaging="True" AllowSorting="True" EnableViewState="False" 
         >
        <Columns>
            <asp:BoundField DataField="Baslik" HeaderText="Baslik" 
                SortExpression="Baslik" />
            <asp:BoundField DataField="Sira" HeaderText="Sira" SortExpression="Sira" />
            <asp:CheckBoxField DataField="Aktif" HeaderText="Aktif" 
                SortExpression="Aktif" />
            <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Select" ImageUrl="~/AdminZ0n/resources/images/icons/pencil.png" Text="Select" />
                        <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" OnClientClick="if(confirm('silmek için onaylamal&#305;s&#305;n&#305;z.')){}else{return false;}" CommandName="Delete" ImageUrl="~/AdminZ0n/resources/images/icons/cross.png" Text="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagebarUTH" />
    </asp:GridView>
    
     <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
         ConnectionString="name=ZonDBEntities" DefaultContainerName="ZonDBEntities" 
         EnableDelete="True" EnableFlattening="False" 
         EntitySetName="Zon_SSS">
     </asp:EntityDataSource>
  </asp:Panel>
  <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">
        <table>
        <tr>
            <td>Başlık</td>
           <td><asp:TextBox ID="txtBaslik" CssClass="text-input" TextMode="MultiLine" Width="300" runat="server"></asp:TextBox></td>
        </tr>
      <tr>
            <td>İçerik</td>
          <td><CKEditor:CKEditorControl ID="ckIcerik" CssClass="" ClientIDMode="Static" runat="server" Height="100px"  Width="500px"></CKEditor:CKEditorControl></td>
        </tr>
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="True">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="False">Yay&#305;nda De&#287;il</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
      <asp:Panel ID="pnlSort" runat="server" Visible="false">
  <asp:Repeater ID="sayfaSirala" runat="server" >
    <HeaderTemplate><ul id="list"></HeaderTemplate>
    <FooterTemplate></ul></FooterTemplate>
    <ItemTemplate>
        <li style="padding-top:10px;padding-bottom:10px;background-color:Menu;border-style:inset;border-color:Black;border-width:thin; " id='item_<%# Eval("Id") %>'>
        <img src="../resources/images/arrow.png" alt="move" width="16" height="16" class="handle" style="cursor:pointer;" />
        
        <%# Eval("Baslik") %>
        </li>
    </ItemTemplate>
</asp:Repeater>
</asp:Panel>

    <div class="clear"></div>
    </div>
</div> 
</asp:Content>



