﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class AdminZ0n_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            FormsAuthentication.SignOut();
    }

    protected void btnGir_Click(object sender, EventArgs e)
    {
        UserRepositories rep = new UserRepositories();

        var validate = rep.ValidateMusteri(username.Value, password.Value);

        if (validate.result)
        {
            FormsAuthentication.RedirectFromLoginPage(username.Value, true);
            Session["user"]=validate.data;
        }
        else
        {
            notif.Visible = true;
            notif.InnerHtml = validate.message;
        }
    }
}