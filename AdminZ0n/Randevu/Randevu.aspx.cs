﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class AdminZ0n_Randevu_Randevu : System.Web.UI.Page
{
    RandevuRepositories rep = new RandevuRepositories();
    ResultAction result = new ResultAction();
    public List<Zon_Randevu> ofis = new List<Zon_Randevu>();
    ZonDBEntities db = new ZonDBEntities();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
           
           
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                FillForm();
                var sf = rep.GetRandevu(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                List<object> catAksesuar = new List<object>();
             
                catAksesuar = rep.categories(treeSaat,hdnAction.Value,hdnID.Value);
                
                txtTarih.Text = sf.Tarih.Value.ToShortDateString();
                drpStatu.SelectedValue = sf.Aktif.ToString();
                ddlSirket.SelectedValue = sf.SirketId.ToString();
                Zon_Indirim indirim =rep.getRandevuIndirim(hdnID.Value);
                if(indirim!=null)
                    txtIndirim.Text = indirim.MiktarYuzde.ToString();
                txtNot.Text = sf.Aciklama;
                int id=int.Parse(hdnID.Value);
                fillOfis();
                txtTolerans.Text = sf.SaatSablonId.ToString();
                ddlOfis.SelectedValue = sf.OfisId.ToString();
                var hizmet = (from hr in db.Zon_Randevu_Hizmet
                              join h in db.Zon_HizmetTur on hr.HizmetId equals h.Id
                              where hr.RandevuId == id
                              select new { h.Id, h.HizmetAd }).ToList();

                foreach (var  h in hizmet)
                {
                    chkListHizmet.Items.FindByValue(h.Id.ToString()).Selected = true;
                    
                }

                foreach (TreeNode parent in treeSaat.Nodes)
                {

                    foreach (var item in db.Zon_Randevu_Saat.Where(z=>z.RandevuId==sf.Id))
                    {
                        if (Convert.ToInt32(parent.Value)==item.SaatId)
                        {
                            parent.Checked = true;
                            break;
                        }
                    }

                }


            }
            else if (Request.QueryString["action"] == "add")
            {
               pnlListe.Visible = false;
               pnlSayfaEkle.Visible = true;
               hdnAction.Value = "add";
                List<object> catAksesuar = new List<object>();
                catAksesuar = rep.categories(treeSaat,hdnAction.Value,"-1");
               FillForm();
            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }
            else if (Request.QueryString["action"] == "saatDolu")
            {
                 int rID = int.Parse(Request.QueryString["pid"].ToString());
                 int saatId =int.Parse( Request.QueryString["saatId"].ToString());
                 var rezerve = db.Zon_Rezerve.Where(z => z.UrunId == rID && z.SaatId == saatId).FirstOrDefault();
                 if (rezerve == null)
                 {
                     Zon_Rezerve r = new Zon_Rezerve();
                     r.SaatId = saatId;
                     r.UrunId = rID;
                     r.HizmetTip = 1;
                     //r.Tarih =DateTime.Parse(txtTarih.Text);
                     db.AddToZon_Rezerve(r);
                     db.SaveChanges();
                 }
                 pnlListe.Visible = false;
                 pnlSayfaEkle.Visible = true;
                 Response.Redirect("Randevu.aspx?action=update&pid=" + rID);
            }
            else if (Request.QueryString["action"] == "saatBos")
            {
                int rID = int.Parse(Request.QueryString["pid"].ToString());
                int saatId = int.Parse(Request.QueryString["saatId"].ToString());
                var rezerve = db.Zon_Rezerve.Where(z => z.UrunId == rID && z.SaatId == saatId).FirstOrDefault();
                if (rezerve != null)
                {
                    db.DeleteObject(rezerve);
                    db.SaveChanges();
                }
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                Response.Redirect("Randevu.aspx?action=update&pid=" + rID);
            }
        }
    }

    private void fillOfis()
    {
        int sirketId = int.Parse(ddlSirket.SelectedValue);
        ddlOfis.DataSource = db.Zon_Ofis.Where(z => z.SirketId == sirketId).ToList();
        ddlOfis.DataTextField = "OfisAd";
        ddlOfis.DataValueField = "Id";
        ddlOfis.DataBind();
    }

    private void FillForm()
    {
        TanimRepositories repTanim = new TanimRepositories();
        chkListHizmet.DataSource = repTanim.GetHizmetList();
        chkListHizmet.DataTextField = "HizmetAd";
        chkListHizmet.DataValueField = "Id";
        chkListHizmet.DataBind();

        ddlSirket.DataSource = repTanim.GetSirketList();
        ddlSirket.DataTextField = "SiretAd";
        ddlSirket.DataValueField = "Id";
        ddlSirket.DataBind();

    }


    private void getDelete(int p)
    {
        var d = rep.deleteRowRandevu(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            List<string> items=new List<string>();
            List<int> saatList = new List<int>();
            foreach (TreeNode tn in treeSaat.CheckedNodes)
            {
                if (tn.Checked)
                {
                    int id = Convert.ToInt32(tn.Value);
                    saatList.Add(id);
                }
            }
            // foreach (ListItem item in chkListHizmet.Items)
            //    {
            //        if ((item as CheckBoxList))
            //            items.Add((item as CheckBoxList));
            //    }



            items=getSelected();
            result = rep.ActionRandevu(hdnID.Value, ddlSirket.SelectedValue,ddlOfis.SelectedValue,items,txtTarih.Text, hdnAction.Value, drpStatu.SelectedValue,txtNot.Text,saatList,txtIndirim.Text,txtTolerans.Text);
            if (result.result)
            {
                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    private List<string> getSelected()
    {
        return chkListHizmet.Items.Cast<ListItem>().Where(z => z.Selected).Select(i => i.Value).ToList();
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        //pnlListe.Visible = false;
        //pnlSayfaEkle.Visible = true;
        //hdnAction.Value = "add";
        Response.Redirect("randevu.aspx?action=add");
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }

    protected void ddlSirket_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sirketId=int.Parse( ddlSirket.SelectedValue);
        ddlOfis.DataSource = db.Zon_Ofis.Where(z => z.SirketId == sirketId).ToList();
        ddlOfis.DataTextField = "OfisAd";
        ddlOfis.DataValueField = "Id";
        ddlOfis.DataBind();
    }

    public string sirketAd(int id)
    {
        TanimRepositories t = new TanimRepositories();
        string ret = "-";
        if (id != null)
            ret = t.GetSirket(id).SiretAd;

        return ret;
    }
    public string ofisAd(int id)
    {
        TanimRepositories t = new TanimRepositories();
        string ret = "-";
        if (id != null)
            ret = t.GetOfis(id).OfisAd;

        return ret;
    }
}