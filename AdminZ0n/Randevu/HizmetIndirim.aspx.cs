﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class AdminZ0n_Randevu_HizmetIndirim : System.Web.UI.Page
{
    public RandevuRepositories rep = new RandevuRepositories();
    ResultAction result = new ResultAction();
    public List<Zon_Hizmet_Indirim> indirim = new List<Zon_Hizmet_Indirim>();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            FillCat();
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.getHizmetIndirim(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                ddlSirket.SelectedValue =sf.SirketId.ToString();
                ddlHizmet1.SelectedValue = sf.Hizmet1Id.ToString();
                ddlHizmet2.SelectedValue = sf.Hizmet2Id.ToString();
                txtIndirim.Text = sf.Indirim.ToString();
                ddlIndirimTip.SelectedValue = sf.IndirimTip.ToString();
                drpStatu.SelectedValue = sf.Aktif.ToString();
            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }

        }
    }


    private void getDelete(int p)
    {
        var d = rep.deleteRowIndirim(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            result = rep.actionHizmetIndirim(hdnID.Value, hdnAction.Value, ddlSirket.SelectedValue, ddlHizmet1.SelectedValue, ddlHizmet2.SelectedValue,txtIndirim.Text,ddlIndirimTip.SelectedValue,drpStatu.SelectedValue);
            if (result.result)
            {
                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }

    private void FillCat()
    {
        TanimRepositories repTanim = new TanimRepositories();
        ddlSirket.DataSource = repTanim.GetSirketList();
        ddlSirket.DataTextField = "SiretAd";
        ddlSirket.DataValueField = "Id";
        ddlSirket.DataBind();

        //ddlSirket.Items.Insert(0, new ListItem("Seçiniz", ""));
        ddlHizmet1.DataSource = repTanim.GetHizmetList();
        ddlHizmet1.DataTextField = "HizmetAd";
        ddlHizmet1.DataValueField = "Id";
        ddlHizmet1.DataBind();

        ddlHizmet2.DataSource = repTanim.GetHizmetList();
        ddlHizmet2.DataTextField = "HizmetAd";
        ddlHizmet2.DataValueField = "Id";
        ddlHizmet2.DataBind();

        //listFotoGaleri.Items.Clear();
        //listFotoGaleri.DataTextField = "Aciklama";
        //listFotoGaleri.DataValueField = "Id";
        //listFotoGaleri.DataSource = rep.getFotoGaleri();
        //listFotoGaleri.DataBind();
        //listFotoGaleri.Items.Insert(0, new ListItem("Galeri Yok", ""));
        //listFotoGaleri.SelectedIndex = -1;
        //ddlDil.Items.Insert(0, new ListItem("Dil Seçiniz", "0"));
    }
    
}