﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="Randevu.aspx.cs" Inherits="AdminZ0n_Randevu_Randevu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#randevu").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	    });
	    $(function () {
	        $("#list").sortable({
	            handle: '.handle',
	            update: function () {
	                var order = $('#list').sortable('serialize');
	                $.post("../service.aspx", { action: "ListSirket", item: order }, function (msg) {
	                    var json = $.parseJSON(msg);
	                    if (json.result) {

	                    }
	                });
	            }
	        });
	    });
	    jQuery(function ($) {
	        $.datepicker.regional['tr'] = {
	            closeText: 'kapat',
	            prevText: '&#x3c;geri',
	            nextText: 'ileri&#x3e',
	            currentText: 'bugün',
	            monthNames: ['Ocak', '&#350;ubat', 'Mart', 'Nisan', 'May&#305;s', 'Haziran',
                'Temmuz', 'A&#287;ustos', 'Eylül', 'Ekim', 'Kas&#305;m', 'Aral&#305;k'],
	            monthNamesShort: ['Oca', '&#350;ub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'A&#287;u', 'Eyl', 'Eki', 'Kas', 'Ara'],
	            dayNames: ['Pazar', 'Pazartesi', 'Sal&#305;', 'Çar&#351;amba', 'Per&#351;embe', 'Cuma', 'Cumartesi'],
	            dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
	            dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
	            weekHeader: 'Hf',
	            dateFormat: 'dd.mm.yy',
	            firstDay: 1,
	            isRTL: false,
	            showMonthAfterYear: false,
	            yearSuffix: ''
	        };
	        $.datepicker.setDefaults($.datepicker.regional['tr']);
	    });
	    $(document).ready(function () {
	        $("input.date").datepicker({ "dateFormat": "dd.mm.yy" });
	    });
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> &#350;irket Randevu</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<%--<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> --%>
			<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
  <table id="table2">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">&#350;irket Ad&#305;</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Ofis Ad&#305;</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Hizmet Türü</th>
                  <th style="color:#000;text-align:center;cursor:pointer;">Tarih</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Yayin<div id="ajaxloading" style="display:none; position:absolute;"><img src="/adminz0n/resources/images/ajax-loader.gif" /></div> </th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%
              ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
              ofis = db.Zon_Randevu.Where(z => z.Aktif != "3").ToList();
              foreach (var item in ofis)
            {%>
             <tr>
                <td ><%=item.SirketId!=null ? sirketAd((int)item.SirketId) : "-"%></td>
                <td ><%=item.OfisId!=null ? ofisAd((int)item.OfisId) : "-"%></td>
                <td>
                <%var hizmet = db.Zon_Randevu_Hizmet.Where(z => z.RandevuId == item.Id).ToList();
                  foreach(var h in hizmet)
                  {
                  %>
                  <%=h.Zon_HizmetTur.HizmetAd%>,
                  <%} %>
                </td>
                <td ><%=string.Format("{0:d}" ,item.Tarih)%></td>
                 <td>
                <%if (item.Aktif=="1")
                    {%>
                    <input type="checkbox" id="chkTrue"  onclick="CallIsVisible(<%=item.Id%>,'sirketRandevu');" checked="checked"   />Aktif
                    <%} %>
                                  
                    <%else
                    { %>
                    <input type="checkbox" id="chkTrue" onclick="CallIsVisible(<%=item.Id%>,'sirketRandevu');" />Aktif
                    
                    <%} %>
                    </td>
                               
                    <td style="width: 20px;">
                   <a href="Randevu.aspx?action=update&pid=<%=item.Id%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

</td>
                <td style="width: 20px;">
                <a href="Randevu.aspx?action=delete&pid=<%=item.Id%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <%--<asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>

        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">
    <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
        <table>
        <tr>
            <td>&#350;irket</td>
           <td>
               <asp:DropDownList ID="ddlSirket" AutoPostBack="true"  class="multiSelect"  
                   Width="300" runat="server" 
                   onselectedindexchanged="ddlSirket_SelectedIndexChanged">
                </asp:DropDownList>
           </td>
           </tr>
           <tr>
            <td>Ofis</td>
           <td>
               <asp:DropDownList ID="ddlOfis" ClientIDMode="Static"  class="multiSelect"  Width="300" runat="server">
                </asp:DropDownList>
           </td>
        </tr>
      
        <tr>
            <td>Randevu Tarih</td>
           <td>
             <asp:TextBox ID="txtTarih" CssClass="text-input date"  Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
          <tr>
            <td>Hizmet Türü</td>
           <td>
               <asp:CheckBoxList ID="chkListHizmet" runat="server" 
                   RepeatDirection="Horizontal" RepeatLayout="Flow">
               </asp:CheckBoxList>
           </td>
        </tr>
           <tr>
            <td>Not(Açıklama):</td>
           <td>
             <asp:TextBox ID="txtNot" CssClass="text-input" TextMode="MultiLine"  Width="300" Height="50" runat="server"></asp:TextBox>
           </td>
        </tr>
        
           <tr>
            <td>İndirim Miktarı:</td>
           <td>
            % <asp:TextBox ID="txtIndirim" CssClass="text-input"   runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Saat Tolerans:</td>
           <td>
            <asp:TextBox ID="txtTolerans" CssClass="text-input"  runat="server"></asp:TextBox>
           </td>
        </tr>
       
         <tr>
            <td>Randevu Saatleri</td>
            <td>
            <div style="float:left;width:250px;">
               <div class="title"> Saatler</div>
            <div class="scrollbox">
                
            <br />
             <asp:TreeView ID="treeSaat" CssClass="tbl" style="margin-top:21px;margin-left:-15px" runat="server"  ShowLines="false"></asp:TreeView>
             </div>
             </div>
            </td>
        </tr>
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="2">Yay&#305;nda De&#287;il</asp:ListItem>
                    <asp:ListItem Value="3">Silinebilir</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="return kontrol();" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
    <div class="clear">
    
    </div>
    </div>
</div> 
<script>
    function kontrol() {
        if ($("#ddlOfis").val() == null) {
            alert("Ofis Seçiniz..");
            return false;
        }
    }
    $.fn.dataTableExt.oSort['uk_date-asc'] = function (a, b) {
        var ukDatea = a.split('.');
        var ukDateb = b.split('.');

        var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    };

    $.fn.dataTableExt.oSort['uk_date-desc'] = function (a, b) {
        var ukDatea = a.split('.');
        var ukDateb = b.split('.');

        var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    }; var asInitVals = new Array();
    $(document).ready(function () {

        var oTable2 = $('#table2').dataTable(
         {
             "sPaginationType": "full_numbers",
             "sRowSelect": "single",
             "bProcessing": true,
             "bFilter": true,
             "aoColumnDefs": [{ "aTargets": [3], "sType": "uk_date"}],
             "aaSorting": [[3, "desc"]]
         });


    });


    $(function () {
        $('.scrollbox input[type=checkbox]').css("float", "left");
    });

    function bosGoster() {
        var x = confirm("Emin misiniz?");
        if (x) {
            var action = $('#bosGoster').attr('href');
            window.location.href = action;
        }
        else
            return false;
    }
    function doluGoster() {
        var x = confirm("Emin misiniz?");
        if (x) {
            var action = $('#doluGoster').attr('href');
            window.location.href = action;
        }
        else
            return false;
    }

        </script>
</asp:Content>




