﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="SayfaYonetimi.aspx.cs" Inherits="AdminZ0n_Sayfalar_SayfaYonetimi" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../../ckfinder/ckfinder.js" type="text/javascript"></script>
	<script type="text/javascript">
	    $(function () {
	        $("#list").sortable({
	            handle: '.handle',
	            update: function () {
                openLoader();	                var order = $('#list').sortable('serialize');
	                $.post("../service.aspx", { action: "ListPage", item: order }, function (msg) {
	                    var json = $.parseJSON(msg);
	                    if (json.result) {
                        closeLoader();	                    }
	                });
	            }
	        });
	    });
	    $(function () {
	        $("#sayfa").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	    });
	    function BrowseServerImage() {

	        var finder = new CKFinder();
	        finder.basePath = '../ckfinder/';
	        finder.selectActionFunction = SetFileField;
	        finder.popup();
	    }

	    function SetFileField(fileUrl1) {
	        document.getElementById('txtImage').value = fileUrl1;
	    }

	    function BrowseServerDokuman() {

	        var finder = new CKFinder();
	        finder.basePath = '../ckfinder/';
	        finder.selectActionFunction = SetFileFieldDokuman;
	        finder.popup();
	    }

	    function SetFileFieldDokuman(fileUrl) {
	        document.getElementById('txtDokuman').value = fileUrl;
	    }

	    function BrowseServerBanner() {

	        var finder = new CKFinder();
	        finder.basePath = '../ckfinder/';
	        finder.selectActionFunction = SetFileFieldBanner;
	        finder.popup();
	    }

	    function SetFileFieldBanner(fileUrl) {
	        document.getElementById('txtBannerResim').value = fileUrl;
	    }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
    <div class="content-box">
    <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Sayfa Yönetimi</h3>
       <ul class="content-box-tabs">
						
                        <li> <asp:LinkButton CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> 
						<li><asp:LinkButton CssClass="button" ID="Button1" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" /></li>
                       
					</ul>
    </div>
    <div class="content-box-content">
    
 <asp:HiddenField ID="hdnAction" runat="server" />
            <asp:HiddenField ID="hdnID" runat="server" />
            <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  <asp:LinkButton CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />

     <br /><br />
     <table id="table"  class="display">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                
                <th style="color:#000;text-align:center;cursor:pointer;">Ba&#351;l&#305;k</th>
		        <th style="color:#000;text-align:center;cursor:pointer;">Üst Sayfa </th>
                <th style="color:#000;text-align:center;cursor:pointer;">Menü Gör</th>
               <%-- <th style="color:#000;text-align:center;cursor:pointer;">Foto Galeri</th>--%>
                <th style="color:#000;text-align:center;cursor:pointer;">Tarih</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Sira</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Yayin <div id="ajaxloading" style="display:none; position:absolute;"><img src="/adminz0n/resources/images/ajax-loader.gif" /></div>  </th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%
              
                ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
            var p = (from item in db.Zon_SayfaIcerik
                     where item.Tip==(int)Constants.safyaIcerikTip.sayfa && item.Aktif!="3"
               select item).ToList();
            foreach (var item in p)
            {%>
             <tr>
                <td ><%=item.Baslik%></td>
               <%-- <td style="width: 30px;"> <img ID="imgGet" src='<%=item.Resim %>' width="50"  /></td>--%>
                <td ><%string mainCat = getMainCat(item.Id); %><%=mainCat%> </td>
                <td ><%=item.MenuVisible==true ? "Görünür" :"Görünmez" %></td>
              <%--  <td ><%=item.ResimGaleriId!=null ? item.Zon_ResimGaleri_Kategori.Aciklama :"Galeri Yok" %></td>--%>
                <td width="60"  style="width: 60px;"><%=item.EklemeTarih.Value.ToShortDateString() %></td>
                <td ><%=item.Sira %></td>
                <td>
                <%if (item.Aktif=="1")
                    {%>
                    <input type="checkbox" id="chkTrue"  onclick="CallIsVisible(<%=item.Id%>,'SayfaIcerik');" checked="checked"   />Yay&#305;n
                    <%} %>
                    <%else
                    { %>
                    <input type="checkbox" id="chkTrue" onclick="CallIsVisible(<%=item.Id%>,'SayfaIcerik');" />Yay&#305;n
                    <%} %>
                    </td>
                    <td style="width: 20px;">
                   <a href="SayfaYonetimi.aspx?action=update&pid=<%=item.Id%>&sayfaid=<%=item.SayfaId%>&l=<%=item.DilId%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 
                </td>
                <td style="width: 20px;">
                    <a href="SayfaYonetimi.aspx?action=delete&pid=<%=item.Id%>" onclick='return Delete();' ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> 
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<th><input type="text" name="search_name" value="Baslik" class="search_init" style="width:120px" /></th>
     
		<th><input type="text" name="search_detail" value="K&#305;sa &#304;çerik" class="search_init" style="width:120px" /></th>
		<th><input type="text"  name="search_detail" value="Menu Gör." class="search_init" style="width:80px" /></th>
        <%--<th><input type="text"  name="search_detail" value="Foto Galeri" class="search_init" style="width:120px" /></th>--%>
        <th><input type="text"  name="search_detail" value=" Tarih" class="search_init" style="width:60px" /></th>
        <th><input type="text"  name="search_detail" value="S&#305;ra" class="search_init" style="width:40px" /></th>
        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
   
    
    </asp:Panel>
     <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">
     <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
        <table class="display">
       
        <tr>
            <td>Üst Sayfa</td>
            <td>
                <asp:DropDownList ID="ddlKategori" runat="server" AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Ba&#351;l&#305;k</td>
            <td><asp:TextBox ID="txtBaslik" CssClass="text-input" Width="300" runat="server"></asp:TextBox>
                <asp:Label ID="lblTrBaslik"  runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>K&#305;sa &#304;çerik</td>
            <td><asp:TextBox ID="txtKisaIcerik" TextMode="MultiLine"  Width="500" runat="server"></asp:TextBox>
            <div style="float:right;width:400px;"> <asp:Label ID="lblTrKisaicerik"  runat="server" Text=""></asp:Label></div>
            </td>
        </tr>
        <tr>
            <td>&#304;çerik</td>
            <td><CKEditor:CKEditorControl ID="ckIcerik" CssClass="" ToolTip="Editor notu giriniz" ClientIDMode="Static" runat="server" Height="100px"  Width="500px"></CKEditor:CKEditorControl>
            <%-- <div style="float:right;width:400px;">
             <iframe width="400" height="400" scrolling="yes">
             <asp:Label ID="lblTrIcerik"  runat="server" Text=""></asp:Label>
             </iframe>
                  </div>--%>
            </td>
        </tr>
       
        
        <tr style="">
            <td>Resim</td>
            <td>
                <asp:TextBox ID="txtImage" Width="300" ClientIDMode="Static" CssClass="text-input" runat="server"></asp:TextBox>
                <input type="button" value="Resim Ekle" onclick="BrowseServerImage();" />
                <br />
                <asp:Image ID="imgResim" runat="server" Width="100" />

            </td>
        </tr>
        <tr style="display:none;">
            <td>Döküman</td>
            <td>
                <asp:TextBox ID="txtDokuman" CssClass="text-input" ClientIDMode="Static" Width="300" runat="server"></asp:TextBox>
                <input type="button" value="Döküman Ekle" onclick="BrowseServerDokuman();" />
                <br />
                <asp:HyperLink ID="linkDokuman" runat="server"></asp:HyperLink>
            </td>
        </tr>
            <tr>
            <td>Banner Resmi</td>
            <td>
                <asp:TextBox ID="txtBannerResim" Width="300" ClientIDMode="Static" CssClass="text-input" runat="server"></asp:TextBox>
                <input type="button" value="Banner Ekle" onclick="BrowseServerBanner();" />
                <br />
                <asp:Image ID="imgBanner" runat="server" Width="100" />
            </td>
        </tr>
          <tr>
            <td>Banner Url</td>
            <td>
                <asp:TextBox ID="txtBannerUrl" Width="300" ClientIDMode="Static" CssClass="text-input" runat="server"></asp:TextBox>
               
            </td>
        </tr>
         <tr>
            <td>Foto Galeri</td>
            <td>
                <asp:ListBox ID="listFotoGaleri" Width="300" runat="server">
                </asp:ListBox>
            </td>
        </tr>
        <tr>
            <td>Direk Link</td>
            <td><asp:TextBox ID="txtUrl" CssClass="text-input" Width="500" runat="server"></asp:TextBox>
            Yeni Sekmede Aç&#305;ls&#305;n 
                <asp:CheckBox ID="chkNewTab" runat="server" />
            </td>
        </tr>
        <tr>
            <td>T&#305;klanabilir</td>
            <td><asp:CheckBox ID="chkMenuClick"  runat="server" /></td>
        </tr>
         <tr>
            <td>Menüde Görünür</td>
            <td><asp:CheckBox ID="chkMenuVisible"  runat="server" /></td>
        </tr>
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="2">Yay&#305;nda De&#287;il</asp:ListItem>
                     <asp:ListItem Value="3">Silinebilir</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:LinkButton CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:LinkButton CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
     <div class="clear">
   
      <asp:Panel ID="pnlSort" runat="server" Visible="false">
  
    <ul id="list">
   <%
     ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
     var p = (from item in db.Zon_SayfaIcerik
              where item.Tip == (int)Constants.safyaIcerikTip.sayfa  && item.Aktif != "3"
              orderby item.Sira
              select item).ToList();
     foreach (var item in p)
     {%>
 
        <li style="padding-top:10px;padding-bottom:10px;background-color:Menu;border-style:inset;border-color:Black;border-width:thin;" id='item_<%=item.Id %>'>
        <img src="../resources/images/arrow.png" alt="move" width="16" height="16" class="handle" style="cursor:pointer;" />
        <%=item.Baslik %>
        </li>
        <%} %>
 </ul>
</asp:Panel>
</div>
    </div>
</div>
<script type="text/javascript">
    function Delete() {
        var x = confirm("Kay&#305;t silinecek.\nEmin misiniz?");
        if (x) {
            var action = $('#deleteRow').attr('href');
            window.location.href = action;
        }
        else
            return false;
    }
</script>
   
</asp:Content>



