﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Collections.Specialized;
public partial class AdminZ0n_Sayfalar_SayfaYonetimi :BasePage
{
    SayfaRepositories rep = new SayfaRepositories();
    ResultAction result = new ResultAction();
    protected void Page_Load(object sender, EventArgs e)
    {}
    protected override void OnLoad(EventArgs e)
    {
       if (!Page.IsPostBack)
       {
           //getLanguageFill();
           //getLanguageFilterFill();

           if (Request.QueryString["action"] == "update")
            {
                CKFinder.FileBrowser _FileBrowser = new CKFinder.FileBrowser();
                _FileBrowser.BasePath = "../../ckfinder/";//ckfinder klasör yolu
                _FileBrowser.SetupCKEditor(ckIcerik);//Sayfadaki entegre edilicek olan ckeditor IDsi
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                hdnSayfaId.Value = Request.QueryString["sayfaid"].ToString();
                pageFill();
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                SayfaRepositories rep = new SayfaRepositories();
                var sf = rep.GetSayfa(Convert.ToInt32(Request.QueryString["sayfaid"].ToString()), (int)Constants.safyaIcerikTip.sayfa,int.Parse(Request.QueryString["l"]));
                

                ddlKategori.SelectedValue = rep.getUstKategori(Convert.ToInt32(sf.SayfaId));
                //txtSoyad.Text = sf.EditorSoyad;
                txtBaslik.Text = sf.Baslik;
                txtKisaIcerik.Text = sf.KisaIcerik;
                ckIcerik.Text = sf.Icerik;
                txtImage.Text = sf.ResimTh;
                imgResim.ImageUrl = sf.ResimTh;
                imgBanner.ImageUrl = sf.BannerImage;
                linkDokuman.NavigateUrl = sf.DokumanId.ToString();
                txtDokuman.Text = sf.DokumanId.ToString();
                txtUrl.Text = sf.StaticUrl;
                txtBannerResim.Text = sf.BannerImage;
                txtBannerUrl.Text = sf.BannerImageUrl;
                chkNewTab.Checked = Convert.ToBoolean(sf.YeniSekme);
                chkMenuClick.Checked = Convert.ToBoolean(sf.MenuClick);
                chkMenuVisible.Checked = Convert.ToBoolean(sf.MenuVisible);
                drpStatu.SelectedValue = sf.Aktif.ToString();
                listFotoGaleri.SelectedValue = sf.ResimGaleriId.ToString();
                //ddlDil.SelectedValue = sf.DilId.ToString();

                if (Request.QueryString["l"] != "1")
                {
                    Zon_SayfaIcerik tricerik = rep.getTrBaslik(int.Parse(Request.QueryString["sayfaid"].ToString()));
                    lblTrBaslik.Text = tricerik.Baslik;
                    lblTrKisaicerik.Text = tricerik.KisaIcerik;
                   // lblTrIcerik.Text = tricerik.Icerik;
                }
            }

           else if (Request.QueryString["action"] == "addL" && Request.QueryString["l"] != "0")
           {
               pnlListe.Visible = false;
               pnlSayfaEkle.Visible = true;
               hdnAction.Value = "addL";
               hdnSayfaId.Value = Request.QueryString["sayfaid"];
               pageFill();
               ddlKategori.SelectedValue = rep.getUstKategori(Convert.ToInt32(Request.QueryString["sayfaid"].ToString()));
              // ddlDil.SelectedValue = Request.QueryString["l"].ToString();
               Zon_SayfaIcerik tricerik = rep.getTrBaslik(int.Parse(Request.QueryString["sayfaid"].ToString()));
               lblTrBaslik.Text = tricerik.Baslik;
               lblTrKisaicerik.Text = tricerik.KisaIcerik;
               txtUrl.Text = tricerik.StaticUrl;
               txtBannerResim.Text = tricerik.BannerImage;
               imgBanner.ImageUrl = tricerik.BannerImage;
               //lblTrIcerik.Text = tricerik.Icerik;
           }

           else if (Request.QueryString["action"] == "sort")
           {
               Sirala();
           }
           else if (Request.QueryString["action"] == "delete")
           {
               if (Request.QueryString["pid"] != null)
                   getDelete(int.Parse(Request.QueryString["pid"].ToString()));
           }
           else if (Request.QueryString["action"] == "add")
           {
               pnlListe.Visible = false;
               pnlSayfaEkle.Visible = true;
               hdnAction.Value = "add";
               pageFill();
           }
       }
        
    }
    private void getDelete(int p)
    {
        var d = rep.deleteRow(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);
    }
    //private void getLanguageFilterFill()
    //{
    //    ddlFilterDil.DataSource = Constants.getDil();
    //    ddlFilterDil.DataTextField = "Value";
    //    ddlFilterDil.DataValueField = "Key";
    //    ddlFilterDil.DataBind();
    //}

    //private void getLanguageFill()
    //{
    //    ddlDil.DataSource = Constants.getDil();
    //    ddlDil.DataTextField = "Value";
    //    ddlDil.DataValueField = "Key";
    //    ddlDil.DataBind();
    //    //ddlDil.Items.Insert(0, new ListItem("Dil Seçiniz", "0"));
    //}

    private void Sirala()
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = false;
        pnlSort.Visible = true;
        //sayfaSirala.DataSource = rep.getSayfaList(1,ddlFilterDil.SelectedValue);
        //sayfaSirala.DataBind();
    }

    private void pageFill()
    {
        ddlKategori.Items.Clear();
        ddlKategori.DataSource = rep.getSayfaKategori();
        ddlKategori.DataTextField = "Baslik";
        ddlKategori.DataValueField = "Id";
        ddlKategori.DataBind();
        ddlKategori.Items.Insert(0, new ListItem("Üst Sayfa Yok", "-1"));

        listFotoGaleri.Items.Clear();
        listFotoGaleri.DataTextField = "Aciklama";
        listFotoGaleri.DataValueField = "Id";
        //listFotoGaleri.DataSource = rep.getFotoGaleri();
        //listFotoGaleri.DataBind();
        //listFotoGaleri.Items.Insert(0, new ListItem("Galeri Yok", ""));
        //listFotoGaleri.SelectedIndex = -1;
    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            string tempPath = "/App_themes";
            
            result = rep.ActionSayfa(hdnID.Value,hdnSayfaId.Value,ddlKategori.SelectedValue,"1", txtBaslik.Text, txtKisaIcerik.Text, ckIcerik.Text,
                txtDokuman.Text, txtImage.Text,txtBannerResim.Text,txtBannerUrl.Text, listFotoGaleri.SelectedValue, txtUrl.Text,Convert.ToBoolean(chkNewTab.Checked), Convert.ToBoolean(chkMenuClick.Checked), Convert.ToBoolean(chkMenuVisible.Checked),"", drpStatu.SelectedValue, (int)Constants.safyaIcerikTip.sayfa, mode, tempPath,"");

            if (result.result)
            {
                //EntityDataSource1.DataBind();
                //GridView1.DataBind();

                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";

                //ddlKategori.Items.Clear();
                //ddlKategori.DataSource = rep.getSayfaKategori();
                //ddlKategori.DataTextField = "Baslik";
                //ddlKategori.DataValueField = "Id";
                //ddlKategori.DataBind();
                //ddlKategori.Items.Insert(0, new ListItem(" ", "-1"));
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }
    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
        Response.Redirect("sayfaYonetimi.aspx");
    }
    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        
        
        Response.Redirect("sayfaYonetimi.aspx?action=add");
    }
   

    //protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    pnlListe.Visible = false;
    //    pnlSayfaEkle.Visible = true;
    //    hdnAction.Value = "update";
    //    //hdnID.Value = GridView1.SelectedValue.ToString();

    //    SayfaRepositories rep = new SayfaRepositories();
    //    var sf = rep.GetSayfa(Convert.ToInt32(GridView1.SelectedValue));


    //    ddlKategori.SelectedValue = rep.getUstKategori(Convert.ToInt32(sf.SayfaId));
    //    //txtSoyad.Text = sf.EditorSoyad;
    //    txtBaslik.Text = sf.Baslik;
    //    txtKisaIcerik.Text = sf.KisaIcerik;
    //    ckIcerik.Text = sf.Icerik;
    //    txtImage.Text = sf.ResimTh;
    //    txtDokuman.Text = sf.DokumanId.ToString();
    //    txtUrl.Text = sf.StaticUrl;
    //    chkMenuClick.Checked = Convert.ToBoolean(sf.MenuVisible);
    //    drpStatu.SelectedValue = sf.Aktif.ToString();
    //    listFotoGaleri.SelectedValue = sf.ResimGaleriId.ToString();

    //}

    public string getMainCat(int id)
    {
        string ret="";
        ZonDBEntities db=new ZonDBEntities();
        View_Sayfalar s2=null;
        View_Sayfalar s = db.View_Sayfalar.Where(z => z.Id == id).FirstOrDefault();
        if(s.ParentId!=-1)
        {
            s2 = db.View_Sayfalar.Where(z => z.Id == s.ParentId).FirstOrDefault();
            ret = s2.Baslik;
        }

        else
            ret="Üst Sayfa Yok";

        return ret;
        
    }
    protected void btnSirala_Click(object sender, EventArgs e)
    {
        Sirala();
    }
    //protected void ddlDil_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string dilID = ddlDil.SelectedValue;

    //    var d = rep.GetSayfaLanguage(Convert.ToInt32(Request.QueryString["sayfaid"]),int.Parse(dilID),(int)Constants.safyaIcerikTip.sayfa);

    //    if(d!=null)
    //        Response.Redirect("sayfayonetimi.aspx?action=update&pid="+Request.QueryString["pid"]+"&sayfaid="+d.SayfaId+"&l="+ddlDil.SelectedValue+"&ul=1");

    //    else
    //        Response.Redirect("sayfayonetimi.aspx?action=addL&pid=" + Request.QueryString["pid"] + "&sayfaid=" + Request.QueryString["sayfaid"] + "&l=" + ddlDil.SelectedValue);



       
    //}
}