﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class AdminZ0n_Tanim_Tanim : System.Web.UI.Page
{
    TanimRepositories rep = new TanimRepositories();
    ResultAction result = new ResultAction();
    public List<Zon_Sirket> sirket = new List<Zon_Sirket>();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.GetSirket(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                txtBaslik.Text = sf.SiretAd;
                ddlTip.SelectedValue = sf.Tip.ToString();
                drpStatu.SelectedValue = sf.Aktif.ToString();
            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }
           
        }
    }

    private void getDelete(int p)
    {
        var d = rep.deleteRow(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }
    
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            result = rep.ActionSirket(hdnID.Value,txtBaslik.Text, hdnAction.Value, drpStatu.SelectedValue,ddlTip.SelectedValue);
            if (result.result)
            {
                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
    //protected void btnSirala_Click(object sender, EventArgs e)
    //{
    //    Sirala();
    //}

    //private void Sirala()
    //{
    //    pnlListe.Visible = false;
    //    pnlSayfaEkle.Visible = false;
    //    pnlSort.Visible = true;
    //    sayfaSirala.DataSource = rep.GetBannerList();
    //    sayfaSirala.DataBind();
    //}
}