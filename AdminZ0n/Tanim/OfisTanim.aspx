﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="OfisTanim.aspx.cs" Inherits="AdminZ0n_Tanim_OfisTanim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#tanim").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	    });
	    $(function () {
	        $("#list").sortable({
	            handle: '.handle',
	            update: function () {
	                var order = $('#list').sortable('serialize');
	                $.post("../service.aspx", { action: "ListSirket", item: order }, function (msg) {
	                    var json = $.parseJSON(msg);
	                    if (json.result) {

	                    }
	                });
	            }
	        });
	    });
	  
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Ofis Tanımı</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<%--<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> --%>
			<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Ofis Adı</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Şirket Adı</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Yayin<div id="ajaxloading" style="display:none; position:absolute;"><img src="/adminz0n/resources/images/ajax-loader.gif" /></div> </th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%
              ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
              ofis = db.Zon_Ofis.Where(z => z.Aktif != "3").ToList();
              foreach (var item in ofis)
            {%>
             <tr>
                <td ><%=item.OfisAd%></td>
                 <td ><%=item.Zon_Sirket.SiretAd%></td>
                 <td>
                <%if (item.Aktif=="1")
                    {%>
                    <input type="checkbox" id="chkTrue"  onclick="CallIsVisible(<%=item.Id%>,'banner');" checked="checked"   />Aktif
                    <%} %>
                                  
                    <%else
                    { %>
                    <input type="checkbox" id="chkTrue" onclick="CallIsVisible(<%=item.Id%>,'banner');" />Aktif
                    
                    <%} %>
                    </td>
                               
                    <td style="width: 20px;">
                   <a href="Ofistanim.aspx?action=update&pid=<%=item.Id%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

</td>
                <td style="width: 20px;">
                <a href="Ofistanim.aspx?action=delete&pid=<%=item.Id%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <%--<asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>

        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">
        <table>
        <tr>
            <td>Şirket</td>
           <td>
               <asp:DropDownList ID="ddlKategori"  class="multiSelect"  Width="300" runat="server">
                </asp:DropDownList>
           </td>
        </tr>
        <tr>
            <td>Ofis Adı</td>
           <td><asp:TextBox ID="txtBaslik" CssClass="text-input"  Width="300" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Ofis Adres</td>
           <td>
             <asp:TextBox ID="txtAdres" CssClass="text-input" TextMode="MultiLine"  Width="300" Height="50" runat="server"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="loginValidate" Text="Adres Giriniz" ForeColor="Red" runat="server" ControlToValidate="txtAdres" ErrorMessage="Adres Giriniz."></asp:RequiredFieldValidator>
           </td>
        </tr>
        
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="2">Yay&#305;nda De&#287;il</asp:ListItem>
                    <asp:ListItem Value="3">Silinebilir</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
    <div class="clear">
    
    </div>
    </div>
</div> 
</asp:Content>


