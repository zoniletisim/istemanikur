﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="emailGonder.aspx.cs" Inherits="AdminZ0n_email_emailGonder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script src="../../ckfinder/ckfinder.js" type="text/javascript"></script>
	<script type="text/javascript">
	 
	    function BrowseServerImage() {

	        var finder = new CKFinder();
	        finder.basePath = '../ckfinder/';
	        finder.selectActionFunction = SetFileField;
	        finder.popup();
	    }

	    function SetFileField(fileUrl1) {
	        document.getElementById('txtImage').value = fileUrl1;
	    }

	    function BrowseServerDokuman() {

	        var finder = new CKFinder();
	        finder.basePath = '../ckfinder/';
	        finder.selectActionFunction = SetFileFieldDokuman;
	        finder.popup();
	    }

	    function SetFileFieldDokuman(fileUrl) {
	        document.getElementById('txtDokuman').value = fileUrl;
	    }

	    function BrowseServerBanner() {

	        var finder = new CKFinder();
	        finder.basePath = '../ckfinder/';
	        finder.selectActionFunction = SetFileFieldBanner;
	        finder.popup();
	    }

	    function SetFileFieldBanner(fileUrl) {
	        document.getElementById('txtBannerResim').value = fileUrl;
	    }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;">Email Gonder</h3>
       
    </div>
    <div class="content-box-content">
        <table>
        <tr>
            <td>Subject</td>
           <td><asp:TextBox ID="txtSubject" CssClass="text-input" Width="500"  runat="server"></asp:TextBox>
           <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="loginValidate" ForeColor="Red" runat="server" ControlToValidate="txtSubject" ErrorMessage="Email başlığı giriniz."></asp:RequiredFieldValidator>
           </td>
        </tr>
        <tr>
            <td>Kime:</td>
           <td><asp:TextBox ID="txtFrom" CssClass="text-input" TextMode="MultiLine" Height="70"  Width="600"  runat="server"></asp:TextBox>
           <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="loginValidate"  ForeColor="Red" runat="server" ControlToValidate="txtFrom" ErrorMessage="Email adresi giriniz."></asp:RequiredFieldValidator>
           <br />
                <span style="color:Green;">Email adreslerini virgül(,) ile ayırarak(aralarında boşluk bırakmadan) birden fazla email adresine gönderim yapabilirsiniz.</span>
           </td>
        </tr>
         <tr style="">
            <td>Resim</td>
            <td>
                <asp:TextBox ID="txtImage" Width="300" ClientIDMode="Static" CssClass="text-input" runat="server"></asp:TextBox>
                
                <input type="button" value="Resim Ekle" onclick="BrowseServerImage();" />
                <br />
                <span style="color:Green;">Resim genişliği 750px ve .jpg formatına e göre ayarlanmıştır.</span><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="loginValidate"  ForeColor="Red" runat="server" ControlToValidate="txtImage" ErrorMessage="Resim yükleyin."></asp:RequiredFieldValidator>
            </td>
        </tr>
        
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" ID="btnKaydet" runat="server" Text="Gönder" />
       <%-- <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>--%>
        </tr>
        </table>

    <div class="clear">
    
    </div>
    </div>
</div> 


</asp:Content>



