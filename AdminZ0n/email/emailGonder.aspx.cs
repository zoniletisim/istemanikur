﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminZ0n_email_emailGonder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        bool ret = false;
        try
        {
            
            string[] reslts = txtFrom.Text.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            foreach(var r in reslts)
                ret = Mailer.teaserMail(r.Trim(), txtSubject.Text, txtImage.Text);


            if(ret)
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> alert('Mail başarıyla gönderildi.')</script>");
            else
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), Guid.NewGuid().ToString(), "<script> alert('Hata Oluştu')</script>");
        
        }
        catch (Exception)
        {

            throw;
        }
    }
}