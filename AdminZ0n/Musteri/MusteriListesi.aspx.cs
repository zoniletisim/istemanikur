﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Text;
using System.IO;

public partial class AdminZ0n_Musteri_MusteriListesi : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();
    
    ResultAction result = new ResultAction();
    public List<Zon_Kullanici> users = new List<Zon_Kullanici>();
    public ZonDBEntities db = new ZonDBEntities();
    public List<Zon_Siparis> siparisList = new List<Zon_Siparis>();
    public List<View_Siparis> siparisDetayList = new List<View_Siparis>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["action"] == "update")
        {
            hdnID.Value = Request.QueryString["pid"].ToString();
            int id = int.Parse(hdnID.Value);
            siparisList = db.Zon_Siparis.Where(z => z.MusteriID == id).ToList();
        }
        if (!Page.IsPostBack)
        {

            FillForm();
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.GetUser(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                txtTarih.Text = sf.SonGiris.Value.ToShortDateString();
                drpStatu.SelectedValue = sf.Aktif.ToString();
                ddlSirket.SelectedValue = sf.SirketId.ToString();
                int id = int.Parse(hdnID.Value);
                fillOfis();
                ddlOfis.SelectedValue = sf.OfisId.ToString();
                txtAd.Text = sf.Ad;
                txtSoyad.Text= sf.Soyad;
                txtEmail.Text = sf.Email;
                txtTel.Text = sf.Telefon;
                txtKayitTarih.Text = sf.Tarih.Value.ToShortDateString();
                txtIp.Text = sf.IP;

                txtUnvan.Text = sf.Unvan;
                if(sf.DogumTarihi!=null)
                    txtDogumTarih.Text = sf.DogumTarihi.Value.ToShortDateString();
                txtOzelTarih.Text = sf.OzelTarih;
                txtCocuk.Text = sf.CocukDurum;
                txtNeredeManikur.Text = sf.NeredeManikur;
                txtRenkTercihi.Text = sf.RenkTercih;
                txtMarkaTercih.Text = sf.MarkaTercih;
                txtTirnakTeshis.Text = sf.TirnakTeshis;
                txtNot.Text = sf.Notlar;
                txtTckn.Text = sf.TcNo;
                ddlOnay.SelectedValue = sf.Onay.ToString();

            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }

        }
    }

    private void fillOfis()
    {
        int sirketId = int.Parse(ddlSirket.SelectedValue);
        ddlOfis.DataSource = db.Zon_Ofis.Where(z => z.SirketId == sirketId).ToList();
        ddlOfis.DataTextField = "OfisAd";
        ddlOfis.DataValueField = "Id";
        ddlOfis.DataBind();
    }

    private void FillForm()
    {
        TanimRepositories repTanim = new TanimRepositories();

        ddlSirket.DataSource = repTanim.GetSirketList();
        ddlSirket.DataTextField = "SiretAd";
        ddlSirket.DataValueField = "Id";
        ddlSirket.DataBind();

    }


    private void getDelete(int p)
    {
        var d = rep.deleteRowUser(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            List<string> items = new List<string>();


            result = rep.ActionMusteri(hdnID.Value, txtAd.Text,txtSoyad.Text, txtEmail.Text, txtTel.Text, ddlSirket.SelectedValue, ddlOfis.SelectedValue, txtUnvan.Text, txtDogumTarih.Text,txtOzelTarih.Text,txtCocuk.Text,txtNeredeManikur.Text,txtRenkTercihi.Text,txtMarkaTercih.Text,txtTirnakTeshis.Text,txtNot.Text,drpStatu.SelectedValue,mode,txtTckn.Text,ddlOnay.SelectedValue);
            if (result.result)
            {
                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

   

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
    protected void ddlSirket_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sirketId = int.Parse(ddlSirket.SelectedValue);
        ddlOfis.DataSource = db.Zon_Ofis.Where(z => z.SirketId == sirketId).ToList();
        ddlOfis.DataTextField = "OfisAd";
        ddlOfis.DataValueField = "Id";
        ddlOfis.DataBind();
    }


    protected void BtnExcel_Click(object sender, EventArgs e)
    {
        exportExcel();
    }

    public void exportExcel()
    {
        var grid = new System.Web.UI.WebControls.GridView();
       

        var users = db.Zon_Kullanici.OrderByDescending(z => z.Tarih).ToList();
        grid.DataSource = from u in users select new {u.Ad,u.Soyad,u.Email,u.Telefon,u.DogumTarihi,u.Cinsiyet,KayitTarihi=u.Tarih,u.SonGiris,IP=u.IP,Unvan=u.Unvan,KisiOZelTarih=u.OzelTarih,CocukDurum=u.CocukDurum,NeredeManikurYaptiriyor=u.NeredeManikur,RenkTercihi=u.RenkTercih,MarkaTercihi=u.MarkaTercih,TirnakTeshisi=u.TirnakTeshis,Notlar=u.Notlar,u.TcNo };
        grid.DataBind();

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=Müşteri_listesi_" + DateTime.Now + ".xls");
        Response.ContentType = "application/excel";
        Response.ContentEncoding = Encoding.Unicode;
        Response.BinaryWrite(Encoding.Unicode.GetPreamble());
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        grid.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

    }
}

