﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Text;
using System.IO;

public partial class AdminZ0n_Musteri_MusteriListesiServis : System.Web.UI.Page
{
    UserRepositories rep = new UserRepositories();

    ResultAction result = new ResultAction();
    public List<Zon_Kullanici> users = new List<Zon_Kullanici>();
    public ZonDBEntities db = new ZonDBEntities();
    public List<Zon_Siparis> siparisList = new List<Zon_Siparis>();
    public List<View_Siparis> siparisDetayList = new List<View_Siparis>();
    public View_Siparis siparis = new View_Siparis();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            FillForm();
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.GetUser(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                txtTarih.Text = sf.SonGiris.Value.ToShortDateString();
                drpStatu.SelectedValue = sf.Aktif.ToString();
                ddlSirket.SelectedValue = sf.SirketId.ToString();
                int id = int.Parse(hdnID.Value);
                fillOfis();
                ddlOfis.SelectedValue = sf.OfisId.ToString();
                txtAd.Text = sf.Ad + " " + sf.Soyad;
                txtEmail.Text = sf.Email;
                txtTel.Text = sf.Telefon;
                txtKayitTarih.Text = sf.Tarih.Value.ToShortDateString();
                txtIp.Text = sf.IP;

                siparisList = db.Zon_Siparis.Where(z => z.MusteriID == id).ToList();


            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }

        }
    }

    private void fillOfis()
    {
        var sirketId = int.Parse(ddlSirket.SelectedValue);
        ddlOfis.DataSource = db.Zon_Ofis.Where(z => z.SirketId == sirketId).ToList();
        ddlOfis.DataTextField = "OfisAd";
        ddlOfis.DataValueField = "Id";
        ddlOfis.DataBind();
    }

    private void FillForm()
    {
        TanimRepositories repTanim = new TanimRepositories();

        ddlSirket.DataSource = repTanim.GetSirketList();
        ddlSirket.DataTextField = "SiretAd";
        ddlSirket.DataValueField = "Id";
        ddlSirket.DataBind();

    }


    private void getDelete(int p)
    {
        var d = rep.deleteRowUser(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);

    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            //string mode = hdnAction.Value;
            //List<string> items = new List<string>();


            //result = rep.ActionMusteri(hdnID.Value, ddlSirket.SelectedValue, ddlOfis.SelectedValue, items, txtTarih.Text, hdnAction.Value, drpStatu.SelectedValue);
            //if (result.result)
            //{
            //    if (mode.Equals("add"))
            //        AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
            //    else
            //        AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

            //    pnlListe.Visible = true;
            //    pnlSayfaEkle.Visible = false;
            //    hdnAction.Value = "";
            //    hdnID.Value = "";
            //}
            //else
            //{
            //    AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            //}

            //AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }



    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
    protected void ddlSirket_SelectedIndexChanged(object sender, EventArgs e)
    {
        int sirketId = int.Parse(ddlSirket.SelectedValue);
        ddlOfis.DataSource = db.Zon_Ofis.Where(z => z.SirketId == sirketId).ToList();
        ddlOfis.DataTextField = "OfisAd";
        ddlOfis.DataValueField = "Id";
        ddlOfis.DataBind();
    }


    protected void BtnExcel_Click(object sender, EventArgs e)
    {
        exportExcel();
    }

    public void exportExcel()
    {
        var grid = new System.Web.UI.WebControls.GridView();

        var servis = new List<siparisServis>();
        var users = db.Zon_Kullanici.OrderByDescending(z => z.Tarih).ToList();
        foreach (var u in users)
        {
            if (u != null)
            {
                var manikurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 1).Count();
                var makyajCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 2).Count();
                var ChanelCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 3).Count();
                var OjeCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 4).Count();
                var KuruOjeCount =db.View_Siparis.Where(z=>z.userId==(int)u.ID && z.HizmetId==5).Count();
                var SmartCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 6).Count();
                var MavalaCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 7).Count();
                var MasculineCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 8).Count();
                var EcoKalyonCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 9).Count();
                var IsteManukurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 10).Count();
                var IstePedikurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 11).Count();
                var SmartLuxCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 12).Count();
                var IncocoCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 13).Count();
                var TenAprilCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 14).Count();
                var EcoEssenceCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 15).Count();
                ///burdan itibaren
                var EcoEssencePedikurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 16).Count();
                var EcoKalyonPedikurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 17).Count();
                var AvonHediyeManikurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 18).Count();
                var AvonRengarenkAktiviteCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 19).Count();
                var AvonUrunleriIleManikurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 20).Count();
                var AvonUrunleriIlePedikurCount = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 21).Count();
                var Classic = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 22).Count();
                var IsteManikurClassic = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 23).Count();
                var LuxClassic = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 24).Count();
                var IstePedikurClassic = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 25).Count();
                var Classic2 = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 26).Count();
                var LuxClassic2 = db.View_Siparis.Where(z => z.userId == (int)u.ID && z.HizmetId == 27).Count();

                var total = manikurCount + makyajCount + OjeCount + ChanelCount+KuruOjeCount+SmartCount+MavalaCount+
                    MasculineCount+EcoKalyonCount+IsteManukurCount+IstePedikurCount+SmartLuxCount+IncocoCount+TenAprilCount+EcoEssenceCount
                    + EcoEssencePedikurCount + EcoKalyonPedikurCount + AvonHediyeManikurCount + AvonRengarenkAktiviteCount + AvonUrunleriIleManikurCount + AvonUrunleriIlePedikurCount;

                var siparis = db.View_Siparis.Where(z => z.userId == u.ID).FirstOrDefault();
                if(siparis!=null)
                    servis.Add(new siparisServis { Ad = u.Ad, Soyad = u.Soyad, Email = u.Email, Telefon = u.Telefon, SirketAd = siparis.SiretAd, OfisAd = siparis.OfisAd, Smart_Mavala_Ile_Manukur = manikurCount, Essence_Ile_Makyaj_Deneyimi = makyajCount, Dudak_Ustu_Oje_Kas = OjeCount, Smart_Lux_Chanel_Ve_Mavala_Ile_Manikur = ChanelCount,Kuru_Oje_Bandi_Uygulamasi=KuruOjeCount,Smart_Mavala_Ile_Pedikur=SmartCount,Mavala_Oje=MavalaCount,Masculine_Erkekler_Icin_Manikur=MasculineCount,Eco_Kalyon_Ile_Manikur=EcoKalyonCount,Iste_Manikur=IsteManukurCount,Iste_Pedikur=IstePedikurCount,Smart_Lux_Chanel_Ve_Mavala_Ile_Pedikur=SmartCount,Incoco_Ile_Desenli_Tirnaklar=IncocoCount,Nisan_10_Essence_Makyaj_Deneyimi=TenAprilCount,Eco_Esscence_Ile_Manikur=EcoEssenceCount,
                       Eco_Esscence_Ile_Pedikur=EcoEssencePedikurCount,Eco_Esscence_Kalyon_Ile_Pedikur=EcoKalyonPedikurCount,Avon_Hediye_Manikur=AvonHediyeManikurCount,
                     Avon_Rengarenk_Aktivitesi=AvonRengarenkAktiviteCount,Avon_Urunleriyle_Manikur=AvonUrunleriIleManikurCount,Avon_Urunleriyle_Pedikur=AvonUrunleriIlePedikurCount,Etler_Manikur_Classic=Classic,IsteManikur_Classic=IsteManikurClassic,Lux_Classic_Manikur=LuxClassic,İstePedikur_Classic=IstePedikurClassic,Etler_Pedikur_Classic=Classic2,Lux_Classic_Pedikur=LuxClassic2  ,Toplam=total });
            }
        }

        grid.DataSource = servis;
        grid.DataBind();

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=Müşteri_Servis_" + DateTime.Now + ".xls");
        Response.ContentType = "application/excel";
        Response.ContentEncoding = Encoding.Unicode;
        Response.BinaryWrite(Encoding.Unicode.GetPreamble());
        var sw = new StringWriter();
        var htw = new HtmlTextWriter(sw);
        grid.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

    }

    public class siparisServis
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Email { get; set; }
        public string Telefon { get; set; }
        public string SirketAd { get; set; }
        public string OfisAd { get; set; }
        public int Smart_Mavala_Ile_Manukur { get; set; }
        public int Essence_Ile_Makyaj_Deneyimi { get; set; }
        public int Dudak_Ustu_Oje_Kas { get; set; }
        public int Smart_Lux_Chanel_Ve_Mavala_Ile_Manikur { get; set; }
        public int Kuru_Oje_Bandi_Uygulamasi { get; set; }
        public int Smart_Mavala_Ile_Pedikur { get; set; }
        public int Mavala_Oje { get; set; }
        public int Masculine_Erkekler_Icin_Manikur { get; set; }
        public int Eco_Kalyon_Ile_Manikur { get; set; }
        public int Iste_Manikur { get; set; }
        public int Iste_Pedikur { get; set; }
        public int Smart_Lux_Chanel_Ve_Mavala_Ile_Pedikur { get; set; }
        public int Incoco_Ile_Desenli_Tirnaklar { get; set; }
        public int Nisan_10_Essence_Makyaj_Deneyimi { get; set; }
        public int Eco_Esscence_Ile_Manikur { get; set; }

        public int Eco_Esscence_Ile_Pedikur { get; set; }
        public int Eco_Esscence_Kalyon_Ile_Pedikur { get; set; }
        public int Avon_Hediye_Manikur { get; set; }
        public int Avon_Rengarenk_Aktivitesi { get; set; }
        public int Avon_Urunleriyle_Manikur { get; set; }
        public int Avon_Urunleriyle_Pedikur { get; set; }
        public int Etler_Manikur_Classic { get; set; }
        public int IsteManikur_Classic { get; set; }
        public int Lux_Classic_Manikur { get; set; }
        public int İstePedikur_Classic { get; set; }
        public int Etler_Pedikur_Classic { get; set; }
        public int Lux_Classic_Pedikur { get; set; }
        public int Toplam { get; set; }
    
    }
}