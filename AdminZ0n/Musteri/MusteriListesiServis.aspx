﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="MusteriListesiServis.aspx.cs" Inherits="AdminZ0n_Musteri_MusteriListesiServis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#musteri").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	    });
	    $(function () {
	        $("#list").sortable({
	            handle: '.handle',
	            update: function () {
	                var order = $('#list').sortable('serialize');
	                $.post("../service.aspx", { action: "ListSirket", item: order }, function (msg) {
	                    var json = $.parseJSON(msg);
	                    if (json.result) {

	                    }
	                });
	            }
	        });
	    });
	    jQuery(function ($) {
	        $.datepicker.regional['tr'] = {
	            closeText: 'kapat',
	            prevText: '&#x3c;geri',
	            nextText: 'ileri&#x3e',
	            currentText: 'bugün',
	            monthNames: ['Ocak', '&#350;ubat', 'Mart', 'Nisan', 'May&#305;s', 'Haziran',
                'Temmuz', 'A&#287;ustos', 'Eylül', 'Ekim', 'Kas&#305;m', 'Aral&#305;k'],
	            monthNamesShort: ['Oca', '&#350;ub', 'Mar', 'Nis', 'May', 'Haz',
                'Tem', 'A&#287;u', 'Eyl', 'Eki', 'Kas', 'Ara'],
	            dayNames: ['Pazar', 'Pazartesi', 'Sal&#305;', 'Çar&#351;amba', 'Per&#351;embe', 'Cuma', 'Cumartesi'],
	            dayNamesShort: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
	            dayNamesMin: ['Pz', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct'],
	            weekHeader: 'Hf',
	            dateFormat: 'dd.mm.yy',
	            firstDay: 1,
	            isRTL: false,
	            showMonthAfterYear: false,
	            yearSuffix: ''
	        };
	        $.datepicker.setDefaults($.datepicker.regional['tr']);
	    });
	    $(document).ready(function () {
	        $("input.date").datepicker({ "dateFormat": "dd.mm.yy" });
	    });
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;">Kullanıcı Servis Raporu</h3>
        <ul class="content-box-tabs">
           <%-- <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> 
			<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>--%>
            <li><asp:Button CssClass="button" ID="BtnExcel" runat="server" Text="Excele Aktar" OnClick="BtnExcel_Click" /></li>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
 <%-- <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />--%>
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
 #main-content table td,
#main-content table th {
                padding: 7px;
                line-height: 1.3em;
                font-size:11px;
                } 
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Ad-Soyad</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Email</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Tel</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Şirket</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Ofis</th>
                <th style="color:#000;text-align:center;cursor:pointer;">SMART (MAVALA ile manikür)</th>
                <th style="color:#000;text-align:center;cursor:pointer;">ESSENCE ile Makyaj Deneyimi</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Kaş, dudak üstü, oje</th>
                <th style="color:#000;text-align:center;cursor:pointer;">SMART LUX (CHANEL & MAVALA ile manikür)</th>
		    </tr>
	    </thead>
        <tbody>
            <%
              ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
              users = db.Zon_Kullanici.Where(z => z.Aktif!="3").ToList();
              
              foreach (var item in users)
                {
               
                siparis = db.View_Siparis.Where(z => z.MusteriID == item.ID).FirstOrDefault();
                  %>
             <tr>
                <td><%=item.Ad%> <%=item.Soyad %></td>
                <td><%=item.Email %></td>
                <td><%=item.Telefon %></td>
                <td>
                <%
                if (item.SirketId != null)
                {
                    var sirket = db.Zon_Sirket.Where(z => z.Id == item.SirketId).FirstOrDefault();
                    if (sirket != null)
                    {
                         %>
                        <%=sirket.SiretAd%>
                <%}
                } %>
                </td>
                <td>
                <%if (item.OfisId != null)
                  {
                      var ofis = db.Zon_Ofis.Where(z => z.Id == item.OfisId).FirstOrDefault();
                      if (ofis != null)
                      { %>
                  <%=ofis.OfisAd%>
                <%}
                  }%>
                </td>
                <%int manikurCount  = db.View_Siparis.Where(z => z.MusteriID == (int)item.ID && z.HizmetId==1).Count();
                  int makyajCount = db.View_Siparis.Where(z => z.MusteriID == (int)item.ID && z.HizmetId == 2).Count();
                  int OjeCount = db.View_Siparis.Where(z => z.MusteriID == (int)item.ID && z.HizmetId == 4).Count();
                  int ChanelCount = db.View_Siparis.Where(z => z.MusteriID == (int)item.ID && z.HizmetId == 3).Count(); 
                  %>
                <td><%=manikurCount %></td>
                <td><%=makyajCount%></td>
                <td><%=OjeCount%></td>
                <td><%=ChanelCount%></td>
                               
                </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>

        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">
  <style>
  #main-content table td,
#main-content table th {
                padding: 3px;
                line-height: 1.3em;
                } 
  </style>
  
        <table width="50%;" style="width:40%;float:left;">
          <tr>
            <td>Ad-Soyad</td>
           <td>
             <asp:TextBox ID="txtAd" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
         <tr>
            <td>Email</td>
           <td>
             <asp:TextBox ID="txtEmail" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
           <tr>
            <td>Telefon</td>
           <td>
             <asp:TextBox ID="txtTel" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Şirket</td>
           <td>
               <asp:DropDownList ID="ddlSirket" AutoPostBack="true"  class="multiSelect"  
                   Width="200" runat="server" 
                   onselectedindexchanged="ddlSirket_SelectedIndexChanged">
                </asp:DropDownList>
           </td>
           </tr>
           <tr>
            <td>Ofis</td>
           <td>
               <asp:DropDownList ID="ddlOfis"  class="multiSelect"  Width="200" runat="server">
                </asp:DropDownList>
           </td>
        </tr>
        <tr>
            <td>Son Giriş Tarih</td>
           <td>
             <asp:TextBox ID="txtTarih" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Kayıt Tarih</td>
           <td>
             <asp:TextBox ID="txtKayitTarih" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
          <tr>
            <td>IP</td>
           <td>
             <asp:TextBox ID="txtIp" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="2">Yay&#305;nda De&#287;il</asp:ListItem>
                    <asp:ListItem Value="3">Silinebilir</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>

        </table>
        <div style="width:650px;float:left;">
       <h3> Müşterinin vermiş olduğu siparişler</h3>
   <div id="accordion">
   <%foreach (var s in siparisList.OrderByDescending(z => z.EklenmeTarihi))
     {
         siparisDetayList = db.View_Siparis.Where(z => z.SiparisId == s.ID).ToList();
         %>
  <h3>Randevu No: #<%=s.SiparisNO %> </h3>
  <div>
    <table>
    <tr style="border-bottom:1px solid #333;font-weight:bold">
    <th><b> Hizmet Türü</b></th>
    <th><b>Tarih</b></th>
    <th><b>Saat</b></th>
    <th><b>Fiyat</b></th>
    <th><b>İşlem Tarihi</b></th>
    </tr>
    <%foreach (var sd in siparisDetayList)
      { %>
    <tr>
    <td><%=sd.HizmetAd %> </td>
    <td><%=string.Format("{0:d MMMM yyyy dddd}", sd.Tarih)%></td>
    <td><%=string.Format("{0:t}", sd.BaslangicSaat)%> - <%=string.Format("{0:t}", sd.BitisSaat)%> </td>
    <td><%=sd.ToplamTutar%> TL </td>
     <td><%=sd.EklenmeTarihi.Value.ToShortDateString()%> </td>
    </tr>
    <%} %>
    </table>
  </div>
  <%} %>
</div>
</div>

        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
    <div class="clear">
    
    </div>
    </div>
</div> 

<script>
    $(function () {
        $("#accordion").accordion();
    });
    //    $(document).ready(function () {
    //        var oTable = $('#table').dataTable();

    //        oTable.fnSort([[7, 'desc']]);
    //    });

  </script>
</asp:Content>



