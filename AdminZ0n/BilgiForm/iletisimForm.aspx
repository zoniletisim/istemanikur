﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="iletisimForm.aspx.cs" Inherits="AdminZ0n_Kesif_iletisimForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script type="text/javascript">
     $(function () {
         $("#formBil").addClass("current");
         $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
     });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
    <div class="content-box-header">
        <h3 style="cursor: s-resize;"> İletişim Formu</asp:Label></h3>
         <ul class="content-box-tabs">
		<li> <asp:Button CssClass="button" ID="btnExcel" runat="server" 
                Text="Excel Ç&#305;kt&#305;s&#305; Al" onclick="btnExcel_Click"  /></li> 
		</ul>
    </div>
    <div class="content-box-content">
    
 <asp:HiddenField ID="hdnAction" runat="server" />
            <asp:HiddenField ID="hdnID" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
 <%-- <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />--%>
     <br /><br />
     <table id="table"  >
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                
                <th style="color:#000;text-align:center;cursor:pointer;">Ad</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Soyad</th>
		        <th style="color:#000;text-align:center;cursor:pointer;">Email</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Telefon</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Konu</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Mesaj</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Basvuru Tarih</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Durum <div id="ajaxloading" style="display:none; position:absolute;"><img src="/adminz0n/resources/images/ajax-loader.gif" /></div>  </th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
            var p = (from item in db.Zon_Iletisim
                    select item).ToList();
            foreach (var item in p)
            {%>
             <tr>
                <td ><%=item.Ad%></td>
                <td ><%=item.Soyad%></td>
                <td ><%=item.Email %></td>
                <td ><%=item.Tel %></td>
                <td >
                
                <%string temp="";
                  if (item.Konu == "1")
                      temp = "Bayi İstek";
                  else if (item.Konu == "2")
                      temp = "Teknik Destek";
                  else if (item.Konu == "3")
                      temp = "Şikayet";
                  else if (item.Konu == "4")
                      temp = "Diğer";
                        %>
                <%=temp %>
                </td>
                <td ><%=item.Mesaj %></td>
                <td width="60"  style="width: 60px;"><%=item.Tarih.Value.ToShortDateString() %></td>
                <td>
                                  
                <%if ((bool)item.Okundu)
                    {%>
                    <input type="checkbox" id="chkTrue"  onclick="CallIsVisible(<%=item.Id%>,'iletisim');" checked="checked"   />Okundu
                    <%} %>
                                  
                    <%else
                    { %>
                    <input type="checkbox" id="chkTrue" onclick="CallIsVisible(<%=item.Id%>,'iletisim');" />Okundu
                    
                    <%} %>
                    </td>
                               
                    <td style="width: 20px;">
                   <%--<a href="SayfaYonetimi.aspx?action=update&pid=<%=item.Id%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> --%>

</td>
                <td style="width: 20px;">
               <%-- <a href="iletisim.aspx?action=delete&pid=<%=item.Id%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
			      
       
		<th><input type="text" name="search_name" value="Ad" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_name" value="Soyad" class="search_init" style="width:120px" /></th>
		<th><input type="text" name="search_detail" value="Email" class="search_init" style="width:120px" /></th>
		<th><input type="text"  name="search_detail" value="Telefon" class="search_init" style="width:80px" /></th>
        <th><input type="text"  name="search_detail" value="Konu" class="search_init" style="width:120px" /></th>
        <th><input type="text"  name="search_detail" value="Mesaj" class="search_init" style="width:120px" /></th>
        <th><input type="text"  name="search_detail" value="Baş. Tarih" class="search_init" style="width:60px" /></th>
        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
   
    
    </asp:Panel>
    </div>
</div>
<script type="text/javascript">
    function Delete() {
        var x = confirm("Kay&#305;t silinecek.\nEmin misiniz?");
        if (x) {
            var action = $('#deleteRow').attr('href');
            window.location.href = action;
        }
        else
            return false;
    }
</script>
</asp:Content>



