﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using ZonDBModel;
public partial class AdminZ0n_Kesif_iletisimForm : BasePage
{
    ZonDBEntities db = new ZonDBEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["action"] != null)
            {
                if (Request.QueryString["action"] == "delete")
                {
                    if (Request.QueryString["pid"] != null)
                        getDelete(int.Parse(Request.QueryString["pid"].ToString()));
                }
            }
        }
    }

    private void getDelete(int p)
    {
        //var d = rep.deleteRowIletisim(p);
        //if (d.result)
        //    AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        //else
        //    AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);
    }

    public void exportExcel()
    {
        var grid = new System.Web.UI.WebControls.GridView();
        grid.DataSource = db.Zon_Iletisim.ToList();
        grid.DataBind();

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=IletisimFormListesi_" + DateTime.Now + ".xls");
        Response.ContentType = "application/excel";
        Response.ContentEncoding = Encoding.Unicode;
        Response.BinaryWrite(Encoding.Unicode.GetPreamble());
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        grid.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        exportExcel();
    }
}