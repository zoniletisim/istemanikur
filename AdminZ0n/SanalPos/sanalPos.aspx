﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="sanalPos.aspx.cs" Inherits="AdminZ0n_SanalPos_sanalPos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">
    $(function () {
        $("#pos").addClass("current");
        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
    });
        </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;">Müşteri Bilgileri</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<%--<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> --%>
			<%--<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>--%>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
 <%-- <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />--%>
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
  #main-content table td,
#main-content table th {
                padding: 7px;
                line-height: 1.3em;
                font-size:11px;
                } 
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Ad-Soyad</th>
                 <th style="color:#000;text-align:center;cursor:pointer;">Email</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Tel</th>
                <th style="color:#000;text-align:center;cursor:pointer;">İşlem Ta.</th>
                  <th style="color:#000;text-align:center;cursor:pointer;">Tutar</th>
                   <th style="color:#000;text-align:center;cursor:pointer;">Trans ID</th>
                   <th style="color:#000;text-align:center;cursor:pointer;">Kart Üz. İsim</th>
                   <th style="color:#000;text-align:center;cursor:pointer;">Durum</th>
                   <th style="color:#000;text-align:center;cursor:pointer;">Msj</th>
		    </tr>
	    </thead>
        <tbody>
            <%
                var posList= (from p in db.cctrans
                       join k in db.Zon_Kullanici
                       on p.userRef equals k.ID
                       select new
                       {
                           p,
                           k
                       }).ToList();
                foreach (var item in posList)
            {%>
             <tr>
                <td><%=item.k.Ad%> <%=item.k.Soyad%></td>
                <td><%=item.k.Email%></td>
                <td><%=item.k.Telefon%></td>
                <td><%=item.p.date%></td>
                <td><%=item.p.anapara%> TL</td>
                <td><%=item.p.transid%></td>
                <td><%=item.p.cardholder%></td>
                <td > <%if (item.p.response == "Approved")
                        {%><span style="color:Green"> Başarılı</span> <%}
                        else
                        { %><span style="color:Red">Başarısız</span> <%} %></td>
                <td ><%=item.p.mderrmsg %></td>
                
              
                               
                  <%--  <td style="width: 20px;">
                   <a href="musteriListesi.aspx?action=update&pid=<%=item.ID%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

                    </td>
                <td style="width: 20px;">
                <a href="musteriListesi.aspx?action=delete&pid=<%=item.ID%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
            --%></tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_detail" value="" style="display:none;" class="search_init" style="width:120px" /></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
 <%-- <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">
  <style>
  #main-content table td,
#main-content table th {
                padding: 3px;
                line-height: 1.3em;
                } 
  </style>
  
        <table width="50%;" style="width:40%;float:left;">
          <tr>
            <td>Ad-Soyad</td>
           <td>
             <asp:TextBox ID="txtAd" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
         <tr>
            <td>Email</td>
           <td>
             <asp:TextBox ID="txtEmail" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
           <tr>
            <td>Telefon</td>
           <td>
             <asp:TextBox ID="txtTel" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Şirket</td>
           <td>
               <asp:DropDownList ID="ddlSirket" AutoPostBack="true"  class="multiSelect"  
                   Width="200" runat="server" 
                   onselectedindexchanged="ddlSirket_SelectedIndexChanged">
                </asp:DropDownList>
           </td>
           </tr>
           <tr>
            <td>Ofis</td>
           <td>
               <asp:DropDownList ID="ddlOfis"  class="multiSelect"  Width="200" runat="server">
                </asp:DropDownList>
           </td>
        </tr>
        <tr>
            <td>Son Giriş Tarih</td>
           <td>
             <asp:TextBox ID="txtTarih" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Kayıt Tarih</td>
           <td>
             <asp:TextBox ID="txtKayitTarih" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
          <tr>
            <td>IP</td>
           <td>
             <asp:TextBox ID="txtIp" CssClass="text-input"   Width="200" runat="server"></asp:TextBox>
           </td>
        </tr>
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="2">Yay&#305;nda De&#287;il</asp:ListItem>
                    <asp:ListItem Value="3">Silinebilir</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>

        </table>
        <div style="width:650px;float:left;">
       <h3> Müşterinin vermiş olduğu siparişler</h3>
   <div id="accordion">
   <%foreach (var s in siparisList.OrderByDescending(z => z.EklenmeTarihi))
     {
         siparisDetayList = db.View_Siparis.Where(z => z.SiparisId == s.ID).ToList();
         %>
  <h3>Randevu No: #<%=s.SiparisNO %> </h3>
  <div>
    <table>
    <tr style="border-bottom:1px solid #333;font-weight:bold">
    <th><b> Hizmet Türü</b></th>
    <th><b>Tarih</b></th>
    <th><b>Saat</b></th>
    <th><b>Fiyat</b></th>
    <th><b>İşlem Tarihi</b></th>
    </tr>
    <%foreach (var sd in siparisDetayList)
      { %>
    <tr>
    <td><%=sd.HizmetAd %> </td>
    <td><%=string.Format("{0:d MMMM yyyy dddd}", sd.Tarih)%></td>
    <td><%=string.Format("{0:t}", sd.BaslangicSaat)%> - <%=string.Format("{0:t}", sd.BitisSaat)%> </td>
    <td><%=sd.ToplamTutar%> TL </td>
     <td><%=sd.EklenmeTarihi.Value.ToShortDateString()%> </td>
    </tr>
    <%} %>
    </table>
  </div>
  <%} %>
</div>
</div>

        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>--%>
    <div class="clear">
    
    </div>
    </div>
</div>
</asp:Content>



