﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="rezervasyon.aspx.cs" Inherits="AdminZ0n_Rezervasyon_rezervasyon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#Rezervasyon").addClass("current");
            $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
        });
        $(function () {
            $("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
            $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
        });
    </script>
    <style>
        .ui-tabs-vertical {
            width: 100%;
        }

            .ui-tabs-vertical .ui-tabs-nav {
                padding: .2em .1em .2em .2em !important;
                float: left;
                width: 182px;
            }

                .ui-tabs-vertical .ui-tabs-nav li {
                    clear: left;
                    width: 100%;
                    border-bottom-width: 1px !important;
                    border-right-width: 0 !important;
                    margin: 0 -1px .2em 0;
                }

                    .ui-tabs-vertical .ui-tabs-nav li a {
                        display: block;
                    }

                    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active {
                        padding-bottom: 0;
                        padding-right: .1em;
                        border-right-width: 1px;
                        border-right-width: 1px;
                    }

            .ui-tabs-vertical .ui-tabs-panel {
                padding: 1em;
                float: right;
                width: 80%;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="Server">
    <div class="content-box">
        <div class="content-box-header">
            <h3 style="cursor: s-resize;">Rezervasyon Takibi</h3>
            <ul class="content-box-tabs">
                <li>
                    <asp:Button OnClientClick="window.document.forms[0].target='_blank'" CssClass="button" ID="Button3" runat="server" Text="Fatura Yazd&#305;r" OnClick="Button3_Click" /></li>
                <li>
                    <asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
                <%--<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> --%>
                <li>
                    <asp:Button OnClientClick="window.document.forms[0].target='_blank'" CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>
            </ul>
        </div>
        <div class="content-box-content">
            <asp:HiddenField ID="hdnAction" runat="server" />
            <asp:HiddenField ID="hdnID" runat="server" />
            <asp:HiddenField ID="hdnSayfaId" runat="server" />
            <asp:Panel ID="pnlListe" runat="server">
                <%-- <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />--%>
                <br />
                <br />
                <style>
                    #main-content tbody tr.alt-row {
                        background: #f3f3f3;
                    }
                </style>
                <table id="table2">
                    <thead>
                        <tr style="background: #D0E799; height: 25px; color: #000;">
                            <th style="color: #000; text-align: center; cursor: pointer;">Rezervasyon No</th>
                            <th style="color: #000; text-align: center; cursor: pointer;">Ad-Soyad</th>
                            <th style="color: #000; text-align: center; cursor: pointer;">Tel</th>
                            <%--              <th style="color:#000;text-align:center;cursor:pointer;">&#350;irket</th>--%>
                            <th style="color: #000; text-align: center; cursor: pointer;">Toplam Tutar</th>
                            <th style="color: #000; text-align: center; cursor: pointer;">Kay&#305;t Tarihi</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        <%
                            ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
                            siparis = db.Zon_Siparis.ToList();
                            foreach (var item in siparis)
                            {%>
                        <tr>
                            <td><%=item.SiparisNO %></td>
                            <td><%=item.Zon_Kullanici.Ad%> <%=item.Zon_Kullanici.Soyad%></td>
                            <td><%=item.Zon_Kullanici.Telefon %></td>
                            <%--  <td ><%=item.Zon_Kullanici.Zon_Ofis.Zon_Sirket.SiretAd%></td>--%>
                            <td><%=item.ToplamTutar%> TL</td>
                            <td><%=item.EklenmeTarihi.Value.ToShortDateString()%></td>

                            <td style="width: 20px;">
                                <a href="rezervasyon.aspx?action=update&pid=<%=item.ID%>">
                                    <img id="imog" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/pencil.png" /></a>
                            </td>
                            <%--<td style="width: 20px;">
                <a href="musteriListesi.aspx?action=delete&pid=<%=item.ID%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a>
                 </td>--%>
                        </tr>
                        <%} %>
                    </tbody>
                    <tfoot>
                        <tr>
                            <%--<th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>--%>

                            <th></th>

                        </tr>
                    </tfoot>
                </table>
                <br />
                <br />

            </asp:Panel>
            <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">
                <style>
                    #main-content tbody tr.alt-row {
                        background: #F3F3F3;
                    }

                    #main-content ul, #main-content ol {
                        padding: 0px 0;
                    }
                </style>

                <div id="tabs">
                    <ul>
                        <li><a href="#tab-order" style="">Rezervasyon Bilgileri</a></li>
                        <li><a href="#tab-product" style="" class="">Rezervasyon Detay&#305;</a></li>
                        <li><a href="#tab-payment" style="" class="">Ödeme(Fatura) Bilgileri</a></li>
                        <%--<a href="#tab-shipping" style="" class="">Kargo Detay&#305;</a>--%>

                        <%--<a href="#tab-history" style="" class="">Sipari&#351; Geçmi&#351;i</a>--%>
                    </ul>
                    <div id="tab-order">
                        <table class="form">
                            <tbody>
                                <tr>
                                    <td style="width: 180px">Randevu NO:</td>
                                    <td>#<asp:Label ID="lblSiparisNO" runat="server" Text="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Mü&#351;teri:</td>
                                    <td>
                                        <asp:Label ID="lblMusteri" runat="server" Text="Label"></asp:Label></td>
                                </tr>

                                <tr>
                                    <td>E-Mail:</td>
                                    <td>
                                        <asp:Label ID="lblMusteriEmail" runat="server" Text="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Telefon:</td>
                                    <td>
                                        <asp:Label ID="lblMusteriTelefon" runat="server" Text="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Toplam Sipari&#351; Tutar&#305;:</td>
                                    <td>
                                        <asp:Label ID="lblToplamTutar" runat="server" Text="Label"></asp:Label></td>
                                </tr>
                                <%-- <tr>
           <td>Sipari&#351; Durumu:</td>
            <td id="order-status">
                <asp:Label ID="lblSiparisDurumu" runat="server" Text="Label"></asp:Label>
            </td>
          </tr>--%>
                                <tr>
                                    <td>IP Adresi:</td>
                                    <td>
                                        <asp:Label ID="lblIPAdres" runat="server" Text="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Sipari&#351; Tarih:</td>
                                    <td>
                                        <asp:Label ID="lblEklenmeTarihi" runat="server" Text="Label"></asp:Label></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div id="tab-product">
                        <table class="list table">
                            <thead>
                                <tr>
                                    <th class="right">Servis</th>
                                    <th class="left">&#350;irket</th>
                                    <th class="left">Ofis</th>
                                    <th class="right">Randevu Tarih</th>
                                    <th class="right">Ba&#351;. Saati</th>
                                    <th class="right">Biti&#351; Saati</th>
                                    <th class="right">Fiyat</th>

                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    decimal toplam = 0;
                                    foreach (var s in siparisDetay)
                                    { %>
                                <tr>
                                    <td class="right"><%=s.HizmetAd%></td>
                                    <td class="left"><%=s.SiretAd%></td>
                                    <td class="right"><%=s.OfisAd %></td>
                                    <td class="right"><%=string.Format("{0:d MMMM yyyy dddd}" ,s.Tarih)%></td>
                                    <td class="right"><%=string.Format("{0:t}", s.BaslangicSaat)%></td>
                                    <td class="right"><%=string.Format("{0:t}",s.BitisSaat)%></td>
                                    <td class="right">
                                        <%if (s.IndirimliFiyat != null)
                                          {
                                              toplam += s.IndirimliFiyat.Value;%>
                                        <%=s.IndirimliFiyat %> TL        
                <%}
                                          else
                                          {
                                              toplam += s.BirimFiyat.Value; %>
                                        <%=s.BirimFiyat %> TL         
                <%} %>
                                    </td>
                                </tr>
                                <%
                } %>
                            </tbody>



                            <tbody id="Tbody2">
                                <tr>
                                    <td colspan="6" class="right">Toplam Tutar:</td>
                                    <td class="right">
                                        <%=toplam %> TL</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="tab-payment">
                        <table class="form">
                            <tbody>
                                <tr>
                                    <td>Fatura ismi:</td>
                                    <td>
                                        <asp:Label ID="lblFaturaAd" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Adres:</td>
                                    <td>
                                        <asp:Label ID="lblFaturaAdres" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&#350;ehir:</td>
                                    <td>
                                        <asp:Label ID="lblFaturaSehir" runat="server" Text="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>&#304;lçe:</td>
                                    <td>
                                        <asp:Label ID="lblFaturaIlce" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>PostaKodu:</td>
                                    <td>
                                        <asp:Label ID="lblPostaKodu" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ödeme &#350;ekli:</td>
                                    <td>
                                        <asp:Label ID="lblOdemeSekli" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <table width="100%">
                    <tr align="center" style="text-align: center">
                        <td style="text-align: center" align="center">
                            <%-- <asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />--%>
                            <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
                    </tr>
                </table>

            </asp:Panel>
            <div class="clear">
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(".vtabs a").tabs();

        $.fn.dataTableExt.oSort['uk_date-asc'] = function (a, b) {
            var ukDatea = a.split('.');
            var ukDateb = b.split('.');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        };

        $.fn.dataTableExt.oSort['uk_date-desc'] = function (a, b) {
            var ukDatea = a.split('.');
            var ukDateb = b.split('.');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
        }; var asInitVals = new Array();
        $(document).ready(function () {

            var oTable2 = $('#table2').dataTable(
         {
             "sPaginationType": "full_numbers",
             "sRowSelect": "single",
             "bProcessing": true,
             "bFilter": true,
             "aoColumnDefs": [{ "aTargets": [4], "sType": "uk_date" }],
             "aaSorting": [[4, "desc"]]
         });


        });
    </script>
</asp:Content>




