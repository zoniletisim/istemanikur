﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
using System.Text;
using System.IO;
public partial class AdminZ0n_Rezervasyon_rezervasyonDetay : System.Web.UI.Page
{
    public List<View_Siparis> siparis = new List<View_Siparis>();
    ZonDBEntities db = new ZonDBEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void btnSirala_Click(object sender, EventArgs e)
    {
        exportExcel();
    }

    public void exportExcel()
    {
        var grid = new System.Web.UI.WebControls.GridView();


        var users = db.View_Siparis.OrderByDescending(z => z.EklenmeTarihi).ToList();
        grid.DataSource = from u in users select new { RandevuNo = u.SiparisNO, u.Ad, u.Soyad, u.Email, u.Telefon, Servis = u.HizmetAd, u.SiretAd, u.OfisAd, RandevuTarih = u.Tarih, RandevuBasSaat = string.Format("{0:t}", u.BaslangicSaat), RandevuBitSaat = string.Format("{0:t}", u.BitisSaat), Fiyat = u.BirimFiyat.Value.ToString("#0,0.00") + " TL", IndirimYuzdesi = u.IndirimMiktar, IndirimliFiyat = u.IndirimliFiyat, KartSahibi = u.KartSahibi, KayitTarihi = u.EklenmeTarihi };
        grid.DataBind();

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=Randevu_listesi_" + DateTime.Now + ".xls");
        Response.ContentType = "application/excel";
        Response.ContentEncoding = Encoding.Unicode;
        Response.BinaryWrite(Encoding.Unicode.GetPreamble());
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        grid.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

    }
}