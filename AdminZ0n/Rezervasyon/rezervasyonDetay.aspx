﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="rezervasyonDetay.aspx.cs" Inherits="AdminZ0n_Rezervasyon_rezervasyonDetay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#Rezervasyon").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	    });
	  
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">

<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;">Rezervasyon Takibi</h3>
        <ul class="content-box-tabs">
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="Excele Aktar" OnClick="btnSirala_Click" /></li> 
			<%--<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>--%>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
 <%-- <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />--%>
  <br /><br />
  <style>
     #main-content tbody tr.alt-row {
background: #f3f3f3;
}
 #main-content table td,
#main-content table th {
                padding: 7px;
                line-height: 1.3em;
                font-size:11px;
                } 
</style>
  <table id="table2">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Ran.No</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Ad-Soyad</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Servis</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Şirket</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Ofis</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Ran. Tarih</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Baş.Saati</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Bit.Saati</th>
                <th style="color:#000;text-align:center;cursor:pointer;width:50px">Fiyat</th>
                <%--<th></th>    --%>
                           
		    </tr>
	    </thead>
        <tbody>
            <%
              ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
              siparis = db.View_Siparis.ToList();
              foreach (var s in siparis)
            {%>
             <tr>
             <td><%=s.SiparisNO %></td>
                <td><%=s.Ad %> <%=s.Soyad %></td>
                <td><%=s.HizmetAd %></td>
                <td><%=s.SiretAd%></td>
                <td ><%=s.OfisAd%></td>
                <td ><%=string.Format("{0:d}", s.Tarih)%> </td>
                <td >
                <%=string.Format("{0:t}", s.BaslangicSaat)%></td>
                <td>
                <%=string.Format("{0:t}",s.BitisSaat)%>
                </td>
                <td>
                <%if (s.IndirimliFiyat != null)
                  { %>
                  <%=s.IndirimliFiyat %> TL        
                <%}else{ %>
                <%=s.BirimFiyat %> TL         
                <%} %>
                </td> 
                <%--<td style="width: 20px;">
                <a href="musteriListesi.aspx?action=delete&pid=<%=item.ID%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a>
                 </td>--%>

            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
		<%--<th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
        <th><input type="text" name="search_detail" value="" style="" class="search_init" style="width:100px" /></th>
     --%>
        <th></th>
                 
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  
    <div class="clear">
    
    </div>
    </div>
</div>
<script type="text/javascript">
    $.fn.dataTableExt.oSort['uk_date-asc'] = function (a, b) {
        var ukDatea = a.split('.');
        var ukDateb = b.split('.');

        var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    };

    $.fn.dataTableExt.oSort['uk_date-desc'] = function (a, b) {
        var ukDatea = a.split('.');
        var ukDateb = b.split('.');

        var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    };  var asInitVals = new Array();
    $(document).ready(function () {
      
       var oTable2=$('#table2').dataTable(
         {
           "sPaginationType": "full_numbers", 
            "sRowSelect": "single",
            "bProcessing": true,
            "bFilter": true,
            "aoColumnDefs" : [{"aTargets" : [5] , "sType" : "uk_date"}],
            "aaSorting": [[ 5, "desc" ]]
         });
       
         
    });
</script>
</asp:Content>




