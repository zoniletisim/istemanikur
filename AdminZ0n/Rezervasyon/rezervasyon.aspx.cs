﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;

public partial class AdminZ0n_Rezervasyon_rezervasyon : System.Web.UI.Page
{
    SiparisClass rep = new SiparisClass();
    ResultAction result = new ResultAction();
    public List<Zon_Siparis> siparis = new List<Zon_Siparis>();
    public List<View_Siparis> siparisDetay = new List<View_Siparis>();
    ZonDBEntities db = new ZonDBEntities();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnID.Value = Request.QueryString["pid"].ToString();
                var sf = rep.GetSiparis(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                lblSiparisNO.Text = sf.SiparisNO;
                lblMusteri.Text = sf.Zon_Kullanici.Ad + " " + sf.Zon_Kullanici.Soyad;
                lblMusteriEmail.Text = sf.Zon_Kullanici.Email;
                lblMusteriTelefon.Text = sf.Zon_Kullanici.Telefon;
                lblToplamTutar.Text = sf.ToplamTutar.Value.ToString() + "TL";
                lblIPAdres.Text = sf.IP;
                lblEklenmeTarihi.Text = sf.EklenmeTarihi.Value.ToString();
                string strSQL = "select * from View_Siparis where SiparisId=" + Request.QueryString["pid"].ToString();
                siparisDetay = db.ExecuteStoreQuery<View_Siparis>(strSQL).ToList();


                //var hizmet = (from hr in db.Zon_Randevu_Hizmet
                //              join h in db.Zon_HizmetTur on hr.HizmetId equals h.Id
                //              where hr.RandevuId == id
                //              select new { h.Id, h.HizmetAd }).ToList();


            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }

        }
    }




    private void getDelete(int p)
    {
        var d = rep.deleteRowRandevu(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            //    string mode = hdnAction.Value;
            //    result = rep.ActionRandevu(hdnID.Value, ddlSirket.SelectedValue, ddlOfis.SelectedValue, items, txtTarih.Text, hdnAction.Value, drpStatu.SelectedValue);
            //    if (result.result)
            //    {
            //        if (mode.Equals("add"))
            //            AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
            //        else
            //            AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

            //        pnlListe.Visible = true;
            //        pnlSayfaEkle.Visible = false;
            //        hdnAction.Value = "";
            //        hdnID.Value = "";
            //    }
            //    else
            //    {
            //        AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            //    }

            //    AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("fatura.aspx?Id=" + Request.QueryString["pid"]);
    }
}