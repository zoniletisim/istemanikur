﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZonDBModel;
public partial class AdminZ0n_Banner_Banner : BasePage
{
    BannerRepositories rep = new BannerRepositories();
    ResultAction result = new ResultAction();
    public List<Zon_Banner> banner = new List<Zon_Banner>();
    public Zon_Kullanici user = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
            user = (Zon_Kullanici)Session["user"];
        else
            Response.Redirect("/adminz0n/login.aspx");

        if (!Page.IsPostBack)
        {
            //getLanguageFill();
            //getLanguageFilterFill();
            if (Request.QueryString["action"] == "update")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "update";
                hdnSayfaId.Value = Request.QueryString["sayfaid"].ToString();
                hdnID.Value = Request.QueryString["pid"].ToString();
                //var sf = rep.GetUrunKategori(Convert.ToInt32(Request.QueryString["pid"].ToString()));
                var sf = rep.GetBanner(Convert.ToInt32(Request.QueryString["sayfaid"].ToString()), int.Parse(Request.QueryString["l"]));
                txtBaslik.Text = sf.Baslik;
                ddlDil.SelectedValue = sf.DilId.ToString();
                //ddlKategori.SelectedValue = sf.ParentId.ToString();
                drpStatu.SelectedValue = sf.Aktif.ToString();
                //txtResim.Text = sf.Resim;
                ckIcerik.Text = sf.Icerik;
                imgBanner.ImageUrl = sf.ResimTh;
                if (Request.QueryString["l"] != "1")
                {
                    Zon_Banner tricerik = rep.getTrBaslik(int.Parse(Request.QueryString["sayfaid"].ToString()));
                    lblTrBaslik.Text = tricerik.Baslik;
                }

            }
            else if (Request.QueryString["action"] == "delete")
            {
                if (Request.QueryString["pid"] != null)
                    getDelete(int.Parse(Request.QueryString["pid"].ToString()));
            }
            else if (Request.QueryString["action"] == "addL" && Request.QueryString["l"] != "0")
            {
                pnlListe.Visible = false;
                pnlSayfaEkle.Visible = true;
                hdnAction.Value = "add";
                hdnID.Value = Request.QueryString["pid"];
                hdnSayfaId.Value = Request.QueryString["sayfaid"];
                //ddlKategori.SelectedValue = rep.GetUrunKategori(Convert.ToInt32(Request.QueryString["sid"].ToString()));
                ddlDil.SelectedValue = Request.QueryString["l"].ToString();
                Zon_Banner tricerik = rep.getTrBaslik(int.Parse(Request.QueryString["sayfaid"].ToString()));
                lblTrBaslik.Text = tricerik.Baslik;
            }
        }
    }

   
    private void getDelete(int p)
    {
        var d = rep.deleteRowBanner(p);
        if (d.result)
            AdminUtility.setSuccessBox(Master, AdminUtility.SucessDelete);
        else
            AdminUtility.setErrorBox(Master, AdminUtility.Fail + d.message);



    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        try
        {
            string mode = hdnAction.Value;
            int bayiID=0;
            if (user.RoleID == (int)Constants.KullaniciTip.bayi)
                bayiID = user.ID;



            result = rep.ActionBanner(hdnID.Value, hdnSayfaId.Value, txtBaslik.Text, Request.Files, ckIcerik.Text, hdnAction.Value, drpStatu.SelectedValue, ddlDil.SelectedValue,bayiID);
            if (result.result)
            {
                if (mode.Equals("add"))
                    AdminUtility.setSuccessBox(Master, AdminUtility.SuccessAdd);
                else
                    AdminUtility.setSuccessBox(Master, AdminUtility.SucessUpdate);

                pnlListe.Visible = true;
                pnlSayfaEkle.Visible = false;
                hdnAction.Value = "";
                hdnID.Value = "";
            }
            else
            {
                AdminUtility.setErrorBox(Master, AdminUtility.Fail + result.message);
            }

            AdminUtility.ClearPanelForm(pnlSayfaEkle);

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnAddSayfa_Click(object sender, EventArgs e)
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = true;
        hdnAction.Value = "add";
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        pnlSayfaEkle.Visible = false;
        pnlListe.Visible = true;
    }
    protected void btnSirala_Click(object sender, EventArgs e)
    {
        Sirala();
    }
    private void Sirala()
    {
        pnlListe.Visible = false;
        pnlSayfaEkle.Visible = false;
        pnlSort.Visible = true;
        sayfaSirala.DataSource = rep.GetBannerList();
        sayfaSirala.DataBind();
    }

    protected void ddlDil_SelectedIndexChanged(object sender, EventArgs e)
    {
        string dilID = ddlDil.SelectedValue;

        var d = rep.GetSayfaLanguage(Convert.ToInt32(Request.QueryString["sayfaid"]), int.Parse(dilID));

        if (d != null)
            Response.Redirect("banner.aspx?action=update&pid=" + Request.QueryString["pid"] + "&sayfaid=" + d.SayfaId + "&l=" + ddlDil.SelectedValue + "&ul=1");

        else
            Response.Redirect("banner.aspx?action=addL&pid=" + Request.QueryString["pid"] + "&sayfaid=" + Request.QueryString["sayfaid"] + "&l=" + ddlDil.SelectedValue);




    }
    protected void ddlFilterDil_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception)
        {

            throw;
        }
    }
}