﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminZ0n/adminMasterPage.master" AutoEventWireup="true" CodeFile="Banner.aspx.cs" Inherits="AdminZ0n_Banner_Banner" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<script type="text/javascript">
	    $(function () {
	        $("#banner").addClass("current");
	        $("#main-nav li a.current").parent().find("ul").slideToggle("slow");
	    });
	    $(function () {
	        $("#list").sortable({
	            handle: '.handle',
	            update: function () {
	                var order = $('#list').sortable('serialize');
	                $.post("../service.aspx", { action: "ListBanner", item: order }, function (msg) {
	                    var json = $.parseJSON(msg);
	                    if (json.result) {

	                    }
	                });
	            }
	        });
	    });
	  
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" Runat="Server">
<div class="content-box">
 <div class="content-box-header">
        <h3 style="cursor: s-resize;"> Ürünler</h3>
        <ul class="content-box-tabs">
            <li><asp:DropDownList ID="ddlFilterDil" AutoPostBack="true" runat="server" 
                 onselectedindexchanged="ddlFilterDil_SelectedIndexChanged"></asp:DropDownList></li>
            <li><asp:Button CssClass="button" ID="Button2" runat="server" Text="Listele" /></li>
			<li> <asp:Button CssClass="button" ID="btnSirala" runat="server" Text="S&#305;rala" OnClick="btnSirala_Click" /></li> 
			<li><asp:Button CssClass="button" ID="Button1" runat="server" Text="Yeni Kay&#305;t Ekle" OnClick="btnAddSayfa_Click" /></li>
	    </ul>
    </div>
    <div class="content-box-content">
    <asp:HiddenField ID="hdnAction" runat="server" />
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnSayfaId" runat="server" />
 <asp:Panel ID="pnlListe" runat="server">
  <asp:Button CssClass="button" ID="btnAddSayfa" runat="server" Text="Yeni Sayfa Ekle" OnClick="btnAddSayfa_Click" />
  <br /><br />
  <style>
    #main-content tbody tr.alt-row {
background: #f3f3f3;
}
</style>
  <table id="table">
        <thead>
		    <tr style="background:#D0E799;height:25px;color:#000;">
                <th style="color:#000;text-align:center;cursor:pointer;">Baslik</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Resim</th>
		        <th style="color:#000;text-align:center;cursor:pointer;">Sira</th>
                <th style="color:#000;text-align:center;cursor:pointer;">Yayin<div id="ajaxloading" style="display:none; position:absolute;"><img src="/adminz0n/resources/images/ajax-loader.gif" /></div> </th>
                <th></th>    
                <th></th>                     
		    </tr>
	    </thead>
        <tbody>
            <%int filterLan = int.Parse(ddlFilterDil.SelectedValue);
              ZonDBModel.ZonDBEntities db = new ZonDBModel.ZonDBEntities();
              
                  banner = (from item in db.Zon_Banner
                            where item.DilId == filterLan && item.Aktif != "3"
                            select item).ToList();
              
            foreach (var item in banner)
            {%>
             <tr>
                <td ><%=item.Baslik%></td>
                <td style="width: 30px;"> <img ID="imgGet" src='<%=item.Resim %>' width="50"  /></td>
                <td ><%=item.Sira %></td>
                 <td>
                <%if (item.Aktif=="1")
                    {%>
                    <input type="checkbox" id="chkTrue"  onclick="CallIsVisible(<%=item.Id%>,'banner');" checked="checked"   />Aktif
                    <%} %>
                                  
                    <%else
                    { %>
                    <input type="checkbox" id="chkTrue" onclick="CallIsVisible(<%=item.Id%>,'banner');" />Aktif
                    
                    <%} %>
                    </td>
                               
                    <td style="width: 20px;">
                   <a href="banner.aspx?action=update&pid=<%=item.Id%>&sayfaid=<%=item.SayfaId%>&l=<%=item.DilId%>"><img ID="imog" alt="Güncelle" title="Güncelle" src="/adminz0n/resources/images/icons/pencil.png"  /></a> 

</td>
                <td style="width: 20px;">
                <a href="banner.aspx?action=delete&pid=<%=item.Id%>" onclick='return Delete();' id="deleteRow" ><img ID="Img1" alt="Detay" title="Detay" src="/adminz0n/resources/images/icons/cross.png"  /></a> </td>
                    <%--<asp:ImageButton ID="ImageButton2" OnClientClick="if(confirm('Kay&#305;d&#305; silmek istiyor musunuz ?')){}else{return false;}"  CommandName="sil" CommandArgument='<%=item.Id%>' ImageUrl="/adminz0n/resources/images/icons/cross.png" title="Sil" runat="server" /> --%>
            </tr>
                <%} %>
                </tbody>
                <tfoot>
		    <tr>
			      
       
		<th><input type="text" name="search_name" value="Baslik" style="" class="search_init" style="width:120px" /></th>
        <th><input type="text" name="search_name" value="Kategori" class="search_init" style="width:120px" /></th>
		<th><input type="text" name="search_detail" value="Sira" style="display:none;" class="search_init" style="width:120px" /></th>

        <th> </th><th></th>
        <th></th>
                    
		    </tr>
	    </tfoot>
        </table>
        <br /><br />
 
  </asp:Panel>
  <asp:Panel ID="pnlSayfaEkle" runat="server" Visible="false">

        <table>
        <tr>
            <td>Dil</td>
            <td>
                <asp:DropDownList ID="ddlDil" runat="server" AutoPostBack="true" onselectedindexchanged="ddlDil_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlDil"
                    ErrorMessage="Dil Seçiniz!"  InitialValue="0"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Ba&#351;l&#305;k</td>
           <td><asp:TextBox ID="txtBaslik" CssClass="text-input" TextMode="MultiLine" Width="300" runat="server"></asp:TextBox>
             <asp:Label ID="lblTrBaslik"  runat="server" Text=""></asp:Label>
           </td>
        </tr>
      <tr>
            <td>&#304;çerik</td>
          <td><CKEditor:CKEditorControl ID="ckIcerik" CssClass="" ClientIDMode="Static" runat="server" Height="100px"  Width="500px"></CKEditor:CKEditorControl></td>
        </tr>
        <tr>
            <td>Resmi</td>
            <td>
              <%--  <asp:TextBox ID="txtResim" Width="300" ClientIDMode="Static" CssClass="text-input" runat="server"></asp:TextBox>
                <input type="button" value="Banner Ekle" onclick="BrowseServerImage();" />--%>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <br />
                <asp:Image ID="imgBanner" runat="server" Width="100" />
            </td>
        </tr>
        <tr>
            <td>Yay&#305;n</td>
            <td>
            <asp:DropDownList ID="drpStatu" runat="server">
                    <asp:ListItem Value="1">Yay&#305;nda</asp:ListItem>
                    <asp:ListItem Value="2">Yay&#305;nda De&#287;il</asp:ListItem>
                    <asp:ListItem Value="3">Silinebilir</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        </table>
        <table  width="100%">
        <tr align="center" style="text-align:center">
        <td  style="text-align:center" align="center"><asp:Button CssClass="button" OnClick="btnKaydet_Click" OnClientClick="if(validate()){}else{return false;}" ID="btnKaydet" runat="server" Text="Kaydet" />
        <asp:Button CssClass="button" OnClick="btnExit_Click" ID="btnExit" runat="server" Text="Ç&#305;k&#305;&#351;" /></td>
        </tr>
        </table>
            
    </asp:Panel>
    <div class="clear">
    <asp:Panel ID="pnlSort" runat="server" Visible="false">
  <asp:Repeater ID="sayfaSirala" runat="server" >
    <HeaderTemplate><ul id="list"></HeaderTemplate>
    <FooterTemplate></ul></FooterTemplate>
    <ItemTemplate>
        <li style="padding-top:10px;padding-bottom:10px;background-color:Menu;border-style:inset;border-color:Black;border-width:thin;" id='item_<%# Eval("Id") %>'>
        <img src="../resources/images/arrow.png" alt="move" width="16" height="16" class="handle" style="cursor:pointer;" />
        
        <%# Eval("Baslik") %>
        </li>
    </ItemTemplate>
</asp:Repeater>
</asp:Panel>
    </div>
    </div>
</div> 
</asp:Content>



