﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using ZonDBModel;
using System.Web.Script.Serialization;

public partial class AdminZ0n_service : System.Web.UI.Page
{
    public class response
        {
            public string message { get; set; }
            public int id { get; set; }
            public bool result { get; set; }
            public object data { get; set; }
        }

    [WebMethod]
    protected void Page_Load(object sender, EventArgs e)
    {
        ZonDBEntities db = new ZonDBEntities();
        JavaScriptSerializer jss = new JavaScriptSerializer();
        response r = new response();
        int dilid=1;
        try
        {
            if (Request["action"] is object)
            {
                if (Request["action"] == "ListSSS")
                {
                    string item = Request["item"];
                    item = item.Replace("item[]=", "");
                    string[] arr = item.Split('&');
                    for (int i = 0; i < arr.Length; i++)
                    {
                        int id = Convert.ToInt32(arr[i]);
                        var sayfa = (from s in db.Zon_SSS
                                     where s.Id == id
                                     select s).Single();
                        if (sayfa != null)
                        {
                            sayfa.Sira = i + 1;
                            db.SaveChanges();
                        }
                    }
                    r.data = item;
                    r.result = true;
                    Response.Write(jss.Serialize(r));
                }

            //    else if (Request["action"] == "ListUrun")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            //id'si geliyor.
            //            int id = Convert.ToInt32(arr[i]);
            //            //burada sırasını databasde güncelle
            //            //(i+1) sırasını belirtiyor. (Örnek kod hygiene'de de var.)
            //            var sayfa = (from s in db.Zon_Urunler
            //                         where s.Id == id
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            //    else if (Request["action"] == "ListDokuman")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            //id'si geliyor.
            //            int id = Convert.ToInt32(arr[i]);
            //            //burada sırasını databasde güncelle
            //            //(i+1) sırasını belirtiyor. (Örnek kod hygiene'de de var.)
            //            var sayfa = (from s in db.Zon_Dokuman
            //                         where s.Id == id
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            //    else if (Request["action"] == "ListHizmet")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            //id'si geliyor.
            //            int id = Convert.ToInt32(arr[i]);
            //            //burada sırasını databasde güncelle
            //            //(i+1) sırasını belirtiyor. (Örnek kod hygiene'de de var.)
            //            var sayfa = (from s in db.Zon_Hizmetler
            //                         where s.Id == id && s.SayfaTip==1
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            //    else if (Request["action"] == "ListFirma")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            //id'si geliyor.
            //            int id = Convert.ToInt32(arr[i]);
            //            //burada sırasını databasde güncelle
            //            //(i+1) sırasını belirtiyor. (Örnek kod hygiene'de de var.)
            //            var sayfa = (from s in db.Zon_Hizmetler
            //                         where s.Id == id && s.SayfaTip==2
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            //    else if (Request["action"] == "urunlerKategori")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            //id'si geliyor.
            //            int id = Convert.ToInt32(arr[i]);
            //            //burada sırasını databasde güncelle
            //            //(i+1) sırasını belirtiyor. (Örnek kod hygiene'de de var.)
            //            var sayfa = (from s in db.Zon_Kategori
            //                         where s.Id == id 
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            //    else if (Request["action"] == "ListHaber")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            //id'si geliyor.
            //            int id = Convert.ToInt32(arr[i]);
            //            //burada sırasını databasde güncelle
            //            //(i+1) sırasını belirtiyor. (Örnek kod hygiene'de de var.)
            //            var sayfa = (from s in db.Zon_SayfaIcerik
            //                         where s.Id == id && s.Tip==2
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            //    else if (Request["action"] == "galeriResim")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            int id = Convert.ToInt32(arr[i]);
            //            var sayfa = (from s in db.Zon_ResimGaleri
            //                         where s.ResimGaleriId == id 
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            //    else if (Request["action"] == "kutuphane")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            int id = Convert.ToInt32(arr[i]);
            //            var sayfa = (from s in db.Zon_Kutuphane
            //                         where s.Id == id
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }

            //    else if (Request["action"] == "ListBanner")
            //    {
            //        string item = Request["item"];
            //        item = item.Replace("item[]=", "");
            //        string[] arr = item.Split('&');
            //        for (int i = 0; i < arr.Length; i++)
            //        {
            //            int id = Convert.ToInt32(arr[i]);
            //            var sayfa = (from s in db.Zon_Banner
            //                         where s.Id == id
            //                         select s).Single();
            //            if (sayfa != null)
            //            {
            //                sayfa.Sira = i + 1;
            //                db.SaveChanges();
            //            }
            //        }
            //        r.data = item;
            //        r.result = true;
            //        Response.Write(jss.Serialize(r));
            //    }
            }
        }
        catch (Exception ex)
        {
            r.result = false;
            r.message = ex.Message;
            Response.Write(jss.Serialize(r));
        }
    }

    [WebMethod]
    public static string VisibleRow(string id, string table)
    {
        ZonDBEntities db = new ZonDBEntities();
        try
        {

            int pId = Convert.ToInt32(id);
            if (table.Equals("RandevuSaat"))
            {
                var a = (from item in db.Zon_Rezerve_Saat
                         where item.Id == pId
                         select item).FirstOrDefault();

                if (a.Aktif == "1")
                    a.Aktif = "2";
                else
                    a.Aktif = "1";
            }
            else if (table.Equals("sirketRandevu"))
            {
                var a = (from item in db.Zon_Randevu
                         where item.Id == pId
                         select item).FirstOrDefault();

                if (a.Aktif == "1")
                    a.Aktif = "2";
                else
                    a.Aktif = "1";
            }
            //else if (table.Equals("iletisim"))
            //{
            //    var a = (from item in db.Zon_Iletisim
            //             where item.Id == pId
            //             select item).FirstOrDefault();

            //    if (a.Okundu == true)
            //        a.Okundu = false;
            //    else
            //        a.Okundu = true;
            //}
            //else if (table.Equals("urunler"))
            //{
            //    var a = (from item in db.Zon_Urunler
            //             where item.Id == pId
            //             select item).FirstOrDefault();

            //    if (a.Aktif == "1")
            //        a.Aktif = "2";
            //    else
            //        a.Aktif = "1";
            //}
            //else if (table.Equals("urunlerKategori"))
            //{
            //    var a = (from item in db.Zon_Kategori
            //             where item.Id == pId
            //             select item).FirstOrDefault();

            //    if (a.Aktif == "1")
            //        a.Aktif = "2";
            //    else
            //        a.Aktif = "1";
            //}
            //else if (table.Equals("banner"))
            //{
            //    var a = (from item in db.Zon_Banner
            //             where item.Id == pId
            //             select item).FirstOrDefault();

            //    if (a.Aktif == "1")
            //        a.Aktif = "2";
            //    else
            //        a.Aktif = "1";
            //}
            //else if (table.Equals("kutuphaneDosya"))
            //{
            //    var a = (from item in db.Zon_Dokuman
            //             where item.Id == pId
            //             select item).FirstOrDefault();

            //    if (a.Aktif == "1")
            //        a.Aktif = "2";
            //    else
            //        a.Aktif = "1";
            //}
            //else if (table.Equals("kutuphane"))
            //{
            //    var a = (from item in db.Zon_Dokuman
            //             where item.RefId == pId
            //             select item).FirstOrDefault();

            //    if (a.Aktif == "1")
            //        a.Aktif = "2";
            //    else
            //        a.Aktif = "1";
            //}
            //else if (table.Equals("galeriResim"))
            //{
            //    var a = (from item in db.Zon_ResimGaleri_Kategori
            //             where item.Id == pId
            //             select item).FirstOrDefault();

            //    if (a.Aktif == true)
            //        a.Aktif = false;
            //    else
            //        a.Aktif = true;
            //}
            //else if (table.Equals("bayi"))
            //{
            //    var a = (from item in db.Zon_Kullanici
            //             where item.ID == pId
            //             select item).FirstOrDefault();

            //    if (a.Statu == true)
            //        a.Statu = false;
            //    else
            //        a.Statu = true;
            //}
            //else if (table.Equals("bayiIletisim"))
            //{
            //    var a = (from item in db.Zon_Bayi_Iletisim
            //             where item.Id == pId
            //             select item).FirstOrDefault();

            //    if (a.Aktif == "1")
            //        a.Aktif = "2";
            //    else
            //        a.Aktif = "1";
            //}
            db.SaveChanges();

            return "basarili";
        }
        catch (Exception)
        {
            return "basarisz";
        }

    }
}