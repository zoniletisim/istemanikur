﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="iletisim.aspx.cs" Inherits="iletisim" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="iletisimContainer">

<div class="iletisimText">
        <div class="iletisimBanner">
        	<span class="iletisimTitle">İletişim</span>
		</div>
</div>        

<div class="iletisimShadow"></div>	

<div class="iletisimLeft">
<div class="leftTitle"><b>işte</b>manikür</div>
	<div class="iletisimLeftItem">
    	
    	<div class="leftLabel">Adres:</div>
        <div class="leftItemText">Bayar cad. Oyak Sitesi A Grubu Sevgi Apt D:10 Kozyatağı - İstanbul</div>
    </div>
    <div class="iletisimLeftItem">
    	<div class="leftLabel">Telefon:</div>
        <div class="leftItemText">0534 749 78 87 (Hafta içi 08:00-17:30)</div>
    </div>
    <div class="iletisimLeftItem">
    	<div class="leftLabel">E-Posta:</div>
        <div class="leftItemText">info@istemanikur.com</div>
    </div>
</div>

<div class="iletisimRight">
	<div class="rightTitle">bize ulaşın</div>
<input name="" id="txtAd" class="iletisimFormInput" type="text"   placeholder="Ad-Soyad" />
<input name="" id="txtEmail"  class="iletisimFormInput"  type="text"  placeholder="Email" />
<input name=""  id="txtTel" class="iletisimFormInput"  type="text"  placeholder="Telefon" />
<input name=""  id="txtKonu" class="iletisimFormInput"  type="text"  placeholder="Konu" />
<textarea name="" cols="" rows="" id="txtNot" class="iletisimFormArea"  placeholder="Mesaj" ></textarea>

<input name="" type="button" class="iletisimFormSubmit" id="btnKaydet" value=""/>
    <div class="Confirm" style="font:12px;">
        <span id="lblError" style="color:Red;margin-left:20px;font-size:13px;margin-top:55px;float:left;"></span>
        <span id="lblSuccess" style="color:Green;margin-left:20px;font-size:16px;margin-top:20px;float:left;"></span>
        <div id="loading" style="display:none"><img src="/resources/images/loading.gif" /> İşleminiz Yapılıyor Lütfen Bekleyiniz...</div>
    </div>

</div>

</div>

<script src="/Resources/js/jquery.placeholder.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('input, textarea').placeholder();
        });
    $('#btnKaydet').click(function () {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if ($('#txtEmail').val() != "" && $('#txtAd').val() != "" && $('#txtTel').val() != "" && $('#txtKonu').val() != "0" && $('#txtNot').val() != "") {
            if (emailReg.test($('#txtEmail').val())) {
                $('#loading').css("display", "block");
                $('#btnKaydet').css("display", "none");
                $.ajax({
                    type: "post",
                    url: "iletisim.aspx/saveForm",
                    data: "{ad: '" + $('#txtAd').val() + "',email: '" + $('#txtEmail').val() + "',tel: '" + $('#txtTel').val() + "',konu: '" + $('#txtKonu').val() + "',not: '" + $('#txtNot').val() + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $('#txtEmail').val('');
                        $('#txtAd').val('');
                        $('#txtTel').val('');
                        $('#txtNot').val('');
                        $('#txtKonu').val('');
                        $('#lblError').text('');
                        alert('Kaydınız Başarıyla Yapıldı. ');
                        $('#loading').css("display", "none");
                        $('#btnKaydet').css("display", "block");
                    },
                    error: function () {

                    }
                });
            }
            else {
                $('#lblError').html("Lütfen geçerli email adresi girin.");
            }
        }
        else {
            $('#lblError').html("Lütfen Tüm Alanları Doldurun.");
        }
    });
  
  </script>
</asp:Content>














