﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gizlilik.aspx.cs" Inherits="gizlilik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/resources/css/style.css">
</head>

<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
try {
 document.execCommand('BackgroundImageCache', false, true);
} catch(e) {}
</script>
    <div class="popupTextsContaıner" style="font-size:12px;line-height:18px;">
    <div class="popupTextsTitle">Gizlilik Sözleşmesi</div>
    	<p>Alıcı tarafından iş bu sözleşmede  belirtilen  bilgiler ile  ödeme
    	  yapmak amacı ile satıcıya bildirdiği bilgiler satıcı tarafından 3.
    	  şahıslarla paylaşılmayacaktır.    	  </p>
    	<p>&nbsp;</p>
    	<p>Satıcı  bu bilgileri sadece idari/ yasal zorunluluğun mevcudiyeti
    	  çerçevesinde açıklayabilecektir. Araştırma ehliyeti belgelenmiş her
    	  türlü adli soruşturma dahilinde satıcı  kendisinden istenen bilgiyi
    	  elinde bulunduruyorsa ilgili makama sağlayabilir.</p>
    	<p>&nbsp;</p>
    	<p>Kredi Kartı bilgileri kesinlikle saklanmaz,Kredi Kartı bilgileri sadece
    	  tahsilat işlemi sırasında ilgili bankalara güvenli bir şekilde
    	  iletilerek provizyon alınması için kullanılır ve provizyon sonrası
    	  sistemden silinir.</p>
    	<p>&nbsp;</p>
    	<p>Alıcıya ait e-posta adresi, posta adresi ve telefon gibi bilgiler
    	  yalnızca satıcı  tarafından standart ürün teslim ve bilgilendirme
    	  prosedürleri için kullanılır.</p>
    	<p>&nbsp;</p>
    	<p> Bazı dönemlerde kampanya bilgileri, yeni
    	  ürünler hakkında bilgiler, promosyon bilgileri alıcıya onayı sonrasında
    	  gönderilebilir. </p>
    </div>
    </form>
</body>
</html>











