﻿<%@ Page Title="" Language="C#" MasterPageFile="~/siteMasterPage.master" AutoEventWireup="true" CodeFile="sss.aspx.cs" Inherits="sss" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="mainContainer">
<script type="text/javascript">
    $(document).ready(function () {
        //Add Inactive Class To All Accordion Headers
        $('.accordion-header').toggleClass('inactive-header');

        //Set The Accordion Content Width
        var contentwidth = $('.accordion-header').width();
        $('.accordion-content').css({ 'width': contentwidth });

        //Open The First Accordion Section When Page Loads
        $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
        $('.accordion-content').first().slideDown().toggleClass('open-content');

        // The Accordion Effect
        $('.accordion-header').click(function () {
            if ($(this).is('.inactive-header')) {
                $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
                $(this).toggleClass('active-header').toggleClass('inactive-header');
                $(this).next().slideToggle().toggleClass('open-content');
            }

            else {
                $(this).toggleClass('active-header').toggleClass('inactive-header');
                $(this).next().slideToggle().toggleClass('open-content');
            }
        });

        return false;
    });
</script>
	<div class="sssText">
        <div class="sssBanner">
        	<span class="sssTitle">Sıkça Sorulan Sorular</span>
        </div>
        <div class="shadow"></div>
<div id="accordion-container">
    <%foreach (var s in sayfa)
      {%>

			<h2 class="accordion-header"><%=s.Baslik %></h2>
			<div class="accordion-content">
				<p class="first-p"><%=s.Icerik %> </p>	
			</div>
            <%} %>
		</div>
      </div>
        
        
      </div>

    
    
</div>
</asp:Content>








